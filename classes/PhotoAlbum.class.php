<?php

/* Models and managers used by the PhotoAlbum model */
require_once 'Model.class.php';

class PhotoAlbum extends Model {

    const IMAGES_PATH = '/images/photoAlbums';

    public $photoAlbumId;
    public $windowTitle;
    public $metaKeywords;
    public $metaDescription;
    public $title;
    public $content;
    public $shortTitle;
    public $date;
    public $online = 1;
    public $created;
    public $modified;
    public $folderName; // name of the folder to place the photos
    // association with Image class
    private $aImages = null; //array with different lists of images

    /**
     * get all images by specific list name for a page
     * @param string $sList
     * @return Image or array Images
     */

    public function getImages($sList = 'online') {
        if (!isset($this->aImages[$sList])) {
            switch ($sList) {
                case 'online':
                    $this->aImages[$sList] = PhotoAlbumManager::getImagesByFilter($this->photoAlbumId);
                    break;
                case 'cover':
                    $aImages = PhotoAlbumManager::getImagesByFilter($this->photoAlbumId, array('showAll' => 1, 'coverImage' => 1), 1);
                    if (!empty($aImages))
                        $oImage = $aImages[0];
                    else
                        $oImage = null;
                    $this->aImages[$sList] = $oImage;
                    break;
                case 'all':
                    $this->aImages[$sList] = PhotoAlbumManager::getImagesByFilter($this->photoAlbumId, array('showAll' => true));
                    break;
                default:
                    die('no option');
                    break;
            }
        }
        return $this->aImages[$sList];
    }

    /**
     * validate object
     */
    public function validate() {
        if (empty($this->title))
            $this->setPropInvalid('title');
        if (empty($this->date))
            $this->setPropInvalid('date');
        if (!is_numeric($this->online))
            $this->setPropInvalid('online');
    }

    /**/

    public function isEditable() {
        return true;
    }

    /**/

    public function isDeletable() {
        return true;
    }

    /**
     * get url to photo album optional with extension .html
     * @param boolean $bWithExtension (optional) DEFAULT FALSE
     * @return string
     */
    public function getUrlPath($bWithExtension = false) {
        $sUrlPath = '/fotoalbums/' . $this->photoAlbumId . '/' . prettyUrlPart($this->title);

        if ($bWithExtension)
            return $sUrlPath . '.html';
        return $sUrlPath;
    }

    /**
     * return the window title if there is one, otherwise return title
     * @return string
     */
    public function getWindowTitle() {
        return $this->windowTitle ? $this->windowTitle : $this->title;
    }

    /**
     * return meta description if exists or a 
     * @return string
     */
    public function getMetaDescription() {
        return $this->metaDescription ? $this->metaDescription : generateMetaDescription($this->content);
    }

    /**
     * return meta keywords
     * @return string
     */
    public function getMetaKeywords() {
        return $this->metaKeywords;
    }

    /**
     * return short title but fall back on title if short does not exists
     * @return string
     */
    public function getShortTitle() {
        return $this->shortTitle ? $this->shortTitle : $this->title;
    }

    /**
     * get an array of the breadcrumbs
     * @return array
     */
    public function getCrumbles() {
        return array('Fotoalbums' => '/fotoalbums', $this->getShortTitle() => $this->getUrlPath());
    }

}

?>
