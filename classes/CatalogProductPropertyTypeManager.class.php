<?php

// Models and managers used by this class
require_once 'CatalogProductPropertyType.class.php';

class CatalogProductPropertyTypeManager {

    /**
     * get a CatalogProductPropertyType by id
     * @param int $iProductPropertyTypeId
     * @return CatalogProductPropertyType
     */
    public static function getProductPropertyTypeById($iProductPropertyTypeId) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `catalogProductPropertyTypes`
                    WHERE
                        `catalogProductPropertyTypeId` = ' . db_int($iProductPropertyTypeId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "CatalogProductPropertyType");
    }

    /**
     * get all CatalogProductPropertyType objects
     * @return array CatalogProductPropertyType
     */
    public static function getAllProductPropertyTypes() {
        $sQuery = ' SELECT
                        *
                    FROM
                        `catalogProductPropertyTypes`
                    ORDER BY
                        `order` ASC,
                        `catalogProductPropertyTypeId` ASC
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_OBJECT, "CatalogProductPropertyType");
    }

    /**
     * get CatalogProductPropertyType objects by catalogProductTypeId
     * @param int $iProductTypeId
     * @return array CatalogProductPropertyType
     */
    public static function getProductPropertyTypesByProductTypeId($iProductTypeId) {
        $sQuery = ' SELECT
                        `cppt`.*
                    FROM
                        `catalogProductPropertyTypes` `cppt`
                    JOIN
                        `catalogProductPropertyTypeGroups` `cpptg` USING (`catalogProductPropertyTypeGroupId`)
                    WHERE
                        `catalogProductTypeId` = ' . db_int($iProductTypeId) . '
                    ORDER BY
                        `order` ASC,
                        `catalogProductPropertyTypeId` ASC
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_OBJECT, "CatalogProductPropertyType");
    }

    /**
     * get CatalogProductPropertyType objects by catalogProductPropertyTypeGroupId
     * @param int $iProductPropertyTypeGroupId
     * @return array CatalogProductPropertyType
     */
    public static function getProductPropertyTypesByProductPropertyTypeGroupId($iProductPropertyTypeGroupId) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `catalogProductPropertyTypes`
                    WHERE
                        `catalogProductPropertyTypeGroupId` = ' . db_int($iProductPropertyTypeGroupId) . '
                    ORDER BY
                        `order` ASC,
                        `catalogProductPropertyTypeId` ASC
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_OBJECT, "CatalogProductPropertyType");
    }

    /**
     * return products filtered by a few options
     * @param array $aFilter filter properties (catalogProductTypeId)
     * @param int $iLimit limit number of records returned
     * @param int $iStart start from this record
     * @param int $iFoundRows foundRows when there was no limit (default = false so doesn't check by default)
     * @param array $aOrderBy array(database coloumn name => order) add order by columns and orders
     * @return array CatalogProductPropertyType objects 
     */
    public static function getProductPropertyTypesByFilter(array $aFilter = array(), $iLimit = null, $iStart = 0, &$iFoundRows = false, $aOrderBy = array('`cppt`.`order`' => 'ASC', '`cppt`.`catalogProductPropertyTypeId`' => 'ASC')) {

        $sWhere = '';
        $sFrom = '';
        # join the catalogProductPropertyTypeGroups table if necessary
        if (!empty($aFilter['catalogProductTypeId']) || !empty($aFilter['catalogProductPropertyTypeGroupId'])) {
            $sFrom .= 'JOIN `catalogProductPropertyTypeGroups` `cpptg` USING (`catalogProductPropertyTypeGroupId`)';
        }

        # search for catalogProductTypeId
        if (!empty($aFilter['catalogProductTypeId'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`cpptg`.`catalogProductTypeId` = ' . db_int($aFilter['catalogProductTypeId']);
        }

        # search for catalogProductPropertyTypeGroupId
        if (!empty($aFilter['catalogProductPropertyTypeGroupId'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`cpptg`.`catalogProductPropertyTypeGroupId` = ' . db_int($aFilter['catalogProductPropertyTypeGroupId']);
        }

        # handle order by
        $sOrderBy = '';
        if (count($aOrderBy) > 0) {
            foreach ($aOrderBy AS $sColumn => $sOrder) {
                $sOrderBy .= ($sOrderBy !== '' ? ',' : '') . $sColumn . ' ' . $sOrder;
            }
        }
        $sOrderBy = ($sOrderBy !== '' ? 'ORDER BY ' : '') . $sOrderBy;

        # handle start,limit
        $sLimit = '';
        if (is_numeric($iLimit)) {
            $sLimit .= db_int($iLimit);
        }
        if ($sLimit !== '') {
            $sLimit = (is_numeric($iStart) ? db_int($iStart) . ',' : '0,') . $sLimit;
        }
        $sLimit = ($sLimit !== '' ? 'LIMIT ' : '') . $sLimit;

        $sQuery = ' SELECT SQL_CALC_FOUND_ROWS
                        `cppt`.*
                    FROM
                        `catalogProductPropertyTypes` AS `cppt`
                    ' . $sFrom . '
                    ' . ($sWhere != '' ? 'WHERE ' . $sWhere : '') . '
                    ' . $sOrderBy . '
                    ' . $sLimit . '
                    ;';

        $oDb = DBConnections::get();
        $aProductPropertyTypes = $oDb->query($sQuery, QRY_OBJECT, "CatalogProductPropertyType");
        if ($iFoundRows !== false) {
            $iFoundRows = $oDb->query('SELECT FOUND_ROWS() AS `found_rows`;', QRY_UNIQUE_OBJECT)->found_rows;
        }
        return $aProductPropertyTypes;
    }

    /**
     * save a CatalogProductPropertyType
     * @param CatalogProductPropertyType $oProductPropertyType
     */
    public static function saveProductPropertyType(CatalogProductPropertyType $oProductPropertyType) {
        $sQuery = ' INSERT INTO `catalogProductPropertyTypes`(
                        `catalogProductPropertyTypeId`,
                        `title`,
                        `type`,
                        `filterType`,
                        `order`,
                        `catalogProductPropertyTypeGroupId`,
                        `created`
                    )
                    VALUES (
                        ' . db_int($oProductPropertyType->catalogProductPropertyTypeId) . ',
                        ' . db_str($oProductPropertyType->title) . ',
                        ' . db_str($oProductPropertyType->type) . ',
                        ' . db_str($oProductPropertyType->filterType) . ',
                        ' . db_int($oProductPropertyType->order) . ',
                        ' . db_int($oProductPropertyType->catalogProductPropertyTypeGroupId) . ',
                        NOW()
                    )
                    ON DUPLICATE KEY UPDATE
                        `title`=VALUES(`title`),
                        `type`=VALUES(`type`),
                        `filterType`=VALUES(`filterType`),
                        `order`=VALUES(`order`),
                        `catalogProductPropertyTypeGroupId`=VALUES(`catalogProductPropertyTypeGroupId`)
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);

        if ($oProductPropertyType->catalogProductPropertyTypeId === null)
            $oProductPropertyType->catalogProductPropertyTypeId = $oDb->insert_id;
    }

    /**
     * delete a CatalogProductPropertyType
     * @param CatalogProductPropertyType $oProductPropertyType
     * @return bool true
     */
    public static function deleteProductPropertyType(CatalogProductPropertyType $oProductPropertyType) {
        # delete object
        $sQuery = ' DELETE FROM
                        `catalogProductPropertyTypes`
                    WHERE
                        `catalogProductPropertyTypeId` = ' . db_int($oProductPropertyType->catalogProductPropertyTypeId) . '
                    LIMIT 1
                    ;';
        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);
        return true;
    }

}

?>
