<?php

/* Models and managers used by this class */
require_once 'Model.class.php';
require_once 'CouponRuleManager.class.php';
require_once 'CouponManager.class.php';

class CouponRule extends Model {
    # list of discountTypes. Do not edit the status numbers afterwards

    const DISCOUNTTYPE_ORDER_FIXED_PRICE = 'OrderFixed'; // discount by a fixed price on the entire order
    const DISCOUNTTYPE_ORDER_PERCENTAGE = 'OrderPercentage'; // discount by a percentage on the entire order

    public $couponRuleId = null;
    public $title;
    public $reference;
    public $discountType;
    public $discountAmount;
    public $timesRedeemable = 1;
    public $activeFrom = null;
    public $activeTo = null;
    public $active = 1;
    public $created = null;
    public $modified = null;
    private $aCoupons = null;

    /**
     * validate object
     */
    public function validate() {
        if (empty($this->title))
            $this->setPropInvalid('title');
        if (empty($this->reference))
            $this->setPropInvalid('reference');
        if (empty($this->discountType))
            $this->setPropInvalid('discountType');
        if (!is_numeric($this->discountAmount))
            $this->setPropInvalid('discountAmount');
        if (!is_numeric($this->timesRedeemable))
            $this->setPropInvalid('timesRedeemable');
        if (empty($this->activeFrom))
            $this->setPropInvalid('activeFrom');
        if (!is_numeric($this->active))
            $this->setPropInvalid('active');
    }

    /**
     * get the discountAmount with optional sign
     * @param bool $bWithSign
     * @return string
     */
    public function getDiscountAmount($bWithSign = false) {
        if ($bWithSign && $this->discountType == self::DISCOUNTTYPE_ORDER_FIXED_PRICE) {
            return decimal2valuta($this->discountAmount);
        } elseif ($bWithSign && $this->discountType == self::DISCOUNTTYPE_ORDER_PERCENTAGE) {
            return $this->discountAmount . '%';
        } else {
            return $this->discountAmount;
        }
    }

    /**
     * get the related Coupon objects
     * @param array $aFilter 
     * @param int $iLimit limit number of records returned
     * @param int $iStart start from this record
     * @param int $iFoundRows foundRows when there was no limit (default = false so doesn't check by default)
     * @param array $aOrderBy array(database coloumn name => order) add order by columns and orders
     * @return array Coupon
     */
    public function getCoupons() {
        if ($this->aCoupons === null)
            $this->aCoupons = CouponManager::getCouponsByFilter(array('couponRuleId' => $this->couponRuleId));
        return $this->aCoupons;
    }

    /**
     * Check wether this Coupon is active.
     * @return bool
     */
    public function isActive() {
        $oDateFrom = new Date($this->activeFrom);
        $oDateTo = new Date($this->activeTo);
        $oDateNow = new Date('NOW');

        return $this->active && (($oDateFrom->lowerEqualTo($oDateNow) && $this->activeTo === null) || ($oDateFrom->lowerEqualTo($oDateNow) && $oDateNow->lowerEqualTo($oDateTo)));
    }

    /**
     * check if this object is deletable
     * @return bool 
     */
    public function isDeletable() {
        return !$this->isActive() && !$this->getCoupons();
    }

}

?>
