<?php

// Models and managers used by this class
require_once 'CatalogProductPropertyTypeManager.class.php';
require_once 'CatalogProductPropertyValueManager.class.php';
require_once 'CatalogProductPropertyTypeGroupManager.class.php';

class CatalogProductPropertyType extends Model {
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_TEXT = 'text';
    const TYPE_SELECT = 'select';

    public $catalogProductPropertyTypeId = null;
    public $title;
    public $type;
    public $filterType;
    public $order = 99999;
    public $catalogProductPropertyTypeGroupId;
    public $icecatFeatureId = null; // icecat Id of the related Feature
    public $created = null;
    public $modified = null;
    private $aPossibleValues = null; // association with catalogProductPropertyTypePossibleValue class
    private $aValues = array(); // association with CatalogProductPropertyValue class
    private $oProductPropertyTypeGroup = null; // association with CatalogProductPropertyTypeGroup class

    /**
     * validate object
     */

    public function validate() {
        if (empty($this->title))
            $this->setPropInvalid('title');
        if (empty($this->type))
            $this->setPropInvalid('type');
        if (empty($this->order))
            $this->setPropInvalid('order');
        if (empty($this->catalogProductPropertyTypeGroupId))
            $this->setPropInvalid('catalogProductPropertyTypeGroupId');
    }

    /**
     * get all catalogProductPropertyTypePossibleValue related to this object
     * @return array CatalogProductPropertyTypePossibleValue $this->aPossibleValues
     */
    public function getPossibleValues() {
        if ($this->aPossibleValues === null) {
            $this->aPossibleValues = CatalogProductPropertyTypePossibleValueManager::getPossibleValuesByProductPropertyTypeId($this->catalogProductPropertyTypeId);
        }
        return $this->aPossibleValues;
    }

    /**
     * get the CatalogProductPropertyValue related to this catalogProductPropertyType and a CatalogProduct
     * @param int $iProductId
     * @return array CatalogProductPropertyValue $this->aValues
     */
    public function getValuesByProductId($iProductId) {
        if (!isset($this->aValues[$iProductId])) {
            $this->aValues[$iProductId] = CatalogProductPropertyValueManager::getProductPropertyValuesByProductIdProductPropertyTypeId($iProductId, $this->catalogProductPropertyTypeId);
        }
        return $this->aValues[$iProductId];
    }

    /**
     * get the related CatalogProductPropertyTypeGroup
     * @return CatalogProductPropertyTypeGroup 
     */
    public function getPropertyTypeGroup() {
        if ($this->oProductPropertyTypeGroup === null) {
            $this->oProductPropertyTypeGroup = CatalogProductPropertyTypeGroupManager::getProductPropertyTypeGroupById($this->catalogProductPropertyTypeGroupId);
        }
        return $this->oProductPropertyTypeGroup;
    }
    
    /**
     * get the related CatalogProductType
     * @return CatalogProductType 
     */
    public function getProductType() {
        return $this->getPropertyTypeGroup()->getProductType();
    }

}

?>
