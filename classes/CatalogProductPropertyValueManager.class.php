<?php

// Models and managers used by this class
require_once 'CatalogProductPropertyValue.class.php';

class CatalogProductPropertyValueManager {

    /**
     * get CatalogProductPropertyValue objects by catalogProductId
     * @param int $iProductId
     * @return array CatalogProductPropertyValue
     */
    public static function getProductPropertyValuesByProductId($iProductId) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `catalogProductPropertyValues`
                    WHERE
                        `catalogProductId` = ' . db_int($iProductId) . '
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_OBJECT, "CatalogProductPropertyValue");
    }

    /**
     * get CatalogProductPropertyValue objects by catalogProductId and catalogProductPropertyTypeId
     * @param int $iProductId
     * @param int $iProductPropertyTypeId
     * @return array CatalogProductPropertyValue
     */
    public static function getProductPropertyValuesByProductIdProductPropertyTypeId($iProductId, $iProductPropertyTypeId) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `catalogProductPropertyValues`
                    WHERE
                        `catalogProductId` = ' . db_int($iProductId) . ' AND
                        `catalogProductPropertyTypeId` = ' . db_int($iProductPropertyTypeId) . '
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_OBJECT, "CatalogProductPropertyValue");
    }

}

?>