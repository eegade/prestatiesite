<?php

/* Models and managers used by the Agenda model */
require_once 'Model.class.php';
require_once 'ImageManager.class.php';

class AgendaItem extends Model {
    const IMAGES_PATH = '/images/agenda';
    
    public $agendaItemId = null;
    public $windowTitle;
    public $metaKeywords; 
    public $metaDescription; 
    public $online = 1;
    public $title;
    public $intro;
    public $content = null; 
    public $shortTitle;
    public $dateTimeFrom;
    public $dateTimeTo;
    public $allDay = 0;
    public $created; //created timestamp
    public $modified; //last modified timestamp

    /**
     * validate object 
     */

    public function validate() {
        if (empty($this->title))
            $this->setPropInvalid('title');
        if (empty($this->dateTimeFrom))
            $this->setPropInvalid('dateTimeFrom');
        $this->bIsValid = true;
    }
    
    /**
     * check wheter a Agenda Item is online or not
     * @return bool
     */
    public function isOnline() {
        return $this->online;
    }
    
    /**
     * get all images by specific list name for a agendasItem
     * @param string $sList
     * @return Image or array Images
     */
    public function getImages($sList = 'online') {
        if (!isset($this->aImages[$sList])) {
            switch ($sList) {
                case 'online':
                    $this->aImages[$sList] = AgendaItemManager::getImagesByFilter($this->agendaItemId);
                    break;
                case 'first-online':
                    $aImages = AgendaItemManager::getImagesByFilter($this->agendaItemId, array(), 1);
                    if (!empty($aImages))
                        $oImage = $aImages[0];
                    else
                        $oImage = null;
                    $this->aImages[$sList] = $oImage;
                    break;
                case 'all':
                    $this->aImages[$sList] = AgendaItemManager::getImagesByFilter($this->agendaItemId, array('showAll' => true));
                    break;
                default:
                    die('no option');
                    break;
            }
        }
        return $this->aImages[$sList];
    }
    
    
    /**
     * get url to news item optional with extension .html
     * @param boolean $bWithExtension (optional) DEFAULT FALSE
     * @return string
     */
    public function getUrlPath($bWithExtension = false) {
        $sUrlPath = '/agenda/' . $this->agendaItemId . '/' . prettyUrlPart($this->title);

        if ($bWithExtension)
            return $sUrlPath . '.html';
        return $sUrlPath;
    }
    
    /**
     * return the window title if there is one, otherwise return title
     * @return string
     */
    public function getWindowTitle() {
        return $this->windowTitle ? $this->windowTitle : $this->title;
    }

    /**
     * return meta description if exists or a
     * @return string
     */
    public function getMetaDescription() {
        return $this->metaDescription ? $this->metaDescription : generateMetaDescription($this->content);
    }

    /**
     * return meta keywords
     * @return string
     */
    public function getMetaKeywords() {
        return $this->metaKeywords;
    }
    
    /**
     * 
     * get an array of the breadcrumbs
     * @param type $iAgendaItemId
     * @return array
     */
    public function getCrumbles($iAgendaItemId = null) {
                
        $aCrumbles = array();
        $aCrumbles['Agenda'] = '/agenda';
        if ($iAgendaItemId){
            $aCrumbles[$this->title] = $this->getUrlPath();
        }
        $aCrumbles[$this->getShortTitle()] = $this->getUrlPath();
        return $aCrumbles;
    }
    
    /**
     * return short title but fall back on title if short does not exists
     * @return string
     */
    public function getShortTitle() {
        return $this->shortTitle ? $this->shortTitle : $this->title;
    }
}
?>
