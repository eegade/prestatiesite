<?php

// Models and managers used by this class
require_once 'Model.class.php';

class CatalogProductPropertyValue extends Model {

    public $catalogProductPropertyValueId = null;
    public $catalogProductId;
    public $catalogProductPropertyTypeId;
    public $value;

    /**
     * validate object
     */
    public function validate() {
        if (!is_numeric($this->catalogProductId))
            $this->setPropInvalid('catalogProductId');
        if (!is_numeric($this->catalogProductPropertyTypeId))
            $this->setPropInvalid('catalogProductPropertyTypeId');
        if (empty($this->value))
            $this->setPropInvalid('value');
    }

}

?>
