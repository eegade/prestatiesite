<?php

class DeliveryMethod extends Model {

    public $deliveryMethodId = null;
    public $name;
    public $price; // price without tax
    public $freeFromPrice; // price with tax from where there are no extra costs (default null -> always extra price)
    public $deliveryTime; // Delivery time in days or period
    public $order = 99999;
    private $aPaymentMethods = null; //array with different lists of paymentMethods

    /**
     * validate object
     */
    public function validate() {
        if (empty($this->name))
            $this->setPropInvalid('name');
        if (!is_numeric($this->price))
            $this->setPropInvalid('price');
        if (!empty($this->freeFromPrice) && !is_numeric($this->freeFromPrice))
            $this->setPropInvalid('freeFromPrice');
    }

    /**
     * check if is deletable
     * @return boolean
     */
    public function isDeletable() {
        return true;
    }

    /**
     * check if is editable
     * @return boolean
     */
    public function isEditable() {
        return true;
    }

    /**
     * get all payment methods for this delivery method
     * @param string $sList
     * @return array PaymentMethod
     */
    public function getPaymentMethods($sList = 'online') {
        if (!isset($this->aPaymentMethods[$sList])) {
            switch ($sList) {
                case 'all':
                    $this->aPaymentMethods[$sList] = PaymentMethodManager::getPaymentMethodsByFilter(array('deliveryMethodId' => $this->deliveryMethodId));
                    break;
                default:
                    die('no option');
                    break;
            }
        }
        return $this->aPaymentMethods[$sList];
    }
    
        /**
     * set paymentMethods
     * @param array $aPaymentMethods
     * @param string $sList (set in specific list)
     */
    public function setPaymentMethods(array $aPaymentMethods, $sList = 'all') {
        $this->aPaymentMethods[$sList] = $aPaymentMethods;
    }

}

?>
