<?php

class PaymentMethod extends Model {

    public $paymentMethodId = null;
    public $name;
    public $price = 0; // price with tax
    public $isOnlinePaymentMethod = 0; // set to 1 (true) if the payment needs to be handled online (like with iDEAL)
    public $redirectPage = '/winkelwagen/bestelling-geplaatst'; // the page to go to to handle the payment
    public $order = 99999;

    /**
     * validate object
     */
    public function validate() {
        if (empty($this->name))
            $this->setPropInvalid('name');
        if (!is_numeric($this->price))
            $this->setPropInvalid('price');
        if (!is_numeric($this->isOnlinePaymentMethod))
            $this->setPropInvalid('isOnlinePaymentMethod');
        if (empty($this->redirectPage))
            $this->setPropInvalid('redirectPage');
        if (!is_numeric($this->order))
            $this->setPropInvalid('order');
    }

    /**
     * check if is deletable
     * @return boolean
     */
    public function isDeletable() {
        return true;
    }

    /**
     * check if is editable
     * @return boolean
     */
    public function isEditable() {
        return true;
    }

}

?>
