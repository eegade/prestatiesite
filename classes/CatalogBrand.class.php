<?php

// Models and managers used by this class
require_once 'Model.class.php';
require_once 'CatalogBrandManager.class.php';
require_once 'CatalogProductManager.class.php';

class CatalogBrand extends Model {

    public $catalogBrandId = null;
    public $name;
    public $online = 1;
    public $order = 99999;
    public $created = null;
    public $modified = null;
    private $aProducts = null; // association with CatalogProduct class

    /**
     * validate object
     */

    public function validate() {
        if (empty($this->name))
            $this->setPropInvalid('name');
        if (!is_numeric($this->online))
            $this->setPropInvalid('online');
    }

    /**
     * check if object is deletable
     * @return boolean
     */
    public function isDeletable() {
        return CatalogBrandManager::getNumberOfProductsByBrandId($this->catalogBrandId) == 0; // check for product relations
    }

}

?>
