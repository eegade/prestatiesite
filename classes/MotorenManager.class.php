<?

class MotorenManager {

    public static function getBikesByFilter(array $aFilter = array(), $iLimit = null, $iStart = 0, &$iFoundRows = false, $aOrderBy = array()) {
        $sUrl = 'http://www.motoren.nl/api/getBikes?apiKey=' . rawurlencode(Settings::get('motorenAPIKey')) . '&companyId=' . Settings::get('motorenCompanyId');

        if ($iLimit && is_numeric($iLimit)) {
            $sUrl .= '&limit=' . db_int($iLimit);
        }

        if ($iStart && is_numeric($iStart)) {
            $sUrl .= '&start=' . db_int($iStart);
        }

        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $sUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //execute post
        $sResponse = curl_exec($ch);

        $iHTTPCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //close connection
        curl_close($ch);

        if ($iHTTPCode === 200 && $sResponse && ($oResponse = json_decode($sResponse))) {
            if ($oResponse->error) {
                if ($oResponse->error == 1) {
                    return false;
                } else {
                    die($oResponse->errorMsg);
                }
            } else {
                return $oResponse->aBikes;
            }
        } else {
            return null;
        }
    }

    public static function getBikeById($iBikeId) {
        $sUrl = 'http://www.motoren.nl/api/getBike?apiKey=' . rawurlencode(Settings::get('motorenAPIKey')) . '&companyId=' . Settings::get('motorenCompanyId') . '&bikeId=' . $iBikeId;

        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $sUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //execute post
        $sResponse = curl_exec($ch);

        $iHTTPCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //close connection
        curl_close($ch);

        if ($iHTTPCode === 200 && $sResponse && ($oResponse = json_decode($sResponse))) {
            if ($oResponse->error) {
                if ($oResponse->error == 1) {
                    return false;
                } else {
                    die($oResponse->errorMsg);
                }
            } else {
                return $oResponse->oBike;
            }
        } else {
            return null;
        }
    }

}

?>