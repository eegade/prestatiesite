<?php

/* Models and managers used by this class */
require_once 'Model.class.php';
require_once 'CouponManager.class.php';
require_once 'CouponRuleManager.class.php';

class Coupon extends Model {

    public $couponId = null;
    public $code;
    public $timesRedeemed = 0;
    public $active = 1;
    public $couponRuleId;
    public $created = null;
    public $modified = null;
    private $oCouponRule = null; // association with CouponRule class
    private $aDatesRedeemed = null; // datetimes of when this coupon was redeemed

    /**
     * validate object
     */

    public function validate() {
        if (empty($this->code))
            $this->setPropInvalid('code');
        if (CouponManager::codeExists($this->code, $this->couponId))
            $this->setPropInvalid('codeExists');
        if (!is_numeric($this->timesRedeemed))
            $this->setPropInvalid('timesRedeemed');
        if (!is_numeric($this->active))
            $this->setPropInvalid('active');       
        if (!is_numeric($this->couponRuleId))
            $this->setPropInvalid('couponRuleId');
    }

    /**
     * set a CouponRule
     * @param CouponRule $oCouponRule 
     */
    public function setCouponRule(CouponRule $oCouponRule) {
        $this->couponRuleId = $oCouponRule->couponRuleId;
        $this->oCouponRule = $oCouponRule;
    }

    /**
     * get the related CouponRule
     * @return CouponRule
     */
    public function getCouponRule() {
        if ($this->oCouponRule === null) {
            $this->oCouponRule = CouponRuleManager::getCouponRuleById($this->couponRuleId);
        }
        return $this->oCouponRule;
    }

    /**
     * get the datetimes of when this Coupon was redeemed
     * @return array
     */
    public function getDatesRedeemed() {
        if ($this->aDatesRedeemed === null) {
            $this->aDatesRedeemed = CouponManager::getDatesRedeemedByCouponId($this->couponRuleId);
        }
        return $this->aDatesRedeemed;
    }

    /**
     * Check wether this Coupon is active.
     * @return bool
     */
    public function isActive() {
        return $this->active && $this->getCouponRule()->isActive();
    }

    /**
     * Check if this Coupon is redeemable.
     * @return bool
     */
    public function isRedeemable() {
        return $this->isActive() && ($this->timesRedeemed < $this->getCouponRule()->timesRedeemable);
    }

    /**
     * check if this object is deletable
     * @return bool 
     */
    public function isDeletable() {
        return (!$this->isActive()) || ($this->isActive() && $this->timesRedeemed == 0);
    }
}

?>
