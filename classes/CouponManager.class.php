<?php

/* Models and managers used by this class */
require_once 'Coupon.class.php';

class CouponManager {

    /**
     * get a Coupon by Id
     * @param int $iCouponId
     * @return Coupon 
     */
    public static function getCouponById($iCouponId) {
        $sQuery = ' SELECT
                        `c`.*
                    FROM
                        `coupons` `c`
                    WHERE
                        `c`.`couponId` = ' . db_int($iCouponId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "Coupon");
    }

    /**
     * get a Coupon by code
     * @param string $sCode
     * @return Coupon 
     */
    public static function getCouponByCode($sCode) {
        $sQuery = ' SELECT
                        `c`.*
                    FROM
                        `coupons` `c`
                    WHERE
                        `c`.`code` = ' . db_str($sCode) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "Coupon");
    }

    /**
     * get Coupon objects by filter
     * @param array $aFilter
     * @param int $iLimit limit number of records returned
     * @param int $iStart start from this record
     * @param int $iFoundRows foundRows when there was no limit (default = false so doesn't check by default)
     * @param array $aOrderBy array(database coloumn name => order) add order by columns and orders
     * @return array Coupon 
     */
    public static function getCouponsByFilter(array $aFilter = array(), $iLimit = null, $iStart = 0, &$iFoundRows = false, $aOrderBy = array('`c`.`couponRuleId`' => 'ASC', '`cr`.`reference`' => 'ASC', '`cr`.`title`' => 'ASC', '`c`.`created`' => 'DESC', '`c`.`modified`' => 'DESC')) {
        $sGroupBy = '';
        $sWhere = '';
        $sFrom = '';

        # filter by redeemability
        if (isset($aFilter['isRedeemable']) && $aFilter['isRedeemable'] !== '') {
            if ($aFilter['isRedeemable']) {
                $sWhere .= ($sWhere != '' ? ' AND ' : '') . '
                            `c`.`timesRedeemed` < `cr`.`timesRedeemable` AND
                            `c`.`active` = 1 AND
                            `cr`.`active` = 1 AND
                            `cr`.`activeFrom` <= NOW() AND (
                                `cr`.`activeTo` >= NOW() OR
                                `cr`.`activeTo` IS NULL
                            )
                            ';
            } else {
                $sWhere .= ($sWhere != '' ? ' AND ' : '') . ' (
                                `c`.`timesRedeemed` >= `cr`.`timesRedeemable` OR
                                `c`.`active` = 0 OR
                                `cr`.`active` = 0 OR                              
                                `cr`.`activeFrom` > NOW() OR (
                                    `cr`.`activeTo` IS NOT NULL AND
                                    `cr`.`activeTo` < NOW()
                                )
                            )
                            ';
            }
        }

        # search code
        if (!empty($aFilter['code'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`c`.`code` LIKE ' . db_str('%' . $aFilter['code'] . '%');
        }

        # filter by couponRuleId
        if (isset($aFilter['couponRuleId']) && is_numeric($aFilter['couponRuleId'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`c`.`couponRuleId` = ' . db_int($aFilter['couponRuleId']);
        }

        # handle order by
        $sOrderBy = '';
        if (count($aOrderBy) > 0) {
            foreach ($aOrderBy AS $sColumn => $sOrder) {
                $sOrderBy .= ($sOrderBy !== '' ? ',' : '') . '' . $sColumn . '' . ' ' . $sOrder;
            }
        }
        $sOrderBy = ($sOrderBy !== '' ? 'ORDER BY ' : '') . $sOrderBy;

        # handle start,limit
        $sLimit = '';
        if (is_numeric($iLimit)) {
            $sLimit .= db_int($iLimit);
        }
        if ($sLimit !== '') {
            $sLimit = (is_numeric($iStart) ? db_int($iStart) . ',' : '0,') . $sLimit;
        }
        $sLimit = ($sLimit !== '' ? 'LIMIT ' : '') . $sLimit;

        $sQuery = ' SELECT ' . ($iFoundRows !== false ? 'SQL_CALC_FOUND_ROWS' : '') . '
                        `c`.*
                    FROM
                        `coupons` `c`
                    JOIN
                        `couponRules` `cr` USING (`couponRuleId`)
                    ' . $sFrom . '
                    ' . ($sWhere != '' ? 'WHERE ' . $sWhere : '') . '
                    ' . ($sGroupBy != '' ? 'GROUP BY ' . $sGroupBy : '') . '
                    ' . $sOrderBy . '
                    ' . $sLimit . '
                    ;';

        $oDb = DBConnections::get();

        $aCoupons = $oDb->query($sQuery, QRY_OBJECT, "Coupon");
        if ($iFoundRows !== false) {
            $iFoundRows = $oDb->query('SELECT FOUND_ROWS() AS `found_rows`;', QRY_UNIQUE_OBJECT)->found_rows;
        }
        return $aCoupons;
    }

    /**
     * get the dates that a Coupon is redeemed
     * @param int $iCouponId
     * @return array 
     */
    public static function getDatesRedeemedByCouponId($iCouponId) {
        $sQuery = ' SELECT
                        `cdr`.*
                    FROM
                        `couponsDatesRedeemed` `cdr`
                    WHERE
                        `cdr`.`couponId` = ' . db_int($iCouponId) . '
                    ;';

        $oDb = DBConnections::get();
        $aDatesRedeemedAssoc = $oDb->query($sQuery, QRY_ASSOC);

        # make a simpler array with only the dates
        $aDatesRedeemed = array();
        foreach ($aDatesRedeemedAssoc as $aDateRedeemed) {
            $aDatesRedeemed[] = $aDateRedeemed['dateRedeemed'];
        }

        return $aDatesRedeemed;
    }

    /**
     * check if code exists
     * @param string $sCode
     * @param int $iCouponId
     * @return boolean
     */
    public static function codeExists($sCode, $iCouponId) {
        if (empty($sCode))
            return false;

        $oCoupon = self::getCouponByCode($sCode);
        if (empty($oCoupon)) {
            return false;
        } else {
            if ($oCoupon->couponId == $iCouponId) {
                return false;
            }
            return true;
        }
    }

    /**
     * generate a unique code
     * @param int $iCouponId
     * @return string
     */
    public static function generateUniqueCode($iCouponId = null) {
        while (true) {
            $sCode = randomPassword(12, 0, 6, 4, 0, '', 'ABCDEFGHJKLMNPQRSTUVWXYZ', '123456789', '');
            if (!self::codeExists($sCode, $iCouponId)) {
                return $sCode;
                break;
            }
        }
    }

    /**
     * save a Coupon
     * @param Coupon $oCoupon
     * @return int
     */
    public static function saveCoupon(Coupon $oCoupon) {
        if (empty($oCoupon->couponId)) {
            $sQuery = ' INSERT IGNORE INTO `coupons`(
                            `code`,
                            `timesRedeemed`,
                            `active`,
                            `couponRuleId`,
                            `created`
                        )
                        VALUES (
                            ' . db_str($oCoupon->code) . ',
                            ' . db_int($oCoupon->timesRedeemed) . ',
                            ' . db_int($oCoupon->active) . ',
                            ' . db_int($oCoupon->couponRuleId) . ',
                            NOW()
                        );';
        } else {
            $sQuery = ' UPDATE
                            `coupons`
                        SET
                            `code` = ' . db_str($oCoupon->code) . ',
                            `timesRedeemed` = ' . db_int($oCoupon->timesRedeemed) . ',
                            `active` = ' . db_int($oCoupon->active) . ',   
                            `couponRuleId` = ' . db_int($oCoupon->couponRuleId) . '
                        WHERE
                            `couponId` = ' . db_int($oCoupon->couponId) . '
                        LIMIT 1
                        ;';
        }

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);
        return $oDb->affected_rows;
    }

    /**
     * save generated Coupon objects in bulk
     * @param array $aCoupons
     * @return int
     */
    public static function saveCouponsInBulk(array $aCoupons) {
        if (empty($aCoupons))
            return 0;

        $sValues = '';
        foreach ($aCoupons as $oCoupon) {
            $sValues .= ($sValues != '' ? ',' : '') . '(' . db_str($oCoupon->code) . ',' . db_int($oCoupon->timesRedeemed) . ',' . db_int($oCoupon->active) . ', ' . db_int($oCoupon->couponRuleId) . ', NOW())';
        }

        if (empty($sValues))
            return 0;

        $sQuery = ' INSERT IGNORE INTO `coupons`(
                        `code`,
                        `timesRedeemed`,
                        `active`,
                        `couponRuleId`,
                        `created`
                    )
                    VALUES
                    ' . $sValues . '
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT, null, true, true);
        return $oDb->affected_rows;
    }

    /**
     * generate Coupon objects based on a CouponRule with a secified amount
     * @param int $iCouponRuleId
     * @param int $iAmount
     * @return int 
     */
    public static function generateCouponsByCouponRuleId(CouponRule $oCouponRule, $iAmount = 1) {
        if (empty($oCouponRule) || empty($oCouponRule->couponRuleId))
            return false;

        $iCoupons = 0;
        while ($iCoupons < $iAmount) {
            $aCoupons = array();
            for ($i = 0; $i < ($iAmount - $iCoupons); $i++) {
                $oCoupon = new Coupon(array('code' => self::generateUniqueCode()));
                $oCoupon->setCouponRule($oCouponRule);
                $aCoupons[] = $oCoupon;
            }
            $iCoupons += self::saveCouponsInBulk($aCoupons); // add the actual saved Coupon objects to the variable
        }

        return $iCoupons;
    }

    /**
     * redeem a Coupon by code
     * @param string $sCode
     * @return bool
     */
    public static function redeemCouponByCode($sCode) {
        $oCoupon = self::getCouponByCode($sCode);

        if (empty($oCoupon) || !$oCoupon->isRedeemable())
            return 0;

        $oDb = DBConnections::get();

        # update the Coupon
        $sQuery = ' UPDATE
                        `coupons` `c`
                    JOIN
                        `couponRules` `cr`
                    SET
                        `c`.`timesRedeemed` = (`c`.`timesRedeemed` + 1)
                    WHERE
                        `c`.`couponId` = ' . db_int($oCoupon->couponId) . ' AND
                        `c`.`timesRedeemed` < `cr`.`timesRedeemable`
                    ;';

        $oDb->query($sQuery, QRY_NORESULT);

        if ($oDb->affected_rows > 0) {
            # insert a dateRedeemed
            $sQuery = ' INSERT INTO `couponsDatesRedeemed` (
                            `couponId`,
                            `dateRedeemed`
                        )
                        VALUES (
                            ' . $oCoupon->couponId . ',
                            NOW()
                        )
                        ;';

            $oDb->query($sQuery, QRY_NORESULT);
        }
        return $oDb->affected_rows;
    }

    /**
     * update active by couponId
     * @param int $bActive
     * @param int $iCouponId
     * @return bool
     */
    public static function updateActiveByCouponId($bActive, $iCouponId) {
        $sQuery = ' UPDATE
                        `coupons`
                    SET
                        `active` = ' . db_int($bActive) . '
                    WHERE
                        `couponId` = ' . db_int($iCouponId) . '
                    LIMIT 1
                    ;';
        $oDb = DBConnections::get();

        $oDb->query($sQuery, QRY_NORESULT);

        # check if something happened
        return $oDb->affected_rows > 0;
    }

    /**
     * delete Coupon
     * @param Coupon $oCoupon
     * @return bool
     */
    public static function deleteCoupon(Coupon $oCoupon) {
        if ($oCoupon->isDeletable()) {
            $sQuery = " DELETE FROM
                            `coupons`
                        WHERE
                            `couponId` = " . db_int($oCoupon->couponId) . "
                        LIMIT 1
                        ;";

            $oDb = DBConnections::get();
            $oDb->query($sQuery, QRY_NORESULT);
            return true;
        }
        return false;
    }

}

?>
