<?php

// Models and managers used by this class
require_once 'Model.class.php';
require_once 'CatalogProductColorManager.class.php';
require_once 'CatalogProductManager.class.php';

class CatalogProductColor extends Model {

    const colorId_nocolor = -1;

    public $catalogProductColorId = null;
    public $name;
    public $order = 99999;
    public $created = null;
    public $modified = null;

    /**
     * validate object
     */
    public function validate() {
        if (empty($this->name))
            $this->setPropInvalid('name');
        if (!is_numeric($this->order))
            $this->setPropInvalid('order');
    }

    /**
     * check if object is deletable
     * @return boolean
     */
    public function isDeletable() {
        return CatalogProductColorManager::isUnused($this->catalogProductColorId);
    }

}

?>
