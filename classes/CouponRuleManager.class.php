<?php

/* Models and managers used by this class */
require_once 'CouponRule.class.php';

class CouponRuleManager {

    /**
     * get all discountTypes
     * @return array
     */
    public static function getAllDiscountTypes() {
        return array(
            CouponRule::DISCOUNTTYPE_ORDER_FIXED_PRICE,
            CouponRule::DISCOUNTTYPE_ORDER_PERCENTAGE
        );
    }

    /**
     * get the discountType's label by discountType
     * @param string $sDiscountType
     * @return string
     */
    public static function getLabelByDiscountType($sDiscountType) {
        if (empty($sDiscountType))
            return null;
        switch ($sDiscountType) {
            case CouponRule::DISCOUNTTYPE_ORDER_FIXED_PRICE:
                return 'Vaste prijs';
                break;
            case CouponRule::DISCOUNTTYPE_ORDER_PERCENTAGE:
                return 'Percentage';
                break;
            default:
                return $sDiscountType;
                break;
        }
    }

    /**
     * get a CouponRule by id
     * @param int $iCouponRuleId
     * @return CouponRule 
     */
    public static function getCouponRuleById($iCouponRuleId) {
        $sQuery = ' SELECT
                        `cr`.*
                    FROM
                        `couponRules` `cr`
                    WHERE
                        `cr`.`couponRuleId` = ' . db_int($iCouponRuleId) . '
                    LIMIT 1;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "CouponRule");
    }

    /**
     * get CouponRule objects by filter
     * @param array $aFilter
     * @param int $iLimit limit number of records returned
     * @param int $iStart start from this record
     * @param int $iFoundRows foundRows when there was no limit (default = false so doesn't check by default)
     * @param array $aOrderBy array(database coloumn name => order) add order by columns and orders
     * @return array CouponRule 
     */
    public static function getCouponRulesByFilter(array $aFilter = array(), $iLimit = null, $iStart = 0, &$iFoundRows = false, $aOrderBy = array('reference' => 'ASC', 'title' => 'ASC', 'created' => 'DESC', 'modified' => 'DESC')) {
        $sGroupBy = '';
        $sWhere = '';
        $sFrom = '';

        # check Active
        if (empty($aFilter['showAll'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '
                            `cr`.`active` = 1
                        AND
                            `cr`.`activeFrom` <= NOW()
                        AND (
                                `cr`.`activeTo` >= NOW() OR
                                `cr`.`activeTo` IS NULL
                            )
                        ';
        }

        # search by title
        if (!empty($aFilter['title'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`cr`.`title` LIKE ' . db_str('%' . $aFilter['title'] . '%');
        }

        # filter by reference
        if (!empty($aFilter['reference'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`cr`.`reference` LIKE ' . db_str('%' . $aFilter['reference'] . '%');
        }

        # filter by discountType
        if (!empty($aFilter['discountType'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . ' `cr`.`discountType` = ' . db_str($aFilter['discountType']);
        }

        # search for minimum salePrice
        if (isset($aFilter['minDiscountAmount']) && is_numeric($aFilter['minDiscountAmount'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`cr`.`discountAmount` >= ' . db_deci($aFilter['minDiscountAmount']);
        }

        # search for maximum salePrice
        if (isset($aFilter['maxDiscountAmount']) && is_numeric($aFilter['maxDiscountAmount'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`cr`.`discountAmount` <= ' . db_deci($aFilter['maxDiscountAmount']);
        }

        # handle order by
        $sOrderBy = '';
        if (count($aOrderBy) > 0) {
            foreach ($aOrderBy AS $sColumn => $sOrder) {
                $sOrderBy .= ($sOrderBy !== '' ? ',' : '') . '`cr`.`' . $sColumn . '`' . ' ' . $sOrder;
            }
        }
        $sOrderBy = ($sOrderBy !== '' ? 'ORDER BY ' : '') . $sOrderBy;

        # handle start,limit
        $sLimit = '';
        if (is_numeric($iLimit)) {
            $sLimit .= db_int($iLimit);
        }
        if ($sLimit !== '') {
            $sLimit = (is_numeric($iStart) ? db_int($iStart) . ',' : '0,') . $sLimit;
        }
        $sLimit = ($sLimit !== '' ? 'LIMIT ' : '') . $sLimit;

        $sQuery = ' SELECT ' . ($iFoundRows !== false ? 'SQL_CALC_FOUND_ROWS' : '') . '
                        `cr`.*
                    FROM
                        `couponRules` `cr`
                    ' . $sFrom . '
                    ' . ($sWhere != '' ? 'WHERE ' . $sWhere : '') . '
                    ' . ($sGroupBy != '' ? 'GROUP BY ' . $sGroupBy : '') . '
                    ' . $sOrderBy . '
                    ' . $sLimit . '
                    ;';

        $oDb = DBConnections::get();

        $aCoupons = $oDb->query($sQuery, QRY_OBJECT, "CouponRule");
        if ($iFoundRows !== false) {
            $iFoundRows = $oDb->query('SELECT FOUND_ROWS() AS `found_rows`;', QRY_UNIQUE_OBJECT)->found_rows;
        }
        return $aCoupons;
    }

    /**
     * save a CouponRule
     * @param CouponRule $oCouponRule
     */
    public static function saveCouponRule(CouponRule $oCouponRule) {
        $sQuery = ' INSERT INTO `couponRules`(
                        `couponRuleId`,
                        `title`,
                        `reference`,
                        `discountType`,
                        `discountAmount`,
                        `timesRedeemable`,
                        `activeFrom`,
                        `activeTo`,
                        `active`,
                        `created`
                    )
                    VALUES (
                        ' . db_int($oCouponRule->couponRuleId) . ',
                        ' . db_str($oCouponRule->title) . ',
                        ' . db_str($oCouponRule->reference) . ',
                        ' . db_str($oCouponRule->discountType) . ',
                        ' . db_str($oCouponRule->discountAmount) . ',
                        ' . db_int($oCouponRule->timesRedeemable) . ',
                        ' . db_date($oCouponRule->activeFrom) . ',
                        ' . db_date($oCouponRule->activeTo) . ',
                        ' . db_int($oCouponRule->active) . ',   
                        NOW()
                    )
                    ON DUPLICATE KEY UPDATE
                        `title`=VALUES(`title`),
                        `reference`=VALUES(`reference`),
                        `discountType`=VALUES(`discountType`),
                        `discountAmount`=VALUES(`discountAmount`),
                        `timesRedeemable`=VALUES(`timesRedeemable`),
                        `activeFrom`=VALUES(`activeFrom`),
                        `activeTo`=VALUES(`activeTo`),
                        `active`=VALUES(`active`)
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);

        if ($oCouponRule->couponRuleId === null)
            $oCouponRule->couponRuleId = $oDb->insert_id;
    }

    /**
     * update active by couponRuleId
     * @param int $bActive
     * @param int $iCouponId
     * @return bool
     */
    public static function updateActiveByCouponRuleId($bActive, $iCouponRuleId) {
        $sQuery = ' UPDATE
                        `couponRules`
                    SET
                        `active` = ' . db_int($bActive) . '
                    WHERE
                        `couponRuleId` = ' . db_int($iCouponRuleId) . ' 
                    LIMIT 1
                    ;';
        $oDb = DBConnections::get();

        $oDb->query($sQuery, QRY_NORESULT);

        # check if something happened
        return $oDb->affected_rows > 0;
    }

    /**
     * delete CouponRule and all related Coupon objects
     * @param CouponRule $oCouponRule
     * @return bool
     */
    public static function deleteCouponRule(CouponRule $oCouponRule) {
        if ($oCouponRule->isDeletable()) {
            $sQuery = " DELETE FROM
                            `couponRules`
                        WHERE
                            `couponRuleId` = " . db_int($oCouponRule->couponRuleId) . " 
                        LIMIT 1
                        ;";

            $oDb = DBConnections::get();
            $oDb->query($sQuery, QRY_NORESULT);
            return true;
        }
        return false;
    }

}

?>
