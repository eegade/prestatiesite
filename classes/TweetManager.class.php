<?php

/* Models and managers used by the TwitterManager model */
require_once 'Tweet.class.php';

class TweetManager {

    /**
     * get a Tweet by id
     * @return Tweet
     */
    public static function getTweetById($iTweetId) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `tweets`
                    WHERE
                        `tweetId` = ' . db_int($iTweetId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "Tweet");
    }

    /**
     * Get all tweets from the database
     * @param $iLimit
     * @return array Tweet
     */
    public static function getAllTweets($iLimit = 0) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `tweets`
                    ORDER BY
                        `tweetId` DESC';
        if ($iLimit > 0) {
            $sQuery .= ' LIMIT '.db_int($iLimit);
        }
        
        $sQuery .= ';';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_OBJECT, "Tweet");
    }

    /**
     * get the Tweet objects by filter
     * @param array $aFilter filter properties (checkOnline)
     * @param int $iLimit limit number of records returned
     * @param int $iStart start from this record
     * @param int $iFoundRows foundRows when there was no limit (default = false so doesn't check by default)
     * @param array $aOrderBy array(database coloumn name => order) add order by columns and orders
     * @return array Tweet objects 
     */
    public static function getTweetsByFilter(array $aFilter = array(), $iLimit = null, $iStart = 0, &$iFoundRows = false, $aOrderBy = array('tweetId' => 'DESC')) {
        $sWhere = '';

        # search only online tweets
        if (!empty($aFilter['checkOnline'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`t`.`online` = 1';
        }

        # handle order by
        $sOrderBy = '';
        if (count($aOrderBy) > 0) {
            foreach ($aOrderBy AS $sColumn => $sOrder) {
                $sOrderBy .= ($sOrderBy !== '' ? ',' : '') . '`t`.`' . $sColumn . '`' . ' ' . $sOrder;
            }
        }
        $sOrderBy = ($sOrderBy !== '' ? 'ORDER BY ' : '') . $sOrderBy;

        # handle start,limit
        $sLimit = '';
        if (is_numeric($iLimit)) {
            $sLimit .= $iLimit;
        }
        if ($sLimit !== '') {
            $sLimit = (is_numeric($iStart) ? $iStart . ',' : '0,') . $sLimit;
        }
        $sLimit = ($sLimit !== '' ? 'LIMIT ' : '') . $sLimit;

        $sQuery = ' SELECT SQL_CALC_FOUND_ROWS
                        `t`.*
                    FROM
                        `tweets` `t`
                    ' . ($sWhere != '' ? 'WHERE ' . $sWhere : '') . '
                    ' . $sOrderBy . '
                    ' . $sLimit . '
                    ;';

        $oDb = DBConnections::get();
        $aTweets = $oDb->query($sQuery, QRY_OBJECT, "Tweet");
        if ($iFoundRows !== false) {
            $iFoundRows = $oDb->query('SELECT FOUND_ROWS() AS `found_rows`;', QRY_UNIQUE_OBJECT)->found_rows;
        }
        return $aTweets;
    }

    /**
     * save the tweets to the database.
     * @param array $aTweets array with stdClass object from twitter feed (json)
     */
    public static function saveTweets(array $aTweets = array()) {
        if(!is_array($aTweets))
            return false;
        
        $sDeleteNotIn = '';
        $sInsertValues = '';
        foreach ($aTweets as $oTweet) {
            if (!isset($oTweet->id) || !isset($oTweet->text) || !isset($oTweet->created_at) || !isset($oTweet->text) || !isset($oTweet->user) || !isset($oTweet->user->id) || !isset($oTweet->user->screen_name))
                continue;

            $sDeleteNotIn .= ($sDeleteNotIn == '' ? '' : ', ') . db_int($oTweet->id);
            $sInsertValues .= ($sInsertValues == '' ? '' : ', ') . '(' . db_int($oTweet->id) . ',' . db_str($oTweet->text) . ',' . db_date(date('Y-m-d H:i:s', strtotime($oTweet->created_at))) . ',' . (empty($oTweet->retweeted_status->user) ? '0' : '1') . ',' . db_int($oTweet->user->id) . ',' . db_str($oTweet->user->name) . ',' . db_str($oTweet->user->screen_name) . ',' . (empty($oTweet->user->profile_image_url) ? 'NULL' : db_str($oTweet->user->url)) . ',' . (empty($oTweet->user->profile_image_url) ? 'NULL' : db_str($oTweet->user->profile_image_url)) . ', 1, NOW())';
        }

        $oDb = DBConnections::get();

        # delete tweets that are not in the Tweets array
        $sQuery = ' DELETE FROM
                        `tweets`
                    ' . ($sDeleteNotIn == '' ? '' : 'WHERE `tweetId` NOT IN (' . $sDeleteNotIn . ')') . '
                    ;';

        $oDb->query($sQuery, QRY_NORESULT);

        if ($sInsertValues == '')
            return;

        # save the tweets in the database
        $sQuery = ' INSERT INTO `tweets` (
                        `tweetId`,
                        `text`,
                        `created_at`,
                        `retweet`,
                        `userId`,
                        `name`,
                        `screen_name`,
                        `url`,
                        `profile_image_url`,
                        `online`,
                        `created`
                    )
                    VALUES ' . $sInsertValues . '
                    ON DUPLICATE KEY UPDATE
                        `text`=VALUES(`text`),
                        `created_at`=VALUES(`created_at`),
                        `retweet`=VALUES(`retweet`),
                        `userId`=VALUES(`userId`),
                        `name`=VALUES(`name`),
                        `screen_name`=VALUES(`screen_name`),
                        `url`=VALUES(`url`),
                        `profile_image_url`=VALUES(`profile_image_url`)
                    ;';

        $oDb->query($sQuery, QRY_NORESULT);
    }

}
?>

