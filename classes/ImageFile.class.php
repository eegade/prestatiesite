<?php

/* Models and managers used by the ImageFile model */
require_once 'File.class.php';

class ImageFile extends File {

    public $imageId;
    public $reference;
    public $type = Media::IMAGE;
    public $online = 1;
    public $imageSizeAttr;

    /**
     * validate object
     */
    public function validate() {
        if (empty($this->imageId))
            $this->setPropInvalid('imageId');
        if (empty($this->reference))
            $this->setPropInvalid('reference');
        parent::validate();
    }

}
?>
