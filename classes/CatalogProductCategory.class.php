<?php

// Models and managers used by this class
require_once 'Model.class.php';
require_once 'CatalogProductCategoryManager.class.php';

class CatalogProductCategory extends Model {

    const MAX_LEVELS = 2;

    public $catalogProductCategoryId = null;
    public $name;
    public $content;
    public $windowTitle; //browser window title
    public $metaKeywords; //meta tag keywords
    public $metaDescription; //meta tag description
    private $urlPart; // part of the url to use in stead of the name
    public $online = 1;
    public $order = 99999;
    public $parentCatalogProductCategoryId;
    public $level = 1;
    public $created = null;
    public $modified = null;
    private $lockParent = 0; // lock parent category
    private $aSubCategories = null;  // array with different lists of sub categories
    private $oParentCategory = null; // references CatalogProductCategory class

    /**
     * validate object
     */

    public function validate() {
        if (empty($this->name))
            $this->setPropInvalid('name');
        if (!is_numeric($this->online))
            $this->setPropInvalid('online');
    }

    /**
     * calculate level and set in object
     */
    public function setLevel() {
        if ($this->parentCatalogProductCategoryId) {
            $this->level = $this->getParent()->level + 1;
        } else {
            $this->level = 1;
        }
    }

    /**
     * just returns integer, DO NOT USE FOR LOCK PARENT CHECKING
     * return value of lockParent
     * @return int
     */
    public function getLockParent() {
        return $this->lockParent;
    }

    /**
     * check to lock parent
     * @return boolean
     */
    public function lockParent() {
        return $this->lockParent;
    }

    /**
     * get url to a product optional with extension .html
     * @param boolean $bWithExtension (optional) DEFAULT FALSE
     * @return string
     */
    public function getUrlPath($bWithExtension = false) {
        $sUrlPath = '/productcategorieen/' . $this->catalogProductCategoryId . '/' . prettyUrlPart(!empty($this->urlPart) ? $this->urlPart : $this->name);

        if ($bWithExtension)
            return $sUrlPath . '.html';
        return $sUrlPath;
    }

    /**
     * return the window title if there is one, otherwise return title
     * @return string
     */
    public function getWindowTitle() {
        if (!empty($this->windowTitle)) {
            return $this->windowTitle;
        } elseif (($oPage = PageManager::getPageByUrlPath('/producten'))) {
            return $oPage->getShortTitle() . ' - ' . $this->name;
        } else {
            return 'Webshop - ' . $this->name;
        }
    }

    /**
     * return meta description if exists or a
     * @return string
     */
    public function getMetaDescription() {
        return !empty($this->metaDescription) ? $this->metaDescription : generateMetaDescription($this->content);
    }

    /**
     * return meta keywords
     * @return string
     */
    public function getMetaKeywords() {
        return $this->metaKeywords;
    }

    /**
     * generates an array of the breadcrumbs, but in reverse order
     * @param array $aCrumbles
     */
    private function generateCrumbles(&$aCrumbles = array()) {
        $aCrumbles[$this->name] = $this->getUrlPath();

        $oParentCategory = $this->getParent();
        if (!empty($oParentCategory)) {
            $oParentCategory->generateCrumbles($aCrumbles);
        }
    }

    /**
     * get an array of the breadcrumbs in the right order
     * @return array
     */
    public function getCrumbles() {

        // try to get /producten page and set as first crumble after home
        if (($oPage = PageManager::getPageByUrlPath('/producten'))) {
            $this->generateCrumbles($aCrumbles);
            $aCrumbles[$oPage->getShortTitle()] = $oPage->getUrlPath();
        } else {
            $this->generateCrumbles($aCrumbles);
            $aCrumbles['Categorieën'] = '/productcategorieen';
        }

        return array_reverse($aCrumbles);
    }

    /**
     * return all subcategories for this category
     */
    public function getSubCategories($sList = 'online') {
        if (!isset($this->aSubCategories[$sList])) {
            switch ($sList) {
                case 'online':
                    $this->aSubCategories[$sList] = CatalogProductCategoryManager::getProductCategoriesByFilter(array('parentCatalogProductCategoryId' => $this->catalogProductCategoryId));
                    break;
                case 'all':
                    $this->aSubCategories[$sList] = CatalogProductCategoryManager::getProductCategoriesByFilter(array('parentCatalogProductCategoryId' => $this->catalogProductCategoryId, 'showAll' => 1));
                    break;
                default:
                    die('no option');
                    break;
            }
        }
        return $this->aSubCategories[$sList];
    }

    /**
     * return parent category
     * @return CatalogProductCategory
     */
    public function getParent() {
        if ($this->oParentCategory === null) {
            $this->oParentCategory = CatalogProductCategoryManager::getProductCategoryById($this->parentCatalogProductCategoryId);
        }
        return $this->oParentCategory;
    }

    /**
     * return part of the url for this page
     * @return string
     */
    public function getUrlPart() {
        return $this->urlPart;
    }

    /**
     * set url part for this page
     * @param string $sUrlPart
     */
    public function setUrlPart($sUrlPart) {
        $this->urlPart = prettyUrlPart($sUrlPart);
    }

    /**
     * check if object is deletable
     * @return boolean
     */
    public function isDeletable() {
        return count($this->getSubCategories('all')) == 0 && count(CatalogProductManager::getProductsByFilter(array('catalogProductCategoryId' => $this->catalogProductCategoryId, 'showAll' => true))) == 0; // check for product relations and subcategories
    }

}

?>