<?php

// Models and managers used by this class
require_once 'ContinuousSliderItem.class.php';

class ContinuousSliderItemManager {

    /**
     * get a ContinuousSliderItem by id
     * @param int $iContinuousSliderItemId
     * @return ContinuousSliderItem
     */
    public static function getContinuousSliderItemById($iContinuousSliderItemId) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `continuousSliderItems`
                    WHERE
                        `continuousSliderItemId` = ' . db_int($iContinuousSliderItemId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "ContinuousSliderItem");
    }

    /**
     * return continuousSlider items filtered by a few options
     * @param array $aFilter filter properties
     * @param int $iLimit limit number of records returned
     * @param int $iStart start from this record
     * @param int $iFoundRows foundRows when there was no limit (default = false so doesn't check by default)
     * @param array $aOrderBy array(database coloumn name => order) add order by columns and orders
     * @return array ContinuousSliderItem 
     */
    public static function getContinuousSliderItemsByFilter(array $aFilter = array(), $iLimit = null, $iStart = 0, &$iFoundRows = false, $aOrderBy = array('`bi`.`order`' => 'ASC', '`bi`.`continuousSliderItemId`' => 'ASC')) {
        $sFrom = '';
        $sWhere = '';

        // default is not to show all items
        if (empty($aFilter['showAll'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`bi`.`online` = 1';
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`bi`.`imageId` IS NOT NULL';
        }

        # handle order by
        $sOrderBy = '';
        if (count($aOrderBy) > 0) {
            foreach ($aOrderBy AS $sColumn => $sOrder) {
                $sOrderBy .= ($sOrderBy !== '' ? ',' : '') . $sColumn . ' ' . $sOrder;
            }
        }
        $sOrderBy = ($sOrderBy !== '' ? 'ORDER BY ' : '') . $sOrderBy;

        # handle start,limit
        $sLimit = '';
        if (is_numeric($iLimit)) {
            $sLimit .= db_int($iLimit);
        }
        if ($sLimit !== '') {
            $sLimit = (is_numeric($iStart) ? db_int($iStart) . ',' : '0,') . $sLimit;
        }
        $sLimit = ($sLimit !== '' ? 'LIMIT ' : '') . $sLimit;

        $sQuery = ' SELECT ' . ($iFoundRows !== false ? 'SQL_CALC_FOUND_ROWS' : '') . '
                        `bi`.*
                    FROM
                        `continuousSliderItems` AS `bi`
                    ' . $sFrom . '
                    ' . ($sWhere != '' ? 'WHERE ' . $sWhere : '') . '
                    ' . $sOrderBy . '
                    ' . $sLimit . '
                    ;';

        $oDb = DBConnections::get();
        $aContinuousSliderItems = $oDb->query($sQuery, QRY_OBJECT, "ContinuousSliderItem");
        if ($iFoundRows !== false) {
            $iFoundRows = $oDb->query('SELECT FOUND_ROWS() AS `found_rows`;', QRY_UNIQUE_OBJECT)->found_rows;
        }

        return $aContinuousSliderItems;
    }

    /**
     * save a ContinuousSliderItem
     * @param ContinuousSliderItem $oContinuousSliderItem
     */
    public static function saveContinuousSliderItem(ContinuousSliderItem $oContinuousSliderItem) {
        $sQuery = ' INSERT INTO `continuousSliderItems`(
                        `continuousSliderItemId`,
                        `name`,
                        `link`,
                        `pageId`,
                        `newsItemId`,
                        `line1`,
                        `line2`,
                        `online`,
                        `order`,
                        `created`
                    )
                    VALUES (
                        ' . db_int($oContinuousSliderItem->continuousSliderItemId) . ',
                        ' . db_str($oContinuousSliderItem->name) . ',
                        ' . db_str($oContinuousSliderItem->link) . ',
                        ' . db_int($oContinuousSliderItem->pageId) . ',
                        ' . db_int($oContinuousSliderItem->newsItemId) . ',
                        ' . db_str($oContinuousSliderItem->line1) . ',
                        ' . db_str($oContinuousSliderItem->line2) . ',
                        ' . db_int($oContinuousSliderItem->online) . ',
                        ' . db_int($oContinuousSliderItem->order) . ',
                        NOW()
                    )
                    ON DUPLICATE KEY UPDATE
                        `name`=VALUES(`name`),
                        `link`=VALUES(`link`),
                        `pageId`=VALUES(`pageId`),
                        `newsItemId`=VALUES(`newsItemId`),
                        `line1`=VALUES(`line1`),
                        `line2`=VALUES(`line2`),
                        `online`=VALUES(`online`),
                        `order`=VALUES(`order`)
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);

        if ($oContinuousSliderItem->continuousSliderItemId === null)
            $oContinuousSliderItem->continuousSliderItemId = $oDb->insert_id;
    }

    /**
     * update online status of ContinuousSliderItem by id
     * @param int $bOnline
     * @param int $iContinuousSliderItemId
     * @return bool
     */
    public static function updateOnlineByContinuousSliderItemId($bOnline, $iContinuousSliderItemId) {
        $sQuery = ' UPDATE
                        `continuousSliderItems`
                    SET
                        `online` = ' . db_int($bOnline) . '
                    WHERE
                        `continuousSliderItemId` = ' . db_int($iContinuousSliderItemId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);

        # check if something happened
        return $oDb->affected_rows > 0;
    }

    /**
     * update a ContinuousSliderItem's order
     * @param ContinuousSliderItem $oContinuousSliderItem
     */
    public static function updateContinuousSliderItemOrder(ContinuousSliderItem $oContinuousSliderItem) {
        $sQuery = ' UPDATE 
                        `continuousSliderItems`
                    SET
                        `order` = ' . db_int($oContinuousSliderItem->order) . '
                    WHERE
                        `continuousSliderItemId` = ' . db_int($oContinuousSliderItem->continuousSliderItemId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);
    }

    /**
     * update a ContinuousSliderItem with the related imageId
     * @param int $iContinuousSliderItemId
     * @param int $iImageId 
     */
    public static function saveContinuousSliderItemImageRelations($iContinuousSliderItemId, $iImageId) {
        $sQuery = ' UPDATE 
                        `continuousSliderItems`
                    SET
                        `imageId` = ' . db_int($iImageId) . '
                    WHERE
                        `continuousSliderItemId` = ' . db_int($iContinuousSliderItemId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);
    }

    /**
     * delete a ContinuousSliderItem
     * @param ContinuousSliderItem $oContinuousSliderItem
     * @return bool true
     */
    public static function deleteContinuousSliderItem(ContinuousSliderItem $oContinuousSliderItem) {
        # delete related Image first
        if ($oContinuousSliderItem->getImage())
            ImageManager::deleteImage($oContinuousSliderItem->getImage());

        # delete object
        $sQuery = ' DELETE FROM
                        `continuousSliderItems`
                    WHERE
                        `continuousSliderItemId` = ' . db_int($oContinuousSliderItem->continuousSliderItemId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);
        return true;
    }

}

?>
