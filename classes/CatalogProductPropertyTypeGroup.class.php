<?php

// Models and managers used by this class
require_once 'CatalogProductPropertyTypeGroupManager.class.php';
require_once 'CatalogProductTypeManager.class.php';
require_once 'CatalogProductPropertyTypeManager.class.php';

class CatalogProductPropertyTypeGroup extends Model {

    public $catalogProductPropertyTypeGroupId = null;
    public $title;
    public $order = 99999;
    public $created = null;
    public $modified = null;
    public $catalogProductTypeId;
    private $oProductType = null; // association with CatalogProductType class
    private $aPropertyTypes = null; // association with CatalogProductPropertyType class

    /**
     * validate object
     */
    public function validate() {
        if (empty($this->title))
            $this->setPropInvalid('title');
        if (empty($this->order))
            $this->setPropInvalid('order');
        if (empty($this->catalogProductTypeId))
            $this->setPropInvalid('catalogProductTypeId');
    }
    
    /**
     * get the CatalogProductType
     * @return CatalogProductType 
     */
    public function getProductType() {
        if ($this->oProductType === null) {
            $this->oProductType = CatalogProductTypeManager::getProductTypeById($this->catalogProductTypeId);
        }
        return $this->oProductType;
    }
    
    public function getPropertyTypes() {
        if ($this->aPropertyTypes === null) {
            $this->aPropertyTypes = CatalogProductPropertyTypeManager::getProductPropertyTypesByProductPropertyTypeGroupId($this->catalogProductPropertyTypeGroupId);
        }
        return $this->aPropertyTypes;
    }
    
    /**
     * check if object is deletable
     * @return boolean
     */
    public function isDeletable(){
        return CatalogProductPropertyTypeGroupManager::getNumberOfProductPropertyTypesByProductPropertyTypeGroupId($this->catalogProductPropertyTypeGroupId) == 0; // check for product relations
    }

}

?>
