<?php

// Models and managers used by this class
require_once 'Model.class.php';
require_once 'CatalogProductSizeManager.class.php';
require_once 'CatalogProductManager.class.php';

class CatalogProductSize extends Model {

    const sizeId_nosize = -1;

    public $catalogProductSizeId = null;
    public $name;
    public $order = 99999;
    public $created = null;
    public $modified = null;

    /**
     * validate object
     */
    public function validate() {
        if (empty($this->name))
            $this->setPropInvalid('name');
        if (!is_numeric($this->order))
            $this->setPropInvalid('order');
    }

    /**
     * check if object is deletable
     * @return boolean
     */
    public function isDeletable() {
        return CatalogProductSizeManager::isUnused($this->catalogProductSizeId);
    }

}

?>
