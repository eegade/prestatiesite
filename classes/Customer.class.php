<?php

// Models and managers used by this class
require_once 'Model.class.php';
require_once 'CustomerManager.class.php';
require_once 'OrderManager.class.php';

class Customer extends Model {

    public $customerId = null;
    public $companyName = null;
    public $gender;
    public $firstName;
    public $insertion;
    public $lastName;
    public $address;
    public $houseNumber;
    public $houseNumberAddition;
    public $postalCode;
    public $city;
    public $phone = null;
    public $mobilePhone= null;
    public $email;
    public $password;
    public $confirmCode = null;
    public $online = 0;
    public $lastLogin = null;
    public $created = null;
    public $modified = null;
    private $aOrders = null;

    /**
     * validate object
     */
    public function validate() {
        if (empty($this->gender))
            $this->setPropInvalid('gender');
        if (empty($this->firstName))
            $this->setPropInvalid('firstName');
        if (empty($this->lastName))
            $this->setPropInvalid('lastName');
        if (empty($this->address))
            $this->setPropInvalid('address');
        if (!is_numeric($this->houseNumber))
            $this->setPropInvalid('houseNumber');
        if (empty($this->postalCode))
            $this->setPropInvalid('postalCode');
        if (empty($this->city))
            $this->setPropInvalid('city');
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
            $this->setPropInvalid('email');
        if (CustomerManager::emailExists($this->email, $this->customerId))
            $this->setPropInvalid('emailExists');
        if (empty($this->password) || (!empty($this->password) && strlen($this->password) < 8))
            $this->setPropInvalid('password');
        if (!is_numeric($this->online))
            $this->setPropInvalid('online');
    }

    /**
     * mask pasword for session use
     */
    function maskPass() {
        $this->password = 'XXX';
    }
    
    /**
     * get all Order objects related to this Customer
     * @return array Order $this->aOrders
     */
    public function getOrders() {
        if($this->aOrders === null) {
            $this->aOrders = OrderManager::getOrdersByCustomerId($this->customerId);
        }
        return $this->aOrders;
    }
    
}

?>
