<?php

class CatalogProductPropertyTypePossibleValue extends Model {

    public $catalogProductPropertyTypePossibleValueId = null;
    public $value;
    public $order = 99999;
    public $catalogProductPropertyTypeId;

    /**
     * validate object
     */
    public function validate() {
        if (empty($this->value))
            $this->setPropInvalid('value');
        if (empty($this->order))
            $this->setPropInvalid('order');
        if (empty($this->catalogProductPropertyTypeId))
            $this->setPropInvalid('catalogProductPropertyTypeId');
    }

}

?>
