<?php

// Models and managers used by this class
require_once 'Customer.class.php';

class CustomerManager {

    /**
     * get a Customer by id
     * @param int $iCustomerId
     * @return Customer
     */
    public static function getCustomerById($iCustomerId) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `customers`
                    WHERE
                        `customerId` = ' . db_int($iCustomerId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "Customer");
    }

    /**
     * get a Customer by email
     * @param string $sEmail
     * @return Customer 
     */
    public static function getCustomerByEmail($sEmail) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `customers`
                    WHERE
                        `email` = ' . db_str($sEmail) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "Customer");
    }

    /**
     * get a Customer by confirmCode and Email
     * @param string $sConfirmCode
     * @param string $sEmail
     * @return Customer 
     */
    public static function getCustomerByConfirmCodeAndEmail($sConfirmCode, $sEmail) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `customers`
                    WHERE
                        `confirmCode` = ' . db_str($sConfirmCode) . '
                    AND
                        `email` = ' . db_str($sEmail) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "Customer");
    }

    /**
     * get all Customer objects
     * @return array Customer
     */
    public static function getAllCustomers() {
        $sQuery = ' SELECT
                        *
                    FROM
                        `customers`
                    ORDER BY
                        `created` DESC
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_OBJECT, "Customer");
    }

    /**
     * return customers filtered by a few options
     * @param array $aFilter filter properties (q)
     * @param int $iLimit limit number of records returned
     * @param int $iStart start from this record
     * @param int $iFoundRows foundRows when there was no limit (default = false so doesn't check by default)
     * @param array $aOrderBy array(database coloumn name => order) add order by columns and orders
     * @return array Customer objects 
     */
    public static function getCustomersByFilter(array $aFilter = array(), $iLimit = null, $iStart = 0, &$iFoundRows = false, $aOrderBy = array('`c`.`firstName`' => 'ASC', '`c`.`lastName`' => 'ASC')) {

        $sWhere = '';
        $sFrom = '';
        # search for q
        if (!empty($aFilter['name'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '(`c`.`firstName` LIKE ' . db_str('%' . $aFilter['name'] . '%') . ' OR `c`.`lastName` LIKE ' . db_str('%' . $aFilter['name'] . '%') . ')';
        }

        # handle order by
        $sOrderBy = '';
        if (count($aOrderBy) > 0) {
            foreach ($aOrderBy AS $sColumn => $sOrder) {
                $sOrderBy .= ($sOrderBy !== '' ? ',' : '') . $sColumn . ' ' . $sOrder;
            }
        }
        $sOrderBy = ($sOrderBy !== '' ? 'ORDER BY ' : '') . $sOrderBy;

        # handle start,limit
        $sLimit = '';
        if (is_numeric($iLimit)) {
            $sLimit .= db_int($iLimit);
        }
        if ($sLimit !== '') {
            $sLimit = (is_numeric($iStart) ? db_int($iStart) . ',' : '0,') . $sLimit;
        }
        $sLimit = ($sLimit !== '' ? 'LIMIT ' : '') . $sLimit;

        $sQuery = ' SELECT SQL_CALC_FOUND_ROWS
                        `c`.*
                    FROM
                        `customers` AS `c`
                    ' . $sFrom . '
                    ' . ($sWhere != '' ? 'WHERE ' . $sWhere : '') . '
                    ' . $sOrderBy . '
                    ' . $sLimit . '
                    ;';

        $oDb = DBConnections::get();
        $aProducts = $oDb->query($sQuery, QRY_OBJECT, "Customer");
        if ($iFoundRows !== false) {
            $iFoundRows = $oDb->query('SELECT FOUND_ROWS() AS `found_rows`;', QRY_UNIQUE_OBJECT)->found_rows;
        }
        return $aProducts;
    }

    /**
     * check if the email address exists
     * @param string $sEmail
     * @param int $iCustomerId
     * @return bool 
     */
    public static function emailExists($sEmail, $iCustomerId = null) {
        $oCustomer = self::getCustomerByEmail($sEmail);
        if (!empty($oCustomer)) {
            if ($iCustomerId === null || $oCustomer->customerId != $iCustomerId) {
                return true;
            }
        }
        return false;
    }

    /**
     * save a Customer
     * @param Customer $oCustomer
     */
    public static function saveCustomer(Customer $oCustomer) {
        $sQuery = ' INSERT INTO `customers`(
                        `customerId`,
                        `companyName`,
                        `gender`,
                        `firstName`,
                        `insertion`,
                        `lastName`,
                        `address`,
                        `houseNumber`,
                        `houseNumberAddition`,
                        `postalCode`,
                        `city`,
                        `phone`,
                        `mobilePhone`,
                        `email`,
                        `password`,
                        `confirmCode`,
                        `online`,
                        `created`
                    )
                    VALUES (
                        ' . db_int($oCustomer->customerId) . ',
                        ' . db_str($oCustomer->companyName) . ',
                        ' . db_str($oCustomer->gender) . ',
                        ' . db_str($oCustomer->firstName) . ',
                        ' . db_str($oCustomer->insertion) . ',
                        ' . db_str($oCustomer->lastName) . ',
                        ' . db_str($oCustomer->address) . ',
                        ' . db_int($oCustomer->houseNumber) . ',
                        ' . db_str($oCustomer->houseNumberAddition) . ',
                        ' . db_str($oCustomer->postalCode) . ',
                        ' . db_str($oCustomer->city) . ',
                        ' . db_str($oCustomer->phone) . ',
                        ' . db_str($oCustomer->mobilePhone) . ',
                        ' . db_str($oCustomer->email) . ',
                        ' . db_str($oCustomer->password) . ',
                        ' . db_str($oCustomer->confirmCode) . ',
                        ' . db_int($oCustomer->online) . ',
                        NOW()
                    )
                    ON DUPLICATE KEY UPDATE
                        `companyName`=VALUES(`companyName`),
                        `gender`=VALUES(`gender`),
                        `firstName`=VALUES(`firstName`),
                        `insertion`=VALUES(`insertion`),
                        `lastName`=VALUES(`lastName`),
                        `address`=VALUES(`address`),
                        `houseNumber`=VALUES(`houseNumber`),
                        `houseNumberAddition`=VALUES(`houseNumberAddition`),
                        `postalCode`=VALUES(`postalCode`),
                        `city`=VALUES(`city`),
                        `phone`=VALUES(`phone`),
                        `mobilePhone`=VALUES(`mobilePhone`),
                        `email`=VALUES(`email`),
                        `password`=VALUES(`password`),
                        `confirmCode`=VALUES(`confirmCode`),
                        `online`=VALUES(`online`)
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);

        if ($oCustomer->customerId === null)
            $oCustomer->customerId = $oDb->insert_id;
    }

    /**
     * update online status of Customer by id
     * @param int $bOnline
     * @param int $iCustomerId
     * @return bool
     */
    public static function updateOnlineByCustomerId($bOnline, $iCustomerId) {
        $sQuery = ' UPDATE
                        `customers`
                    SET
                        `online` = ' . db_int($bOnline) . '
                    WHERE
                        `customerId` = ' . db_int($iCustomerId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);

        # check if something happened
        return $oDb->affected_rows > 0;
    }

    /**
     * confirm a Customer by confirmCode and email (make him online)
     * @param string $sConfirmCode
     * @param string $sEmail
     * @return bool 
     */
    public static function confirmCustomerByConfirmCodeAndEmail($sConfirmCode, $sEmail) {
        # get the Customer by confirmCode
        $oCustomer = self::getCustomerByConfirmCodeAndEmail($sConfirmCode, $sEmail);

        # check if Customer exists
        if (empty($oCustomer)) {
            return false;
        } else {
            # update the Customer
            $sQuery = ' UPDATE
                            `customers`
                        SET
                            `confirmCode` = NULL,
                            `online` = 1
                        WHERE
                            `customerId` = ' . db_int($oCustomer->customerId) . '
                        LIMIT 1
                        ;';

            $oDb = DBConnections::get();
            $oDb->query($sQuery, QRY_NORESULT);

            self::updateLastLoginByCustomerId($oCustomer->customerId); //update last login date and time
            self::setCustomerInSession($oCustomer); // set Customer in session
            # check if something happened
            return $oDb->affected_rows > 0;
        }
    }

    /**
     * delete a Customer
     * @param Customer $oCustomer
     * @return bool true
     */
    public static function deleteCustomer(Customer $oCustomer) {
        $sQuery = ' DELETE FROM
                        `customers`
                    WHERE
                        `customerId` = ' . db_int($oCustomer->customerId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);
        return true;
    }

    /**
     * update last login timestamp and reset the confirmCode
     * @param int $iCustomerId
     */
    public static function updateLastLoginByCustomerId($iCustomerId) {
        $sQuery = ' UPDATE
                        `customers`
                    SET
                        `confirmCode` = NULL,
                        `lastLogin` = NOW(),
                        `modified` = `modified`
                    WHERE
                        `customerId` = ' . db_int($iCustomerId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);
    }

    /**
     * set Customer in session
     * @param Customer $oCustomer 
     */
    public static function setCustomerInSession(Customer $oCustomer) {
        $oCustomer->maskPass(); // mask pass XXX for session
        $_SESSION['oCurrentCustomer'] = $oCustomer; // set Customer in session
    }

    /**
     * get Customer by email and pass and set in session
     * @param string $sEmail
     * @param string $sPassword
     * @return mixed Customer/false
     */
    public static function login($sEmail, $sPassword) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `customers`
                    WHERE
                        `email` = ' . db_str($sEmail) . ' AND
                        `password` = ' . db_str(hashPasswordForDb($sPassword)) . ' AND
                        `online` = 1
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        $oCustomer = $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "Customer");

        if (!empty($oCustomer)) {
            self::updateLastLoginByCustomerId($oCustomer->customerId); //update last login date and time
            self::setCustomerInSession($oCustomer); // set Customer in session

            return $oCustomer;
        }

        # no Customer found return false
        return false;
    }

    /**
     * update a Customer's password and then login
     * @param Customer $oCustomer
     * @return mixed Customer/false
     */
    public static function updatePasswordAndLogin(Customer $oCustomer) {
        # update the Customer's password
        $sQuery = ' UPDATE
                        `customers`
                    SET
                        `password` = ' . db_str($oCustomer->password) . ',
                        `confirmCode` = NULL,
                        `lastLogin` = NOW()
                    WHERE
                        `customerId` = ' . db_int($oCustomer->customerId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);

        # login
        self::setCustomerInSession($oCustomer); // set Customer in session
    }

    /**
     * logout Customer
     * @param string $sRedirectLocation (redirect location)
     */
    public static function logout($sRedirectLocation) {
        unset($_SESSION['oCurrentCustomer']);
        http_redirect($sRedirectLocation); //go to redirect location
    }

}

?>