<?

/* Models and managers used by the AgendaManager model */
require_once 'AgendaItem.class.php';

class AgendaItemManager {

    /**
     * get a Agenda by agendaId
     * @param int $iAgendaItemId
     * @return Agenda
     */
    public static function getAgendaItemById($iAgendaItemId) {

        $sQuery = " SELECT
                        `a`.*
                    FROM
                        `agendaItems` AS `a`
                    WHERE `a`.`agendaItemId` = " . db_int($iAgendaItemId) . "
                ;";

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "AgendaItem");
    }

    /**
     * save Agenda object
     * @param Agenda $oAgendaItem
     */
    public static function saveAgenda(AgendaItem $oAgendaItem) {
        $oDb = DBConnections::get();
        /* new Agenda */
        if (!$oAgendaItem->agendaItemId) {
            $sQuery = " INSERT INTO
                            `agendaItems`
                            (
                                `windowTitle`,
                                `metaKeywords`,
                                `metaDescription`,
                                `title`,
                                `intro`,
                                `content`,
                                `shortTitle`,
                                `dateTimeFrom`,
                                `dateTimeTo`,
                                `allDay`,
                                `online`,
                                `created`,
                                `modified`    
                            )
                            VALUES
                            ("
                    . db_str($oAgendaItem->windowTitle) . ", "
                    . db_str($oAgendaItem->metaKeywords) . ", "
                    . db_str($oAgendaItem->metaDescription) . ", "
                    . db_str($oAgendaItem->title) . ", "
                    . db_str($oAgendaItem->intro) . ", "
                    . db_str($oAgendaItem->content) . ", "
                    . db_str($oAgendaItem->shortTitle) . ", "
                    . db_date($oAgendaItem->dateTimeFrom) . ", "
                    . db_date($oAgendaItem->dateTimeTo) . ", "
                    . db_str($oAgendaItem->allDay) . ", "
                    . db_str($oAgendaItem->online) . ", "
                    . db_date($oAgendaItem->created) . ", "
                    . db_date($oAgendaItem->modified) . "
                            )
                        ;";
            $oDb->query($sQuery, QRY_NORESULT);

            /* get new AgendaId */
            $oAgendaItem->agendaItemId = $oDb->insert_id;
        } else {
            $sQuery = " UPDATE
                            `agendaItems`
                        SET
                                `windowTitle` = " . db_str($oAgendaItem->windowTitle) . ",
                                `metaKeywords` = " . db_str($oAgendaItem->metaKeywords) . ",
                                `metaDescription` = " . db_str($oAgendaItem->metaDescription) . ",
                                `title` = " . db_str($oAgendaItem->title) . ",
                                `intro` = " . db_str($oAgendaItem->intro) . ",
                                `content` = " . db_str($oAgendaItem->content) . ",
                                `shortTitle` = " . db_str($oAgendaItem->shortTitle) . ",
                                `dateTimeFrom` = " . db_date($oAgendaItem->dateTimeFrom) . ",
                                `dateTimeTo` = " . db_date($oAgendaItem->dateTimeTo) . ",
                                `allDay` = " . db_str($oAgendaItem->allDay) . ",
                                `online` = " . db_str($oAgendaItem->online) . ",
                                `created` = " . db_date($oAgendaItem->created) . ",
                                `modified` = " . db_date($oAgendaItem->modified) . "
                        WHERE
                            `agendaItemId` = " . db_int($oAgendaItem->agendaItemId) . "
                        ;";
            $oDb->query($sQuery, QRY_NORESULT);
        }
    }

    /**
     * delete Agenda
     * @param Agenda $oAgendaItem
     * @return boolean
     */
    public static function deleteAgenda(AgendaItem $oAgendaItem) {

        $oDb = DBConnections::get();
        # get and delete images
        foreach ($oAgendaItem->getImages('all') AS $oImage) {
            ImageManager::deleteImage($oImage);
        }

        $sQuery = "DELETE FROM `agendaItems` WHERE `agendaItemId` = " . db_int($oAgendaItem->agendaItemId) . ";";
        $oDb->query($sQuery, QRY_NORESULT);

        return true;
    }

    /**
     * save connection between a agendaItem and an image
     * @param int $iagendaItemId
     * @param int $iImageId
     */
    public static function saveAgendaItemImageRelation($iagendaItemId, $iImageId) {
        $sQuery = ' INSERT IGNORE INTO
                        `agendaItemsImages`
                    (
                        `agendaItemId`,
                        `imageId`
                    )
                    VALUES
                    (
                        ' . db_int($iagendaItemId) . ',
                        ' . db_int($iImageId) . '
                    )
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);
    }

    /**
     * get images for agendaItem by filter
     * @param int $iAgendaItemId
     * @param array $aFilter
     * @param int $iLimit
     * @return array Image
     */
    public static function getImagesByFilter($iAgendaItemId, array $aFilter = array(), $iLimit = null) {
        $sWhere = '';
        if (empty($aFilter['showAll'])) {
            $sWhere .= ' AND `i`.`online` = 1';
        }

        $sQuery = ' SELECT
                        `i`.*
                    FROM
                        `images` AS `i`
                    JOIN
                        `agendaItemsImages` AS `pi` USING (`imageId`)
                    WHERE
                        `pi`.`agendaItemId` = ' . db_int($iAgendaItemId) . '
                    ' . $sWhere . '
                    ORDER BY
                        `i`.`order` ASC, `i`.`imageId` ASC
                    ' . ($iLimit ? 'LIMIT ' . db_int($iLimit) : '') . '
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_OBJECT, 'Image');
    }

    /**
     * update online by Agenda item id
     * @param int $bOnline
     * @param int $iAgendaItemId
     * @return boolean
     */
    public static function updateOnlineByAgendaItemId($bOnline, $iAgendaItemId) {
        $sQuery = ' UPDATE
                        `agendaItems`
                    SET
                        `online` = ' . db_int($bOnline) . '
                    WHERE
                        `agendaItemId` = ' . db_int($iAgendaItemId) . '
                    ;';
        $oDb = DBConnections::get();

        $oDb->query($sQuery, QRY_NORESULT);

        # check if something happened
        return $oDb->affected_rows > 0;
    }

    /**
     * return Agenda items filtered by a few options
     * @param array $aFilter filter properties (checkOnline)
     * @param int $iLimit limit number of records returned
     * @param int $iStart start from this record
     * @param int $iFoundRows foundRows when there was no limit (default = false so doesn't check by default)
     * @param array $aOrderBy array(database coloumn name => order) add order by columns and orders
     * @return array AgendaItem 
     */
    public static function getAgendaItemsByFilter(array $aFilter = array(), $iLimit = null, $iStart = 0, &$iFoundRows = false, $aOrderBy = array('`ai`.`dateTimeFrom`' => 'DESC', '`ai`.`agendaItemId`' => 'DESC')) {
        $sFrom = '';
        $sWhere = '';
        $sGroupBy = '';

        // no show all? only show "valid" items
        if (empty($aFilter['showAll'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '
                            `ai`.`online` = 1
                        ';
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . 'IF(`ai`.`allDay`, CONCAT(DATE(`ai`.`dateTimeFrom`), \' 23:59:59\'), IFNULL(`ai`.`dateTimeTo`,NOW())) >= NOW()';
        }
        
        # get agendaitems with that changed last hour
        if (isset($aFilter['lastHourOnly'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . 'IFNULL(`ai`.`modified`, `ai`.`created`) > DATE_ADD(NOW(), INTERVAL -1 HOUR)';
        }
        
        # search for query
        if (!empty($aFilter['q'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '(`ai`.`title` LIKE ' . db_str('%' . $aFilter['q'] . '%') . ' OR `ai`.`content` LIKE ' . db_str('%' . $aFilter['q'] . '%') . ' OR `ai`.`intro` LIKE ' . db_str('%' . $aFilter['q'] . '%') . ')';
        }

        // Front-end - Searching for NO OUT OF DATE Agenda items in an specific date
        if (isset($aFilter['outOfDate']) && !$aFilter['outOfDate']) {
            
        }

        // Front-end - Searching for OUT OF DATE Agenda items in an specific date
        if (isset($aFilter['outOfDate']) && $aFilter['outOfDate']) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . 'NOT (IF(`ai`.`allDay`, CONCAT(DATE(`ai`.`dateTimeFrom`), \' 23:59:59\'), IFNULL(`ai`.`dateTimeTo`,NOW())) >= NOW())'
                    . ' AND `ai`.`online` = 1';
        }

        // Admin side - Date From and Date to
        if (!empty($aFilter['dateFrom']) || !empty($aFilter['dateTo'])) {
            if (empty($aFilter['dateFrom'])) {
                $aFilter['dateFrom'] = '0001-01-01';
            }
            if (empty($aFilter['dateTo'])) {
                $aFilter['dateTo'] = '9999-12-31';
            }

            $sWhere .= ($sWhere != '' ? ' AND ' : '') .
                    '(' .
                    ' `ai`.`dateTimeFrom` <= ' . db_str(Date::strToDate($aFilter['dateTo'])->format('%Y-%m-%d 23:59:59')) .
                    ' AND IF(`ai`.`allDay`, CONCAT(DATE(`ai`.`dateTimeFrom`), \' 23:59:59\'), IFNULL(`ai`.`dateTimeTo`,' . db_str(Date::strToDate($aFilter['dateTo'])->format('%Y-%m-%d 23:59:59')) . ')) >= ' . db_str(Date::strToDate($aFilter['dateFrom'])->format('%Y-%m-%d')) .
                    ')';
        }

        # handle order by
        $sOrderBy = '';
        if (count($aOrderBy) > 0) {
            foreach ($aOrderBy AS $sColumn => $sOrder) {
                $sOrderBy .= ($sOrderBy !== '' ? ',' : '') . $sColumn . ' ' . $sOrder;
            }
        }
        $sOrderBy = ($sOrderBy !== '' ? 'ORDER BY ' : '') . $sOrderBy;

        # handle start,limit
        $sLimit = '';
        if (is_numeric($iLimit)) {
            $sLimit .= db_int($iLimit);
        }
        if ($sLimit !== '') {
            $sLimit = (is_numeric($iStart) ? db_int($iStart) . ',' : '0,') . $sLimit;
        }
        $sLimit = ($sLimit !== '' ? 'LIMIT ' : '') . $sLimit;

        $sQuery = ' SELECT ' . ($iFoundRows !== false ? 'SQL_CALC_FOUND_ROWS' : '') . '
                        `ai`.*
                    FROM
                        `agendaItems` AS `ai`
                    ' . $sFrom . '
                    ' . ($sWhere != '' ? 'WHERE ' . $sWhere : '') . '
                    ' . ($sGroupBy != '' ? 'GROUP BY ' . $sGroupBy : '') . '
                    ' . $sOrderBy . '
                    ' . $sLimit . '
                    ;';

        $oDb = DBConnections::get();
        $aAgendaItems = $oDb->query($sQuery, QRY_OBJECT, "AgendaItem");
        if ($iFoundRows !== false) {
            $iFoundRows = $oDb->query('SELECT FOUND_ROWS() AS `found_rows`;', QRY_UNIQUE_OBJECT)->found_rows;
        }

        return $aAgendaItems;
    }

    /**
     * search within Agenda item objects
     * @param string $sQ search query
     * @param int &$iFoundRows reference for the number of total items without the LIMIT
     * @return array Page
     */
    public static function searchAgendaItems($sQ, &$iFoundRows = 0) {
        # trim the search query first
        $sQ = trim($sQ);

        # check if the search query is empty
        if (empty($sQ))
            return array();

        # explode the search query to split it into words
        $aQ = explode(' ', $sQ);

        # check if the search query is empty
        if (empty($aQ) || empty($aQ[0]))
            return array();

        # define the WHERE part
        $sWherePart = "";
        foreach ($aQ as $sQPart) {
            $sWherePart .= "    AND
                                (
                                    `ai`.`title` LIKE " . db_str("%" . $sQPart . "%") . " OR 
                                    `ai`.`content` LIKE " . db_str("%" . $sQPart . "%") . " 
                                ) ";
}

        # define the query
        $sQuery = " SELECT SQL_CALC_FOUND_ROWS
                        `ai`.*
                    FROM
                        `agendaItems` `ai`
                    WHERE
                        `ai`.`online` = 1
                        " . $sWherePart . "
                    ORDER BY
                        CASE
                            WHEN `ai`.`title` LIKE " . db_str($sQ . "%") . " THEN 0
                            WHEN `ai`.`content` LIKE " . db_str("%" . $sQ . "%") . " THEN 1
                            ELSE 99
                        END,
                        `ai`.`title` ASC
                    ;";

        $oDb = DBConnections::get();

        # get the objects
        $aAgendaItems = $oDb->query($sQuery, QRY_OBJECT, "AgendaItem");

        # get the total number of items
        $sQuery = " SELECT FOUND_ROWS() 'count'";
        $aCount = $oDb->query($sQuery, QRY_UNIQUE_ARRAY);
        $iFoundRows = $aCount['count'];

        # return the objects
        return $aAgendaItems;
    }

}

?>