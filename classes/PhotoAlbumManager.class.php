<?php

if (!BB_WITH_PHOTO_ABLUMS) {
    die('Photo albums are inactive!!');
}

/* Models and managers used by the PhotoAlbum model */
require_once 'PhotoAlbum.class.php';
require_once 'Image.class.php';

class PhotoAlbumManager {

    /**
     * get the full PhotoAlbum object by id
     * @param int $iPhotoAlbumId
     * @return PhotoAlbum
     */
    public static function getPhotoAlbumById($iPhotoAlbumId) {
        $sQuery = ' SELECT
                        `pa`.*
                    FROM
                        `photoAlbums` AS `pa`
                    WHERE
                        `pa`.`photoAlbumId` = ' . db_int($iPhotoAlbumId) . '
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "PhotoAlbum");
    }

    /**
     * get the full PhotoAlbum object by imageId
     * @param int $iImageId
     * @return PhotoAlbum
     */
    public static function getPhotoAlbumByImageId($iImageId) {
        $sQuery = ' SELECT
                        `pa`.*
                    FROM
                        `photoAlbums` AS `pa`
                    JOIN
                        `photoAlbumsImages` AS `pai` USING(`photoAlbumId`)
                    WHERE
                        `pai`.`imageId` = ' . db_int($iImageId) . '
                    GROUP BY `pa`.`photoAlbumId`
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "PhotoAlbum");
    }

    /**
     * save PhotoAlbum object
     * @param PhotoAlbum $oPhotoAlbum
     */
    public static function savePhotoAlbum(PhotoAlbum $oPhotoAlbum) {

        # set folderName on insert
        if ($oPhotoAlbum->photoAlbumId === null) {

            # set folderName
            $oPhotoAlbum->folderName = '/' . prettyUrlPart($oPhotoAlbum->title);

            # make unique folderName
            $iT = 1; //counter for making unique addition
            $sUnique = ''; //addition for making folderName unique
            while (file_exists(DOCUMENT_ROOT . PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . $sUnique)) {
                $sUnique = '(' . $iT . ')';
                $iT++;
            }
            # set folderName again
            $oPhotoAlbum->folderName .= $sUnique;
        }

        # save PhotoAlbum
        $sQuery = ' INSERT INTO `photoAlbums` (
                        `photoAlbumId`,
                        `windowTitle`,
                        `metaKeywords`,
                        `metaDescription`,
                        `title`,
                        `content`,
                        `shortTitle`,
                        `date`,
                        `online`,
                        `folderName`,
                        `created`
                    ) 
                    VALUES (
                        ' . db_int($oPhotoAlbum->photoAlbumId) . ',
                        ' . db_str($oPhotoAlbum->windowTitle) . ',
                        ' . db_str($oPhotoAlbum->metaKeywords) . ',
                        ' . db_str($oPhotoAlbum->metaDescription) . ',
                        ' . db_str($oPhotoAlbum->title) . ',
                        ' . db_str($oPhotoAlbum->content) . ',
                        ' . db_str($oPhotoAlbum->shortTitle) . ',
                        ' . db_date($oPhotoAlbum->date) . ',
                        ' . db_int($oPhotoAlbum->online) . ',
                        ' . db_str($oPhotoAlbum->folderName) . ',
                        ' . 'NOW()' . '
                    )
                    ON DUPLICATE KEY UPDATE
                        `windowTitle`=VALUES(`windowTitle`),
                        `metaKeywords`=VALUES(`metaKeywords`),
                        `metaDescription`=VALUES(`metaDescription`),
                        `title`=VALUES(`title`),
                        `content`=VALUES(`content`),
                        `shortTitle`=VALUES(`shortTitle`),
                        `date`=VALUES(`date`),
                        `online`=VALUES(`online`)
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);

        # new one, do some things
        if ($oPhotoAlbum->photoAlbumId === null) {
            $oPhotoAlbum->photoAlbumId = $oDb->insert_id;

            # make directories
            mkdir(DOCUMENT_ROOT . PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/');
            mkdir(DOCUMENT_ROOT . PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/original/');
            mkdir(DOCUMENT_ROOT . PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/detail/');
            mkdir(DOCUMENT_ROOT . PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/cms_thumb/');
            mkdir(DOCUMENT_ROOT . PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/crop_small/');
        }
    }

    /**
     * delete PhotoAlbum and all media
     * @param PhotoAlbum $oPhotoAlbum
     * @return boolean
     */
    public static function deletePhotoAlbum($oPhotoAlbum) {

        $oDb = DBConnections::get();

        /* check if news item exists and is deletable */
        if ($oPhotoAlbum) {
            if ($oPhotoAlbum->isDeletable()) {

                # get and delete images
                foreach ($oPhotoAlbum->getImages('all') AS $oImage) {
                    ImageManager::deleteImage($oImage);
                }

                $sQuery = "DELETE FROM `photoAlbums` WHERE `photoAlbumId` = " . db_int($oPhotoAlbum->photoAlbumId) . ";";
                $oDb->query($sQuery, QRY_NORESULT);

                # remove album folder
                if (!empty($oPhotoAlbum->folderName)) {
                    emptyAndRemoveDir(DOCUMENT_ROOT . PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/', true);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * update online by photoAlbumId
     * @param int $bOnline
     * @param int $iPhotoAlbumId
     * @return boolean
     */
    public static function updateOnlineByPhotoAlbumId($bOnline, $iPhotoAlbumId) {
        $sQuery = ' UPDATE
                        `photoAlbums`
                    SET
                        `online` = ' . db_int($bOnline) . '
                    WHERE
                        `photoAlbumId` = ' . db_int($iPhotoAlbumId) . '
                    ;';
        $oDb = DBConnections::get();

        $oDb->query($sQuery, QRY_NORESULT);

        # check if something happened
        return $oDb->affected_rows > 0;
    }

    /**
     * return photo albums filtered by a few options
     * @param array $aFilter filter properties (checkOnline)
     * @param int $iLimit limit number of records returned
     * @param int $iStart start from this record
     * @param int $iFoundRows foundRows when there was no limit (default = false so doesn't check by default)
     * @param array $aOrderBy array(database coloumn name => order) add order by columns and orders
     * @return array PhotoAlbum 
     */
    public static function getPhotoAlbumsByFilter(array $aFilter = array(), $iLimit = null, $iStart = 0, &$iFoundRows = false, $aOrderBy = array('`pa`.`date`' => 'DESC', '`pa`.`photoAlbumId`' => 'DESC')) {

        $sFrom = '';
        $sWhere = '';
        # check if photoalbum is online and has images
        if (empty($aFilter['showAll'])) {
            $sWhere = ($sWhere != '' ? ' AND ' : '') . '`pa`.`online` = 1';
            $sFrom .= 'JOIN `photoAlbumsImages` AS `pai` ON `pai`.`photoAlbumId` = `pa`.`photoAlbumId`';
        }

        # get photo albums that changed last hour
        if (isset($aFilter['lastHourOnly'])) {
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . 'IFNULL(`pa`.`modified`, `pa`.`created`) > DATE_ADD(NOW(), INTERVAL -1 HOUR)';
        }

        # handle order by
        $sOrderBy = '';
        if (count($aOrderBy) > 0) {
            foreach ($aOrderBy AS $sColumn => $sOrder) {
                $sOrderBy .= ($sOrderBy !== '' ? ',' : '') . $sColumn . ' ' . $sOrder;
            }
        }
        $sOrderBy = ($sOrderBy !== '' ? 'ORDER BY ' : '') . $sOrderBy;

        # handle start,limit
        $sLimit = '';
        if (is_numeric($iLimit)) {
            $sLimit .= db_int($iLimit);
        }
        if ($sLimit !== '') {
            $sLimit = (is_numeric($iStart) ? db_int($iStart) . ',' : '0,') . $sLimit;
        }
        $sLimit = ($sLimit !== '' ? 'LIMIT ' : '') . $sLimit;

        $sQuery = ' SELECT SQL_CALC_FOUND_ROWS
                        `pa`.*
                    FROM
                        `photoAlbums` AS `pa`
                    ' . $sFrom . '
                    ' . ($sWhere != '' ? 'WHERE ' . $sWhere : '') . '
                    GROUP BY
                        `pa`.`photoAlbumId`
                    ' . $sOrderBy . '
                    ' . $sLimit . '
                    ;';

        $oDb = DBConnections::get();
        $aPhotoAlbums = $oDb->query($sQuery, QRY_OBJECT, "PhotoAlbum");
        if ($iFoundRows !== false) {
            $iFoundRows = $oDb->query('SELECT FOUND_ROWS() AS `found_rows`;', QRY_UNIQUE_OBJECT)->found_rows;
        }
        return $aPhotoAlbums;
    }

    /**
     * save connection between a photo album and an image
     * @param int $iPhotoAlbumId
     * @param int $iImageId 
     */
    public static function savePhotoAlbumImageRelation($iPhotoAlbumId, $iImageId) {
        $sQuery = ' INSERT IGNORE INTO
                        `photoAlbumsImages`
                    (
                        `photoAlbumId`,
                        `imageId`
                    )
                    VALUES
                    (
                        ' . db_int($iPhotoAlbumId) . ',
                        ' . db_int($iImageId) . '
                    )
                    ;';
        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);
    }

    /**
     * get images for photo album by filter
     * @param int $iPhotoAlbumId
     * @param array $aFilter
     * @param int $iLimit
     * @return array Image
     */
    public static function getImagesByFilter($iPhotoAlbumId, array $aFilter = array(), $iLimit = null) {
        $sWhere = '';
        $sOrderBy = '';
        if (empty($aFilter['showAll'])) {
            $sWhere .= ' AND `i`.`online` = 1';
        }

        if (!empty($aFilter['coverImage'])) {
            $sOrderBy .= '`i`.`coverImage` DESC, ';
        }

        $sQuery = ' SELECT
                        `i`.*
                    FROM
                        `images` AS `i`
                    JOIN
                        `photoAlbumsImages` AS `pai` USING (`imageId`)
                    WHERE
                        `pai`.`photoAlbumId` = ' . db_int($iPhotoAlbumId) . '
                    ' . $sWhere . '
                    ORDER BY
                    ' . $sOrderBy . '
                        `i`.`order` ASC, `i`.`imageId` ASC
                    ' . ($iLimit ? 'LIMIT ' . db_int($iLimit) : '') . '
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_OBJECT, 'Image');
    }

    /**
     * update image coverImage property for a specific photoablum
     * @param Image $oImage
     * @param PhotoAlbum $oPhotoalbum
     * @return boolean
     */
    public static function updateCoverImageByPhotoAlbum(Image $oImage, PhotoAlbum $oPhotoalbum) {
        $oDb = DBConnections::get();
        $sQuery = ' UPDATE
                        `images` AS `i`
                    JOIN
                        `photoAlbumsImages` AS `pai` USING(`imageId`)
                    SET
                        `i`.`coverImage` = IF(`i`.`imageId` = ' . db_int($oImage->imageId) . ' , 1, 0)
                    WHERE
                        `photoAlbumId` = ' . db_int($oPhotoalbum->photoAlbumId) . '
                    ;';
        $oDb->query($sQuery, QRY_NORESULT);
        return $oDb->affected_rows > 0;
    }

}

?>
