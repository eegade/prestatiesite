<?php

/* Models and managers used by the Tweet model */
require_once 'TweetManager.class.php';

class Tweet {

    public $tweetId = null; // id of the tweet at Twitter
    public $text; // tweet text at Twitter
    public $created_at; // tweet created at Twitter
    public $retweet = 0; // wether or not this tweet is a retweet
    public $userId; // id of the user
    public $name; //user name
    public $screen_name; // screen_name of the user
    public $url; // url of the user
    public $profile_image_url; // profile_image_url of the user
    public $online = 1;
    public $created;
    public $modified;
    
    /**
     * gets the text
     * @param type $bConvertLinks DEFAULT = true (true = convert URLs to links)
     * @return string $sText
     */
    public function getText($bConvertLinks = true) {
        $sText = $this->text;
        if ($bConvertLinks) {
            # Replace URLs with actual clickable links
            $sText = preg_replace("/([\w]+:\/\/([\w-?&;#~=\.\/\@]+[\w\/]))/i", '<a target="_blank" href="$1">$2</a>', $sText);
            $sText = preg_replace("/@(\w+)/", '<a target="_blank" href="http://twitter.com/$1">@$1</a>', $sText);
            $sText = preg_replace("/#(\w+)/", '<a target="_blank" href="http://twitter.com/search/$1">#$1</a>', $sText);
        }
        return $sText;
    }

}

?>
