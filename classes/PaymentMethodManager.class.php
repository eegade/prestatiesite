<?php

// Models and managers used by this class
require_once 'PaymentMethod.class.php';

class PaymentMethodManager {

    /**
     * get a PaymentMethod by id
     * @param int $iPaymentMethodId
     * @return PaymentMethod
     */
    public static function getPaymentMethodById($iPaymentMethodId) {
        $sQuery = ' SELECT
                        *
                    FROM
                        `paymentMethods`
                    WHERE
                        `paymentMethodId` = ' . db_int($iPaymentMethodId) . '
                    LIMIT 1
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_UNIQUE_OBJECT, "PaymentMethod");
    }

    /**
     * return paymentMethods filtered by a few options
     * @param array $aFilter filter properties
     * @param int $iLimit limit number of records returned
     * @param array $aOrderBy array(database coloumn name => order) add order by columns and orders
     * @return array PaymentMethod
     */
    public static function getPaymentMethodsByFilter(array $aFilter = array()) {

        $sFrom = '';
        $sWhere = '';

        // get paymentMethods for deliveryMethod
        if (!empty($aFilter['deliveryMethodId'])) {
            $sFrom .= 'JOIN `deliveryMethodPaymentMethodRelations` AS `dmpmr` USING(`paymentMethodId`)';
            $sWhere .= ($sWhere != '' ? ' AND ' : '') . '`dmpmr`.`deliveryMethodId` = ' . db_int($aFilter['deliveryMethodId']);
        }

        $sQuery = ' SELECT
                        `pm`.*
                    FROM
                        `paymentMethods` AS `pm`
                    ' . $sFrom . '
                    ' . ($sWhere != '' ? 'WHERE ' . $sWhere : '') . '
                    ORDER BY
                        `pm`.`order` ASC, `pm`.`name` ASC
                    ;';

        $oDb = DBConnections::get();
        return $oDb->query($sQuery, QRY_OBJECT, "PaymentMethod");
    }

    /**
     * save a PaymentMethod
     * @param productSize $oProductSize
     */
    public static function savePaymentMethod(PaymentMethod $oPaymentMethod) {
        $sQuery = ' INSERT INTO `paymentMethods`(
                        `paymentMethodId`,
                        `name`,
                        `price`,
                        `isOnlinePaymentMethod`,
                        `redirectPage`,
                        `order`
                    )
                    VALUES (
                        ' . db_int($oPaymentMethod->paymentMethodId) . ',
                        ' . db_str($oPaymentMethod->name) . ',
                        ' . db_str($oPaymentMethod->price) . ',
                        ' . db_int($oPaymentMethod->isOnlinePaymentMethod) . ',
                        ' . db_str($oPaymentMethod->redirectPage) . ',
                        ' . db_int($oPaymentMethod->order) . '
                    )
                    ON DUPLICATE KEY UPDATE
                        `name`=VALUES(`name`),
                        `price`=VALUES(`price`),
                        `isOnlinePaymentMethod`=VALUES(`isOnlinePaymentMethod`),
                        `redirectPage`=VALUES(`redirectPage`),
                        `order`=VALUES(`order`)
                    ;';

        $oDb = DBConnections::get();
        $oDb->query($sQuery, QRY_NORESULT);

        if ($oPaymentMethod->paymentMethodId === null)
            $oPaymentMethod->paymentMethodId = $oDb->insert_id;
    }

    /**
     * delete a PaymentMethod
     * @param PaymentMethod $oPaymentMethod
     * @return bool true
     */
    public static function deletePaymentMethod(PaymentMethod $oPaymentMethod) {
        if ($oPaymentMethod->isDeletable()) {
            # delete object
            $sQuery = ' DELETE FROM
                        `paymentMethods`
                    WHERE
                        `paymentMethodId` = ' . db_int($oPaymentMethod->paymentMethodId) . '
                    LIMIT 1
                    ;';

            $oDb = DBConnections::get();
            $oDb->query($sQuery, QRY_NORESULT);
            return true;
        } else {
            return false;
        }
    }

}

?>
