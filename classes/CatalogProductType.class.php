<?php

// Models and managers used by this class
require_once 'Model.class.php';
require_once 'CatalogProductManager.class.php';

class CatalogProductType extends Model {

    public $catalogProductTypeId = null;
    public $title;
    public $withSizes = 0;
    public $withColors = 0;
    public $withGenders = 0;
    public $order = 99999;
    public $created;
    public $modified;

    /**
     * validate object
     */
    public function validate() {
        if (empty($this->title))
            $this->setPropInvalid('title');
    }

    /**
     * check if producttype is deletable
     * @return boolean
     */
    public function isDeletable() {
        return CatalogProductTypeManager::getNumberOfProductsByProductTypeId($this->catalogProductTypeId) == 0; // check for product relations
    }

}

?>
