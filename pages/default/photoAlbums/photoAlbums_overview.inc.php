<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <article class="photoalbum-overview">
            <div class="head">
                <h1><?= _e($oPage->title) ?></h1>
            </div>
            <?= $oPage->content ?>
            <?php
            $iPhotoAlbumCounter = 0;
            foreach ($aPhotoAlbums AS $oPhotoAlbum) {
                $iPhotoAlbumCounter++;
                if (($oImage = $oPhotoAlbum->getImages('cover'))) {
                    $oImageFile = $oImage->getImageFileByReference('crop_small');
                    $oImageFileDetail = $oImage->getImageFileByReference('detail');
                }
                ?>
                <div class="column-25 float-left">
                    <a href="<?= $oPhotoAlbum->getUrlPath() ?>">
                        <? if (!empty($oImageFile) && !empty($oImageFileDetail)) { ?>
                            <span data-picture data-alt="<?= $oImageFile->title ?>">
                                <span data-src="<?= $oImageFile->link ?>"></span>
                                <span data-src="<?= $oImageFileDetail->link ?>" data-media="(max-width: 767px)"></span>
                            </span>
                        <? } ?>


                        <h3><?= _e($oPhotoAlbum->title) ?></h3>
                    </a>
                    <?= count($oPhotoAlbum->getImages()) ?> foto's
                </div>
                <?
                if ($iPhotoAlbumCounter == 4) {
                    echo '<br class="clear"><br>';
                    $iPhotoAlbumCounter = 0;
                } else {
                    echo '<div class="column-width float-left">&nbsp;</div>';
                }
            }
            ?>
        </article>
        <div class="column-width float-left">&nbsp;</div>
    </section>
</div>