<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <article class="photoalbum-detail">
            <div class="head">
                <h1><?= _e($oPhotoAlbum->title) ?></h1>
            </div>
            <?= $oPhotoAlbum->content ?>
            <?
            $iPhotoAlbumImagesCounter = 0;
            foreach ($oPhotoAlbum->getImages() AS $oImage) {
                $iPhotoAlbumImagesCounter++;
                $oImageFileCrop = $oImage->getImageFileByReference('crop_small');
                $oImageFileOriginal = $oImage->getImageFileByReference('detail');
                if ($oImageFileCrop && $oImageFileOriginal) {
                    ?>
                    <div class="column-25 float-left">
                        <a href="<?= $oImageFileOriginal->link ?>" title="<?= $oImageFileCrop->title ?>" class="fancyBoxLink" data-fancybox-group="gallery">
                            <span data-picture data-alt="<?= $oImageFileCrop->title ?>">
                                <span data-src="<?= $oImageFileCrop->link ?>"></span>
                                <span data-src="<?= $oImageFileOriginal->link ?>" data-media="(max-width: 767px)"></span>
                            </span>
                        </a>
                    </div>
                    <?
                    if ($iPhotoAlbumImagesCounter == 4) {
                        echo '<br class="clear">';
                        $iPhotoAlbumImagesCounter = 0;
                    } else {
                        echo '<div class="column-width float-left">&nbsp;</div>';
                    }
                }
            }
            ?>
        </article>
        <div class="column-width float-left">&nbsp;</div>
    </section>
</div>