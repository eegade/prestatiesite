<!-- BRANDBOX -->

<div class="<?= BB_TYPE === 'wide' ? '' : 'container-brandbox' ?>">
    <? if ($aBrandboxItems) { ?>
        <div id="brandbox-module">
            <div id="slider">
                <?
                foreach ($aBrandboxItems AS $oBrandboxItem) {
                    $oCropLarge = $oBrandboxItem->getImage()->getImageFileByReference('crop_large');
                    $oCropSmall = $oBrandboxItem->getImage()->getImageFileByReference('crop_small');
                    if (empty($oCropLarge) || empty($oCropSmall)) {
                        continue;
                    }
                    ?>
                    <div class="slide">
                        <?= $oBrandboxItem->getLinkLocation() ? '<a target="' . getLinkTarget($oBrandboxItem->getLinkLocation()) . '" href="' . $oBrandboxItem->getLinkLocation() . '">' : '' ?>
                        <span data-picture data-alt="<?= $oCropLarge->title ?>">
                            <span data-src="<?= $oCropLarge->link ?>"></span>
                            <span data-src="<?= $oCropSmall->link ?>" data-media="(max-width: 767px)"></span>
                        </span>
                        <? if ($oBrandboxItem->line1 || $oBrandboxItem->line2) { ?>
                            <div class="brandbox-item-text">
                                <? if ($oBrandboxItem->line1) { ?>
                                    <span class="line"><?= $oBrandboxItem->line1 ?></span><br />
                                <? } ?>
                                <? if ($oBrandboxItem->line2) { ?>
                                    <span class="line secondLine"><?= $oBrandboxItem->line2 ? $oBrandboxItem->line2 : '' ?></span>
                                <? } ?>
                            </div>
                        <? } ?>
                        <?= $oBrandboxItem->getLinkLocation() ? '</a>' : '' ?>
                    </div>
                <? } ?>
            </div>
        </div>
    <? } ?>
</div>
<!-- /BRANDBOX -->

<div class="default-container-width clearfix">
    <!-- CALL ME BACK -->
    <?
    if (BB_WITH_CALL_ME_BACK && Settings::get('showCallMeBackOnHome')) {
        include TEMPLATES_FOLDER . '/elements/callMeBack.inc.php';
    }
    ?>
    <!-- /CALL ME BACK -->

    <!-- TWITTER FEED -->
    <?
    if (BB_WITH_TWITTER_FEED && Settings::get('showTwitterFeedOnHome') && !empty($aTweets)) {
        include TEMPLATES_FOLDER . '/elements/twitterFeed.inc.php';
    }
    ?>
    <!-- /TWITTER FEED -->

    <!-- MAIN CONTENT -->
    <br class="clear">
    <br class="clear">
    <article class="column-75 float-left">
        <h1><?= _e($oPage->title) ?></h1>
        <?= $oPage->content ?>
    </article>
    <!-- /MAIN CONTENT -->

    <!-- IMAGES -->
    <figure class="column-25 float-right">
        <div class="images">
            <?
            if ($oPage->getImages()) {
                foreach ($oPage->getImages() AS $oImage) {
                    $oImageFile = $oImage->getImageFileByReference('crop_small');
                    $oImageFileDetail = $oImage->getImageFileByReference('detail');
                    if (empty($oImageFile) && empty($oImageFileDetail)) {
                        continue;
                    }
                    ?>
                    <a href="<?= $oImageFileDetail->link ?>" class="fancyBoxLink" data-fancybox-group="page"><img src="<?= $oImageFile->link ?>" alt="<?= $oImageFile->title ?>" /></a>
                    <p class="image-title"><?= $oImage->getImageFileByReference('crop_small')->title ?></p>
                    <?
                }
            }
            ?>
        </div>
    </figure>
    <!-- /IMAGES -->

    <?
    // bikes first, include bikes
    if (!empty($aBikes) && Settings::get('showBikesFirst')) {
        include TEMPLATES_FOLDER . '/elements/homepageBikes.inc.php';
    }

    if (!empty($aProducts)) {
        include TEMPLATES_FOLDER . '/elements/homepageProducts.inc.php';
    }

    if (!empty($aBikes) && !Settings::get('showBikesFirst')) {
        include TEMPLATES_FOLDER . '/elements/homepageBikes.inc.php';
    }

    if (!empty($aNewsItems)) {
        include TEMPLATES_FOLDER . '/elements/homepageNewsItems.inc.php';
    }
    
    if (BB_WITH_CONTINUOUS_SLIDER && Settings::get('showSliderOnHome')) {
        include TEMPLATES_FOLDER . '/elements/continuousSlider.inc.php';
    }
    ?>
</div>