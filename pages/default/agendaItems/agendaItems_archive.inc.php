<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">

    <div class="breadcrumbs">
        <p>U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?><p>
    </div>
    <section class="page-content float-left">
        <div class="agendaitems-archive-overview">
            <h1><?= _e($oPage->title) ?></h1>
            <?= $oPage->content ?>
            <?
            if (!empty($aAgendaItems)) {
                foreach ($aAgendaItems AS $oAgendaItem) {
                    ?>
                    <div class="agendaitem">
                        <div class="agendaitem-content">
                            <span class="agendaitem-date-categories">
                                <?= !empty($oAgendaItem->dateTimeTo) ? strftime('%d %B %Y %H:%M:%S', strtotime($oAgendaItem->dateTimeFrom)) : strftime('%d %B %Y', strtotime($oAgendaItem->dateTimeFrom)) ?> - 
                                <?= !empty($oAgendaItem->dateTimeTo) ? strftime('%d %B %Y %H:%M:%S', strtotime($oAgendaItem->dateTimeTo)) : ' ' ?>
                                <a href="<?= $oAgendaItem->getUrlPath() ?>"><?= _e($oAgendaItem->title) ?></a>
                            </span>
                        </div>
                    </div>
                    <?
                }
            }
            ?>
            <div class="cf"><?= generatePaginationHTML($iPageCount, $iCurrPage) ?></div>
            <p id="agendaarchive-link"><a href="<?= '/' . http_get('controller') ?>" class="read-more">Naar het volledige overzicht</a></p>
        </div>
    </section>
</div>