<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <div class="breadcrumbs">
        <p>U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></p>
    </div>

    <article class="column-75 float-left">
        <h1><?= _e($oAgendaItem->title) ?></h1>
        <span class="agendaitem-date-categories">
            <?= empty($oAgendaItem->allDay) ? Date::strToDate($oAgendaItem->dateTimeFrom)->format('%d %B %Y %H:%M:%S') : Date::strToDate($oAgendaItem->dateTimeFrom)->format('%d %B %Y')?>
            <?= !empty($oAgendaItem->dateTimeTo) ? ' - '. Date::strToDate($oAgendaItem->dateTimeTo)->format('%d %B %Y %H:%M:%S') : ''  ?>
        </span>
        <div class="agendaitem-summary">
            <?= $oAgendaItem->content ?>
        </div>
        <a href="<?= $sBackLink ?>" class="read-more">Naar het overzicht</a>
    </article>
    <? include TEMPLATES_FOLDER . '/elements/asideMedia.inc.php'; ?>
</div>