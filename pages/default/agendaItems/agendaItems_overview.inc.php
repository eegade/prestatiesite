<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfixdefault-container-width">
    <div class="breadcrumbs">
        <p>U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></p>
    </div>
    <section class="column-75 page float-left">
        <h1><?= _e($oPage->title) ?></h1>
        <?= $oPage->content ?>
        <?

        if (!empty($aAgendaItems)) {
            
            foreach ($aAgendaItems as $oAgendaItem) {
                $oImage = $oAgendaItem->getImages('first-online');
                if (!empty($oImage)) {
                    $oImageFile = ImageManager::getImageFileByImageAndReference($oImage->imageId, 'crop_small');
                } else {
                    $oImageFile = null;
                }
                ?>
                <div class="agendaitem-block-container cf">
                    <div class="column-75 agendaitem-block float-left">
                        <a href="<?= $oAgendaItem->getUrlPath() ?>" class="agendaitem-title"><?= _e($oAgendaItem->title) ?></a>
                        <?= $oAgendaItem->intro ?>
                        <span class="agendaitem-date-categories">
                            <?= empty($oAgendaItem->allDay) ? Date::strToDate($oAgendaItem->dateTimeFrom)->format('%d %B %Y %H:%M:%S') : Date::strToDate($oAgendaItem->dateTimeFrom)->format('%d %B %Y')?>
                            <?= !empty($oAgendaItem->dateTimeTo) ? ' - '. Date::strToDate($oAgendaItem->dateTimeTo)->format('%d %B %Y %H:%M:%S'): ''  ?>
                        </span>
                        <a href="<?= $oAgendaItem->getUrlPath() ?>" class="read-more">Lees meer</a>
                    </div>
                    
                    <div class="column-25 float-right images">
                        <?= !empty($oImageFile) ? '<a href="' . $oAgendaItem->getUrlPath() . '"><img src="' . $oImageFile->link . '" alt="' . _e($oImageFile->title) . '" title="' . _e($oAgendaItem->title) . '" /></a>' : '' ?>
                    </div>
                </div>
                <?
            }
        }
        ?>
        <div class="cf"><?= generatePaginationHTML($iPageCount, $iCurrPage) ?></div>
        <p id="newsarchive-link"><a href="/agenda/agendaarchief" class="read-more">Naar het agenda archief</a></p>
    </section>
</div>

