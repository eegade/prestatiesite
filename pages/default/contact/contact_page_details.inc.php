<?= BB_TYPE != 'wide' ? '<div class="default-container-width clearfix">' : '' ?>
<div class="google-maps">
    <div id="map_canvas" style="width: 100%; height: <?= BB_CONTACT_MAPS_H ?>px;" class="no-print"></div>
</div>
<?= BB_TYPE != 'wide' ? '</div>' : '' ?>
<div class="default-container-width clearfix">
    <div class="breadcrumbs">
        <p>U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?><p>
    </div>
    <article>
        <h1 style="margin-bottom: 29px;"><?= _e($oPage->title) ?></h1>
        <?= $oPage->content ?>
    </article>
</div>
<?
// measure conversion if page is /contact/bedankt
if (http_get('param1') == 'bedankt' && http_get('conversion') == 1) {
    ?>
    <? //google or other conversion javascript stuff here ?>
<? } ?>