<?= BB_TYPE == 'small' ? '<div class="default-container-width clearfix">' : '' ?>

<div class="google-maps">
    <div id="map_canvas" style="width: 100%; height: <?= BB_CONTACT_MAPS_H ?>px;" class="no-print"></div>
</div>

<?= BB_TYPE == 'small' ? '</div>' : '' ?>
<div class="default-container-width clearfix">
    <div class="breadcrumbs">
        <p>U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?><p>
    </div>

    <div class="column-50 float-left">
        <h1 style="margin-bottom: 29px;"><?= _e($oPage->title) ?></h1>
        <?= $oPage->content ?>
    </div>

    <div class="column-50 float-right">
        <p style="font: bold 22px/24px Arial, Helvetica, sans-serif; text-transform: uppercase;">Contactformulier</p>
        <form method="post" id="standardForm" class="withForm contactForm validateForm">
            <? include TEMPLATES_FOLDER . '/elements/errorBox.inc.php'; ?>
            <fieldset>
                <div>
                    <label for="naam">Naam *</label>
                    <input type="text" value="<?= _e(http_post('naam')) ?>" name="naam" id="naam" class="required" title="Uw naam is niet (juist) ingevuld" />
                </div>
                <div>
                    <label for="organisatie">Organisatie</label>
                    <input type="text" value="<?= _e(http_post('organisatie')) ?>" name="organisatie" id="organisatie" />
                </div>
                <div>
                    <label for="telefoon">Telefoon</label>
                    <input type="text" value="<?= _e(http_post('telefoon')) ?>" name="telefoon" id="telefoon" />
                </div>
                <div>
                    <label for="email">E-mail *</label>
                    <input type="text" value="<?= _e(http_post('email')) ?>" name="email" id="email" class="required email" title="Onjuist e-mail adres" />
                </div>
                <div>
                    <label for="onderwerp">Onderwerp</label>
                    <input type="text" value="<?= _e(http_post('onderwerp', http_get('onderwerp'))) ?>" name="onderwerp" id="onderwerp" />
                </div>
                <div>
                    <label for="bericht">Bericht *</label>
                    <textarea name="bericht" id="bericht" class="required" title="U heeft geen bericht ingevuld"><?= _e(http_post('bericht', http_get('bericht'))) ?></textarea>
                </div>
                <div style="display: none;">
                    <div>
                        <label for="rumpelstiltskin-empty">Dit veld niet wijzigen (SPAM check)</label>
                        <input autocomplete="off" type="text" name="rumpelstiltskin-empty" value="" />
                    </div>
                    <div>
                        <label for="rumpelstiltskin-filled">Dit veld niet wijzigen (SPAM check)</label>
                        <input autocomplete="off" type="text" name="rumpelstiltskin-filled" value="repelsteeltje" />
                    </div>
                </div>
            </fieldset>
            <div class="submit">
                <input type="hidden" name="action" value="sendForm" />
                <input type="submit" name="done" value="Verzenden" />
            </div>
        </form>
    </div>
</div> 