<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page bike">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="column-50 float-left">
            <div class="bike-detail-column">
                <div class="bike-detail-title">
                    <h1><?= _e($oBike->title) ?></h1>
                </div>
                <div class="bike-detail-content">
                    <div class="column-33 float-left">
                        <div><b>Prijs:</b></div>
                        <div>€ <?= number_format($oBike->price, 0, ',', '.') ?>,-</div>
                        <div><b>Status:</b></div>
                        <div><?
                            if ($oBike->status == 0) {
                                $oBike->status = 'Nieuw';
                            } else {
                                $oBike->status = 'Tweedehands';
                            }
                            ?>
                            <?= $oBike->status ?></div>
                    </div>
                    <div class="column-width float-left">&nbsp;</div>
                    <div class="column-33 float-left">
                        <div><b>Bouwjaar:</b></div>
                        <div><?= $oBike->manufactureYear ?></div>
                    </div>
                    <div class="column-width float-left">&nbsp;</div>
                    <div class="column-33 float-left">
                        <div><b>Km. stand:</b></div>
                        <div><?= $oBike->km ?></div>
                    </div>
                    <br class="clear"><br>
                    <?= $oBike->description ?>
                    <div style="margin-bottom:10px;"class="hide-on-mobile"><a href="/contact?onderwerp=<?= rawurlencode($oBike->title) ?>" class="read-more">Contact opnemen</a></div>
                    <div class="hide-on-desktop hide-on-tablet"><a href="mailto:<?= _e(Settings::get('clientEmail')) ?>?subject=<?= _e($oBike->title) ?>" class="read-more">Mail ons</a></div>
                    <div style="margin-top:10px; margin-bottom: 10px;" class="hide-on-desktop hide-on-tablet"><a href="tel:<?= _e(Settings::get('clientPhone')) ?>" class="read-more">Bel ons</a></div>
                    <a href="/motoren" class="read-more">Naar motorenoverzicht</a>
                </div>
            </div>
            <br class="clear">
        </div>
        <? if (!empty($oBike->aImages)) { ?>
            <div class="column-50 float-right">
                <? if (!empty($oBike->aImages[0])) { ?>
                    <div class="bike-detail-image">
                        <a data-fancybox-group="motoren" class="fancyBoxLink" href="<?= $oBike->aImages[0]->imageFancyboxImage ?>">
                            <img style="width:100%" alt="<?= $oBike->title ?>" class="" src="<?= $oBike->aImages[0]->imageFancyboxImage ?>"/>
                            <span class="image-zoom"><img src="/images/layout/zoom-icon.png" with="20" height="20" alt="Afbeelding vergroten"></span>
                        </a>
                    </div>
                    <br class="clear">
                    <?
                }
                if (array_slice($oBike->aImages, 1)) {
                    $iBikeCounter = 0;
                    ?>
                    <div class="bike-detail-images">
                        <?
                        foreach (array_slice($oBike->aImages, 1) AS $oImage) {
                            $iBikeCounter++;
                            ?>
                            <div class="column-25 float-left bike-detail-thumb">
                                <a data-fancybox-group="motoren" class="fancyBoxLink" href="<?= $oImage->imageFancyboxImage ?>">
                                    <img style="width:100%;" src="<?= $oImage->imageFancyboxImage ?>" alt="<?= $oBike->title ?>" />
                                    <span class="hide-on-desktop hide-on-tablet image-zoom"><img src="/images/layout/zoom-icon.png" with="20" height="20" alt="Afbeelding vergroten"></span>
                                </a>
                            </div>
                            <?
                            if ($iBikeCounter == 4) {
                                echo '<br class="clear">';
                                $iBikeCounter = 0;
                            } else {
                                echo '<div class="hide-on-mobile column-width float-left">&nbsp;</div>';
                            }
                        }
                        ?>
                    </div>
                <? } ?>
            </div>
        <? } ?>
    </section>
</div>