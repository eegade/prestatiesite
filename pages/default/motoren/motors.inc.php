<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page bike">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="head">
            <h1><?= _e($oPage->title) ?></h1>
        </div>
        <?= $oPage->content ?>
        <?
        if (!empty($aBikes)) {
            $iBikeCounter = 0;
            foreach ($aBikes AS $oBike) {
                $iBikeCounter++;
                ?>
                <div class="column-25 float-left">
                    <div class="bike-overview-column">
                        <div class="bike-overview-column-title">        
                            <a href="/motoren/<?= $oBike->bikeId ?>/<?= prettyUrlPart($oBike->title) ?>"><?= $oBike->title ?></a>
                        </div>
                        <a href="/motoren/<?= $oBike->bikeId ?>/<?= prettyUrlPart($oBike->title) ?>"><div class="bike-crop-overview"><img class="bike-overview-column-image" style="width: 100%;" src="<?= !empty($oBike->imageFancyboxImage) ? $oBike->imageFancyboxImage : '/images/layout/motoren/motoren-no-image.jpg' ?>" alt="<?= $oBike->title ?>" /></div></a>
                        <div></div>
                        <table style="width:100%; margin-top:10px; margin-bottom:10px;">
                            <tr>
                                <td><b>Prijs:</b></td>
                                <td>€ <?= number_format($oBike->price, 0, ',', '.') ?>,-</td>
                            </tr>
                            <tr>
                                <td><b>Bouwjaar:</b></td>
                                <td><?= $oBike->manufactureYear ?></td>
                            </tr>
                        </table>
                        <a href="/motoren/<?= $oBike->bikeId ?>/<?= prettyUrlPart($oBike->title) ?>" class="read-more">Details bekijken</a>
                    </div>
                </div>
                <?
                if ($iBikeCounter == 4) {
                    echo '<br class="clear"><br>';
                    $iBikeCounter = 0;
                } else {
                    echo '<div class="hide-on-mobile column-width float-left">&nbsp;</div>';
                }
                ?>
                <?
            }
        }
        ?>
        <div style="float:left; clear:both; margin-top: 20px;"><? echo generatePaginationHTML($iPageCount, $iCurrPage); ?></div>
    </section>
</div>