<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page">
        <section class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></section>
        <? if ($oPageForMenu->getSubPages()) { ?>
            <? include TEMPLATES_FOLDER . '/elements/pagesSubmenu.inc.php'; ?>
        <? } ?>

        <article class="<?= $oPageForMenu->getSubPages() ? 'column-50' : 'column-75' ?> page-content float-left">
            <h1><?= _e($oPage->title) ?></h1>
            <?= $oPage->content ?>
        </article>
        <? include TEMPLATES_FOLDER . '/elements/asideMedia.inc.php'; ?>
    </section>
</div>