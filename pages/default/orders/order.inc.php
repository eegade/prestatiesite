<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page order">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="column-25 float-left steps">
            <a href="/winkelwagen"><div class="step">
                    <div class="step-amount">1</div>
                    <span>Winkelwagen</span>
                </div></a>
            <div class="step-sep"></div>
            <a href="/winkelwagen/bestellen"><div class="step-active">
                    <div class="step-amount">2</div>
                    <span>Uw gegevens</span>
                </div></a>
        </div>
        <div class="column-75 float-right" style="line-height: 28px;">
            <div class="head">
                <h1><?= _e($oPage->title) ?></h1>
            </div>
            <?= $oPage->content ?>
            <? if ($bWithLogin) { ?>
                <div  class="column-50 float-left">
                    <div class="order-column">
                        <h2>Bent u al klant?</h2>
                        <form method="POST" action="/account" class="validateForm">
                            <input type="hidden" value="login" name="action" />
                            <? include TEMPLATES_FOLDER . '/elements/errorBox.inc.php'; ?>
                            <table style="width:100%">
                                <tr>
                                    <td style="width: 100px; font-size:12px;"><label for="order-login-email">E-mail *</label></td>
                                    <td><input class="required" title="Vul uw e-mailadres in" id="order-login-email" type="email" name="email" value="" /></td>
                                </tr>
                                <tr>
                                    <td><label for="order-login-password">Wachtwoord *</label></td>
                                    <td><input class="required" title="Vul uw wachtwoord in" id="order-login-password" type="password" name="password" value="" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <input class="read-more" type="submit" value="Inloggen" name="login" style="margin-top:10px" />
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            <? } ?>
            <form method="POST" action="<?= getCurrentUrlPath() ?>" class="validateForm">
                <?
                if (!$bWithLogin) {
                    // hack for multiple errorBoxes
                    $aErrors = null;
                    if (!empty($aErrorsBasket))
                        $aErrors = $aErrorsBasket;
                    include TEMPLATES_FOLDER . '/elements/errorBox.inc.php';
                }
                ?>

                <div class="column-50 float-<?= $bWithLogin ? 'right' : 'left' ?>">
                    <div class="order-column">
                        <h2>Factuurgegevens</h2>
                        <?
                        if ($bWithLogin) {
                            include TEMPLATES_FOLDER . '/elements/errorBox.inc.php';
                        }
                        ?>
                        <table>
                            <tr>
                                <td style="width: 120px;"><label for="invoice_companyName">Bedrijfsnaam</label></td>
                                <td><input id="invoice_companyName" type="text" name="invoice_companyName" value="<?= _e($_SESSION['oBasket']->invoice_companyName) ?>" /></td>
                            </tr>
                            <tr>
                                <td class="forListLabels"><label>Geslacht *</label></td>
                                <td class="forListFields">
                                    <input class="alignRadio required" title="Kies een geslacht (factuurgegevens)" type="radio" <?= $_SESSION['oBasket']->invoice_gender == 'M' ? 'CHECKED' : '' ?> id="invoice_gender_M" name="invoice_gender" value="M" /> <label for="invoice_gender_M">Man</label><br />
                                    <input class="alignRadio required" title="Kies een geslacht (factuurgegevens)" type="radio" <?= $_SESSION['oBasket']->invoice_gender == 'F' ? 'CHECKED' : '' ?> id="invoice_gender_F" name="invoice_gender" value="F" /> <label for="invoice_gender_F">Vrouw</label>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="email">Email *</label></td>
                                <td><input id="email" class="required email" title="<<<<<<<<<<<<<<<<Vul een geldig emailadres in (factuurgegevens)" type="text" name="email" value="<?= _e($_SESSION['oBasket']->email) ?>" /></td>
                            </tr>
                            <tr>
                                <td><label for="invoice_firstName">Voornaam *</label></td>
                                <td><input id="invoice_firstName" class="required" title="Vul de voornaam in (factuurgegevens)" type="text" name="invoice_firstName" value="<?= _e($_SESSION['oBasket']->invoice_firstName) ?>" /></td>
                            </tr>
                            <tr>
                                <td><label for="invoice_insertion">Tussenvoegsel</label></td>
                                <td><input id="invoice_insertion" type="text" name="invoice_insertion" value="<?= _e($_SESSION['oBasket']->invoice_insertion) ?>" /></td>
                            </tr>
                            <tr>
                                <td><label for="invoice_lastName">Achternaam *</label></td>
                                <td><input id="invoice_lastName" class="required" title="Vul de achternaam in (factuurgegevens)" type="text" name="invoice_lastName" value="<?= _e($_SESSION['oBasket']->invoice_lastName) ?>" /></td>
                            </tr>
                            <tr>
                                <td><label for="invoice_address">Adres *</label></td>
                                <td><input id="invoice_address" class="required address" title="Vul het adres in (factuurgegevens)" type="text" name="invoice_address" value="<?= _e($_SESSION['oBasket']->invoice_address) ?>" /></td>
                            </tr>
                            <tr>
                                <td><label for="invoice_houseNumber">Huisnummer *</label></td>
                                <td><input id="invoice_houseNumber" class="required digits address" title="Vul een geldig huisnummer in met alleen nummers (factuurgegevens)" type="text" name="invoice_houseNumber" value="<?= _e($_SESSION['oBasket']->invoice_houseNumber) ?>" /></td>
                            </tr>
                            <tr>
                                <td><label for="invoice_houseNumberAddition">Toevoeging</label></td>
                                <td><input id="invoice_houseNumberAddition" type="text" name="invoice_houseNumberAddition" value="<?= _e($_SESSION['oBasket']->invoice_houseNumberAddition) ?>" /></td>
                            </tr>
                            <tr>
                                <td><label for="invoice_postalCode">Postcode *</label></td>
                                <td><input id="invoice_postalCode" class="required address" title="Vul de postcode in (factuurgegevens)" type="text" name="invoice_postalCode" value="<?= _e($_SESSION['oBasket']->invoice_postalCode) ?>" /></td>
                            </tr>
                            <tr>
                                <td><label for="invoice_city">Plaats *</label></td>
                                <td><input id="invoice_city" class="required address" title="Vul de plaats in (factuurgegevens)" type="text" name="invoice_city" value="<?= _e($_SESSION['oBasket']->invoice_city) ?>" /></td>
                            </tr>
                            <tr>
                                <td><label for="invoice_phone">Telefoon</label></td>
                                <td><input id="invoice_phone" type="tel" name="invoice_phone" value="<?= _e($_SESSION['oBasket']->invoice_phone) ?>" /></td>
                            </tr>
                        </table>
                    </div>
                    <? if (!$bWithLogin) { ?>
                    </div>
                    <div class="column-50 float-right">
                    <? } ?>
                    <div class="order-column">
                        <h2>Aflevergegevens</h2>
                        <table>
                            <tr>
                                <td colspan="3"><input id="delivery_same_as_invoice" class="address" type="checkbox" name="delivery_same_as_invoice" value="1" <?= empty($_SESSION['aOrderSettings']['delivery_same_as_invoice']) ? '' : 'checked' ?> /> <label for="delivery_same_as_invoice">Zelfde als factuurgegevens</label></td>
                            </tr>
                            <tbody id="delivery_fields">
                                <tr>
                                    <td style="width: 120px;"><label for="delivery_companyName">Bedrijfsnaam</label></td>
                                    <td><input id="delivery_companyName" type="text" name="delivery_companyName" value="<?= _e($_SESSION['oBasket']->delivery_companyName) ?>" /></td>
                                </tr>
                                <tr>
                                    <td class="forListLabels"><label>Geslacht *</label></td>
                                    <td class="forListFields">
                                        <input class="alignRadio required" title="Kies een geslacht (aflevergegevens)" type="radio" <?= $_SESSION['oBasket']->delivery_gender == 'M' ? 'CHECKED' : '' ?> id="delivery_gender_M" name="delivery_gender" value="M" /> <label for="delivery_gender_M">Man</label><br />
                                        <input class="alignRadio required" title="Kies een geslacht (aflevergegevens)" type="radio" <?= $_SESSION['oBasket']->delivery_gender == 'F' ? 'CHECKED' : '' ?> id="delivery_gender_F" name="delivery_gender" value="F" /> <label for="delivery_gender_F">Vrouw</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="delivery_firstName">Voornaam *</label></td>
                                    <td><input id="delivery_firstName" class="required" title="Vul de voornaam in (aflevergegevens)" type="text" name="delivery_firstName" value="<?= _e($_SESSION['oBasket']->delivery_firstName) ?>" /></td>
                                </tr>
                                <tr>
                                    <td><label for="delivery_insertion">Tussenvoegsel</label></td>
                                    <td><input id="delivery_insertion" type="text" name="delivery_insertion" value="<?= _e($_SESSION['oBasket']->delivery_insertion) ?>" /></td>
                                </tr>
                                <tr>
                                    <td><label for="delivery_lastName">Achternaam *</label></td>
                                    <td><input id="delivery_lastName" class="required" title="Vul de achternaam in (aflevergegevens)" type="text" name="delivery_lastName" value="<?= _e($_SESSION['oBasket']->delivery_lastName) ?>" /></td>
                                </tr>
                                <tr>
                                    <td><label for="delivery_address">Adres *</label></td>
                                    <td><input id="delivery_address" class="required address" title="Vul het adres in (aflevergegevens)" type="text" name="delivery_address" value="<?= _e($_SESSION['oBasket']->delivery_address) ?>" /></td>
                                </tr>
                                <tr>
                                    <td><label for="delivery_houseNumber">Huisnummer *</label></td>
                                    <td><input id="delivery_houseNumber" class="required digits address" title="Vul een geldig huisnummer in met alleen nummers (aflevergegevens)" type="text" name="delivery_houseNumber" value="<?= _e($_SESSION['oBasket']->delivery_houseNumber) ?>" /></td>
                                </tr>
                                <tr>
                                    <td><label for="delivery_houseNumberAddition">Toevoeging</label></td>
                                    <td><input id="delivery_houseNumberAddition" type="text" name="delivery_houseNumberAddition" value="<?= _e($_SESSION['oBasket']->delivery_houseNumberAddition) ?>" /></td>
                                </tr>
                                <tr>
                                    <td><label for="delivery_postalCode">Postcode *</label></td>
                                    <td><input id="delivery_postalCode" class="required address" title="Vul de postcode in (aflevergegevens)" type="text" name="delivery_postalCode" value="<?= _e($_SESSION['oBasket']->delivery_postalCode) ?>" /></td>
                                </tr>
                                <tr>
                                    <td><label for="delivery_city">Plaats *</label></td>
                                    <td><input id="delivery_city" class="required address" title="Vul de plaats in (aflevergegevens)" type="text" name="delivery_city" value="<?= _e($_SESSION['oBasket']->delivery_city) ?>" /></td>
                                </tr>
                            </tbody>
                        </table>
                        <br class="clear">
                        <input class="read-more" type="submit" value="Bestelling plaatsen" name="saveOrder" />
                    </div>
                </div>
            </form>
        </div>
    </section>
    <br class="clear">
</div>
<?
$sBottomJavascript = <<<EOT
<script type="text/javascript">
    // copy invoice field values to the delivery fields
    function copyInvoiceToDelivery() {
        $('input#delivery_companyName').val($('input#invoice_companyName').val());
        $('input#delivery_gender_M').attr('checked', $('input#invoice_gender_M').attr('checked'));
        $('input#delivery_gender_F').attr('checked', $('input#invoice_gender_F').attr('checked'));
        $('input#delivery_firstName').val($('input#invoice_firstName').val());
        $('input#delivery_insertion').val($('input#invoice_insertion').val());
        $('input#delivery_lastName').val($('input#invoice_lastName').val());
        $('input#delivery_address').val($('input#invoice_address').val());
        $('input#delivery_houseNumber').val($('input#invoice_houseNumber').val());
        $('input#delivery_houseNumberAddition').val($('input#invoice_houseNumberAddition').val());
        $('input#delivery_postalCode').val($('input#invoice_postalCode').val());
        $('input#delivery_city').val($('input#invoice_city').val());
    };

    $().ready(function() {
        // show/hide delivery fields if the checkbox has been (un)checked
        if($('input#delivery_same_as_invoice').attr('checked')) {
            $('tbody#delivery_fields').hide();
            $('tbody#delivery_fields input').each(function() {
                $(this).attr('disabled', true);
            });
        } else {
            $('tbody#delivery_fields input').each(function() {
                $(this).attr('disabled', false);
            });
            $('tbody#delivery_fields').show();
        }
    });
    
    // show/hide delivery fields if the checkbox has been (un)checked
    $('input#delivery_same_as_invoice').change(function() {
        if($(this).attr('checked')) {
            $('tbody#delivery_fields').hide();
            
            copyInvoiceToDelivery();

            $('tbody#delivery_fields input').each(function() {
                $(this).attr('disabled', true);
            });
        } else {
            $('tbody#delivery_fields input').each(function() {
                $(this).attr('disabled', false);
            });
            
            copyInvoiceToDelivery();

            $('tbody#delivery_fields').show();
        }
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>