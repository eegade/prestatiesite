<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page basket">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="column-25 float-left steps">
            <a href="/winkelwagen"><div class="step-active">
                    <div class="step-amount">1</div>
                    <span>Winkelwagen</span>
                </div></a>
            <div class="step-sep"></div>
            <div class="step">
                <div class="step-amount">2</div>
                <span>Uw gegevens</span>
            </div>
        </div>
        <div class="column-75 float-right">
            <div class="head">
                <h1><?= _e($oPage->title) ?></h1>
            </div>
            <?= $oPage->content ?>
            <div class="orderStatusUpdate statusUpdate"></div>
            <?
            if (count($_SESSION['oBasket']->getProducts()) == 0) {
                echo '<div><p>U heeft geen artikelen in uw winkelwagen.</p></div>';
            } else {
                ?>
                <div>
                    <?
                    foreach ($_SESSION['oBasket']->getProducts() as $oProduct) {
                        ?>
                        <div class="product">
                            <div class="float-left column-50">
                                <div class="float-left column-50">
                                    <?
                                    if (($oImage = $oProduct->getProduct()->getImages('first-online')) && $oImageFileCrop = $oImage->getImageFileByReference('crop_thumb')) {
                                        echo '<a class="product-image" href="' . $oProduct->getProduct()->getUrlPath() . '" title="Meer informatie over de ' . _e($oProduct->brandName . ' ' . $oProduct->productName) . '">'
                                                    . '<img style="height: 100px; width: 100px;" src="' . $oImageFileCrop->link . '" alt="' . $oImageFileCrop->title . '" />'
                                             . '</a>';
                                    } else {
                                        echo'<a class="product-image" href="' . $oProduct->getProduct()->getUrlPath() . '" title="Meer informatie over de ' . _e($oProduct->brandName . ' ' . $oProduct->productName) . '">'
                                                    . '<img style="width:100px; height: 100px;" src="/images/layout/shop-no-image.jpg" alt="No image">'
                                            . '</a>';
                                    }
                                    ?>
                                </div>
                                <div class="float-left column-50">
                                    <a class="word-wrap" href="<?= $oProduct->getProduct()->getUrlPath() ?>"><?= _e($oProduct->brandName . ' ' . $oProduct->productName) ?></a>
                                    <? if ($oProduct->getProduct()->getProductType()->withSizes || $oProduct->getProduct()->getProductType()->withColors) { ?>
                                        <span class="size-color">
                                            <?= $oProduct->getProduct()->getProductType()->withSizes ? '| ' . $oProduct->productSizeName : '' ?>
                                            <?= $oProduct->getProduct()->getProductType()->withColors ? '| ' . $oProduct->productColorName : '' ?> |
                                        </span>
                                    <? } ?>
                                </div>
                            </div>
                            <div class="product-order-details column-25 float-left">
                                <form action="<?= getCurrentUrlPath() ?>" method="post">
                                    <input type="hidden" name="catalogProductId" value="<?= $oProduct->catalogProductId ?>" />
                                    <input type="hidden" name="catalogProductSizeId" value="<?= $oProduct->catalogProductSizeId ?>" />
                                    <input type="hidden" name="catalogProductColorId" value="<?= $oProduct->catalogProductColorId ?>" />
                                    <select style="float:left; height:auto; padding: 5px;" class="amount-select <?= ($oProduct->amount <= 10) ? '' : 'hide' ?>"  name="amount" <?= ($oProduct->amount >= 1 && $oProduct->amount <= 10) ? '' : 'disabled' ?>>
                                        <option value="1" <?= ($oProduct->amount == 1) ? 'selected' : '' ?>>1</option>
                                        <option value="2" <?= ($oProduct->amount == 2) ? 'selected' : '' ?>>2</option>
                                        <option value="3" <?= ($oProduct->amount == 3) ? 'selected' : '' ?>>3</option>
                                        <option value="4" <?= ($oProduct->amount == 4) ? 'selected' : '' ?>>4</option>
                                        <option value="5" <?= ($oProduct->amount == 5) ? 'selected' : '' ?>>5</option>
                                        <option value="6" <?= ($oProduct->amount == 6) ? 'selected' : '' ?>>6</option>
                                        <option value="7" <?= ($oProduct->amount == 7) ? 'selected' : '' ?>>7</option>
                                        <option value="8" <?= ($oProduct->amount == 8) ? 'selected' : '' ?>>8</option>
                                        <option value="9" <?= ($oProduct->amount == 9) ? 'selected' : '' ?>>9</option>
                                        <option value="10" <?= ($oProduct->amount == 10) ? 'selected' : '' ?>>10</option>
                                        <option value="n">meer?</option>
                                    </select>
                                    <input class="amount-input" style="<?= ($oProduct->amount > 10) ? '' : 'display:none;' ?>" type="text" name="amount"  value="<?= $oProduct->amount ?>" <?= ($oProduct->amount > 10) ? '' : 'disabled' ?>/>
                                    <input class="amount-refresh-button refresh-button" <?= ($oProduct->amount > 10) ? '' : 'style="display:none;"' ?> type="submit" name="updateProductAmount" value="" />
                                    &nbsp;<input class="delete-button" type="submit" name="removeProductFromBasket" value="" title="Verwijder de <?= _e($oProduct->brandName . ' ' . $oProduct->productName) ?> van uw winkelwagen" />
                                </form>
                            </div>

                            <div class="column-25 float-right product-order-prices">
                                <? if(decimal2valuta($oProduct->getSubTotalOriginalPrice(true)) != decimal2valuta($oProduct->getSubTotalPrice(true))){ ?>
                                    <div class="product-order-original-price ">
                                        <span id="product_original_<?= $oProduct->catalogProductId ?>_<?= $oProduct->catalogProductSizeId ?>_<?= $oProduct->catalogProductColorId ?>"><?= decimal2valuta($oProduct->getSubTotalOriginalPrice(true)) ?></span>
                                    </div>
                                <? } ?> 
                                <div class="product-order-price ">
                                    <span id="product_<?= $oProduct->catalogProductId ?>_<?= $oProduct->catalogProductSizeId ?>_<?= $oProduct->catalogProductColorId ?>"><?= decimal2valuta($oProduct->getSubTotalPrice(true)) ?></span>
                                </div>

                            </div>
                        </div>
                        <?
                    }
                    ?>

                    
                    
                    <!-- I show the payment information -->
                    <div class="payment">
                        <div class="float-left column-25">
                            <a href="<?= (empty($_SESSION['frontendBasketReferrer']) ? '/' : http_session('frontendBasketReferrer')) ?>">< Verder winkelen</a>
                        </div>
                        
                        <div class=" float-right column-75">
                            <div class="payment_box">
                                <!-- Subtotal -->
                                <div class="payment-concept">
                                    
                                    <div class="float-right column-100 product-order-price">
                                        <span class="float-left">Subtotaal</span>
                                        <span class="float-right" id="subtotal" style="white-space: nowrap; text-align:right;"><?= decimal2valuta($_SESSION['oBasket']->getSubtotalProducts(true)) ?></span><br />
                                    </div>
                                </div>

                                <!-- Discount code -->                  
                                <? if (BB_WITH_COUPONS) { ?>
                                    <div class="payment-concept">
                                        <div class="float-left column-25"> Kortingscode:</div>
                                        <div class="float-left column-50">
                                            <form action="<?= getCurrentUrlPath() ?>" method="post">
                                                <input type="hidden" name="action" value="applyDiscount" />
                                                <div class="discountStatusUpdate statusUpdate float-left">

                                                </div>
                                                <div class="float-left" style="clear:both">
                                                    <input class="coupon-text tooltip" title="Kortingscode is geldig op afgeprijsde producten" id="couponCode" name="couponCode" type="text" value="<?= $_SESSION['oBasket']->couponId ? $_SESSION['oBasket']->getDiscountCoupon()->code : '' ?>" />                                            
                                                    <input class="remove-discount delete-button" type="button" name="removeDiscount" value="" title="Geen gebruik korting" />
                                                </div>
                                                <div class="float-left" style="clear:both">
                                                    <input class="discount-button read-more" type="submit" name="applyDiscount" id="applyDiscount" value="CODE INVOEREN" />  
                                                </div>                                                                                     
                                            </form>
                                        </div>
                                        <div class="float-right column-25 product-order-price">
                                            <span style="white-space: nowrap; text-align:right;"><span id="orderTotalDiscount"><?= decimal2valuta($_SESSION['oBasket']->getDiscount(true)) ?></span></span><br/>
                                        </div>
                                    </div>
                                <? } ?>

                                <!-- Delivery mode -->
                                <div class="payment-concept">
                                    <div class="float-left column-25">Aflevermethode:</div>
                                    <div class="float-left column-50">
                                        <form action="<?= getCurrentUrlPath() ?>" method="post">
                                            <input type="hidden" name="action" value="updateDeliveryMethod" />
                                            <select id="deliveryMethodId" name="deliveryMethodId">
                                                <?
                                                $aDeliveryMethods = DeliveryMethodManager::getAllDeliveryMethods();
                                                foreach  ( $aDeliveryMethods as $oDeliveryMethod) {
                                                    echo '<option value="' . $oDeliveryMethod->deliveryMethodId . '" ' . ($oDeliveryMethod->deliveryMethodId == $_SESSION['oBasket']->deliveryMethodId ? 'selected' : '') . '>' . _e($oDeliveryMethod->name) . '</option>';
                                                }
                                                ?>
                                            </select>
                                            <noscript>
                                                <input type="submit" name="updateDeliveryMethod" value="Wijzig" />
                                            </noscript>
                                        </form>
                                    </div>
                                    <div class="float-right column-25 product-order-price">
                                        <span style="white-space: nowrap; text-align:right;">
                                            <? if (!empty($aDeliveryMethods)){ ?>
                                                <span id="orderDeliveryPriceWithTax"><?= decimal2valuta($_SESSION['oBasket']->getDeliveryCosts(true)) ?></span><br />
                                            <? } ?>
                                        </span>
                                    </div>
                                </div>

                                <!-- Pay method -->
                                <div class="payment-concept">
                                    <div class="float-left column-25">Betaalmethode:</div>
                                    <div class="float-left column-50">
                                        <form action="<?= getCurrentUrlPath() ?>" method="post">
                                            <input type="hidden" name="action" value="updatePaymentMethod" />

                                            <select id="paymentMethodId" name="paymentMethodId">
                                                <?
                                                foreach ($_SESSION['oBasket']->getDeliveryMethod()->getPaymentMethods('all') as $oPaymentMethod) {
                                                    echo '<option value="' . $oPaymentMethod->paymentMethodId . '" ' . ($oPaymentMethod->paymentMethodId == $_SESSION['oBasket']->paymentMethodId ? 'selected' : '') . '>' . _e($oPaymentMethod->name) . '</option>';
                                                }
                                                ?>
                                            </select>
                                            <noscript>
                                                <input type="submit" name="updatePaymentMethod" value="Wijzig" />
                                            </noscript>
                                        </form>
                                    </div>
                                    <div class="float-right column-25 product-order-price">
                                        <span style="white-space: nowrap; text-align:right;"><span id="orderPaymentPriceWithTax"><?= decimal2valuta($_SESSION['oBasket']->getPaymentCosts(true)) ?></span></span><br />
                                    </div>
                                </div>

                                <!-- Total -->
                                <div class="payment-concept">
                                    <div class="float-right column-100 product-order-price">
                                        <span class="float-left">Totaal</span>
                                        <span class="float-right" style="white-space: nowrap; text-align:right;">
                                            <span id="orderTotalPriceWithTax"><?= decimal2valuta($_SESSION['oBasket']->getTotal(true)) ?></span><br />
                                        </span>
                                    </div>
                                </div>

                                <? if (Settings::get('showExclVAT')) { ?>
                                    <!-- Tax -->
                                    <div class="payment-concept">
                                        <div class="float-right column-100 product-order-price">
                                            <span class="float-left">BTW</span>
                                            <span class="float-right" style="white-space: nowrap; text-align:right;"><span id="orderTotalTaxPrice"><?= decimal2valuta($_SESSION['oBasket']->getBTW()) ?></span></span>
                                        </div>
                                    </div>

                                    <!-- Total (without tax) -->
                                    <div class="payment-concept">
                                        
                                        <div class="float-right column-100 product-order-price">
                                            <div class="float-left">Totaal (excl. BTW)</div>
                                            <span class="float-right" style="white-space: nowrap; text-align:right;"><span id="orderTotalPriceWithoutTax"><?= decimal2valuta($_SESSION['oBasket']->getTotal(false)) ?></span></span>
                                        </div>
                                    </div>
                                <? } ?>
                                <?
                                if (count($_SESSION['oBasket']->getProducts()) > 0 && $_SESSION['oBasket']->getSubtotalProducts(true) > 0) {
                                    echo '<a class="read-more float-right" href="/' . http_get('controller') . '/bestellen">Bestellen</a>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>



                </div>
                <? } ?>
        </div>
    </section>
</div>
<?
$sJavascript = <<<EOT
<script>
    // change delivery method
    $('#deliveryMethodId').change(function() {
        var thisForm = $(this).closest('form');
        $.ajax({
            type: "POST",
            url: thisForm.attr('action'),
            data: thisForm.serialize() + '&ajax=1',
            async: false,
            success: function(data){
                var dataObj = eval('(' + data + ')');
                if(dataObj.success == true) {
                    $('#orderDeliveryPriceWithTax').html(dataObj.formattedDeliveryPriceWithTax);
                    $('#orderTotalPriceWithoutTax').html(dataObj.formattedTotalPrice);
                    $('#orderTotalTaxPrice').html(dataObj.formattedTotalTaxPrice);
                    $('#orderTotalPriceWithTax').html(dataObj.formattedTotalPriceWithTax);
                    $('#paymentMethodId').html(dataObj.paymentMethodOptions);
                    $('#paymentMethodId').change();
                } else {
                    showStatusUpdate(dataObj.orderStatusUpdate, '.orderStatusUpdate');
                }
            }
        });
    });
    
    // change payment method
    $('#paymentMethodId').change(function() {
        var thisForm = $(this).closest('form');
        $.ajax({
            type: "POST",
            url: thisForm.attr('action'),
            data: thisForm.serialize() + '&ajax=1',
            async: false,
            success: function(data){
                var dataObj = eval('(' + data + ')');
                if(dataObj.success == true) {
                    $('#orderPaymentPriceWithTax').html(dataObj.formattedPaymentPriceWithTax);
                    $('#orderTotalPriceWithoutTax').html(dataObj.formattedTotalPrice);
                    $('#orderTotalTaxPrice').html(dataObj.formattedTotalTaxPrice);
                    $('#orderTotalPriceWithTax').html(dataObj.formattedTotalPriceWithTax);
                } else {
                    showStatusUpdate(dataObj.orderStatusUpdate, '.orderStatusUpdate');
                }
            }
        });
    }); 
        
    var orderStatusUpdate = '$sOrderStatusUpdate';
        if(orderStatusUpdate){
            showStatusUpdate(orderStatusUpdate, '.orderStatusUpdate');
        }
    
    // apply the discount if the given coupon is valid
    $('#applyDiscount').click(function(e) {
        var thisForm = $(this).closest('form');
        var couponCode = $('#couponCode').val();
        couponCode = $.trim(couponCode);
        couponCode = couponCode.toUpperCase();
        if (couponCode.length > 0) {
            $('#couponCode').val(couponCode);
            $.ajax ({
                type: "POST",
                url: thisForm.attr('action'),
                async: false,
                data: thisForm.serialize() + '&ajax=1',
                success: function(data) {
                    var dataObj = eval('(' + data + ')');
                    if(dataObj.success == true) {
                        $('#orderTotalDiscount').html(dataObj.formattedTotalDiscount);
                        $('#orderTotalPriceWithoutTax').html(dataObj.formattedTotalPrice);
                        $('#orderTotalTaxPrice').html(dataObj.formattedTotalTaxPrice);
                        $('#orderTotalPriceWithTax').html(dataObj.formattedTotalPriceWithTax);
                    }
        
                    showStatusUpdate(dataObj.orderStatusUpdate, '.discountStatusUpdate');
                },
            });
        }
        e.preventDefault();
    });
        
    $('.remove-discount').click(function(e) {
        var thisForm = $(this).closest('form');
        $('#couponCode').val('');
        $.ajax ({
            type: "POST",
            url: thisForm.attr('action'),
            async: false,
            data: 'action=removeDiscount&ajax=1',
            success: function(data) {
                var dataObj = eval('(' + data + ')');
        
                if(dataObj.success == true) {
                    $('#orderTotalDiscount').html(dataObj.formattedTotalDiscount);
                    $('#orderTotalPriceWithoutTax').html(dataObj.formattedTotalPrice);
                    $('#orderTotalTaxPrice').html(dataObj.formattedTotalTaxPrice);
                    $('#orderTotalPriceWithTax').html(dataObj.formattedTotalPriceWithTax);
                } 

                showStatusUpdate(dataObj.orderStatusUpdate, '.discountStatusUpdate');
            }
        });
        e.preventDefault();
    });        
    
        
        var lastVal = '';
    // save last value on focus
    $('select.amount-select').one('focus', function(){
        lastVal = $(this).val();
    });

    // on changing of amount do something
    $('select.amount-select').change(function() {
        var thisForm = $(this).closest('form');
        if ($(this).val() === 'n') {
            // disable select and hide
            $(this).prop('disabled', true).hide();

            // enable input, set value, show and focus
            thisForm.find('.amount-input').prop('disabled', false).val(lastVal).show().focus();
            thisForm.find('.amount-refresh-button').show(); // show refresh button
        }
        else {
            lastVal = $(this).val();
            $.ajax({
                type: "POST",
                url: thisForm.attr('action'),
                data: thisForm.serialize() + "&updateProductAmount=1&ajax=1",
                async: false,
                success: function (data) {
                    var dataObj = eval('(' + data + ')');

                    if(dataObj.success == true) {
                        $('#'+dataObj.productId).html(dataObj.formattedProductSubtotal);
                        $('#'+dataObj.productOriginalId).html(dataObj.formattedProductOriginalSubtotal);
                        $('#subtotal').html(dataObj.formattedSubtotal);
                        $('#orderTotalDiscount').html(dataObj.formattedTotalDiscount);
                        $('#orderTotalPriceWithoutTax').html(dataObj.formattedTotalPrice);
                        $('#orderTotalTaxPrice').html(dataObj.formattedTotalTaxPrice);
                        $('#orderTotalPriceWithTax').html(dataObj.formattedTotalPriceWithTax);
                        $('#orderDeliveryPriceWithTax').html(dataObj.formattedDeliveryPriceWithTax);
                    }
                    showStatusUpdate(dataObj.orderStatusUpdate, '.orderStatusUpdate');
                }
            });
        }
    });

   </script>    
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);
?>