<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <div class="breadcrumbs">
        <p>U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></p>
    </div>
    <? if (!empty($aNewsItemCategoriesForMenu)) { ?>
        <nav class="column-25 float-left sub-navigation">
            <ul>
                <?
                foreach ($aNewsItemCategoriesForMenu AS $oNewsItemCategoryForMenu) {
                    // check if controller equals page urlPath be aware of special treathment for homepage!!
                    echo '<li><a class="' . ($oNewsItemCategoryForMenu->getUrlPath() == getCurrentUrlPath() ? 'active' : '') . '" style="display:block;" href="' . $oNewsItemCategoryForMenu->getUrlPath() . '">' . _e($oNewsItemCategoryForMenu->name) . '</a></li>';
                }
                ?>
            </ul>
        </nav>
        <div class="column-width float-left">&nbsp;</div>
    <? } ?>
    <section class="<?= !empty($aNewsItemCategoriesForMenu) ? 'column-75' : '' ?> page-content float-left">
        <h1><?= _e($oNewsItemCategory->name) ?></h1>
        <?
        if (!empty($aNewsItems)) {
            foreach ($aNewsItems AS $oNewsItem) {
                $oImage = $oNewsItem->getImages('first-online');
                if (!empty($oImage)) {
                    $oImageFile = ImageManager::getImageFileByImageAndReference($oImage->imageId, 'crop_small');
                } else{
                    $oImageFile = null;
                }
                ?>
                <div class="newsitem-block-container cf">
                    <div class="column-75 newsitem-block float-left">
                        <a href="<?= $oNewsItem->getUrlPath() . '?categoryId=' . $oNewsItemCategory->newsItemCategoryId ?>" class="newsitem-title"><?= _e($oNewsItem->title) ?></a>
                        <span class="newsitem-date-categories"><?= strftime('%d %B %Y', strtotime($oNewsItem->date)) ?></span>
                        <?= $oNewsItem->intro ?>
                        <a href="<?= $oNewsItem->getUrlPath() . '?categoryId=' . $oNewsItemCategory->newsItemCategoryId ?>" class="read-more">Lees meer</a>
                    </div>
                    <div class="column-25 float-right images">
                        <?= !empty($oImageFile) ? '<a href="' . $oNewsItem->getUrlPath() . '?categoryId=' . $oNewsItemCategory->newsItemCategoryId . '"><img src="' . $oImageFile->link . '" alt="' . _e($oImageFile->title) . '" title="' . _e($oNewsItem->title) . '" /></a>' : '' ?>
                    </div>
                </div>
                <?
            }
            ?>
            <div class="cf"><?= generatePaginationHTML($iPageCount, $iCurrPage) ?></div>
            <?
        }
        ?>
        <p id="newsarchive-link"><a href="<?= '/' . http_get('controller') ?>" class="read-more">Naar het volledige overzicht</a></p>
    </section>
</div>

