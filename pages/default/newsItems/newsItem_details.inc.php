<?include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <div class="breadcrumbs">
        <p>U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></p>
    </div>
    <? if (!empty($aNewsItemCategoriesForMenu)) { ?>
        <nav class="column-25 float-left sub-navigation hide-on-mobile">
            <ul>
                <?
                foreach ($aNewsItemCategoriesForMenu AS $oNewsItemCategoryForMenu) {
                    // check if controller equals page urlPath be aware of special treathment for homepage!!
                    echo '<li><a style="display:block;" href="' . $oNewsItemCategoryForMenu->getUrlPath() . '">' . _e($oNewsItemCategoryForMenu->name) . '</a></li>';
                }
                ?>
            </ul>
        </nav>
        <div class="column-width float-left">&nbsp;</div>
    <? } ?>
    <article class="<?= !empty($aNewsItemCategoriesForMenu) ? 'column-50' : 'column-75' ?> float-left">
        <h1><?= _e($oNewsItem->title) ?></h1>
        <span class="newsitem-date-categories"><?= strftime('%d %B %Y', strtotime($oNewsItem->date)) ?></span>
        <div class="newsitem-summary">
            <?= $oNewsItem->content ?>
        </div>
        <?
        if ($oNewsItem->source) {
            echo '<p><i>'.$i18n[$oLanguage->abbr]['source'].': ' . $oNewsItem->source . '</i></p>';
        }
        ?>
        <a href="<?= $sBackLink ?>" class="read-more">Naar het overzicht</a>
    </article>
    <? include TEMPLATES_FOLDER . '/elements/asideMedia.inc.php'; ?>
</div>