<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">

    <div class="breadcrumbs">
        <p>U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?><p>
    </div>
    <section class="page-content float-left">
        <div class="newsitems-archive-overview">
            <h1><?= _e($oPage->title) ?></h1>
            <?= $oPage->content ?>
            <?
            if (!empty($aNewsItems)) {
                foreach ($aNewsItems AS $oNewsItem) {
                    ?>
                    <div class="newsitem">
                        <div class="newsitem-content">
                            <span class="newsitem-date-categories"><?= strftime('%d %B %Y', strtotime($oNewsItem->date)) ?> - <a href="<?= $oNewsItem->getUrlPath() ?>"><?= _e($oNewsItem->title) ?></a></span>
                        </div>
                    </div>
                    <?
                }
            }
            ?>
            <div class="cf"><?= generatePaginationHTML($iPageCount, $iCurrPage) ?></div>
            <p id="newsarchive-link"><a href="<?= '/' . http_get('controller') ?>" class="read-more">Naar het volledige overzicht</a></p>
        </div>
    </section>
</div>