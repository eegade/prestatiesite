<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <aside class="column-25 float-left">
            <div class="product-categories-selected">
                <div class="product-categorie-active">
                    <?
                    foreach ($aChosenCategories AS $oChosenCategory) {
                        echo '<div>';
                        echo $oChosenCategory->name;
                        echo '<a class="delete-button-small" href="' . ($oChosenCategory->getParent() ? $oChosenCategory->getParent()->getUrlPath() : '/producten') . '" title="Verwijderen"></a>';
                        echo '</div>';
                    }
                    ?>
                </div>
                <div class="product-categorie-column">
                    <?
                    foreach ($oProductCategory->getSubCategories() AS $oSubCategory) {
                        echo '<div>';
                        echo '<a href="' . $oSubCategory->getUrlPath() . '">' . $oSubCategory->name . '</a>';
                        echo '</div>';
                    }
                    ?>
                </div>
            </div>             
            <br class="clear">
            <div id="product-filter" class="cf">
                <div class="hide-on-mobile product-filter-head">Filter</div>
                <div class="hide-on-desktop hide-on-tablet show-on-mobile product-filter-head-mobile">Filter</div>
                <div class="product-filter-column">
                    <?
                    echo CatalogProductManager::generateProductFilterHtml($_SESSION['aProductFilter']);
                    //echo CatalogProductManager::generateProductFilterHtml($_SESSION['aProductFiltersByCategory'][$oProductCategory->catalogProductCategoryId]);
                    ?>
                </div>
            </div>
        </aside>
        <div class="products-overview column-75 float-right">
            <?
            $iProductCounter = 0;
            foreach ($aProducts AS $oProduct) {
                $iProductCounter++;
                ?>
                <div class="product-block column-25 float-left cf">
                    <div class="product-block-column">
                        <div class="product-name"><a href="<?= $oProduct->getUrlPath() ?>?catId=<?= $oProductCategory->catalogProductCategoryId ?>"><?= _e($oProduct->name) ?></a></div>
                        <div class="product-image"><?
                            $oImage = $oProduct->getImages('first-online');
                            if (!empty($oImage)) {
                                $oImageFileCrop = $oImage->getImageFileByReference('crop_overview');
                                $oImageFileOri = $oImage->getImageFileByReference('detail');
                                if (!empty($oImageFileCrop) && !empty($oImageFileOri))
                                    echo '<a href="' . $oProduct->getUrlPath() . '?catId=' . $oProductCategory->catalogProductCategoryId . '"><img src="' . $oImageFileCrop->link . '" alt="' . _e($oImageFileCrop->title) . '" ' . $oImageFileCrop->imageSizeAttr . ' /></a>';
                            } else {
                                echo '<a href="' . $oProduct->getUrlPath() . '"><img src="/images/layout/shop-no-image.jpg" style="width:100%" alt="No image"></a>';
                            }
                            ?>
                            <a href="<?= $oProduct->getUrlPath() ?>?catId=<?= $oProductCategory->catalogProductCategoryId ?>">
                                <? if ($oProduct->getReducedPrice(true, null, null, true)){ ?>
                                    <div class="product-original-price">
                                        <?= decimal2valuta($oProduct->getSalePrice(true, null, null, true,false)) ?>
                                    </div>
                                <? } ?>
                                <div class="product-price">
                                    <?= decimal2valuta($oProduct->getSalePrice(true)) ?>
                                </div>
                            </a>
                        </div>
                        <div class="product-content">
                            <?= firstXCharacters($oProduct->description, 50) ?>
                        </div>
                        <a href="<?= $oProduct->getUrlPath() ?>?catId=<?= $oProductCategory->catalogProductCategoryId ?>" class="read-more">Meer informatie</a>
                    </div>
                </div>
                <?
                if ($iProductCounter == 3) {
                    echo '<br class="clear"><br>';
                    $iProductCounter = 0;
                } else {
                    echo '<div class="column-width float-left">&nbsp;</div>';
                }
            }
            if (empty($aProducts)) {
                echo '<div class="products-overview-head">';
                echo '<strong>Helaas,</strong><br>';
                echo 'Er zijn geen producten gevonden met dit filter.';
                echo '</div>';
            }

            // If it finish to show all the products but a row is not completly filled, i add a line break
            if ($iProductCounter != 0) {
                echo '<br class="clear" /><br>';
            }
            if (count($aProducts) > 0) {
                ?>
                <div class="cf pagination-perPage">
                    <?
                    echo generatePaginationHTML($iPageCount, $iCurrPage);
                    include TEMPLATES_FOLDER . '/elements/perPage.inc.php';
                    ?>
                </div>    
            <? } ?>
            <? if (!empty($oProductCategory->content)) { ?>
                <div class="products-overview-description">
                    <h1><?= $oProductCategory->name ?></h1>
                    <?= $oProductCategory->content ?>
                </div>
            <? } ?>
        </div>
    </section>
</div>
<?
$sJavascript = <<<EOT
<script>
    $(".product-filter-head-mobile").click(function(e) {
        $(".product-filter-column").stop(true, true);
        if ($(this).hasClass('opened')) {
            $(".product-filter-column").slideUp('slow');
            $(this).removeClass('opened');
        } else {
            $(".product-filter-column").slideDown('slow');
            $(this).addClass('opened');
        }
    });
</script>    
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);
?>