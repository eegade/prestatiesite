<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <aside class="column-25 float-left">
            <div class="product-categories">
                <div class="product-categorie-active">Categorie&euml;n</div>
                <nav class="product-categorie-column">
                    <?

                    function makeCatalogProductCategoryListTree($aProductCategories) {
                        if (!empty($aProductCategories)) {
                            echo '<ul class="categorie level">';
                            foreach ($aProductCategories AS $oProductCategory) {
                                echo '<li><a href="' . $oProductCategory->getUrlPath() . '">' . _e($oProductCategory->name) . '</a>';
                                makeCatalogProductCategoryListTree($oProductCategory->getSubCategories());
                                echo '</li>';
                            }
                            echo '</ul>';
                        }
                    }

                    makeCatalogProductCategoryListTree($aProductCategories);
                    ?>
                </nav>
            </div>
        </aside>
        <div class="column-75 float-right">
            <div class="products-overview-head">
                <h1><?= _e($oPage->title) ?></h1>
                <?= $oPage->content ?>
            </div>
        </div>
    </section>
</div>