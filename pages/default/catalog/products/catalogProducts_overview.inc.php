<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="column-25 float-left">
            <div id="product-filter" class="cf">
                <div class="hide-on-mobile product-filter-head">Filter</div>
                <div class="hide-on-desktop hide-on-tablet show-on-mobile product-filter-head-mobile">Filter</div>
                <div class="product-filter-column">
                    <?
                    $aSettings = array(
                        'showBrands' => true,
                        'showGenders' => true,
                        'alwaysShowOptions' => true,
                        'showName' => false,
                        'showPrice' => false,
                        'showSizes' => false,
                        'showColors' => false,
                        'showSpecificProps' => false,
                        'showProductTypes' => false,
                    );
                    echo CatalogProductManager::generateProductFilterHtml($_SESSION['aProductFilter'], $aSettings);
                    ?>
                </div>
            </div>
            <div class="product-categories">
                <div class="product-categorie-active">Categorie&euml;n</div>
                <nav class="product-categorie-column">
                    <?

                    function makeCatalogProductCategoryListTree($aProductCategories) {
                        if (!empty($aProductCategories)) {
                            echo '<ul>';
                            foreach ($aProductCategories AS $oProductCategory) {
                                echo '<li><a href="' . $oProductCategory->getUrlPath() . '">' . _e($oProductCategory->name) . '</a>';
                                //makeCatalogProductCategoryListTree($oProductCategory->getSubCategories());
                                echo '</li>';
                            }
                            echo '</ul>';
                        }
                    }

                    makeCatalogProductCategoryListTree($aProductCategories);
                    ?>
                </nav>
            </div>
        </div>
        <div class="column-75 float-right products-overview-head">
            <h1><?= _e($oPage->title) ?></h1>
            <?= $oPage->content ?>
        </div>
        <div class="products-overview column-75 float-right">
            <?
            $iProductCounter = 0;
            foreach ($aProducts AS $oProduct) {
                $iProductCounter++;
                ?>

                <div class="product-block column-25 float-left cf">
                    <div class="product-block-column">
                        <div class="product-name"><a href="<?= $oProduct->getUrlPath() ?>"><?= _e($oProduct->name) ?></a></div>
                        <div class="product-image"><?
                            $oImage = $oProduct->getImages('first-online');
                            if (!empty($oImage)) {
                                $oImageFileCrop = $oImage->getImageFileByReference('crop_overview');
                                $oImageFileOri = $oImage->getImageFileByReference('detail');
                                if (!empty($oImageFileCrop) && !empty($oImageFileOri))
                                    echo '<a href="' . $oProduct->getUrlPath() . '"><img src="' . $oImageFileCrop->link . '" alt="' . _e($oImageFileCrop->title) . '" ' . $oImageFileCrop->imageSizeAttr . ' /></a>';
                            } else {
                                echo '<a href="' . $oProduct->getUrlPath() . '"><img src="/images/layout/shop-no-image.jpg" style="width:100%" alt="No image"></a>';
                            }
                            ?>
                            <a href="<?= $oProduct->getUrlPath() ?>">
                                <? if ($oProduct->getReducedPrice(true, null, null, true)){ ?>
                                    <div class="product-original-price">
                                        <?= decimal2valuta($oProduct->getSalePrice(true, null, null, true,false)) ?>
                                    </div>
                                <? } ?>
                                <div class="product-price">
                                    <?= decimal2valuta($oProduct->getSalePrice(true, null, null, true)) ?>
                                </div>
                            </a>
                        </div>
                        <div class="product-content">
                            <?= firstXCharacters($oProduct->description, 50) ?>
                        </div>
                        <a href="<?= $oProduct->getUrlPath() ?>" class="read-more">Meer informatie</a>
                    </div>
                </div>
                <?
                if ($iProductCounter == 3) {
                    echo '<br class="clear"><br>';
                    $iProductCounter = 0;
                } else {
                    echo '<div class="column-width float-left">&nbsp;</div>';
                }
            }
            // If it finish to show all the products but a row is not completly filled, i add a line break
            if ($iProductCounter != 0) {
                echo '<br class="clear" /><br>';
            }
            if (count($aProducts) > 0) {
                ?>
                <div class="cf pagination-perPage">
                    <?
                    echo generatePaginationHTML($iPageCount, $iCurrPage);
                    include TEMPLATES_FOLDER . '/elements/perPage.inc.php';
                    ?>
                </div>
            <? } ?>
        </div>
    </section>
</div>
<?
$sJavascript = <<<EOT
<script>
    $(".product-filter-head-mobile").click(function(e) {
        $(".product-filter-column").stop(true, true);
        if ($(this).hasClass('opened')) {
            $(".product-filter-column").slideUp('slow');
            $(this).removeClass('opened');
        } else {
            $(".product-filter-column").slideDown('slow');
            $(this).addClass('opened');
        }
    });
</script>    
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);
?>