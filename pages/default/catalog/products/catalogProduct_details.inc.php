<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page" itemscope itemtype="http://data-vocabulary.org/Product">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="product-detail column-50 float-<?= count($oProduct->getImages()) > 0 ? 'right' : 'left' ?>">
            <div class="product-detail-column">
                <div class="product-info">
                    <h1 itemprop="name" class="product-detail-title"><?= $oProduct->name ?></h1>
                    <span itemprop="brand">Merk: <?= _e($oProduct->getBrand()->name) ?></span><br/>
                    <span>Type: <?= _e($oProduct->getProductType()->title) ?></span>
                </div>
                <span itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">
                    <meta itemprop="currency" content="EUR" />
                    <? if ($oProduct->getReducedPrice(true)) { ?>
                        <div class="product-original-price" <?= (($oProduct->getReducedPrice(true)) ? '' : 'itemprop="price"')?>>
                            <span class="originalPriceInVAT">
                                <?= decimal2valuta($oProduct->getSalePrice(true, null, null, true, false)) ?>
                            </span>
                        </div>
                    <? } ?>
                    <div class="product-price" itemprop="price">
                        <span class="priceInVAT">
                            <?= decimal2valuta($oProduct->getSalePrice(true)) ?>
                        </span>
                    </div>
                </span>
                <? if (Settings::get('showExclVAT')) { ?><div class="product-price-detail">Prijs (excl. BTW): <span class="priceExVAT"><?= decimal2valuta($oProduct->getSalePrice(false)) ?></span></div><? } ?>
                <? if (BB_WITH_ORDERS) { ?>
                    <form class="addProductToBasket validateForm" action="/winkelwagen" method="post">
                        <?
                    }
                    if (!empty($aSizes)) {
                        echo '<div>maat</div>';
                        // with colors
                        echo '<select onchange="updateDetails(true);" id="catalogProductSizeId" name="catalogProductSizeId">';
                        foreach ($aSizes AS $oSize) {
                            echo '<option ' . ($oCheapestProductColorSizeRelation->catalogProductSizeId == $oSize->catalogProductSizeId ? 'selected' : '') . ' value="' . $oSize->catalogProductSizeId . '">' . $oSize->name . '</option>';
                        }
                        echo '</select>';
                    }
                    if (!empty($aColors)) {
                        echo '<div>kleur</div>';
                        // with colors
                        echo '<select onchange="updateDetails(false);" id="catalogProductColorId" name="catalogProductColorId">';
                        echo '<option value="">-- kies een kleur --</option>';
                        foreach ($aColors AS $oColor) {
                            echo '<option ' . ($oCheapestProductColorSizeRelation->catalogProductColorId == $oColor->catalogProductColorId ? 'selected' : '') . ' value="' . $oColor->catalogProductColorId . '">' . $oColor->name . '</option>';
                        }
                        echo '</select>';
                    }
                    if (BB_WITH_ORDERS) {
                        ?>
                        <div>aantal</div>
                        <div class="orderStatusUpdate statusUpdate"><?= !empty($sOrderStatusUpdate) ? $sOrderStatusUpdate : '' ?></div>
                        <input type="hidden" name="action" value="addProductToBasket" />
                        <input type="hidden" name="catalogProductId" value="<?= $oProduct->catalogProductId ?>" />
                        <input type="text" name="amount" value="1" style="margin-top:-3px;width:50px;border-radius:0; padding:2px;"/>
                        <input type="submit" name="addProductToBasket" class="read-more" value="In winkelwagen" title="Bestel nu de <?= _e($oProduct->name) ?>" />
                    </form>
                    <?
                    if (!empty($aSizes) || !empty($aColors)) {
                        if (!empty($aSizes) && !empty($aColors)) {
                            $sText = 'Maat of kleur';
                        } elseif (!empty($aSizes)) {
                            $sText = 'Maat ';
                        } elseif (!empty($aColors)) {
                            $sText = 'Kleur ';
                        }
                        echo '<div style="font-size:12px; margin-top: 10px;" >' . $sText . ' niet beschikbaar? <a href="/contact">Mail ons.</a></div>';
                    }
                }
                ?>
            </div>
            <br class="clear">
            <div class="tabs">
                <div class="tabsHolder cf unselectable"></div>
                <div class="tabDetails cf"  data-prestatietabs-name="info">
                    <div class="tabLabel unselectable">Informatie</div>
                    <div class="tabContent" itemprop="description">
                        <?= $oProduct->description ?>
                    </div>
                </div>
                <? if (count($oProduct->getPropertyTypeGroups()) > 0) { ?>
                    <div class="tabDetails cf" data-prestatietabs-name="props">
                        <div class="tabLabel unselectable">Specificaties</div>
                        <div class="tabContent">
                            <table>
                                <?
                                foreach ($oProduct->getPropertyTypeGroups() as $oPropertyTypeGroup) {
                                    if (count($oPropertyTypeGroup->getPropertyTypes()) > 0) {
                                        echo '<tr><td colspan="2" style="padding-right: 10px;"><b>' . _e($oPropertyTypeGroup->title) . '</b></td></tr>';
                                        foreach ($oPropertyTypeGroup->getPropertyTypes() AS $oPropertyType) {
                                            $aPropertyValues = $oPropertyType->getValuesByProductId($oProduct->catalogProductId);
                                            if (!empty($aPropertyValues)) {
                                                echo '<tr>';
                                                echo '<td style="padding-right: 10px;">' . _e($oPropertyType->title) . ': </td>';
                                                echo '<td>';
                                                foreach ($aPropertyValues AS $oPropertyValue) {
                                                    echo _e($oPropertyValue->value) . '<br />';
                                                }
                                                echo '</td>';
                                                echo '</tr>';
                                            }
                                        }
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                <? } ?>
                <div class="tabDetails cf" data-prestatietabs-name="share">
                    <div class="tabLabel unselectable">Delen</div>
                    <div class="tabContent">
                        Delen op social media:
                        <!-- Facebook -->
                        <div class="product-detail-share">
                            <div class="fb-share-button-delayed" data-type="button"></div>
                        </div>
                        <div class="product-detail-share">
                            <!-- Twitter -->
                            <a href="https://twitter.com/share" class="twitter-share-button" data-lang="nl" data-count="none">Tweeten</a>
                        </div>
                    </div>
                </div>
            </div>
            <br class="clear">
            <a href="<?= $sOverviewLink ?>" class="read-more">Naar productoverzicht</a>
            <br class="clear"><br/>
        </div>
        <?
        $aImages = $oProduct->getImages();
        if (!empty($aImages)) {
            ?>
            <div class="product-detail column-50 float-left">
                <figure class="product-detail-image">
                    <?
                    if (!empty($aImages[0])) {
                        while (!(($oImageFileCrop = $aImages[0]->getImageFileByReference('crop_detail')) && $aImages[0]->getImageFileByReference('crop_detail') && ($oImageFileOri = $aImages[0]->getImageFileByReference('detail')))) {
                            array_shift($aImages); // remove first array element (just used above)
                        }
                        if (!empty($oImageFileCrop) && !empty($oImageFileOri)) {
                            echo '<a class="fancyBoxLink" id="product-image" data-thumbindex="' . $aImages[0]->imageId . '" href="' . $oImageFileOri->link . '"><img class="crop-detail" itemprop="image" style="width:100%; height:auto;" src="' . $oImageFileCrop->link . '" alt="' . _e($oImageFileCrop->title) . '" ' . $oImageFileCrop->imageSizeAttr . ' /><span class="image-zoom"><img src="/images/layout/zoom-icon.png" with="20" height="20" alt="Afbeelding vergroten"></span></a>';
                        }
                    }
                    ?>
                </figure>
                <? if (count($aImages) > 0) { ?>
                    <div class="<?= count($aImages) == 1 ? 'hide' : '' ?>">
                        <?
                        $iProductCounter = 0;
                        $iProductCounter2 = 0;
                        foreach ($aImages As $oImage) {
                            $iProductCounter++;
                            $iProductCounter2++;
                            $oImageFileCrop = $oImage->getImageFileByReference('crop_thumb');
                            $oImageFileOri = $oImage->getImageFileByReference('detail');
                            $oImageFileCropDetail = $oImage->getImageFileByReference('crop_detail');
                            if (!empty($oImageFileCrop) && !empty($oImageFileOri) && !empty($oImageFileCropDetail)) {
                                echo '<div class="product-detail-image-small column-25 float-left"><a class="fancyBoxLink" data-fancybox-group="1" href="' . $oImageFileOri->link . '"><img class="product-image-thumb" data-thumbindex="' . $oImage->imageId . '" data-cropdetailsrc="' . $oImageFileCropDetail->link . '" data-detailsrc="' . $oImageFileOri->link . '" src="' . $oImageFileCrop->link . '" alt="' . _e($oImageFileCrop->title) . '" width="100%"/></a></div>';
                            }
                            if ($iProductCounter == 4) {
                                echo '<br class="hide-on-mobile clear">';
                                $iProductCounter = 0;
                            } else {
                                echo '<div class="hide-on-mobile column-width float-left">&nbsp;</div>';
                            }
                            if ($iProductCounter2 == 2) {
                                echo '<br class="hide-on-tablet hide-on-desktop clear">';
                                $iProductCounter2 = 0;
                            } else {
                                echo '<div class="hide-on-tablet hide-on-desktop column-width float-left">&nbsp;</div>';
                            }
                        }
                        ?>
                    </div>
                <? } ?>
            </div>
            <div class="column-width float-left">&nbsp;</div>
        <? } ?>
    </section>
</div>
<?
$sJavascript = <<<EOT
<script>
    var facebookLikeActivated = false;
    // initiate prestatie tabs
    $('.tabs').prestatieTabs({
        onAfterTabOpen: function(element){
            if(!facebookLikeActivated && $(element).data('prestatietabs-name') == 'share'){
                try{
                    $('.fb-share-button-delayed').replaceWith('<div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>');
                    FB.XFBML.parse(); 
                    facebookLikeActivated = true;
                }catch(ex){}
            }
        }
    });
    
    if(!Modernizr.touch){
        // no touch, on hover show thumb in big image
        $('.product-detail-image-small img').hover(function(){
            $('#product-image img.crop-detail').attr('src', $(this).data('cropdetailsrc'));
            $('#product-image').attr('href', $(this).data('detailsrc')).data('thumbindex', $(this).data('thumbindex'));
        });
    }else{
        // on touch device do not show first thumb, has no added value
        //$('.product-detail-image-small:first').hide();
    }
    
    $('#product-image').click(function(e){
        // click on big image simulates click on thumb to open fancybox without double images through data-fancybox-group
        $('.product-detail-image-small img[data-thumbindex="' + $(this).data('thumbindex') + '"]').click();
        e.preventDefault();
    });
    
    var updateDetailsRequest = null;
    function updateDetails(resetColors){                          
        if(updateDetailsRequest)
            updateDetailsRequest.abort();
            
        var catalogProductId = '$oProduct->catalogProductId';
        var catalogProductColorId = $('#catalogProductColorId').size() ? $('#catalogProductColorId').val() : ''
        var catalogProductSizeId = $('#catalogProductSizeId').size() ? $('#catalogProductSizeId').val() : '';

        updateDetailsRequest = $.ajax({
            async: false,
            url: '/producten/ajax-getProductDetails',
            data: 'catalogProductId=' + catalogProductId + '&catalogProductColorId=' + catalogProductColorId + '&catalogProductSizeId=' + catalogProductSizeId + '&resetColors=' + resetColors,
            type: 'POST',
            success: function(data){
                try{
                    var dataObj = $.parseJSON(data);
                    if(dataObj.aColors.length > 0){
                        var sHTML = '';
                        sHTML += '<option value="">-- kies een kleur --</option>';
                        for(var i = 0; i < dataObj.aColors.length; i++){
                            var oColor = dataObj.aColors[i];
                            sHTML += '<option ' + (oColor.disabled ? 'disabled' : '') + ' ' + (oColor.selected ? 'selected' : '') + ' value="' + oColor.catalogProductColorId + '">' + oColor.name + '</option>';
                        }
                        $('#catalogProductColorId').html(sHTML);
                    }
                    
                    if(dataObj.aSizes.length > 0){
                        var sHTML = '';
                        for(var i = 0; i < dataObj.aSizes.length; i++){
                            var oSize = dataObj.aSizes[i];
                            sHTML += '<option ' + (oSize.disabled ? 'disabled' : '') + ' ' + (oSize.selected ? 'selected' : '') + ' value="' + oSize.catalogProductSizeId + '">' + oSize.name + '</option>';
                        }
                        $('#catalogProductSizeId').html(sHTML);
                    }
                    
                    $('.originalPriceInVAT').html(dataObj.saleOriginalPriceInclVAT);
                    $('.priceInVAT').html(dataObj.salePriceInclVAT);
                    $('.priceExVAT').html(dataObj.salePriceExclVAT);

                }catch(err){
                    alert('Er is een fout opgetreden bij het ophalen van de voorraden, ververs de pagina en probeer nog eens.');
                }
            }
        });
    }
    updateDetails(false);
</script>
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);
?>