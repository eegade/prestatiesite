<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page order">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="head">
            <h1><?= _e($oPage->title) ?></h1>
        </div>
        <?= $oPage->content ?>
        <div>
            <form method="POST" action="<?= getCurrentUrlPath() ?>" class="validateForm">
                <input type="hidden" value="confirm" name="action" />
                <?
                $aErrors = array();
                if (!empty($aErrorsActivate)) {
                    $aErrors = $aErrorsActivate;
                }
                include TEMPLATES_FOLDER . '/elements/errorBox.inc.php';
                ?>
                <table style="line-height: 30px;">
                    <tr>
                        <td style="width: 150px;"><label for="email">E-mail *</label></td>
                        <td><input class="required email" id="email" type="email" name="email" title="Vul uw e-mailadres in" value="<?= _e(http_post('email', http_session('accountConfirmEmail'))) ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="code">Bevestigingscode *</label></td>
                        <td><input class="required" id="code" type="text" name="code" title="Vul uw code in, deze ontvangt u per e-mail. Code nog niet ontvnagen, check voor de zekerheid uw SPAM folder" value="<?= _e(http_post('code')) ?>" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input type="submit" class="read-more" value="Aanmelden" name="confirm" />
                    </tr>
                </table>
            </form>
        </div>
    </section>
</div>