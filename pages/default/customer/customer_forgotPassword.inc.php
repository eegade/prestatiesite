<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page order">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="head">
            <h1><?= _e($oPage->title) ?></h1>
        </div>
        <?= $oPage->content ?>
        <div>
            <form method="POST" action="<?= getCurrentUrlPath() ?>" class="validateForm">
                <input type="hidden" value="requestPassword" name="action" />
                <?
                $aErrors = array();
                if (!empty($aErrorsForgotPass)) {
                    $aErrors = $aErrorsForgotPass;
                }
                include TEMPLATES_FOLDER . '/elements/errorBox.inc.php';
                ?>
                <table style="line-height: 30px;">
                    <tr>
                        <td style="width: 120px;"><label for="email">E-mail *</label></td>
                        <td><input class="required" title="Vul uw e-mailadres in" id="email" type="email" name="email" value="<? http_post('email') ?>" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input class="read-more" type="submit" value="Stuur mij een reset link" name="requestPassword" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </section>
</div>