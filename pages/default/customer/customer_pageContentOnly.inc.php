<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page order">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="head">
            <h1><?= _e($oPage->title) ?></h1>
        </div>
        <?= $oPage->content ?>
    </section>
</div>