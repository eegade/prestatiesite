<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page order">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="head">
            <h1><?= _e($oPage->title) ?></h1>
        </div>
        <?= $oPage->content ?>
        <div>
            <form method="POST" action="<?= getCurrentUrlPath() ?>" class="validateForm">
                <input type="hidden" value="updatePassword" name="action" />
                <?
                // hack for multiple errorBoxes on one page
                $aErrors = array();
                if (!empty($aErrorsChangePass))
                    $aErrors = $aErrorsChangePass;
                include TEMPLATES_FOLDER . '/elements/errorBox.inc.php';
                ?>
                <table style="line-height: 30px;">
                    <tr>
                        <td style="width: 230px;"><label for="email">E-mail *</label></td>
                        <td><input autocomplete="off" id="email" type="email" name="email" value="<?= http_post('email', http_get('email')) ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="code">Bevestigingscode *</label></td>
                        <td><input id="code" type="text" name="code" value="<?= http_post('code', http_get('code')) ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="password">Nieuw wachtwoord *</label></td>
                        <td><input id="password" class="required" data-rule-minlength="8" title="Vul een veilig password in met minimaal 8 tekens" type="password" name="password" value="" /></td>
                    </tr>
                    <tr>
                        <td><label for="confirmPassword">Wachtwoord nogmaals invoeren *</label></td>
                        <td><input id="confirmPassword" data-rule-equalTo="#password" title="De twee wachtwoordvelden komen niet met elkaar overeen" type="password" name="confirmPassword" value="" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="submit" class="read-more" value="Wachtwoord wijzigen" name="updatePassword" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </section>
</div>