<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page order">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="head">
            <h1><?= _e($oPage->title) ?></h1>
            <?= $oPage->content ?>
        </div>
        <? if (!empty($oPageForMenu) && $oPageForMenu->getSubPages()) { ?>
            <? include TEMPLATES_FOLDER . '/elements/pagesSubmenu.inc.php'; ?>
        <? } ?>
        <div class="column-75 float-right">
            <div class="account-column">
                <div class="accountStatusUpdate statusUpdate"></div>
                <form method="POST" action="<?= getCurrentUrlPath() ?>" class="validateForm">
                    <input type="hidden" value="save" name="action" />
                    <?
                    $aErrors = array();
                    if (!empty($aCustomerErrors)) {
                        $aErrors = $aCustomerErrors;
                    }
                    include TEMPLATES_FOLDER . '/elements/errorBox.inc.php';
                    ?>
                    <table>
                        <tr>
                            <td style="width: 120px;"><label for="email">E-mail *</label></td>
                            <td><input id="email" class="required email" data-rule-remote="/<?= http_get('controller') ?>?ajax=checkEmail&customerId=<?= $oCustomer->customerId ?>" data-msg-remote="Het opgegeven e-mailadres is al in gebruik" title="Vul een geldig en ongebruikt e-mailadres in" type="email" name="email" value="<?= _e($oCustomer->email) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="companyName">Bedrijfsnaam</label></td>
                            <td><input id="companyName" type="text" name="companyName" value="<?= _e($oCustomer->companyName) ?>" /></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><label>Geslacht *</label></td>
                            <td>
                                <input class="required" title="Kies een geslacht" type="radio" <?= $oCustomer->gender == 'M' ? 'CHECKED' : '' ?> id="gender_M" name="gender" value="M" /> <label for="gender_M">Man</label><br />
                                <input class="required" title="Kies een geslacht" type="radio" <?= $oCustomer->gender == 'F' ? 'CHECKED' : '' ?> id="gender_F" name="gender" value="F" /> <label for="gender_F">Vrouw</label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="firstName">Voornaam *</label></td>
                            <td><input id="firstName" class="required" title="Vul uw voornaam in" type="text" name="firstName" value="<?= _e($oCustomer->firstName) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="insertion">Tussenvoegsel</label></td>
                            <td><input id="insertion" type="text" name="insertion" value="<?= _e($oCustomer->insertion) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="lastName">Achternaam *</label></td>
                            <td><input id="lastName" class="required" title="Vul uw achternaam in" type="text" name="lastName" value="<?= _e($oCustomer->lastName) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="address">Adres *</label></td>
                            <td><input id="address" class="required" title="Vul uw adres in" type="text" name="address" value="<?= _e($oCustomer->address) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="houseNumber">Huisnummer *</label></td>
                            <td><input id="houseNumber" class="required digits" title="Vul uw huisnummer in" type="text" name="houseNumber" value="<?= _e($oCustomer->houseNumber) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="houseNumberAddition">Toevoeging</label></td>
                            <td><input id="houseNumberAddition" type="text" name="houseNumberAddition" value="<?= _e($oCustomer->houseNumberAddition) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="postalCode">Postcode *</label></td>
                            <td><input id="postalCode" class="required" title="Vul uw postcode in" type="text" name="postalCode" value="<?= _e($oCustomer->postalCode) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="city">Plaats *</label></td>
                            <td><input id="city" class="required" title="Vul uw plaats in" type="text" name="city" value="<?= _e($oCustomer->city) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="phone">Telefoon</label></td>
                            <td><input id="phone" type="tel" name="phone" value="<?= _e($oCustomer->phone) ?>" /></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <input class="read-more" type="submit" value="Opslaan" name="save" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </section>
</div>
<?
$sJavascript = <<<EOT
<script>

    var statusUpdate = '$sAccountStatusUpdate';
    if(statusUpdate){
        showStatusUpdate(statusUpdate, $('.accountStatusUpdate'));
    }
   
</script>
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);
?>