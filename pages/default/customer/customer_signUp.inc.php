<? include TEMPLATES_FOLDER . '/elements/headerImageDefault.inc.php'; ?>
<div class="default-container-width clearfix">
    <section class="page order">
        <nav class="breadcrumbs">U bevindt zich hier: <?= $oPageLayout->sCrumblePath ?></nav>
        <div class="head">
            <h1><?= _e($oPage->title) ?></h1>
        </div>
        <?= $oPage->content ?>
        <div class="column-50 float-left">
            <div class="account-column">
                <div>
                    <h2>Bent u al klant?</h2>
                    <form method="POST" action="<?= getCurrentUrlPath() ?>" class="validateForm">
                        <input type="hidden" value="login" name="action" />
                        <?
                        // hack for multiple errorBoxes on one page
                        $aErrors = array();
                        if (!empty($aErrorsLogin))
                            $aErrors = $aErrorsLogin;
                        include TEMPLATES_FOLDER . '/elements/errorBox.inc.php';
                        ?>
                        <table>
                            <tr>
                                <td style="width: 120px;"><label for="signup-login-email">E-mail *</label></td>
                                <td><input class="required email" id="signup-login-email" title="Vul uw e-mailadres in" type="email" name="email" value="" /></td>
                            </tr>
                            <tr>
                                <td><label for="signup-login-password">Wachtwoord *</label></td>
                                <td><input class="required" id="signup-login-password" title="Vul uw wachtwoord in" type="password" name="password" value="" /></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <input class="read-more" type="submit" value="Inloggen" name="login" />
                                </td>
                            </tr>
                        </table>
                    </form>
                    <p><a href="/<?= http_get('controller') ?>/wachtwoord-vergeten">Bent u uw wachtwoord vergeten?</a></p>
                </div>
            </div>
        </div>
        <div class="column-50 float-right">
            <div class="account-column">
                <h2>Wilt u klant worden?</h2>
                <form method="POST" action="<?= getCurrentUrlPath() ?>" class="validateForm">
                    <input type="hidden" value="register" name="action" />
                    <?
                    // hack for multiple errorBoxes on one page
                    $aErrors = array();
                    if (!empty($aErrorsSignup))
                        $aErrors = $aErrorsSignup;
                    include TEMPLATES_FOLDER . '/elements/errorBox.inc.php';
                    ?>
                    <table>
                        <tr>
                            <td style="width: 160px;"><label for="signup-email">E-mail *</label></td>
                            <td><input id="signup-email" class="required email" data-rule-remote="/<?= http_get('controller') ?>?ajax=checkEmail" data-msg-remote="Dit e-mailadres is al in gebruik" data-msg-email="Vul een geldig e-mailadres in" title="Vul een geldig en ongebruikt email-adres in" type="email" name="email" value="<?= _e($oCustomer->email) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="signup-password">Gewenst wachtwoord *</label></td>
                            <td><input id="signup-password" class="required" data-rule-minlength="8" title="Vul een veilig wachtwoord in met minimaal 8 tekens" type="password" name="password" value="" /></td>
                        </tr>
                        <tr>
                            <td><label for="signup-confirmPassword">Nogmaals wachtwoord *</label></td>
                            <td><input id="signup-confirmPassword" class="required" data-rule-equalTo="#signup-password" data-msg-equalTo="Wachtwoorden komen niet overeen" title="Vul uw wachtwoord nogmaals in" type="password" value="" /></td>
                        </tr>
                        <tr>
                            <td><label for="signup-companyName">Bedrijfsnaam</label></td>
                            <td><input id="signup-companyName" type="text" name="companyName" value="<?= _e($oCustomer->companyName) ?>" /></td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><label>Geslacht *</label></td>
                            <td>
                                <input class="required" title="Kies een geslacht" type="radio" <?= $oCustomer->gender == 'M' ? 'CHECKED' : '' ?> id="signup-gender_M" name="gender" value="M" /> <label for="signup-gender_M">Man</label><br />
                                <input class="required" title="Kies een geslacht" type="radio" <?= $oCustomer->gender == 'F' ? 'CHECKED' : '' ?> id="signup-gender_F" name="gender" value="F" /> <label for="signup-gender_F">Vrouw</label>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="signup-firstName">Voornaam *</label></td>
                            <td><input id="signup-firstName" class="required" title="Vul uw voornaam in" type="text" name="firstName" value="<?= _e($oCustomer->firstName) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="signup-insertion">Tusenvoegsel</label></td>
                            <td><input id="signup-insertion" type="text" name="insertion" value="<?= _e($oCustomer->insertion) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="signup-lastName">Achternaam *</label></td>
                            <td><input id="signup-lastName" class="required" title="Vul uw achternaam in" type="text" name="lastName" value="<?= _e($oCustomer->lastName) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="signup-address">Adres *</label></td>
                            <td><input id="signup-address" class="required" title="Vul uw adres in" type="text" name="address" value="<?= _e($oCustomer->address) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="signup-houseNumber">Huisnummer *</label></td>
                            <td><input id="signup-houseNumber" class="required digits" title="Vul uw huisnummer in" type="text" name="houseNumber" value="<?= _e($oCustomer->houseNumber) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="signup-houseNumberAddition">Toevoeging</label></td>
                            <td><input id="signup-houseNumberAddition" type="text" name="houseNumberAddition" value="<?= _e($oCustomer->houseNumberAddition) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="signup-postalCode">Postcode *</label></td>
                            <td><input id="signup-postalCode" class="required" title="Vul uw postcode in" type="text" name="postalCode" value="<?= _e($oCustomer->postalCode) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="signup-city">Plaats *</label></td>
                            <td><input id="signup-city" class="required" title="Vul uw plaats in" type="text" name="city" value="<?= _e($oCustomer->city) ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="signup-phone">Telefoon</label></td>
                            <td><input id="signup-phone" type="tel" name="phone" value="<?= _e($oCustomer->phone) ?>" /></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <input class="read-more" type="submit" value="Aanmelden" name="register" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </section>
</div>