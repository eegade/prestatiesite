<?

# get micro time to check what time it takes to load the PHP
$iSMT = microtime(true);

// To see what the document root is you can use next line (not working with cron jobs)
//echo $_SERVER['DOCUMENT_ROOT'];
# set document root
define("DOCUMENT_ROOT", '/home/prestatie/domains/dev.prestatiesite.nl/public_html');

# for checking access in controllers
define("ACCESS", 'xXx');

# include configuration file
include_once DOCUMENT_ROOT . '/inc/config.inc.php';

# login is required for all admin activities
include_once ADMIN_INC_FOLDER . '/loginControl.inc.php';

# get the controller from the $_GET ( is module name)
$sControllerRequest = strtolower(http_get("controller", ''));

# Key = 'module name', Value = 'path to controller file'
$aControllers = array(
    "" => ADMIN_FOLDER . "/controllers/home.cont.php",
    "login" => ADMIN_FOLDER . "/controllers/login.cont.php",
    "gebruikers" => ADMIN_FOLDER . "/controllers/user.cont.php",
    "modules" => ADMIN_FOLDER . "/controllers/module.cont.php",
    "paginas" => ADMIN_FOLDER . "/controllers/page.cont.php",
    "form-backups" => ADMIN_FOLDER . "/controllers/formBackup.cont.php",
    "imagemanagement" => ADMIN_FOLDER . "/controllers/imageManagement.cont.php",
    "filemanagement" => ADMIN_FOLDER . "/controllers/fileManagement.cont.php",
    "linkmanagement" => ADMIN_FOLDER . "/controllers/linkManagement.cont.php",
    "youtubelinkmanagement" => ADMIN_FOLDER . "/controllers/youtubeLinkManagement.cont.php",
    "crop" => ADMIN_FOLDER . "/controllers/crop.cont.php",
    "brandbox" => ADMIN_FOLDER . "/controllers/brandboxItem.cont.php",
    "instellingen" => ADMIN_FOLDER . "/controllers/settings.cont.php",
    "templates-beheer" => ADMIN_FOLDER . "/controllers/template.cont.php",
    "tweets2db" => ADMIN_FOLDER . "/controllers/tweetsToDb.cont.php",
    "doorlopende-slider" => ADMIN_FOLDER. "/controllers/continuousSlider.cont.php"
);

if (BB_WITH_COUPONS){    
    $aControllers["coupon"] = ADMIN_FOLDER . "/controllers/coupon.cont.php";
}

if (BB_WITH_AGENDA_ITEMS){
    $aControllers["agenda"] = ADMIN_FOLDER . "/controllers/agendaItem.cont.php";
}

if (BB_WITH_NEWS) {
    $aControllers["nieuws"] = ADMIN_FOLDER . "/controllers/newsItem/newsItem.cont.php";
    if (BB_WITH_NEWS_CATEGORIES) {
        $aControllers["nieuws-categorieen"] = ADMIN_FOLDER . "/controllers/newsItem/newsItemCategory.cont.php";
    }
}
if (BB_WITH_PHOTO_ABLUMS) {
    $aControllers["fotoalbums"] = ADMIN_FOLDER . "/controllers/photoAlbum.cont.php";
}

if (BB_WITH_CATALOG) {
    $aControllers["catalogus"] = ADMIN_FOLDER . "/controllers/catalog/catalogProduct.cont.php";
    $aControllers["catalogus-merken"] = ADMIN_FOLDER . "/controllers/catalog/catalogBrand.cont.php";
    $aControllers["catalogus-producttypes"] = ADMIN_FOLDER . "/controllers/catalog/catalogProductType.cont.php";
    $aControllers["catalogus-producteigenschappen"] = ADMIN_FOLDER . "/controllers/catalog/catalogProductPropertyType.cont.php";
    $aControllers["catalogus-categorieen"] = ADMIN_FOLDER . "/controllers/catalog/catalogProductCategory.cont.php";
    $aControllers["catalogus-producteigenschap-groepen"] = ADMIN_FOLDER . "/controllers/catalog/catalogProductPropertyTypeGroup.cont.php";
    $aControllers["catalogus-productkleuren"] = ADMIN_FOLDER . "/controllers/catalog/catalogProductColor.cont.php";
    $aControllers["catalogus-productmaten"] = ADMIN_FOLDER . "/controllers/catalog/catalogProductSize.cont.php";
}

if (BB_WITH_ORDERS) {
    $aControllers["bestellingen"] = ADMIN_FOLDER . "/controllers/order/order.cont.php";
    $aControllers["verzendmethoden"] = ADMIN_FOLDER . "/controllers/order/deliveryMethod.cont.php";
    $aControllers["betaalmethoden"] = ADMIN_FOLDER . "/controllers/order/paymentMethod.cont.php";
}

if (BB_WITH_CUSTOMERS) {
    $aControllers["klanten"] = ADMIN_FOLDER . "/controllers/customer.cont.php";
}
if (BB_WITH_ADVANCED_SITEMAP) {
    $aControllers["sitemap-uitgebreid"] = ADMIN_FOLDER . "/controllers/advancedSitemap.cont.php";
}

$aExceptions = array("login", "imagemanagement", "crop", "filemanagement", "linkmanagement", "youtubelinkmanagement");

# if there is a controller request, check rights and access
if ($sControllerRequest) {
    # module check
    if (!in_array($sControllerRequest, $aExceptions)) {
        # if not an exception, check user rights
        if (!$oCurrentUser->hasRightsFor($sControllerRequest) || !ModuleManager::isActive($sControllerRequest)) {
            showHttpError(403);
        }
    }
}

# get the controller 'path to file' from the controller array
if (array_key_exists($sControllerRequest, $aControllers)) {
    $sControllerPath = $aControllers[$sControllerRequest];
} else {
    # Path found? GO
    if (empty($sControllerPath) && !empty($sControllerRequest)) {
        # non existing controller request and page
        showHttpError(404);
    }
}

if (file_exists(DOCUMENT_ROOT . $sControllerPath)) {
    include_once DOCUMENT_ROOT . $sControllerPath;
} else {
    showHttpError(404);
}

# get micro time to check what time it takes to load the PHP
$iEMT = microtime(true);
if (DEBUG) {
    //echo "Time to load PHP: " . ($iEMT - $iSMT) . " sec";
}
?>