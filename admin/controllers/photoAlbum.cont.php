<?

# check if controller is required by index.php
if (!defined('ACCESS'))
    die;

# reset crop settings
unset($_SESSION['aCropSettings']);

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Fotoalbums";
$oPageLayout->sModuleName = "Fotoalbums";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {


    # set crop referrer for photo album module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oPhotoAlbum = PhotoAlbumManager::getPhotoAlbumById(http_get("param2"));
        if (!$oPhotoAlbum)
            http_redirect(ADMIN_FOLDER . "/");
        # is editable?
        if (!$oPhotoAlbum->isEditable()) {
            $_SESSION['statusUpdate'] = 'Foto album kan niet worden bewerkt'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
        }
    } else {
        $oPhotoAlbum = new PhotoAlbum();
    }

    # action = save
    if (http_post("action") == 'save') {

        # load data in object
        $oPhotoAlbum->_load($_POST);

        # set some properties after load, _load strips tags for all inputs
        $oPhotoAlbum->content = http_post('content');


        # if object is valid, save
        if ($oPhotoAlbum->isValid()) {
            PhotoAlbumManager::savePhotoAlbum($oPhotoAlbum); //save photo album
            $_SESSION['statusUpdate'] = 'Fotoalbum is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oPhotoAlbum->photoAlbumId);
        } else {
            Debug::logError("", "Photo album module php validate error", __FILE__, __LINE__, "Tried to save PhotoAlbum with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $oPageLayout->sStatusUpdate = 'Fotoalbum is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    # action saveImage
    if (http_post("action") == 'saveImage') {

        # for upload from SWFUpload
        if (http_post("SWFUpload")) {
            $oResObj = new stdClass();
            $oResObj->success = false;
            session_id(http_post("PHPSESSID"));

            $bCheckMime = false;
        }

        # upload file or return error
        $oUpload = new Upload($_FILES['image'], PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/original/', null, array('jpg', 'png', 'gif', 'jpeg'), $bCheckMime);

        # save image to database on success
        if ($oUpload->bSuccess === true) {

            // resize original to acceptable size (1500*1500 max)
            if (!ImageManager::resizeImage(DOCUMENT_ROOT . $oUpload->sNewFilePath, DOCUMENT_ROOT . $oUpload->sNewFilePath, BB_IMAGE_ORIGINAL_W, BB_IMAGE_ORIGINAL_H, $sErrorMsg, false, 100)) {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar original resize niet gemaakt: ' . $sErrorMsg; //error resizing image
            }
            # make Image object and save
            $oImage = new Image();

            $oImageFile = new ImageFile();
            $oImageFile->link = $oUpload->sNewFilePath;
            $oImageFile->title = http_post('title', '');
            $oImageFile->name = $oUpload->sNewFileBaseName;
            $oImageFile->mimeType = $oUpload->sMimeType;
            $oImageFile->size = $oUpload->iSize;
            $oImageFile->reference = 'original';

            $aImageFiles[] = $oImageFile;

            # always make detail image (resized of original)
            if (ImageManager::resizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/detail/' . $oImageFile->name, BB_IMAGE_DETAIL_W, BB_IMAGE_DETAIL_H, $sErrorMsg, false)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/detail/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'detail';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar detail afbeelding NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            # make crop_small
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/crop_small/' . $oImageFile->name, BB_PHOTO_ALBUM_CROP_SMALL_W, BB_PHOTO_ALBUM_CROP_SMALL_H, $sErrorMsg, true)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/crop_small/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'crop_small';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            # make cms_thumb
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/cms_thumb/' . $oImageFile->name, 100, (100 * BB_PHOTO_ALBUM_CROP_SMALL_H / BB_PHOTO_ALBUM_CROP_SMALL_W), $sErrorMsg, true)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/cms_thumb/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'cms_thumb';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }
            $oImage->setImageFiles($aImageFiles); //set imageFiles to Image object
            # save image
            ImageManager::saveImage($oImage);

            # save photoalbum image relation
            PhotoAlbumManager::savePhotoAlbumImageRelation($oPhotoAlbum->photoAlbumId, $oImage->imageId);

            # for SWFUpload
            if (http_post("SWFUpload")) {
                $oResObj->success = true;
                $oImageFileThumb->imageId = $oImage->imageId; // set for use in SWFUpload
                // add some extra values
                $oImageFileThumb->isOnlineChangeable = $oImage->isOnlineChangeable();
                $oImageFileThumb->isCropable = $oImage->isCropable();
                $oImageFileThumb->isEditable = $oImage->isEditable();
                $oImageFileThumb->isDeletable = $oImage->isDeletable();
                $oImageFileThumb->hasNeededImageFiles = $oImage->hasImageFiles(array('cms_thumb', 'crop_small'));

                $oResObj->imageFile = $oImageFileThumb; // for adding image to list (last imageFile object)
                print json_encode($oResObj);
                die;
            }

            # back to edit
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oPhotoAlbum->photoAlbumId);
        } else {

            # for SWFUpload
            if (http_post("SWFUpload")) {
                $oResObj->errorMsg = $oUpload->getErrorMessage();
                print json_encode($oResObj);
                die;
            }

            $_SESSION['statusUpdate'] = 'Afbeelding kon niet worden geüpload: ' . $oUpload->getErrorMessage(); //error uploading file
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oPhotoAlbum->photoAlbumId);
        }
    }

    # set settings for image management
    $oImageManagerHTML = new ImageManagerHTML();
    $oImageManagerHTML->aImages = $oPhotoAlbum->getImages('all');
    $oImageManagerHTML->cropLink = ADMIN_FOLDER . '/' . http_get('controller') . '/crop-image';
    $oImageManagerHTML->sUploadUrl = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oPhotoAlbum->photoAlbumId;
    $oImageManagerHTML->aNeededImageFileReferences = array('cms_thumb', 'crop_small');
    $oImageManagerHTML->bShowCropAfterUploadOption = false;
    $oImageManagerHTML->bMultipleFileUpload = true;
    $oImageManagerHTML->aMultipleFileUploadAllowedExtensions = array('png', 'jpg', 'gif', 'jpeg');
    $oImageManagerHTML->bCoverImageShow = true;
    $oImageManagerHTML->oCoverImageImageFile = $oPhotoAlbum->getImages('cover') ? $oPhotoAlbum->getImages('cover')->getImageFileByReference('cms_thumb') : null;
    $oImageManagerHTML->sCoverImageUpdateLink = ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-updateCoverImage/' . $oPhotoAlbum->photoAlbumId;
    $oImageManagerHTML->sCoverImageGetLink = ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-getCoverImage/' . $oPhotoAlbum->photoAlbumId;

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/photoAlbums/photoAlbum_form.inc.php';
} elseif (http_get("param1") == 'crop-image' && is_numeric(http_get("imageId"))) {

    $oImage = ImageManager::getImageById(http_get("imageId"));

    if (!$oImage || !$oImage->getImageFileByReference("original")) {
        $_SESSION['statusUpdate'] = 'Afbeelding niet gevonden of heeft geen origineel meer'; //error getting image
        http_redirect(http_session('cropReferrer'));
    }

    # get photoAlbum object for folderName
    $oPhotoAlbum = PhotoAlbumManager::getPhotoAlbumByImageId($oImage->imageId);


    # set crop settings
    $oCropSettings = new CropSettings();
    $oCropSettings->iImageId = $oImage->imageId;
    $oCropSettings->sReferenceName = 'original';

    $oCropSettings->setAspectRatio(BB_PHOTO_ALBUM_CROP_SMALL_W, BB_PHOTO_ALBUM_CROP_SMALL_W);

    $oCropSettings->bShowPreview = true;
    $oCropSettings->iMaxPreviewWidth = BB_PHOTO_ALBUM_CROP_SMALL_W;

    $oCropSettings->sReferrer = http_session('cropReferrer');
    $oCropSettings->sReferrerTekst = 'fotoalbum bewerken';

    # create cropand show on crop form page
    $oCropSettings->addCrop(BB_PHOTO_ALBUM_CROP_SMALL_W, BB_PHOTO_ALBUM_CROP_SMALL_W, PhotoAlbum::IMAGES_PATH . $oPhotoAlbum->folderName . '/crop_small/' . $oImage->getImageFileByReference('original')->name, 'crop_small', true, true);

    # create crop but do not show on crop form page
    $oCropSettings->addCrop(100, (100 * BB_PHOTO_ALBUM_CROP_SMALL_H / BB_PHOTO_ALBUM_CROP_SMALL_W), $oImage->getImageFileByReference('cms_thumb')->link, 'cms_thumb', false, true);

    # add setting to session in an array
    $_SESSION['aCropSettings'][] = clone $oCropSettings;

    http_redirect(ADMIN_FOLDER . '/crop');
} elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {

    if (is_numeric(http_get("param2"))) {
        $oPhotoAlbum = PhotoAlbumManager::getPhotoAlbumById(http_get("param2"));
    }

    if ($oPhotoAlbum && PhotoAlbumManager::deletePhotoAlbum($oPhotoAlbum)) {
        $_SESSION['statusUpdate'] = 'Fotoalbum is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Fotoalbum kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
} elseif (http_get("param1") == 'ajax-setOnline') {

    $bOnline = http_get("online", 0); //no value, set offline by default
    $bAjax = http_get("ajax", false); //controller requested by ajax
    $iPhotoAlbumId = http_get("param2");
    $oResObj = new stdClass(); //standard class for json feedback
    # update online for page
    if (is_numeric($iPhotoAlbumId)) {
        $oResObj->success = PhotoAlbumManager::updateOnlineByPhotoAlbumId($bOnline, $iPhotoAlbumId);
        $oResObj->photoAlbumId = $iPhotoAlbumId;
        $oResObj->online = $bOnline;
    }

    if (!$bAjax) {
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }
    print json_encode($oResObj);
    die;
} elseif (http_get("param1") == 'ajax-updateCoverImage') {
    $iPhotoAlbumId = http_get("param2");
    $iImageId = http_post("imageId");
    $oResObj = new stdClass(); //standard class for json feedback
    # update cover image
    if (is_numeric($iPhotoAlbumId) && is_numeric($iImageId) && ($oPhotoAlbum = PhotoAlbumManager::getPhotoAlbumById($iPhotoAlbumId)) && ($oImage = ImageManager::getImageById($iImageId))) {
        $oResObj->success = PhotoAlbumManager::updateCoverImageByPhotoAlbum($oImage, $oPhotoAlbum);
        $oResObj->imageFile = $oImage->getImageFileByReference('cms_thumb');
    }
    print json_encode($oResObj);
    die;
} elseif (http_get("param1") == 'ajax-getCoverImage') {
    $iPhotoAlbumId = http_get("param2");
    $oResObj = new stdClass(); //standard class for json feedback
    # update cover image
    if (is_numeric($iPhotoAlbumId) && ($oPhotoAlbum = PhotoAlbumManager::getPhotoAlbumById($iPhotoAlbumId))) {
        $oResObj->success = false;
        if (($oImage = $oPhotoAlbum->getImages('cover'))) {
            $oResObj->success = true;
            $oResObj->imageFile = $oImage->getImageFileByReference('cms_thumb');
        }
    }
    print json_encode($oResObj);
    die;
} elseif (http_get('param1') == 'image-list') {
    $oPhotoAlbum = PhotoAlbumManager::getPhotoAlbumById(http_get('param2'));

    if (!$oPhotoAlbum)
        die;

    $aImages = $oPhotoAlbum->getImages('all');
    $aImageFiles = array();
    $aLinks = array();
    foreach ($aImages AS $oImage) {
        $oImageFile = $oImage->getImageFileByReference('detail');
        if (!$oImageFile)
            continue;
        $oLink = new stdClass();
        $oLink->title = $oImageFile->title ? $oImageFile->title : $oImageFile->name;
        $oLink->value = $oImageFile->link;
        $aLinks[] = $oLink;
    }

    die(json_encode($aLinks));
} else {
    #display page overview
    $aPhotoAlbums = PhotoAlbumManager::getPhotoAlbumsByFilter(array('showAll' => true));
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/photoAlbums/photoAlbums_overview.inc.php';
}

# inlcude template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>