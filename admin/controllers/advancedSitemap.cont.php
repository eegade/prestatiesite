<?php

# check if controller is required by index.php
if (!defined('ACCESS'))
    die;

$oPageLayout = new PageLayout();

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once

$oPageLayout->sWindowTitle = "Sitemap uitgebreid";
$oPageLayout->sModuleName = "Sitemap uitgebreid";
$oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/advancedSitemap/advancedSitemap_overview.inc.php';

if ($oCurrentUser->isAdmin() && http_get('executeCron') == 1) {
    ob_start();
    include DOCUMENT_ROOT . CronManager::CRONJOBS_FOLDER . '/advancedSitemap.cron.php';
    $sExecuteLog = ob_get_clean();
    $_SESSION['statusUpdate'] = 'Cron uitgevoerd';
    
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
}

# include default template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>