<?php

# check if controller is required by index.php
if (!defined('ACCESS'))
    die;

$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Tweets2DB";
$oPageLayout->sModuleName = "Tweets2DB";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
// include what normally would be executed by a cronjob
if (isset($_GET['executeCron'])) {
    include_once DOCUMENT_ROOT . ADMIN_FOLDER . '/cronjobs/tweetsToDb.cron.php';
    $_SESSION['statusUpdate'] = 'Tweets opgehaald';
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
}

$oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/tweetsToDb/tweets_overview.inc.php';

# include default template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>