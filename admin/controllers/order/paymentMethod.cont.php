<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Betaalmethoden";
$oPageLayout->sModuleName = "Betaalmethoden";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    # set crop referrer for this module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oPaymentMethod = PaymentMethodManager::getPaymentMethodById(http_get("param2"));
        if (empty($oPaymentMethod) || !$oPaymentMethod->isEditable()) {
            http_redirect(ADMIN_FOLDER . "/" . http_get('param1'));
        }
    } else {
        $oPaymentMethod = new PaymentMethod();
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oPaymentMethod->_load($_POST);

        # if object is valid, save
        if ($oPaymentMethod->isValid()) {
            PaymentMethodManager::savePaymentMethod($oPaymentMethod); //save object
            $_SESSION['statusUpdate'] = 'Betaalmethode is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oPaymentMethod->paymentMethodId);
        } else {
            Debug::logError("", "PaymentMethod module php validate error", __FILE__, __LINE__, "Tried to save PaymentMethod with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Betaalmethode is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/orders/paymentMethods/paymentMethod_form.inc.php';
} elseif (http_get('param1') == 'volgorde-wijzigen') {
    if (http_post('action') == 'saveOrder') {
        if (http_post('order')) {
            $aPaymentMethodIds = explode('|', http_post('order'));
            $iC = 1;
            foreach ($aPaymentMethodIds AS $iPaymentMethodId) {
                $oPaymentMethod = PaymentMethodManager::getPaymentMethodById($iPaymentMethodId);
                $oPaymentMethod->order = $iC;
                if ($oPaymentMethod->isValid()) {
                    PaymentMethodManager::savePaymentMethod($oPaymentMethod);
                }
                $iC++;
            }
        }
        $_SESSION['statusUpdate'] = 'Volgorde is opgeslagen'; //save status update into session
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    // get all items for order changing
    $aPaymentMethods = PaymentMethodManager::getPaymentMethodsByFilter();
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/orders/paymentMethods/paymentMethods_change_order.inc.php';
}

# delete object
elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2")))
        $oPaymentMethod = PaymentMethodManager::getPaymentMethodById(http_get("param2"));

    if (!empty($oPaymentMethod) && PaymentMethodManager::deletePaymentMethod($oPaymentMethod)) {
        $_SESSION['statusUpdate'] = 'Betaalmethode is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Betaalmethode kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
}

# display overview
else {
    $aPaymentMethods = PaymentMethodManager::getPaymentMethodsByFilter();
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/orders/paymentMethods/paymentMethods_overview.inc.php';
}

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>