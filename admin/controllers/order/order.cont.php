<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Bestellingen";
$oPageLayout->sModuleName = "Bestellingen";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once

// get order in PDF format


# handle orderFilter
$aOrderFilter = http_session('orderFilter');
if (http_post('filterForm')) {
    $aOrderFilter = http_post('orderFilter');
    $_SESSION['orderFilter'] = $aOrderFilter;
}

if (http_post('resetFilter') || empty($aOrderFilter)) {
    unset($_SESSION['orderFilter']);
    $aOrderFilter = array();
    $aOrderFilter['orderId'] = '';
    $aOrderFilter['statuses'] = array(
        OrderStatus::STATUS_NEW,
        OrderStatus::STATUS_AWAITING_PAYMENT,
        OrderStatus::STATUS_PAYED,
        OrderStatus::STATUS_READY_FOR_PROCESSING,
        OrderStatus::STATUS_PROCESSING,
        OrderStatus::STATUS_PROCESSED
    );
}


// handle perPage
if (http_post('setPerPage')) {
    $_SESSION['ordersPerPage'] = http_post('perPage');
}

# handle add/edit
if (http_get("param1") == 'bewerken') {
    
    $oOrder = OrderManager::getOrderById(http_get("param2"));
    
    if (empty($oOrder))
        http_redirect(ADMIN_FOLDER . "/");

    if (http_post('action') == 'getPdf') {
        @$oOrder->getPdf()->Output('order-'.$oOrder->orderId.'.pdf', 'D');
        die;
    }
    
    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oOrder->_load($_POST);

        # if object is valid, save
        if ($oOrder->isValid()) {
            OrderManager::saveOrder($oOrder); //save object
            $_SESSION['statusUpdate'] = 'Bestelling is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oOrder->orderId);
        } else {
            Debug::logError("", "Order module php validate error", __FILE__, __LINE__, "Tried to save Order with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Bestelling is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    # define a new OrderStatus to save
    $oStatus = new OrderStatus(array('orderId' => $oOrder->orderId));

    # action = saveStatus
    if (http_post("action") == 'saveStatus') {

        $sNotificationInfo = '';
        if (http_post('comment')) {
            $sNotificationInfo = '<p>Opmerking: <br />' . http_post('comment') . '</p>';
        }
        $oOrder->processStatus(http_post('status'), http_post('notifyCustomer'), false, $sNotificationInfo);
        $_SESSION['statusUpdate'] = 'Status is opgeslagen'; //save status update into session
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oOrder->orderId);
    }
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/orders/order_form.inc.php';
}
 # handle export
elseif (http_get('param1') == 'export') {
        $bWithTax = http_get('withtax');
        // I onlye get the filtered orders
        $aOrders = OrderManager::getOrdersByFilter($aOrderFilter);

        include_once (ADMIN_PAGES_FOLDER . '/orders/orders_export.inc.php');

        die($sExport);
    }
# display overview
else {

    $iPerPage = http_session('ordersPerPage', 10);
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if (!is_numeric($iCurrPage) || $iCurrPage <= 0){
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }
    
    $aOrders = OrderManager::getOrdersByFilter($aOrderFilter, $iPerPage, $iStart, $iFoundRows);
    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/orders/orders_overview.inc.php';
}

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>