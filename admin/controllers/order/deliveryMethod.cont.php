<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Verzendmethoden";
$oPageLayout->sModuleName = "Verzendmethoden";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    # set crop referrer for this module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oDeliveryMethod = DeliveryMethodManager::getDeliveryMethodById(http_get("param2"));
        if (empty($oDeliveryMethod) || !$oDeliveryMethod->isEditable()) {
            http_redirect(ADMIN_FOLDER . "/" . http_get('param1'));
        }
    } else {
        $oDeliveryMethod = new DeliveryMethod();
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oDeliveryMethod->_load($_POST);
        
        $aPaymentMethods = array();
        foreach (http_post('paymentMethodIds', array()) AS $iPaymentMethodId) {
            $aPaymentMethods[] = new PaymentMethod(array('paymentMethodId' => $iPaymentMethodId));
        }
        $oDeliveryMethod->setPaymentMethods($aPaymentMethods);

        # if object is valid, save
        if ($oDeliveryMethod->isValid()) {
            DeliveryMethodManager::saveDeliveryMethod($oDeliveryMethod); //save object
            $_SESSION['statusUpdate'] = 'Verzendmethode is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oDeliveryMethod->deliveryMethodId);
        } else {
            Debug::logError("", "DeliveryMethod module php validate error", __FILE__, __LINE__, "Tried to save DeliveryMethod with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Verzendmethode is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/orders/deliveryMethods/deliveryMethod_form.inc.php';
} elseif (http_get('param1') == 'volgorde-wijzigen') {
    if (http_post('action') == 'saveOrder') {
        if (http_post('order')) {
            $aDeliveryMethodIds = explode('|', http_post('order'));
            $iC = 1;
            foreach ($aDeliveryMethodIds AS $iDeliveryMethodId) {
                $oDeliveryMethod = DeliveryMethodManager::getDeliveryMethodById($iDeliveryMethodId);
                $oDeliveryMethod->order = $iC;
                if ($oDeliveryMethod->isValid()) {
                    DeliveryMethodManager::saveDeliveryMethod($oDeliveryMethod);
                }
                $iC++;
            }
        }
        $_SESSION['statusUpdate'] = 'Volgorde is opgeslagen'; //save status update into session
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    // get all items for order changing
    $aDeliveryMethods = DeliveryMethodManager::getAllDeliveryMethods();
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/orders/deliveryMethods/deliveryMethods_change_order.inc.php';
}

# delete object
elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2")))
        $oDeliveryMethod = DeliveryMethodManager::getDeliveryMethodById(http_get("param2"));

    if (!empty($oDeliveryMethod) && DeliveryMethodManager::deleteDeliveryMethod($oDeliveryMethod)) {
        $_SESSION['statusUpdate'] = 'Verzendmethode is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Verzendmethode kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
}

# display overview
else {
    $aDeliveryMethods = DeliveryMethodManager::getAllDeliveryMethods();
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/orders/deliveryMethods/deliveryMethods_overview.inc.php';
}

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>