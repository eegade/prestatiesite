<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# reset crop settings
unset($_SESSION['aCropSettings']);

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Brandbox";
$oPageLayout->sModuleName = "Brandbox";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# re-order the BrandboxItem objects
if (http_get('ajax') == 'orderBrandboxItems') {
    $oResObj = new stdClass();
    $oResObj->success = false;

    # check for the required POST values
    if (empty($_POST['positions']))
        die(json_encode($oResObj));

    # define the id's of the objects in the new ordered positions
    $sPositions = http_post('positions'); // get object ids comma seperated
    $aBrandboxItemIds = explode(",", $sPositions); // explode ids into array

    foreach ($aBrandboxItemIds as $i => $iBrandboxItemId) {
        # get each object
        $oBrandboxItem = BrandboxItemManager::getBrandboxItemById($iBrandboxItemId);

        if (empty($oBrandboxItem))
            continue;

        # change the order
        $oBrandboxItem->order = $i;


        # update the order of the object
        BrandboxItemManager::updateBrandboxItemOrder($oBrandboxItem);
    }

    $oResObj->success = true;

    die(json_encode($oResObj));
}

# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    # set crop referrer for this module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oBrandboxItem = BrandboxItemManager::getBrandboxItemById(http_get("param2"));
        if (empty($oBrandboxItem))
            http_redirect(ADMIN_FOLDER . "/");
    } else {
        $oBrandboxItem = new BrandboxItem();
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oBrandboxItem->_load($_POST);

        $oBrandboxItem->link = http_post('link') ? addHttp(http_post('link')) : null;
        $oBrandboxItem->pageId = http_post('pageId');

        // with news module
        if (BB_WITH_NEWS) {
            $oBrandboxItem->newsItemId = http_post('newsItemId');
        } else {
            $oBrandboxItem->newsItemId = null;
        }

        # if object is valid, save
        if ($oBrandboxItem->isValid()) {
            BrandboxItemManager::saveBrandboxItem($oBrandboxItem); //save object
            $_SESSION['statusUpdate'] = 'Brandbox item is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oBrandboxItem->brandboxItemId);
        } else {
            Debug::logError("", "BrandboxItem module php validate error", __FILE__, __LINE__, "Tried to save BrandboxItem with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Brandbox item is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    # action saveImage
    if (http_post("action") == 'saveImage') {
        $bCheckMime = true;

        # for upload from SWFUpload
        if (http_post("SWFUpload")) {
            $oResObj = new stdClass();
            $oResObj->success = false;
            session_id(http_post("PHPSESSID"));

            $bCheckMime = false;
        }

        # upload file or return error
        $oUpload = new Upload($_FILES['image'], BrandboxItem::IMAGES_PATH . "/original/", (http_post('title') != '' ? http_post('title') : null), array('jpg', 'png', 'gif', 'jpeg'), $bCheckMime);

        # save image to database on success
        if ($oUpload->bSuccess === true) {
            # make Image object and save
            $oImage = new Image();

            $oImageFile = new ImageFile();
            $oImageFile->link = $oUpload->sNewFilePath;
            $oImageFile->title = http_post('title', '');
            $oImageFile->name = $oUpload->sNewFileBaseName;
            $oImageFile->mimeType = $oUpload->sMimeType;
            $oImageFile->size = $oUpload->iSize;
            $oImageFile->reference = 'original';

            $aImageFiles[] = $oImageFile;

            # make cms_thumb
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . BrandboxItem::IMAGES_PATH . '/cms_thumb/' . $oImageFile->name, 100, (100 * BB_BRANDBOX_CROP_LARGE_H / BB_BRANDBOX_CROP_LARGE_W), $sErrorMsg)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = BrandboxItem::IMAGES_PATH . '/cms_thumb/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'cms_thumb';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding geupload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding geupload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            $oImage->setImageFiles($aImageFiles); //set imageFiles to Image object
            # delete the old Image
            $oOldImage = $oBrandboxItem->getImage();
            if (!empty($oOldImage))
                ImageManager::deleteImage($oOldImage);

            # save Image
            ImageManager::saveImage($oImage);

            # save object's Image relation
            BrandboxItemManager::saveBrandboxItemImageRelations($oBrandboxItem->brandboxItemId, $oImage->imageId);

            # for SWFUpload
            if (http_post("SWFUpload")) {
                $oResObj->success = true;
                $oImageFileThumb->imageId = $oImage->imageId; // set for use in SWFUpload
                // add some extra values
                $oImageFileThumb->isOnlineChangeable = false;
                $oImageFileThumb->isCropable = $oImage->isCropable();
                $oImageFileThumb->isEditable = $oImage->isEditable();
                $oImageFileThumb->isDeletable = $oImage->isDeletable();
                $oImageFileThumb->hasNeededImageFiles = $oImage->hasImageFiles(array('cms_thumb', 'crop_large', 'crop_small'));

                $oResObj->imageFile = $oImageFileThumb; // for adding image to list (last imageFile object)
                print json_encode($oResObj);
                die;
            }

            # back to edit
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/crop-image/?imageId=' . $oImage->imageId);
        } else {
            # for SWFUpload
            if (http_post("SWFUpload")) {
                $oResObj->errorMsg = $oUpload->getErrorMessage();
                print json_encode($oResObj);
                die;
            }

            $_SESSION['statusUpdate'] = 'Afbeelding kon niet worden geupload: ' . $oUpload->getErrorMessage(); //error uploading file
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oBrandboxItem->brandboxItemId);
        }
    }

    $oPage = PageManager::getPageByUrlPath('/');
    # set settings for image management
    $oImageManagerHTML = new ImageManagerHTML();
    $oImageManagerHTML->aImages = ($oBrandboxItem->getImage() ? array($oBrandboxItem->getImage()) : array());
    $oImageManagerHTML->cropLink = ADMIN_FOLDER . '/' . http_get('controller') . '/crop-image';
    $oImageManagerHTML->sUploadUrl = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oBrandboxItem->brandboxItemId;
    $oImageManagerHTML->aNeededImageFileReferences = array('cms_thumb', 'crop_small', 'crop_large');
    $oImageManagerHTML->bShowCropAfterUploadOption = false;
    $oImageManagerHTML->onlineChangeable = false;
    $oImageManagerHTML->sExtraUploadLine = 'Upload een afbeelding met een minimale breedte van <b>' . BB_BRANDBOX_CROP_MIN_W . 'px</b> en een minimale hoogte van <b>' . BB_BRANDBOX_CROP_MIN_H . 'px</b> voor het beste resultaat.';
    $oImageManagerHTML->sExtraUploadedLine = 'Onderstaande afbeeldingen worden getoond op de pagina <a href="' . $oPage->getUrlPath() . '" target="_blank">' . $oPage->getShortTitle() . '</a>';

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/brandboxItems/brandboxItem_form.inc.php';
}

# set object online/offline
elseif (http_get("param1") == 'ajax-setOnline') {
    $bOnline = http_get("online", 0); //no value, set offline by default
    $bAjax = http_get("ajax", false); //controller requested by ajax
    $iBrandboxItemId = http_get("param2");
    $oResObj = new stdClass(); //standard class for json feedback
    # update online for object
    if (is_numeric($iBrandboxItemId)) {
        $oResObj->success = BrandboxItemManager::updateOnlineByBrandboxItemId($bOnline, $iBrandboxItemId);
        $oResObj->brandboxItemId = $iBrandboxItemId;
        $oResObj->online = $bOnline;
    }

    # redirect to overview page if this isn't AJAX
    if (!$bAjax) {
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '');
    }

    die(json_encode($oResObj));
} elseif (http_get('param1') == 'volgorde-wijzigen') {

    if (http_post('action') == 'saveOrder') {
        if (http_post('order')) {
            $aBrandboxItemIds = explode('|', http_post('order'));
            $iC = 1;
            foreach ($aBrandboxItemIds AS $iBrandboxItemId) {
                $oBrandboxItem = BrandboxItemManager::getBrandboxItemById($iBrandboxItemId);
                $oBrandboxItem->order = $iC;
                if ($oBrandboxItem->isValid()) {
                    BrandboxItemManager::saveBrandboxItem($oBrandboxItem);
                }
                $iC++;
            }
        }
        $_SESSION['statusUpdate'] = 'Volgorde is opgeslagen'; //save status update into session

        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    // get all items for order changing
    $aAllBrandboxItems = BrandboxItemManager::getBrandboxItemsByFilter(array('showAll' => true));
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/brandboxItems/brandboxItems_change_order.inc.php';
}

# delete object
elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2")))
        $oBrandboxItem = BrandboxItemManager::getBrandboxItemById(http_get("param2"));

    if (!empty($oBrandboxItem) && BrandboxItemManager::deleteBrandboxItem($oBrandboxItem)) {
        $_SESSION['statusUpdate'] = 'Brandbox item is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Brandbox item kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
} # crop Image
elseif (http_get("param1") == 'crop-image' && is_numeric(http_get("imageId"))) {
    $oImage = ImageManager::getImageById(http_get("imageId"));

    if (!$oImage || !$oImage->getImageFileByReference("original")) {
        $_SESSION['statusUpdate'] = 'Afbeelding niet gevonden of heeft geen origineel meer'; //error getting image
        http_redirect(http_session('cropReferrer'));
    }

    # set crop settings
    $oCropSettings = new CropSettings();
    $oCropSettings->iImageId = $oImage->imageId;
    $oCropSettings->sReferenceName = 'original';
    $oCropSettings->sName = 'voor desktop';

    $oCropSettings->setAspectRatio(BB_BRANDBOX_CROP_LARGE_W, BB_BRANDBOX_CROP_LARGE_H);

    $oCropSettings->bShowPreview = true;

    $oCropSettings->sReferrer = http_session('cropReferrer');
    $oCropSettings->sReferrerTekst = 'brandbox item bewerken';

    # create crop and show on crop form page
    $oCropSettings->addCrop(BB_BRANDBOX_CROP_LARGE_W, BB_BRANDBOX_CROP_LARGE_H, BrandboxItem::IMAGES_PATH . '/crop_large/' . $oImage->getImageFileByReference('original')->name, 'crop_large', true, true);

    # create crop but do not show on crop form page
    $oCropSettings->addCrop(100, (100 * BB_BRANDBOX_CROP_LARGE_H / BB_BRANDBOX_CROP_LARGE_W), $oImage->getImageFileByReference('cms_thumb')->link, 'cms_thumb', false, true);

    # add setting to session in an array
    $_SESSION['aCropSettings'][] = clone $oCropSettings;

    # set crop settings
    $oCropSettings = new CropSettings();
    $oCropSettings->iImageId = $oImage->imageId;
    $oCropSettings->sReferenceName = 'original';
    $oCropSettings->sName = 'voor smartphone';

    $oCropSettings->setAspectRatio(BB_BRANDBOX_CROP_SMALL_W, BB_BRANDBOX_CROP_SMALL_H);

    $oCropSettings->bShowPreview = true;

    $oCropSettings->sReferrer = http_session('cropReferrer');
    $oCropSettings->sReferrerTekst = 'brandbox item bewerken';

    # create crop and show on crop form page
    $oCropSettings->addCrop(BB_BRANDBOX_CROP_SMALL_W, BB_BRANDBOX_CROP_SMALL_H, BrandboxItem::IMAGES_PATH . '/crop_small/' . $oImage->getImageFileByReference('original')->name, 'crop_small', true, true);

    # add setting to session in an array
    $_SESSION['aCropSettings'][] = clone $oCropSettings;


    http_redirect(ADMIN_FOLDER . '/crop');
}

# display overview
else {
    $aAllBrandboxItems = BrandboxItemManager::getBrandboxItemsByFilter(array('showAll' => true));
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/brandboxItems/brandboxItems_overview.inc.php';
}

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>