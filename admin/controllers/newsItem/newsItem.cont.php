<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# reset crop settings
unset($_SESSION['aCropSettings']);

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Nieuws";
$oPageLayout->sModuleName = "Nieuws";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
// handle perPage
if (http_post('setPerPage')) {
    $_SESSION['newsPerPage'] = http_post('perPage');
}

// handle filter
$aNewsItemFilter = http_session('newsItemFilter');
if (http_post('filterNewsItems')) {
    $aNewsItemFilter = http_post('newsItemFilter');
    $aNewsItemFilter['showAll'] = true; // manually set showAll to true
    $_SESSION['newsItemFilter'] = $aNewsItemFilter;
}

if (http_post('resetFilter') || empty($aNewsItemFilter)) {
    unset($_SESSION['newsItemFilter']);
    $aNewsItemFilter = array();
    $aNewsItemFilter['q'] = '';
    $aNewsItemFilter['showAll'] = true; // manually set showAll to true
    $aNewsItemFilter['newsItemCategoryId'] = '';
}

# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {

    # set crop referrer for pages module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");


    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oNewsItem = NewsItemManager::getNewsItemById(http_get("param2"));
        if (!$oNewsItem)
            http_redirect(ADMIN_FOLDER . "/");
        # is editable?
        if (!$oNewsItem->isEditable()) {
            $_SESSION['statusUpdate'] = 'Nieuwsitem kan niet worden bewerkt'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
        }
    } else {
        $oNewsItem = new NewsItem();
    }

    # action = save
    if (http_post("action") == 'save') {

        # load data in object
        $oNewsItem->_load($_POST);

        # set some properties after load, _load strips tags for all inputs
        $oNewsItem->intro = http_post('intro');
        $oNewsItem->content = http_post('content');
        $oNewsItem->onlineFrom = http_post('onlineFromDate') != '' && http_post('onlineFromTime') != '' ? Date::strToDate(http_post('onlineFromDate'))->format('%Y-%m-%d') . ' ' . http_post('onlineFromTime') : null;
        $oNewsItem->onlineTo = http_post('onlineToDate') != '' ? Date::strToDate(http_post('onlineToDate'))->format('%Y-%m-%d') . ' ' . http_post('onlineToTime') : null;

        $aNewsItemCategories = array();
        if (BB_WITH_NEWS_CATEGORIES) {
            foreach (http_post('newsItemCategoryIds', array()) AS $iNewsItemCategoryId) {
                $oNewsItemCategory = NewsItemCategoryManager::getNewsItemCategoryById($iNewsItemCategoryId);
                if (!$oNewsItemCategory) {
                    continue;
                }
                $aNewsItemCategories[] = $oNewsItemCategory;
            }
        }
        $oNewsItem->setCategories($aNewsItemCategories, 'all'); // set in list all for saving properly
        # if object is valid, save
        if ($oNewsItem->isValid()) {
            NewsItemManager::saveNewsItem($oNewsItem); //save news item
            $_SESSION['statusUpdate'] = 'Nieuwsitem is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId);
        } else {
            Debug::logError("", "Nieuws module php validate error", __FILE__, __LINE__, "Tried to save NieuwsItem with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $oPageLayout->sStatusUpdate = 'Nieuws item is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    # action saveImage
    if (http_post("action") == 'saveImage') {

        $bCheckMime = true;
        # for upload from SWFUpload
        if (http_post("SWFUpload")) {
            $oResObj = new stdClass();
            $oResObj->success = false;
            session_id(http_post("PHPSESSID"));

            $bCheckMime = false;
        }

        # upload file or return error
        $oUpload = new Upload($_FILES['image'], NewsItem::IMAGES_PATH . "/original/", (http_post('title') != '' ? http_post('title') : null), array('jpg', 'jpeg', 'png', 'gif'), $bCheckMime);

        # save image to database on success
        if ($oUpload->bSuccess === true) {

            // resize original to acceptable size (1500*1500 max)
            if (!ImageManager::resizeImage(DOCUMENT_ROOT . $oUpload->sNewFilePath, DOCUMENT_ROOT . $oUpload->sNewFilePath, BB_IMAGE_ORIGINAL_W, BB_IMAGE_ORIGINAL_H, $sErrorMsg)) {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar original resize niet gemaakt: ' . $sErrorMsg; //error resizing image
            }

            # make Image object and save
            $oImage = new Image();

            $oImageFile = new ImageFile();
            $oImageFile->link = $oUpload->sNewFilePath;
            $oImageFile->title = http_post('title', '');
            $oImageFile->name = $oUpload->sNewFileBaseName;
            $oImageFile->mimeType = $oUpload->sMimeType;
            $oImageFile->size = $oUpload->iSize;
            $oImageFile->reference = 'original';

            $aImageFiles[] = $oImageFile;

            # always make detail image (resized of original)
            if (ImageManager::resizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . NewsItem::IMAGES_PATH . '/detail/' . $oImageFile->name, BB_IMAGE_DETAIL_W, BB_IMAGE_DETAIL_H, $sErrorMsg)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = NewsItem::IMAGES_PATH . '/detail/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'detail';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload maar detail afbeelding NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            # make crop_small
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . NewsItem::IMAGES_PATH . '/crop_small/' . $oImageFile->name, BB_NEWS_ITEM_CROP_SMALL_W, BB_NEWS_ITEM_CROP_SMALL_H, $sErrorMsg, true)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = NewsItem::IMAGES_PATH . '/crop_small/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'crop_small';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            # make cms_thumb
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . NewsItem::IMAGES_PATH . '/cms_thumb/' . $oImageFile->name, 100, (100 * BB_NEWS_ITEM_CROP_SMALL_H / BB_NEWS_ITEM_CROP_SMALL_W), $sErrorMsg, true)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = NewsItem::IMAGES_PATH . '/cms_thumb/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'cms_thumb';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            $oImage->setImageFiles($aImageFiles); //set imageFiles to Image object
            # save image
            ImageManager::saveImage($oImage);

            # save news item image relation
            NewsItemManager::saveNewsItemImageRelation($oNewsItem->newsItemId, $oImage->imageId);

            # direct crop
            if (http_post("cropAfterUpload") == 1) {
                http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/crop-image/?imageId=' . $oImage->imageId);
            } else {
                # for SWFUpload
                if (http_post("SWFUpload")) {
                    $oResObj->success = true;
                    $oImageFileThumb->imageId = $oImage->imageId; // set for use in SWFUpload
                    // add some extra values
                    $oImageFileThumb->isOnlineChangeable = $oImage->isOnlineChangeable();
                    $oImageFileThumb->isCropable = $oImage->isCropable();
                    $oImageFileThumb->isEditable = $oImage->isEditable();
                    $oImageFileThumb->isDeletable = $oImage->isDeletable();
                    $oImageFileThumb->hasNeededImageFiles = $oImage->hasImageFiles(array('cms_thumb', 'crop_small'));

                    $oResObj->imageFile = $oImageFileThumb; // for adding image to list (last imageFile object)
                    print json_encode($oResObj);
                    die;
                }

                # back to edit
                http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId);
            }
        } else {
            # for SWFUpload
            if (http_post("SWFUpload")) {
                $oResObj->errorMsg = $oUpload->getErrorMessage();
                print json_encode($oResObj);
                die;
            }

            $_SESSION['statusUpdate'] = 'Afbeelding kon niet worden geüpload: ' . $oUpload->getErrorMessage(); //error uploading file
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId);
        }
    }

    # action saveFile
    if (http_post("action") == 'saveFile') {

        $bCheckMime = true;

        # for upload from SWFUpload
        if (http_post("SWFUpload")) {
            $oResObj = new stdClass();
            $oResObj->success = false;
            session_id(http_post("PHPSESSID"));
            $bCheckMime = false;
        }


        # upload file or return error
        $oUpload = new Upload($_FILES['file'], NewsItem::FILES_PATH . '/', (http_post('file_title') != '' ? http_post('file_title') : null), null, $bCheckMime);

        # save file to database on success
        if ($oUpload->bSuccess === true) {

            # make File object and save
            $oFile = new File();
            $oFile->name = $oUpload->sNewFileBaseName;
            $oFile->mimeType = $oUpload->sMimeType;
            $oFile->size = $oUpload->iSize;
            $oFile->link = $oUpload->sNewFilePath;
            $oFile->title = http_post('title') == '' ? $oUpload->sFileName : http_post('title');

            # save file
            FileManager::saveFile($oFile);

            # save page file relation
            NewsItemManager::saveNewsItemFileRelation($oNewsItem->newsItemId, $oFile->mediaId);

            # for SWFUpload
            if (http_post("SWFUpload")) {
                $oResObj->success = true;
                // add some extra values
                $oFile->isOnlineChangeable = $oFile->isOnlineChangeable();
                $oFile->isEditable = $oFile->isEditable();
                $oFile->isDeletable = $oFile->isDeletable();
                $oFile->extension = $oFile->getExtension();

                $oResObj->file = $oFile; // for adding file to list
                print json_encode($oResObj);
                die;
            }

            # back to edit
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId);
        } else {

            # for SWFUpload
            if (http_post("SWFUpload")) {
                $oResObj->errorMsg = $oUpload->getErrorMessage();
                print json_encode($oResObj);
                die;
            }

            $_SESSION['statusUpdate'] = 'Bestand kon niet worden geüpload: ' . $oUpload->getErrorMessage(); //error uploading file
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId);
        }
    }

    # action = saveLink
    if (http_post("action") == 'saveLink') {

        # load data in object
        $oLink = new Link();
        $oLink->_load($_POST);

        # if object is valid, save
        if ($oLink->isValid()) {
            LinkManager::saveLink($oLink); //save link
            NewsItemManager::saveNewsItemLinkRelation($oNewsItem->newsItemId, $oLink->mediaId);
            $_SESSION['statusUpdate'] = 'Link is toegevoegd'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId);
        } else {
            Debug::logError("", "Nieuws item module php validate error", __FILE__, __LINE__, "Tried to save Link with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $oPageLayout->sStatusUpdate = 'Link is niet toegevoegd, niet alle velden zijn (juist) ingevuld';
        }
    }

    # action = saveYoutubeLink
    if (http_post("action") == 'saveYoutubeLink') {

        # load data in object
        $oYoutubeLink = new YoutubeLink();
        $oYoutubeLink->_load($_POST);

        # if object is valid, save
        if ($oYoutubeLink->isValid()) {
            YoutubeLinkManager::saveYoutubeLink($oYoutubeLink); //save youtube link
            NewsItemManager::saveNewsItemYoutubeLinkRelation($oNewsItem->newsItemId, $oYoutubeLink->mediaId);
            $_SESSION['statusUpdate'] = 'Youtube video link is toegevoegd'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId);
        } else {
            Debug::logError("", "Nieuws item module php validate error", __FILE__, __LINE__, "Tried to save Link with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $oPageLayout->sStatusUpdate = 'Youtube video link is niet toegevoegd, niet alle velden zijn (juist) ingevuld';
        }
    }

    # set settings for image management
    $oImageManagerHTML = new ImageManagerHTML();
    $oImageManagerHTML->aImages = $oNewsItem->getImages('all');
    $oImageManagerHTML->cropLink = ADMIN_FOLDER . '/' . http_get('controller') . '/crop-image';
    $oImageManagerHTML->sUploadUrl = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId;
    $oImageManagerHTML->aNeededImageFileReferences = array('cms_thumb', 'detail', 'crop_small');
    $oImageManagerHTML->bCropAfterUploadChecked = true;
    $oImageManagerHTML->bShowCropAfterUploadOption = true;

    # set settings for file management
    $oFileManagerHTML = new FileManagerHTML();
    $oFileManagerHTML->aFiles = $oNewsItem->getFiles('all');
    $oFileManagerHTML->sUploadUrl = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId;

    # set link manager
    $oLinkManagerHTML = new LinkManagerHTML();
    $oLinkManagerHTML->aLinks = $oNewsItem->getLinks('all');
    $oLinkManagerHTML->sUploadUrl = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId;

    # set youtube manager
    $oYoutubeLinkManagerHTML = new YoutubeLinkManagerHTML();
    $oYoutubeLinkManagerHTML->aYoutubeLinks = $oNewsItem->getYoutubeLinks('all');
    $oYoutubeLinkManagerHTML->sUploadUrl = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId;

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/newsItem/newsItem_form.inc.php';
} elseif (http_get("param1") == 'crop-image' && is_numeric(http_get("imageId"))) {

    $oImage = ImageManager::getImageById(http_get("imageId"));

    if (!$oImage || !$oImage->getImageFileByReference("original")) {
        $_SESSION['statusUpdate'] = 'Afbeelding niet gevonden of heeft geen origineel meer'; //error getting image
        http_redirect(http_session('cropReferrer'));
    }

    # set crop settings
    $oCropSettings = new CropSettings();
    $oCropSettings->iImageId = $oImage->imageId;
    $oCropSettings->sReferenceName = 'original';

    $oCropSettings->setAspectRatio(BB_NEWS_ITEM_CROP_SMALL_W, BB_NEWS_ITEM_CROP_SMALL_H);

    $oCropSettings->bShowPreview = true;
    $oCropSettings->iMaxPreviewWidth = BB_NEWS_ITEM_CROP_SMALL_W;

    $oCropSettings->sReferrer = http_session('cropReferrer');
    $oCropSettings->sReferrerTekst = 'newsitem bewerken';

    # create cropand show on crop form page
    $oCropSettings->addCrop(BB_NEWS_ITEM_CROP_SMALL_W, BB_NEWS_ITEM_CROP_SMALL_H, NewsItem::IMAGES_PATH . '/crop_small/' . $oImage->getImageFileByReference('original')->name, 'crop_small', true, true);

    # create crop but do not show on crop form page
    $oCropSettings->addCrop(100, (100 * BB_NEWS_ITEM_CROP_SMALL_H / BB_NEWS_ITEM_CROP_SMALL_W), $oImage->getImageFileByReference('cms_thumb')->link, 'cms_thumb', false, true);

    # add setting to session in an array
    $_SESSION['aCropSettings'][] = clone $oCropSettings;

    http_redirect(ADMIN_FOLDER . '/crop');
} elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {

    if (is_numeric(http_get("param2"))) {
        $oNewsItem = NewsItemManager::getNewsItemById(http_get("param2"));
    }

    if ($oNewsItem && NewsItemManager::deleteNewsItem($oNewsItem)) {
        $_SESSION['statusUpdate'] = 'Nieuwsitem is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Nieuwsitem kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
} elseif (http_get("param1") == 'ajax-setOnline') {

    $bOnline = http_get("online", 0); //no value, set offline by default
    $bAjax = http_get("ajax", false); //controller requested by ajax
    $iNewsItemId = http_get("param2");
    $oResObj = new stdClass(); //standard class for json feedback
    # update online for page
    if (is_numeric($iNewsItemId)) {
        $oResObj->success = NewsItemManager::updateOnlineByNewsItemId($bOnline, $iNewsItemId);
        $oResObj->newsItemId = $iNewsItemId;
        $oResObj->online = $bOnline;
    }

    if (!$bAjax) {
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }
    print json_encode($oResObj);
    die;
} elseif (http_get('param1') == 'image-list') {
    $oNewsItem = NewsItemManager::getNewsItemById(http_get('param2'));

    if (!$oNewsItem)
        die;

    $aImages = $oNewsItem->getImages('all');
    $aImageFiles = array();
    $aLinks = array();
    foreach ($aImages AS $oImage) {
        $oImageFile = $oImage->getImageFileByReference('detail');
        if (!$oImageFile)
            continue;
        $oLink = new stdClass();
        $oLink->title = $oImageFile->title ? $oImageFile->title : $oImageFile->name;
        $oLink->value = $oImageFile->link;
        $aLinks[] = $oLink;
    }

    die(json_encode($aLinks));
} else {
    $iPerPage = http_session('newsPerPage', 10);
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if (!is_numeric($iCurrPage) || $iCurrPage <= 0){
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }
    
    #display news overview
    $aNewsItems = NewsItemManager::getNewsItemsByFilter($aNewsItemFilter, $iPerPage, $iStart, $iFoundRows);
    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;
    
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/newsItem/newsItems_overview.inc.php';
}

# inlcude template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>