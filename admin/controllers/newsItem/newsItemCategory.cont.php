<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Nieuws categorie&euml;n";
$oPageLayout->sModuleName = "Nieuws categorie&euml;n";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oNewsItemCategory = NewsItemCategoryManager::getNewsItemCategoryById(http_get("param2"));
        if (empty($oNewsItemCategory)) {
            http_redirect(ADMIN_FOLDER . "/");
        }
    } else {
        $oNewsItemCategory = new NewsItemCategory();
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oNewsItemCategory->_load($_POST);

        if ($oCurrentUser->isSEO()) {
            $oNewsItemCategory->setUrlPartText(http_post('urlPartText'));
        }

        # if object is valid, save
        if ($oNewsItemCategory->isValid()) {
            NewsItemCategoryManager::saveNewsItemCategory($oNewsItemCategory); //save object
            $_SESSION['statusUpdate'] = 'Nieuws categorie is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItemCategory->newsItemCategoryId);
        } else {
            Debug::logError("", "NewsItemCategory module php validate error", __FILE__, __LINE__, "Tried to save NewsItemCategory with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Nieuws categorie is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/newsItem/newsItemCategory/newsItemCategory_form.inc.php';
}

# set object online/offline
elseif (http_get("param1") == 'ajax-setOnline') {
    $bOnline = http_get("online", 0); //no value, set offline by default
    $bAjax = http_get("ajax", false); //controller requested by ajax
    $iNewsItemCategoryId = http_get("param2");
    $oResObj = new stdClass(); //standard class for json feedback
    # update online for object
    if (is_numeric($iNewsItemCategoryId)) {
        $oResObj->success = NewsItemCategoryManager::updateOnlineByNewsItemCategoryId($bOnline, $iNewsItemCategoryId);
        $oResObj->newsItemCategoryId = $iNewsItemCategoryId;
        $oResObj->online = $bOnline;
    }

    # redirect to overview page if this isn't AJAX
    if (!$bAjax) {
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '');
    }

    die(json_encode($oResObj));
}

# delete object
elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2"))) {
        $oNewsItemCategory = NewsItemCategoryManager::getNewsItemCategoryById(http_get("param2"));
    }
    if (!empty($oNewsItemCategory) && NewsItemCategoryManager::deleteNewsItemCategory($oNewsItemCategory)) {
        $_SESSION['statusUpdate'] = 'Nieuws categorie is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Nieuws categorie kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
} elseif (http_get('param1') == 'volgorde-wijzigen') {

    if (http_post('action') == 'saveOrder') {
        if (http_post('order')) {
            $aNewsItemCategoryIds = explode('|', http_post('order'));
            $iC = 1;
            foreach ($aNewsItemCategoryIds AS $iNewsItemCategoryId) {
                $oNewsItemCategory = NewsItemCategoryManager::getNewsItemCategoryById($iNewsItemCategoryId);
                $oNewsItemCategory->order = $iC;
                if ($oNewsItemCategory->isValid()) {
                    NewsItemCategoryManager::saveNewsItemCategory($oNewsItemCategory);
                }
                $iC++;
            }
        }
        $_SESSION['statusUpdate'] = 'Volgorde is opgeslagen'; //save status update into session
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    // get all items for order changing
    $aNewsItemCategories = NewsItemCategoryManager::getNewsItemCategoriesByFilter(array('showAll' => true));
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/newsItem/newsItemCategory/newsItemCategories_change_order.inc.php';
}

# display overview
else {
    $aNewsItemCategories = NewsItemCategoryManager::getNewsItemCategoriesByFilter(array('showAll' => true));
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/newsItem/newsItemCategory/newsItemCategories_overview.inc.php';
}

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>