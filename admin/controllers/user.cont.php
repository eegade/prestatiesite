<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Gebruiker beheer";
$oPageLayout->sModuleName = "Gebruiker beheer";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oUser = UserManager::getUserById(http_get("param2"));
        if (!$oUser)
            http_redirect(ADMIN_FOLDER . "/");
    } else {
        $oUser = new User ();
    }

    # action = save
    if (http_post("action") == 'save') {

        # do special things before load (because of password hashing)
        if (http_get("param1") == 'bewerken') {
            if (empty($_POST["password"])) {
                #set password from object
                $_POST['password'] = $oUser->password;
            } else {
                #hash and check password for saving later
                if (isValidPassword($_POST['password']))
                    $_POST['password'] = hashPasswordForDb($_POST['password']);
                else
                    $_POST['password'] = null;
            }
        }else {
            # check password is valid
            if (isValidPassword($_POST['password']))
                $_POST['password'] = $_POST['password'];
            else
                $_POST['password'] = null; //object validation will trigger error
        }

        # load data in object
        $oUser->_load($_POST);

        # default empty array
        $aModules = array();

        # foreach module id, add module object to temporary array for filling user object later
        foreach (http_post("moduleIds", array()) AS $sName => $iModuleId) {
            $aModules[] = new Module(array('moduleId' => $iModuleId, 'name' => $sName));
        }

        # set modules into user
        $oUser->setModules($aModules);

        if ($oCurrentUser->isAdmin()) {
            $oUser->setAdministrator(http_post('administrator', 0));
        }
        if ($oCurrentUser->isAdmin() || $oCurrentUser->isSEO()) {
            $oUser->setSEO(http_post('seo', 0));
        }

        # if object is valid, save
        if ($oUser->isValid()) {
            UserManager::saveUser($oUser); //save user
            $_SESSION['statusUpdate'] = 'Gebruiker is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oUser->userId);
        } else {
            Debug::logError("", "User module php validate error", __FILE__, __LINE__, "Tried to save user with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $oPageLayout->sStatusUpdate = 'Gebruiker is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    # mask password for form (just in case)
    $oUser->maskPass();

    $aFilter = array();
    $aFilter['showAll'] = 1;
    $aFilter['active'] = 1;

    // aside admin can see all modules, others only the ones they have rights for
    if (!$oCurrentUser->isAsideAdmin())
        $aFilter['checkRights'] = true;

    # get all modules for rights
    $aModules = ModuleManager::getModulesByFilter($aFilter);

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/users/user_form.inc.php';
} elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2"))) {
        $oUser = UserManager::getUserById(http_get("param2"));
    }

    if ($oUser && UserManager::deleteUser($oUser)) {
        $_SESSION['statusUpdate'] = 'Gebruiker is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Gebruiker is niet verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
} elseif (http_get('param1') == 'ajax-checkUsername') {

    # check if username exists
    $oUser = UserManager::getUserByUsername(http_get('username'));
    if ($oUser && $oUser->userId != http_get('userId')) {
        echo 'false';
    } else {
        echo 'true';
    }
    die;
} else {
    $aUsers = UserManager::getUsersByFilter();
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/users/users_overview.inc.php';
}

# include default template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>