<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Coupons";
$oPageLayout->sModuleName = "Coupons";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
// set per page for coupon rules
if (http_post('setCouponRulesPerPage')) {
    $_SESSION['couponrulesPerPage'] = http_post('perPage');
}

// handle couponrule filter
$aCouponRuleFilter = http_session('couponRuleFilter');
if (http_post('filterCouponRules')) {
    $aCouponRuleFilter = http_post('couponRuleFilter');
    $_SESSION['couponRuleFilter'] = $aCouponRuleFilter;
}

if (http_post('resetCouponRuleFilter') || empty($aCouponRuleFilter)) {
    unset($_SESSION['couponRuleFilter']);
    $aCouponRuleFilter = array();
    $aCouponRuleFilter['title'] = '';
    $aCouponRuleFilter['reference'] = '';
    $aCouponRuleFilter['discountType'] = '';
    $aCouponRuleFilter['minDiscountAmount'] = '';
    $aCouponRuleFilter['maxDiscountAmount'] = '';
    $aCouponRuleFilter['showAll'] = false;
}

// set per page for coupon rules
if (http_post('setCouponsPerPage')) {
    $_SESSION['couponsPerPage'] = http_post('perPage');
}

// handle coupon filter
$aCouponFilter = http_session('couponFilter');
if (http_post('filterCoupons')) {
    $aCouponFilter = http_post('couponFilter');
    $_SESSION['couponFilter'] = $aCouponFilter;
}

if (http_post('resetCouponFilter') || empty($aCouponFilter)) {
    unset($_SESSION['couponFilter']);
    $aCouponFilter = array();
    $aCouponFilter['code'] = '';
    $aCouponFilter['isRedeemable'] = '';
}

# check if the code exists
if (http_get('ajax') == 'checkCode') {
    # return if the required fields are empty
    if (empty($_GET['code']))
        die(json_encode(null));

    # return bool (true if ean doesn't exists and vice versa)
    die(json_encode(!CouponManager::codeExists(strtoupper(http_get('code')), http_get('couponId'))));
}

# handle add/edit for CouponRule
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oCouponRule = CouponRuleManager::getCouponRuleById(http_get("param2"));
        if (empty($oCouponRule)) {
            http_redirect(ADMIN_FOLDER . "/coupon");
        }
    } else {
        $oCouponRule = new CouponRule();
    }

    # handle add for Coupon
    if (http_get("param1") == 'bewerken' && http_get('param3') == 'toevoegen') {
        if (empty($oCouponRule) || !is_numeric($oCouponRule->couponRuleId))
            http_redirect(ADMIN_FOLDER . "/");

        $oCoupon = new Coupon();
        $oCoupon->setCouponRule($oCouponRule);

        if (http_post("action") == 'save') {
            # load data in object
            $oCoupon->_load($_POST);

            # convert code to upperstring
            $oCoupon->code = strtoupper($oCoupon->code);

            # if object is valid, save
            if ($oCoupon->isValid()) {
                CouponManager::saveCoupon($oCoupon);
                $_SESSION['statusUpdate'] = 'Coupon is opgeslagen'; //save status update into session                
                http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCouponRule->couponRuleId);
            } else {
                Debug::logError("", "Coupon module php validate error", __FILE__, __LINE__, "Tried to save Coupon with wrong values despite javascript check.<br />" . _d($oCouponRule, 1, 1) . "<br />" . _d($oCoupon, 1, 1), Debug::LOG_IN_EMAIL);
                $oPageLayout->sStatusUpdate = 'Coupon is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
            }
        }

        $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/coupons/coupon_form.inc.php';
    }
    # handle generating Coupon objects
    elseif (http_get("param1") == 'bewerken' && http_get('param3') == 'genereren') {
        if (empty($oCouponRule) || !is_numeric($oCouponRule->couponRuleId))
            http_redirect(ADMIN_FOLDER . "/");

        if (http_post("action") == 'generate') {
            if (is_numeric(http_post('couponAmount')) && http_post('couponAmount') > 0) {
                if (CouponManager::generateCouponsByCouponRuleId($oCouponRule, intval(http_post('couponAmount')))) {
                    $_SESSION['statusUpdate'] = 'Coupons zijn gegenereerd'; //save status update into session
                    http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCouponRule->couponRuleId);
                } else {
                    Debug::logError("", "Coupon module php validate error", __FILE__, __LINE__, "Tried to generate Coupon objects but failed.<br />" . _d($oCouponRule, 1, 1), Debug::LOG_IN_EMAIL);
                    $oPageLayout->sStatusUpdate = 'Coupons niet kunnen genereren';
                }
            } else {
                Debug::logError("", "Coupon module php validate error", __FILE__, __LINE__, "Tried to generate Coupon objects with wrong values despite javascript check.<br />" . _d($oCouponRule, 1, 1), Debug::LOG_IN_EMAIL);
                $oPageLayout->sStatusUpdate = 'Coupon niet kunnen genereren, niet alle velden zijn (juist) ingevuld';
            }
        }

        $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/coupons/coupons_generate_form.inc.php';
    }
    # handle export
    elseif (http_get("param1") == 'bewerken' && http_get('param3') == 'export') {
        if (empty($oCouponRule) || !is_numeric($oCouponRule->couponRuleId))
            http_redirect(ADMIN_FOLDER . "/");

        $sExport = '<table>';
        $sExport .= '<tr><td>Code</td><td>Aantal keer gebruikt</td><td>Aantal keer bruikbaar</td></tr>';
        foreach ($oCouponRule->getCoupons() as $oCoupon) {
            $sExport .= '<tr><td>' . _e($oCoupon->code) . '</td><td>' . _e($oCoupon->timesRedeemed) . '</td><td>' . _e($oCouponRule->timesRedeemable) . '</td></tr>';
        }
        $sExport .= '</table>';

        $sFileName = $oCouponRule->couponRuleId . '-' . prettyUrlPart($oCouponRule->title) . '-' . date('Ymd_His');

        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=' . $sFileName . '.xls');
        header('Pragma: no-cache');
        header('Expires: 0');
        die($sExport);
    }
    # add/edit for CouponRule
    else {
        $iPerPage = http_session('couponsPerPage', 10);
        $iCurrPage = http_get('page', 1);
        $iStart = (($iCurrPage - 1) * $iPerPage);
        if (!is_numeric($iCurrPage) || $iCurrPage <= 0){
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCouponRule->couponRuleId);
        }
        
        $aCouponFilter['couponRuleId'] = $oCouponRule->couponRuleId;
        $aCoupons = CouponManager::getCouponsByFilter($aCouponFilter, $iPerPage, $iStart, $iFoundRows);
        $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;

        # action = save
        if (http_post("action") == 'save') {
            # load data in object
            $oCouponRule->_load($_POST);

            $oCouponRule->activeFrom = http_post('activeFromDate') != '' && http_post('activeFromTime') != '' ? Date::strToDate(http_post('activeFromDate'))->format('%Y-%m-%d') . ' ' . http_post('activeFromTime') : null;
            $oCouponRule->activeTo = http_post('activeToDate') != '' && http_post('activeToTime') != '' ? Date::strToDate(http_post('activeToDate'))->format('%Y-%m-%d') . ' ' . http_post('activeToTime') : null;

            # if object is valid, save
            if ($oCouponRule->isValid()) {
                CouponRuleManager::saveCouponRule($oCouponRule);
                $_SESSION['statusUpdate'] = 'Couponregel is opgeslagen'; //save status update into session
                http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCouponRule->couponRuleId);
            } else {
                Debug::logError("", "Coupon module php validate error", __FILE__, __LINE__, "Tried to save CouponRule with wrong values despite javascript check.<br />" . _d($oCouponRule, 1, 1), Debug::LOG_IN_EMAIL);
                $oPageLayout->sStatusUpdate = 'Couponregel is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
            }
        }

        $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/coupons/couponRule_form.inc.php';
    }
} elseif (http_get("param1") == 'ajax-setCouponRuleActive') {
    $oResObj = new stdClass(); //standard class for json feedback
    $oResObj->success = false;
    //$bActive = http_get("active", 0); //no value, set offline by default
    $bActive = http_get("param3", 0);
    $bAjax = http_get("ajax", false); //controller requested by ajax
    $iCouponRuleId = http_get("param2");
    # update online for page
    if (is_numeric($iCouponRuleId)) {

        $oResObj->success = CouponRuleManager::updateActiveByCouponRuleId($bActive, $iCouponRuleId);
        $oResObj->couponRuleId = $iCouponRuleId;
        $oResObj->active = $bActive;
    }

    if (!$bAjax) {
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }
    die(json_encode($oResObj));
} elseif (http_get("param1") == 'ajax-setCouponActive') {
    $oResObj = new stdClass(); //standard class for json feedback
    $oResObj->success = false;

    $bActive = http_get("param3", 0); //no value, set offline by default
    $bAjax = http_get("ajax", false); //controller requested by ajax
    $iCouponId = http_get("param2");
    $sRedirect = ADMIN_FOLDER . '/' . http_get('controller');
    # update online for page
    if (is_numeric($iCouponId)) {
        $oResObj->couponId = $iCouponId;
        $oResObj->active = $bActive;
        $oCoupon = CouponManager::getCouponById($iCouponId);
        if (!empty($oCoupon)) {
            $oResObj->success = CouponManager::updateActiveByCouponId($bActive, $iCouponId);
            $sRedirect = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCoupon->couponRuleId;
        }
    }

    if (!$bAjax) {
        http_redirect($sRedirect);
    }
    die(json_encode($oResObj));
} elseif (http_get("param1") == 'couponregel-verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2"))) {
        $oCouponRule = CouponRuleManager::getCouponRuleById(http_get("param2"));
    }

    if (!empty($oCouponRule) && CouponRuleManager::deleteCouponRule($oCouponRule)) {
        $_SESSION['statusUpdate'] = 'Couponregel is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Couponregel kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
} elseif (http_get("param1") == 'coupon-verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2"))) {
        $oCoupon = CouponManager::getCouponById(http_get("param2"));
    }

    if (empty($oCoupon)) {
        $sRedirect = ADMIN_FOLDER . '/' . http_get('controller');
    } else {
        $sRedirect = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCoupon->couponRuleId;
    }

    if (!empty($oCoupon) && CouponManager::deleteCoupon($oCoupon)) {
        $_SESSION['statusUpdate'] = 'Coupon is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Coupon kan niet worden verwijderd'; //save status update into session
    }
    http_redirect($sRedirect);
} else {
    $iPerPage = http_session('couponrulesPerPage', 10);
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if (!is_numeric($iCurrPage) || $iCurrPage <= 0){
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }
        
    $aCouponRules = CouponRuleManager::getCouponRulesByFilter($aCouponRuleFilter, $iPerPage, $iStart, $iFoundRows);
    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/coupons/couponRules_overview.inc.php';
}

# inlcude template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>