<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Agenda";
$oPageLayout->sModuleName = "Agenda";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); // remove statusupdate, always show once
// handle perPage
if (http_post('setPerPage')) {
    $_SESSION['agendaItemsPerPage'] = http_post('perPage');
}

// handle filter
$aAgendaItemFilter = http_session('agendaItemFilter');
if (http_post('filterAgendaItems')) {
    $aAgendaItemFilter = http_post('agendaItemFilter');
    $aAgendaItemFilter['showAll'] = true; // manually set showAll to true
    $_SESSION['agendaItemFilter'] = $aAgendaItemFilter;
}

if (http_post('resetFilter') || empty($aAgendaItemFilter)) {
    unset($_SESSION['agendaItemFilter']);
    $aAgendaItemFilter = array();
    $aAgendaItemFilter['q'] = '';
    $aAgendaItemFilter['showAll'] = true; // manually set showAll to true
    $aAgendaItemFilter['agendaItemCategoryId'] = '';
    $aAgendaItemFilter['dateFrom'] = '';
    $aAgendaItemFilter['dateTo'] = '';
    $aAgendaItemFilter['date'] = '';
}

# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {

    # set crop referrer for pages module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {

        $oAgendaItem = AgendaItemManager::getAgendaItemById(http_get("param2"));

        if (!$oAgendaItem)
            http_redirect(ADMIN_FOLDER . "/");
    } else {

        $oAgendaItem = new AgendaItem ();
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oAgendaItem->_load($_POST);
        $oAgendaItem->allDay = http_post('allDay', 0);
        $oAgendaItem->intro = http_post('intro');
        $oAgendaItem->content = http_post('content');
        $oAgendaItem->dateTimeFrom = http_post('dateFrom') != '' ? Date::strToDate(http_post('dateFrom'))->format('%Y-%m-%d') . ' ' . http_post('timeFrom') : null;
        $oAgendaItem->dateTimeTo = http_post('dateTo') != '' ? Date::strToDate(http_post('dateTo'))->format('%Y-%m-%d') . ' ' . http_post('timeTo') : null;

        # if object is valid, save
        if ($oAgendaItem->isValid()) {
            AgendaItemManager::saveAgenda($oAgendaItem); //save agenda
            $_SESSION['statusUpdate'] = 'Gebruiker is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oAgendaItem->agendaItemId);
        } else {
            Debug::logError("", "Agenda module php validate error", __FILE__, __LINE__, "Tried to save agenda with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $oPageLayout->sStatusUpdate = 'Agenda item is not saved, not all the fields are (right) completed';
        }
    }

    # action saveImage
    if (http_post("action") == 'saveImage') {
        $bCheckMime = true;
        # for upload from SWFUpload
        if (http_post("SWFUpload")) {
            $oResObj = new stdClass();
            $oResObj->success = false;
            session_id(http_post("PHPSESSID"));

            $bCheckMime = false;
        }

        # upload file or return error
        $oUpload = new Upload($_FILES['image'], AgendaItem::IMAGES_PATH . "/original/", (http_post('title') != '' ? http_post('title') : null), array('jpg', 'jpeg', 'png', 'gif'), $bCheckMime);

        # save image to database on success
        if ($oUpload->bSuccess === true) {

            // resize original to acceptable size (1500*1500 max)
            if (!ImageManager::resizeImage(DOCUMENT_ROOT . $oUpload->sNewFilePath, DOCUMENT_ROOT . $oUpload->sNewFilePath, BB_IMAGE_ORIGINAL_W, BB_IMAGE_ORIGINAL_H, $sErrorMsg)) {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar original resize niet gemaakt: ' . $sErrorMsg; //error resizing image
            }

            # make Image object and save
            $oImage = new Image();

            $oImageFile = new ImageFile();
            $oImageFile->link = $oUpload->sNewFilePath;
            $oImageFile->title = http_post('title', '');
            $oImageFile->name = $oUpload->sNewFileBaseName;
            $oImageFile->mimeType = $oUpload->sMimeType;
            $oImageFile->size = $oUpload->iSize;
            $oImageFile->reference = 'original';

            $aImageFiles[] = $oImageFile;

            # always make detail image (resized of original)
            if (ImageManager::resizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . AgendaItem::IMAGES_PATH . '/detail/' . $oImageFile->name, BB_IMAGE_DETAIL_W, BB_IMAGE_DETAIL_H, $sErrorMsg)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = AgendaItem::IMAGES_PATH . '/detail/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'detail';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload maar detail afbeelding NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            # make crop_small
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . AgendaItem::IMAGES_PATH . '/crop_small/' . $oImageFile->name, BB_AGENDA_ITEM_CROP_SMALL_W, BB_AGENDA_ITEM_CROP_SMALL_H, $sErrorMsg, true)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = AgendaItem::IMAGES_PATH . '/crop_small/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'crop_small';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            # make cms_thumb
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . AgendaItem::IMAGES_PATH . '/cms_thumb/' . $oImageFile->name, 100, (100 * BB_AGENDA_ITEM_CROP_SMALL_H / BB_AGENDA_ITEM_CROP_SMALL_W), $sErrorMsg, true)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = AgendaItem::IMAGES_PATH . '/cms_thumb/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'cms_thumb';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding ge&uuml;pload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            $oImage->setImageFiles($aImageFiles); //set imageFiles to Image object
            # save image
            ImageManager::saveImage($oImage);

            # save agenda item image relation
            AgendaItemManager::saveAgendaItemImageRelation($oAgendaItem->agendaItemId, $oImage->imageId);

            # direct crop
            if (http_post("cropAfterUpload") == 1) {
                http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/crop-image/?imageId=' . $oImage->imageId);
            } else {

                # for SWFUpload
                if (http_post("SWFUpload")) {
                    $oResObj->success = true;
                    $oImageFileThumb->imageId = $oImage->imageId; // set for use in SWFUpload
                    // add some extra values
                    $oImageFileThumb->isOnlineChangeable = $oImage->isOnlineChangeable();
                    $oImageFileThumb->isCropable = $oImage->isCropable();
                    $oImageFileThumb->isEditable = $oImage->isEditable();
                    $oImageFileThumb->isDeletable = $oImage->isDeletable();
                    $oImageFileThumb->hasNeededImageFiles = $oImage->hasImageFiles(array('cms_thumb', 'crop_small'));

                    $oResObj->imageFile = $oImageFileThumb; // for adding image to list (last imageFile object)
                    print json_encode($oResObj);
                    die;
                }

                # back to bewerken
                http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oAgendaItem->agendaItemId);
            }
        } else {
            # for SWFUpload
            if (http_post("SWFUpload")) {
                $oResObj->errorMsg = $oUpload->getErrorMessage();
                print json_encode($oResObj);
                die;
            }

            $_SESSION['statusUpdate'] = 'Afbeelding kon niet worden geüpload: ' . $oUpload->getErrorMessage(); //error uploading file
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oAgendaItem->agendaItemId);
        }
    }

    # set settings for image management
    $oImageManagerHTML = new ImageManagerHTML();
    $oImageManagerHTML->aImages = $oAgendaItem->getImages('all');
    $oImageManagerHTML->cropLink = ADMIN_FOLDER . '/' . http_get('controller') . '/crop-image';
    $oImageManagerHTML->sUploadUrl = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oAgendaItem->agendaItemId;
    $oImageManagerHTML->aNeededImageFileReferences = array('cms_thumb', 'detail', 'crop_small');
    $oImageManagerHTML->bCropAfterUploadChecked = true;
    $oImageManagerHTML->bShowCropAfterUploadOption = true;

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/agenda/agendaItem_form.inc.php';

// Set online/offline
} elseif (http_get("param1") == 'ajax-setOnline') {

    $bOnline = http_get("online", 0); //no value, set offline by default
    $bAjax = http_get("ajax", false); //controller requested by ajax
    $iAgendaItemId = http_get("param2");
    $oResObj = new stdClass(); //standard class for json feedback
    # update online for page
    if (is_numeric($iAgendaItemId)) {
        $oResObj->success = AgendaItemManager::updateOnlineByAgendaItemId($bOnline, $iAgendaItemId);
        $oResObj->agendaItemId = $iAgendaItemId;
        $oResObj->online = $bOnline;
    }

    if (!$bAjax) {
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }
    print json_encode($oResObj);
    die;

// Crop image
} elseif (http_get("param1") == 'crop-image' && is_numeric(http_get("imageId"))) {
    $oImage = ImageManager::getImageById(http_get("imageId"));

    if (!$oImage || !$oImage->getImageFileByReference("original")) {
        $_SESSION['statusUpdate'] = 'Afbeelding niet gevonden of heeft geen origineel meer'; //error getting image
        http_redirect(http_session('cropReferrer'));
    }

    # set crop settings
    $oCropSettings = new CropSettings();
    $oCropSettings->iImageId = $oImage->imageId;
    $oCropSettings->sReferenceName = 'original';

    $oCropSettings->setAspectRatio(BB_AGENDA_ITEM_CROP_SMALL_W, BB_AGENDA_ITEM_CROP_SMALL_H);

    $oCropSettings->bShowPreview = true;
    $oCropSettings->iMaxPreviewWidth = BB_AGENDA_ITEM_CROP_SMALL_W;

    $oCropSettings->sReferrer = http_session('cropReferrer');
    $oCropSettings->sReferrerTekst = 'agendaitem bewerken';

    # create cropand show on crop form page
    $oCropSettings->addCrop(BB_AGENDA_ITEM_CROP_SMALL_W, BB_AGENDA_ITEM_CROP_SMALL_H, AgendaItem::IMAGES_PATH . '/crop_small/' . $oImage->getImageFileByReference('original')->name, 'crop_small', true, true);

    # create crop but do not show on crop form page
    $oCropSettings->addCrop(100, (100 * BB_AGENDA_ITEM_CROP_SMALL_H / BB_AGENDA_ITEM_CROP_SMALL_W), $oImage->getImageFileByReference('cms_thumb')->link, 'cms_thumb', false, true);

    # add setting to session in an array
    $_SESSION['aCropSettings'][] = clone $oCropSettings;

    http_redirect(ADMIN_FOLDER . '/crop');

// Delete agenda item
} elseif (http_get("param1") == 'delete' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2"))) {
        $oAgendaItem = AgendaItemManager::getAgendaItemById(http_get("param2"));
    }

    if ($oAgendaItem && AgendaItemManager::deleteAgenda($oAgendaItem)) {
        $_SESSION['statusUpdate'] = 'Agenda is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Agenda is niet verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));

// Overview
} elseif (http_get('param1') == 'image-list') {
    $oAgendaItem = AgendaItemManager::getAgendaItemById(http_get('param2'));

    if (!$oAgendaItem)
        die;

    $aImages = $oAgendaItem->getImages('all');
    $aImageFiles = array();
    $aLinks = array();
    foreach ($aImages AS $oImage) {
        $oImageFile = $oImage->getImageFileByReference('detail');
        if (!$oImageFile)
            continue;
        $oLink = new stdClass();
        $oLink->title = $oImageFile->title ? $oImageFile->title : $oImageFile->name;
        $oLink->value = $oImageFile->link;
        $aLinks[] = $oLink;
    }

    die(json_encode($aLinks));
} else {
    $iPerPage = http_session('agendaItemsPerPage', 10);
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if (!is_numeric($iCurrPage) || $iCurrPage <= 0){
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    $aAgendaItems = AgendaItemManager::getAgendaItemsByFilter($aAgendaItemFilter, $iPerPage, $iStart, $iFoundRows);

    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/agenda/agendaItems_overview.inc.php';
}

# include default template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>