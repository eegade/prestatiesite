<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Producttype specifieke eigenschappen";
$oPageLayout->sModuleName = "Producttype specifieke eigenschappen";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle productPropertyTypeFilter
$aProductPropertyTypeFilter = http_session('productPropertyTypeFilter');
if (http_post('filterForm')) {
    $aProductPropertyTypeFilter = http_post('productPropertyTypeFilter');
    $_SESSION['productPropertyTypeFilter'] = $aProductPropertyTypeFilter;
}

if (http_post('resetFilter') || empty($aProductPropertyTypeFilter)) {
    unset($_SESSION['productPropertyTypeFilter']);
    $aProductPropertyTypeFilter = array();
    $aProductPropertyTypeFilter['catalogProductTypeId'] = -1; // take a non existing catalogProductTypeId to force choosing one before any results are shown
    $aProductPropertyTypeFilter['catalogProductPropertyTypeGroupId'] = -1; // take a non existing catalogProductTypeGroupId to force choosing one before any results are shown
}

# handle perPage
if (http_post('setPerPage')) {
    $_SESSION['productPropertyTypesPerPage'] = http_post('perPage');
}

# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    # set crop referrer for this module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oProductPropertyType = CatalogProductPropertyTypeManager::getProductPropertyTypeById(http_get("param2"));
        if (empty($oProductPropertyType))
            http_redirect(ADMIN_FOLDER . "/");
    } else {
        $oProductPropertyType = new CatalogProductPropertyType();
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oProductPropertyType->_load($_POST);

        # if object is valid, save
        if ($oProductPropertyType->isValid()) {
            CatalogProductPropertyTypeManager::saveProductPropertyType($oProductPropertyType); //save object
            $_SESSION['statusUpdate'] = 'Product type is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductPropertyType->catalogProductPropertyTypeId);
        } else {
            Debug::logError("", "CatalogProductPropertyType module php validate error", __FILE__, __LINE__, "Tried to save CatalogProductPropertyType with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Eigenschap is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    # save possible value
    if (http_post("action") == 'savePossibleValue') {

        $oProductPropertyTypePossibleValue = new CatalogProductPropertyTypePossibleValue();

        # load data in object
        $oProductPropertyTypePossibleValue->_load($_POST);

        if ($oProductPropertyTypePossibleValue->isValid()) {
            CatalogProductPropertyTypePossibleValueManager::saveProductPropertyTypePossibleValue($oProductPropertyTypePossibleValue);
            $_SESSION['statusUpdate'] = 'Waarde is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductPropertyType->catalogProductPropertyTypeId);
        } else {
            Debug::logError("", "CatalogProductPropertyTypePossibleValue module php validate error", __FILE__, __LINE__, "Tried to save CatalogProductPropertyTypePossibleValue with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Waarde is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }
    
    $iProductTypeId = $oProductPropertyType->getPropertyTypeGroup() ? $oProductPropertyType->getPropertyTypeGroup()->catalogProductTypeId : null;
    $aProductTypes = CatalogProductTypeManager::getAllProductTypes();

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productPropertyTypes/catalogProductPropertyType_form.inc.php';
} elseif (http_get('param1') == 'volgorde-wijzigen') {
    if (http_post('action') == 'saveOrder') {
        if (http_post('order')) {
            $aProductPropertyTypeIds = explode('|', http_post('order'));
            $iC = 1;
            foreach ($aProductPropertyTypeIds AS $iProductPropertyTypeId) {
                $oProductPropertyType = CatalogProductPropertyTypeManager::getProductPropertyTypeById($iProductPropertyTypeId);
                $oProductPropertyType->order = $iC;
                if ($oProductPropertyType->isValid()) {
                    CatalogProductPropertyTypeManager::saveProductPropertyType($oProductPropertyType);
                }
                $iC++;
            }
        }
        $_SESSION['statusUpdate'] = 'Volgorde is opgeslagen'; //save status update into session
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    // get all items for order changing
    $aProductPropertyTypes = CatalogProductPropertyTypeManager::getProductPropertyTypesByFilter($aProductPropertyTypeFilter);

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productPropertyTypes/catalogProductPropertyTypes_change_order.inc.php';
}
# delete object
elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2")))
        $oProductPropertyType = CatalogProductPropertyTypeManager::getProductPropertyTypeById(http_get("param2"));

    if (!empty($oProductPropertyType) && CatalogProductPropertyTypeManager::deleteProductPropertyType($oProductPropertyType)) {
        $_SESSION['statusUpdate'] = 'Eigenschap is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Eigenschap kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
}
# delete a CatalogProductPropertyTypePossibleValue
elseif (http_get("param1") == 'ajax-deletePossibleValue') {
    $oResObj = new stdClass();
    $oResObj->success = false;

    $iProductPropertyTypePossibleValueId = http_post('catalogProductPropertyTypePossibleValueId');

    # check for the required POST values
    if (is_numeric($iProductPropertyTypePossibleValueId)) {
        $oProductPropertyTypePossibleValue = CatalogProductPropertyTypePossibleValueManager::getProductPropertyTypePossibleValueById($iProductPropertyTypePossibleValueId);
        if ($oProductPropertyTypePossibleValue) {
            if (CatalogProductPropertyTypePossibleValueManager::deleteProductPropertyTypePossibleValue($oProductPropertyTypePossibleValue)) {
                $oResObj->catalogProductPropertyTypePossibleValueId = $iProductPropertyTypePossibleValueId;
                $oResObj->success = true;
            }
        }
    }

    die(json_encode($oResObj));
}

# edit a CatalogOpeningHours
elseif (http_get("param1") == 'ajax-editPossibleValue') {
    $oResObj = new stdClass();
    $oResObj->success = false;

    $sValue = http_post('value');
    $iProductPropertyTypePossibleValueId = http_post('catalogProductPropertyTypePossibleValueId');

    if (!empty($sValue) && is_numeric($iProductPropertyTypePossibleValueId)) {
        $oProductPropertyTypePossibleValue = CatalogProductPropertyTypePossibleValueManager::getProductPropertyTypePossibleValueById($iProductPropertyTypePossibleValueId);
        CatalogProductPropertyTypePossibleValueManager::updateProductPropertyTypePossibleValue($oProductPropertyTypePossibleValue, $sValue);

        $oResObj->catalogProductPropertyTypePossibleValueId = $iProductPropertyTypePossibleValueId;
        $oResObj->value = $sValue;
        $oResObj->success = true;
    }

    die(json_encode($oResObj));
}

# re-order the CatalogOpeningHours objects
elseif (http_get("param1") == 'ajax-savePossibleValueOrder') {
    $oResObj = new stdClass();
    $oResObj->success = false;

    $sProductPropertyTypePossibleValueIds = http_post("catalogProductPropertyTypePossibleValueIds");
    if (!empty($sProductPropertyTypePossibleValueIds)) {
        $aProductPropertyTypePossibleValueIds = explode(",", $sProductPropertyTypePossibleValueIds); // explode ids to array
        foreach ($aProductPropertyTypePossibleValueIds as $iC => $iProductPropertyTypePossibleValueId) {
            # get each object, update and save
            $oProductPropertyTypePossibleValue = CatalogProductPropertyTypePossibleValueManager::getProductPropertyTypePossibleValueById($iProductPropertyTypePossibleValueId);
            $oProductPropertyTypePossibleValue->order = $iC;
            CatalogProductPropertyTypePossibleValueManager::saveProductPropertyTypePossibleValue($oProductPropertyTypePossibleValue);
        }
        $oResObj->success = true;
    }

    die(json_encode($oResObj));
}

# display overview
else {
    $iPerPage = http_session('productPropertyTypesPerPage', 10);
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if (!is_numeric($iCurrPage) || $iCurrPage <= 0){
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }
    
    $aProductTypes = CatalogProductTypeManager::getAllProductTypes();
    $aProductPropertyTypes = CatalogProductPropertyTypeManager::getProductPropertyTypesByFilter($aProductPropertyTypeFilter, $iPerPage, $iStart, $iFoundRows);
    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productPropertyTypes/catalogProductPropertyTypes_overview.inc.php';
}

# include stylesheet
$oPageLayout->addStylesheet('<link rel="stylesheet" href="' . ADMIN_CSS_FOLDER . '/catalogProductPropertyType_style.css" />');

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>