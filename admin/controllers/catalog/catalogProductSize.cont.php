<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Maten";
$oPageLayout->sModuleName = "Maten";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    # set crop referrer for this module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oProductSize = CatalogProductSizeManager::getProductSizeById(http_get("param2"));
        if (empty($oProductSize))
            http_redirect(ADMIN_FOLDER . "/");
    } else {
        $oProductSize = new CatalogProductSize();
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oProductSize->_load($_POST);

        # if object is valid, save
        if ($oProductSize->isValid()) {
            CatalogProductSizeManager::saveProductSize($oProductSize); //save object
            $_SESSION['statusUpdate'] = 'Maat is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductSize->catalogProductSizeId);
        } else {
            Debug::logError("", "CatalogProductSize module php validate error", __FILE__, __LINE__, "Tried to save CatalogProductSize with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Maat is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productSizes/catalogProductSize_form.inc.php';
} elseif (http_get('param1') == 'volgorde-wijzigen') {
    if (http_post('action') == 'saveOrder') {
        if (http_post('order')) {
            $aProductSizeIds = explode('|', http_post('order'));
            $iC = 1;
            
            foreach ($aProductSizeIds AS $iProductSizeId) {
                $oProductSize = CatalogProductSizeManager::getProductSizeById($iProductSizeId);
                $oProductSize->order = $iC;
                if ($oProductSize->isValid()) {
                    CatalogProductSizeManager::saveProductSize($oProductSize);
                }
                $iC++;
            }
        }
        $_SESSION['statusUpdate'] = 'Volgorde is opgeslagen'; //save status update into session
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    // get all items for order changing
    $aProductSizes = CatalogProductSizeManager::getProductSizesByFilter();
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productSizes/catalogProductSizes_change_order.inc.php';
}

# delete object
elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2")))
        $oProductSize = CatalogProductSizeManager::getProductSizeById(http_get("param2"));

    if (!empty($oProductSize) && CatalogProductSizeManager::deleteProductSize($oProductSize)) {
        $_SESSION['statusUpdate'] = 'Maat is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Maat kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
}

# display overview
else {
    $aProductSizes = CatalogProductSizeManager::getProductSizesByFilter();
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productSizes/catalogProductSizes_overview.inc.php';
}

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>