<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Categorieën";
$oPageLayout->sModuleName = "Categorieën";

# max levels for page structure depth
$iMaxLevels = CatalogProductCategory::MAX_LEVELS;

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    # set crop referrer for this module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oProductCategory = CatalogProductCategoryManager::getProductCategoryById(http_get("param2"));
        if (empty($oProductCategory))
            http_redirect(ADMIN_FOLDER . "/");
    } else {
        $oProductCategory = new CatalogProductCategory();
        if (http_get("parentCatalogProductCategoryId")) {
            $oParentProductCategory = CatalogProductCategoryManager::getProductCategoryById(http_get("parentCatalogProductCategoryId"));
            if (!$oParentProductCategory || $oParentProductCategory->level >= CatalogProductCategory::MAX_LEVELS) {
                http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
            }
            $oProductCategory->parentCatalogProductCategoryId = $oParentProductCategory->catalogProductCategoryId;
        }
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oProductCategory->_load($_POST);

        $oProductCategory->content = http_post('content');

        if ($oCurrentUser->isSEO()) {
            $oProductCategory->setUrlPart(http_post('urlPart'));
        }

        # if object is valid, save
        if ($oProductCategory->isValid()) {
            CatalogProductCategoryManager::saveProductCategory($oProductCategory); //save object
            $_SESSION['statusUpdate'] = 'Categorie is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductCategory->catalogProductCategoryId);
        } else {
            Debug::logError("", "CatalogProductCategory module php validate error", __FILE__, __LINE__, "Tried to save CatalogProductCategory with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Categorie is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productCategories/catalogProductCategory_form.inc.php';
}
# set object online/offline
elseif (http_get("param1") == 'ajax-setOnline') {
    $bOnline = http_get("online", 0); //no value, set offline by default
    $bAjax = http_get("ajax", false); //controller requested by ajax
    $iProductCategoryId = http_get("param2");
    $oResObj = new stdClass(); //standard class for json feedback
    # update online for object
    if (is_numeric($iProductCategoryId)) {
        $oResObj->success = CatalogProductCategoryManager::updateOnlineByProductCategoryId($bOnline, $iProductCategoryId);
        $oResObj->catalogProductCategoryId = $iProductCategoryId;
        $oResObj->online = $bOnline;
    }

    # redirect to overview page if this isn't AJAX
    if (!$bAjax) {
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '');
    }

    die(json_encode($oResObj));
} elseif (http_get('param1') == 'structuur-wijzigen') {
    if (http_post("action") == 'saveProductCategoryStructure') {
        #
        if (http_post("productCategoryStructure")) {
            parse_str(http_post("productCategoryStructure"), $aProductCategoryArray);
        }

        $iProductCategoryOrder = 0;
        # loop trough all categories and save new parentId
        foreach ($aProductCategoryArray['productCategory'] AS $iProductCategoryId => $mParentProductCategoryReference) {

            # get category and save
            $oProductCategory = CatalogProductCategoryManager::getProductCategoryById($iProductCategoryId);

            if (!$oProductCategory)
                continue;
            if ($mParentProductCategoryReference == 'root') {
                $oProductCategory->parentCatalogProductCategoryId = null;
            } elseif (is_numeric($mParentProductCategoryReference)) {
                $oProductCategory->parentCatalogProductCategoryId = $mParentProductCategoryReference;
            } else {
                continue;
            }

            $oProductCategory->order = $iProductCategoryOrder; // set the order to the category
            # save CatalogProductCategory
            if ($oProductCategory->isValid()) {
                CatalogProductCategoryManager::saveProductCategory($oProductCategory);
            }
            $iProductCategoryOrder++;
        }
        $_SESSION['statusUpdate'] = 'Categoriestructuur is opgeslagen'; //save status update into session
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    $aLevel1ProductCategories = CatalogProductCategoryManager::getProductCategoriesByFilter(array('showAll' => true, 'level' => 1));
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productCategories/catalogProductCategories_change_structure.inc.php';
}

# delete object
elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2")))
        $oProductCategory = CatalogProductCategoryManager::getProductCategoryById(http_get("param2"));

    if (!empty($oProductCategory) && CatalogProductCategoryManager::deleteProductCategory($oProductCategory)) {
        $_SESSION['statusUpdate'] = 'Categorie is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Categorie kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
}

# display overview
else {
    $aLevel1ProductCategories = CatalogProductCategoryManager::getProductCategoriesByFilter(array('showAll' => true, 'level' => 1));
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productCategories/catalogProductCategories_overview.inc.php';
}

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>