<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Catalogus";
$oPageLayout->sModuleName = "Catalogus";

$oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/catalog.inc.php';

include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>