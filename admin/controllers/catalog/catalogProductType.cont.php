<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Product types";
$oPageLayout->sModuleName = "Product types";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    # set crop referrer for this module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oProductType = CatalogProductTypeManager::getProductTypeById(http_get("param2"));
        if (empty($oProductType))
            http_redirect(ADMIN_FOLDER . "/");
    } else {
        $oProductType = new CatalogProductType();
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oProductType->_load($_POST);

        # if object is valid, save
        if ($oProductType->isValid()) {
            CatalogProductTypeManager::saveProductType($oProductType); //save object
            $_SESSION['statusUpdate'] = 'Product type is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductType->catalogProductTypeId);
        } else {
            Debug::logError("", "CatalogProductType module php validate error", __FILE__, __LINE__, "Tried to save CatalogProductType with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Product type is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productTypes/catalogProductType_form.inc.php';
} elseif (http_get('param1') == 'volgorde-wijzigen') {
    if (http_post('action') == 'saveOrder') {
        if (http_post('order')) {
            $aProductTypeIds = explode('|', http_post('order'));
            $iC = 1;
            foreach ($aProductTypeIds AS $iProductTypeId) {
                $oProductType = CatalogProductTypeManager::getProductTypeById($iProductTypeId);
                $oProductType->order = $iC;
                if ($oProductType->isValid()) {
                    CatalogProductTypeManager::saveProductType($oProductType);
                }
                $iC++;
            }
        }
        $_SESSION['statusUpdate'] = 'Volgorde is opgeslagen'; //save status update into session
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    // get all items for order changing
    $aAllProductTypes = CatalogProductTypeManager::getAllProductTypes();
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productTypes/catalogProductTypes_change_order.inc.php';
}
# delete object
elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2")))
        $oProductType = CatalogProductTypeManager::getProductTypeById(http_get("param2"));

    if (!empty($oProductType) && CatalogProductTypeManager::deleteProductType($oProductType)) {
        $_SESSION['statusUpdate'] = 'Product type is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Product type kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
}

# display overview
else {
    $aAllProductTypes = CatalogProductTypeManager::getAllProductTypes();
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productTypes/catalogProductTypes_overview.inc.php';
}

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>