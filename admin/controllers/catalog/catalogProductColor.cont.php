<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Kleuren";
$oPageLayout->sModuleName = "Kleuren";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    # set crop referrer for this module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oProductColor = CatalogProductColorManager::getProductColorById(http_get("param2"));
        if (empty($oProductColor))
            http_redirect(ADMIN_FOLDER . "/");
    } else {
        $oProductColor = new CatalogProductColor();
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oProductColor->_load($_POST);

        # if object is valid, save
        if ($oProductColor->isValid()) {
            CatalogProductColorManager::saveProductColor($oProductColor); //save object
            $_SESSION['statusUpdate'] = 'Kleur is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductColor->catalogProductColorId);
        } else {
            Debug::logError("", "CatalogProductColor module php validate error", __FILE__, __LINE__, "Tried to save CatalogProductColor with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Kleur is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productColors/catalogProductColor_form.inc.php';
} elseif (http_get('param1') == 'volgorde-wijzigen') {
    if (http_post('action') == 'saveOrder') {
        if (http_post('order')) {
            $aProductColorIds = explode('|', http_post('order'));
            $iC = 1;
            foreach ($aProductColorIds AS $iProductColorId) {
                $oProductColor = CatalogProductColorManager::getProductColorById($iProductColorId);
                $oProductColor->order = $iC;
                if ($oProductColor->isValid()) {
                    CatalogProductColorManager::saveProductColor($oProductColor);
                }
                $iC++;
            }
        }
        $_SESSION['statusUpdate'] = 'Volgorde is opgeslagen'; //save status update into session
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    // get all items for order changing
    $aProductColors = CatalogProductColorManager::getProductColorsByFilter();
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productColors/catalogProductColors_change_order.inc.php';
}

# delete object
elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2")))
        $oProductColor = CatalogProductColorManager::getProductColorById(http_get("param2"));

    if (!empty($oProductColor) && CatalogProductColorManager::deleteProductColor($oProductColor)) {
        $_SESSION['statusUpdate'] = 'Kleur is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Kleur kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
}

# display overview
else {
    $aProductColors = CatalogProductColorManager::getProductColorsByFilter();
    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productColors/catalogProductColors_overview.inc.php';
}

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>