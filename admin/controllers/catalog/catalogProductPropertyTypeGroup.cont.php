<?

# check if controller is required by index.php 
if (!defined('ACCESS'))
    die;

# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Producttype specifieke eigenschapgroepen";
$oPageLayout->sModuleName = "Producttype specifieke eigenschapgroepen";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle productPropertyTypeGroupFilter
$aProductPropertyTypeGroupFilter = http_session('productPropertyTypeGroupFilter');
if (http_post('filterForm')) {
    $aProductPropertyTypeGroupFilter = http_post('productPropertyTypeGroupFilter');
    $_SESSION['productPropertyTypeGroupFilter'] = $aProductPropertyTypeGroupFilter;
}

if (http_post('resetFilter') || empty($aProductPropertyTypeGroupFilter)) {
    unset($_SESSION['productPropertyTypeGroupFilter']);
    $aProductPropertyTypeGroupFilter = array();
    $aProductPropertyTypeGroupFilter['catalogProductTypeId'] = -1; // take a non existing catalogProductTypeId to force choosing one before any results are shown
}

# handle perPage
if (http_post('setPerPage')) {
    $_SESSION['productPropertyTypeGroupsPerPage'] = http_post('perPage');
}

# handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    # set crop referrer for this module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oProductPropertyTypeGroup = CatalogProductPropertyTypeGroupManager::getProductPropertyTypeGroupById(http_get("param2"));
        if (empty($oProductPropertyTypeGroup))
            http_redirect(ADMIN_FOLDER . "/");
    } else {
        $oProductPropertyTypeGroup = new CatalogProductPropertyTypeGroup();
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oProductPropertyTypeGroup->_load($_POST);

        # if object is valid, save
        if ($oProductPropertyTypeGroup->isValid()) {
            CatalogProductPropertyTypeGroupManager::saveProductPropertyTypeGroup($oProductPropertyTypeGroup); //save object
            $_SESSION['statusUpdate'] = 'Eigenschapgroep is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductPropertyTypeGroup->catalogProductPropertyTypeGroupId);
        } else {
            Debug::logError("", "CatalogProductPropertyTypeGroup module php validate error", __FILE__, __LINE__, "Tried to save CatalogProductPropertyTypeGroup with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Eigenschapgroep is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productPropertyTypeGroups/catalogProductPropertyTypeGroup_form.inc.php';
} elseif (http_get('param1') == 'volgorde-wijzigen') {
    if (http_post('action') == 'saveOrder') {
        if (http_post('order')) {
            $aProductPropertyTypeGroupIds = explode('|', http_post('order'));
            $iC = 1;
            foreach ($aProductPropertyTypeGroupIds AS $iProductPropertyTypeGroupId) {
                $oProductPropertyTypeGroup = CatalogProductPropertyTypeGroupManager::getProductPropertyTypeGroupById($iProductPropertyTypeGroupId);
                $oProductPropertyTypeGroup->order = $iC;
                if ($oProductPropertyTypeGroup->isValid()) {
                    CatalogProductPropertyTypeGroupManager::saveProductPropertyTypeGroup($oProductPropertyTypeGroup);
                }
                $iC++;
            }
        }
        $_SESSION['statusUpdate'] = 'Volgorde is opgeslagen'; //save status update into session
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    // get all items for order changing
    $aProductPropertyTypeGroups = CatalogProductPropertyTypeGroupManager::getProductPropertyTypeGroupsByFilter($aProductPropertyTypeGroupFilter);

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productPropertyTypeGroups/catalogProductPropertyTypeGroups_change_order.inc.php';
}
# delete object
elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2")))
        $oProductPropertyTypeGroup = CatalogProductPropertyTypeGroupManager::getProductPropertyTypeGroupById(http_get("param2"));

    if (!empty($oProductPropertyTypeGroup) && CatalogProductPropertyTypeGroupManager::deleteProductPropertyTypeGroup($oProductPropertyTypeGroup)) {
        $_SESSION['statusUpdate'] = 'Eigenschapgroep is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Eigenschapgroep kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
}

# display overview
else {
    $iPerPage = http_session('productPropertyTypeGroupsPerPage', 10);
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if (!is_numeric($iCurrPage) || $iCurrPage <= 0){
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }
    
    $aProductPropertyTypeGroups = CatalogProductPropertyTypeGroupManager::getProductPropertyTypeGroupsByFilter($aProductPropertyTypeGroupFilter, $iPerPage, $iStart, $iFoundRows);
    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/productPropertyTypeGroups/catalogProductPropertyTypeGroups_overview.inc.php';
}

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>