<?

# check if controller is required by index.php
if (!defined('ACCESS'))
    die;
# reset crop settings
unset($_SESSION['aCropSettings']);
# set page layout properties
$oPageLayout = new PageLayout();
$oPageLayout->sWindowTitle = "Producten";
$oPageLayout->sModuleName = "Producten";

# get status update from session
$oPageLayout->sStatusUpdate = http_session("statusUpdate");
unset($_SESSION['statusUpdate']); //remove statusupdate, always show once
# handle productFilter
$aProductFilter = http_session('productFilter');
if (http_post('filterForm')) {
    $aProductFilter = http_post('productFilter');
    $aProductFilter['showAll'] = true;
    $_SESSION['productFilter'] = $aProductFilter;
}

if (http_post('resetFilter') || empty($aProductFilter)) {
    unset($_SESSION['productFilter']);
    $aProductFilter = array();
    $aProductFilter['q'] = '';
    $aProductFilter['catalogBrandId'] = '';
    $aProductFilter['catalogProductTypeId'] = '';
    $aProductFilter['catalogProductCategoryId'] = '';
    $aProductFilter['showOnHome'] = false;
    $aProductFilter['emptyGoogleCategory'] = false;
    $aProductFilter['showAll'] = true;
}

# handle perPage
if (http_post('setPerPage')) {
    $_SESSION['productsPerPage'] = http_post('perPage');
}

# add tax to price
if (http_get('ajax') == 'addTax') {
    $oResObj = new stdClass();
    $oResObj->success = false;

    # return if the required fields are empty
    if (empty($_GET['salePrice']) || !is_numeric(http_get('salePrice')) || empty($_GET['taxPercentageId']) || !is_numeric(http_get('taxPercentageId')))
        die(json_encode($oResObj));

    $oResObj->salePrice = http_get('salePrice');
    $oResObj->formattedSalePriceWithoutTax = number_format(http_get('salePrice'), 2, '.', '');
    $oResObj->salePriceWithTax = TaxManager::addTax(http_get('salePrice'), TaxManager::getPercentageById(http_get('taxPercentageId')), $fTaxPrice);
    $oResObj->formattedSalePriceWithTax = number_format($oResObj->salePriceWithTax, 2, '.', '');
    $oResObj->taxPrice = $fTaxPrice;
    $oResObj->success = true;

    # return with object
    die(json_encode($oResObj));
}

# subtract tax from price
elseif (http_get('ajax') == 'subtractTax') {
    $oResObj = new stdClass();
    $oResObj->success = false;

    # return if the required fields are empty
    if (empty($_GET['salePrice']) || !is_numeric(http_get('salePrice')) || empty($_GET['taxPercentageId']) || !is_numeric(http_get('taxPercentageId')))
        die(json_encode($oResObj));

    $oResObj->salePriceWithTax = http_get('salePrice');
    $oResObj->formattedSalePriceWithTax = number_format(http_get('salePrice'), 2, '.', '');
    $oResObj->salePrice = TaxManager::subtractTax(http_get('salePrice'), TaxManager::getPercentageById(http_get('taxPercentageId')), $fTaxPrice);
    $oResObj->formattedSalePriceWithoutTax = number_format($oResObj->salePrice, 2, '.', '');
    $oResObj->taxPrice = $fTaxPrice;
    $oResObj->success = true;

    # return with object
    die(json_encode($oResObj));
}

# return google Categories from the website
if (http_get('ajax') == 'googleCategories') {
    $file = fopen("http://www.google.com/basepages/producttype/taxonomy.nl-NL.txt", "r") or die(json_encode($oResObj));

    //Output a line of the file until the end is reached
    while (!feof($file)) {
        $oResObj[] = trim(fgets($file));
    }

    fclose($file);
    # return with object
    die(json_encode($oResObj));
}

# return the template for the related products
if (http_get('ajax') == 'relatedProducts') {
    $oProduct = CatalogProductManager::getProductById(http_get("param2"));

    $aRelatedProducts = $oProduct->getRelatedProducts();

    ob_start();
    require_once(ADMIN_TEMPLATES_FOLDER . '/elements/catalogProduct/catalogProductRelated.inc.php');
    $oResObj->html = ob_get_contents();
    ob_end_clean();
    # return with object
    die(json_encode($oResObj));
}

# return the list with the possible related products
if (http_get('ajax') == 'related-products-autoComplete') {
    $oResObj = Array();

    $sFilter = Array();
    $sFilter['notRelated'] = '1';
    $aProducts = CatalogProductManager::getRelatedProductsByFilter(http_get("param2"), $sFilter);
    //$aProducts = CatalogProductManager::getProductsByFilter();
    $key = 0;
    foreach ($aProducts as $oProduct) {
        $oResObj[$key]['originProductid'] = http_get("catalogProductId");
        $aProductImages = $oProduct->getImages();
        if (!empty($aProductImages)) {
            $oImage = ImageManager::getImageById($aProductImages[0]->imageId);
            $oImageFile = $oImage->getImageFileByReference('detail');
            $oResObj[$key]['link'] = $oImageFile->link;
        } else {
            $oResObj[$key]['link'] = '/images/layout/shop-no-image.jpg';
        }
        $oResObj[$key]['value'] = $oProduct->name;
        $oResObj[$key]['relatedProductid'] = $oProduct->catalogProductId;
        $key++;
    };
    //  # return with object
    die(json_encode($oResObj));
}

# Save a related product
if (http_get('ajax') == 'related-product-save' && is_numeric(http_get('relatedProductId'))) {
    $oResObj = new stdClass();
    $oResObj->success = true;

    $oProduct = CatalogProductManager::getProductById(http_get("param2"));
    $oProduct->saveRelatedProduct(http_get("relatedProductId"));

    # return
    die(json_encode($oResObj));
}

# Delete a related product
if (http_get('ajax') == 'related-product-delete' && is_numeric(http_get('relatedProductId'))) {

    $oResObj = new stdClass();
    $oResObj->success = true;

    $oProduct = CatalogProductManager::getProductById(http_get("param2"));
    $oProduct->deleteRelatedProduct(http_get("relatedProductId"));

    # return
    die(json_encode($oResObj));
}

#handle add/edit
if (http_get("param1") == 'bewerken' || http_get("param1") == 'toevoegen') {
    # set crop referrer for this module
    $_SESSION['cropReferrer'] = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("param2");

    if (http_get("param1") == 'bewerken' && is_numeric(http_get("param2"))) {
        $oProduct = CatalogProductManager::getProductById(http_get("param2"));
        if (empty($oProduct))
            http_redirect(ADMIN_FOLDER . "/");
    } else {
        $oProduct = new CatalogProduct();
    }

    # action = save
    if (http_post("action") == 'save') {
        # load data in object
        $oProduct->_load($_POST);

        $oProduct->setSalePrice(http_post("salePrice"));
        $oProduct->setReducedPrice(http_post("reducedPrice"));
        #if the reducedPrice is not set, I set it to null
        if ($oProduct->getReducedPrice(Settings::get('taxIncluded')) == '') {
            $oProduct->setReducedPrice(NULL);
        }
        $oProduct->setPurchasePrice(http_post("purchasePrice"));

        $oProduct->description = http_post('description');


        if ($oCurrentUser->isSEO()) {
            $oProduct->setUrlPart(http_post('urlPart'));
        }

        # check for categories and set them into the product
        $aCatalogProductCategories = array();
        foreach (http_post('catalogProductCategoryIds', array()) AS $iProductCategoryId) {
            if (!is_numeric($iProductCategoryId))
                continue;
            $aCatalogProductCategories[] = new CatalogProductCategory(array('catalogProductCategoryId' => $iProductCategoryId));
        }
        $oProduct->setCategories($aCatalogProductCategories, 'all');

        # if object is valid, save
        if ($oProduct->isValid()) {
            CatalogProductManager::saveProduct($oProduct); //save object
            $_SESSION['statusUpdate'] = 'Product is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProduct->catalogProductId);
        } else {
            Debug::logError("", "CatalogProduct module php validate error", __FILE__, __LINE__, "Tried to save CatalogProduct with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Product is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }
    }

    # action saveImage
    if (http_post("action") == 'saveImage') {
        $bCheckMime = true;

        # for upload from SWFUpload
        if (http_post("SWFUpload")) {
            $oResObj = new stdClass();
            $oResObj->success = false;
            session_id(http_post("PHPSESSID"));

            $bCheckMime = false;
        }

        # upload file or return error
        $oUpload = new Upload($_FILES['image'], CatalogProduct::IMAGES_PATH . "/original/", (http_post('title') != '' ? http_post('title') : null), array('jpg', 'png', 'gif', 'jpeg'), $bCheckMime);

        # save image to database on success
        if ($oUpload->bSuccess === true) {

            // resize original to acceptable size
            if (!ImageManager::resizeImage(DOCUMENT_ROOT . $oUpload->sNewFilePath, DOCUMENT_ROOT . $oUpload->sNewFilePath, BB_IMAGE_ORIGINAL_W, BB_IMAGE_ORIGINAL_H, $sErrorMsg)) {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar original resize niet gemaakt: ' . $sErrorMsg; //error resizing image
            }

            # make Image object and save
            $oImage = new Image();

            $oImageFile = new ImageFile();
            $oImageFile->link = $oUpload->sNewFilePath;
            $oImageFile->title = http_post('title', '');
            $oImageFile->name = $oUpload->sNewFileBaseName;
            $oImageFile->mimeType = $oUpload->sMimeType;
            $oImageFile->size = $oUpload->iSize;
            $oImageFile->reference = 'original';

            $aImageFiles[] = $oImageFile;

            # always make detail image (resized of original)
            if (ImageManager::resizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . CatalogProduct::IMAGES_PATH . '/detail/' . $oImageFile->name, BB_IMAGE_DETAIL_W, BB_IMAGE_DETAIL_H, $sErrorMsg)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = CatalogProduct::IMAGES_PATH . '/detail/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'detail';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar detail afbeelding NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            # make crop_overview
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . CatalogProduct::IMAGES_PATH . '/crop_overview/' . $oImageFile->name, BB_CATALOGPRODUCT_CROP_OVERVIEW_W, BB_CATALOGPRODUCT_CROP_OVERVIEW_H, $sErrorMsg, true)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = CatalogProduct::IMAGES_PATH . '/crop_overview/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'crop_overview';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            # make crop_thumb
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . CatalogProduct::IMAGES_PATH . '/crop_thumb/' . $oImageFile->name, BB_CATALOGPRODUCT_CROP_THUMB_W, BB_CATALOGPRODUCT_CROP_THUMB_H, $sErrorMsg, true)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = CatalogProduct::IMAGES_PATH . '/crop_thumb/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'crop_thumb';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            # make crop_detail
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . CatalogProduct::IMAGES_PATH . '/crop_detail/' . $oImageFile->name, BB_CATALOGPRODUCT_CROP_DETAIL_W, BB_CATALOGPRODUCT_CROP_DETAIL_H, $sErrorMsg, true)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = CatalogProduct::IMAGES_PATH . '/crop_detail/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'crop_detail';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            # make crop_mobile
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . CatalogProduct::IMAGES_PATH . '/crop_mobile/' . $oImageFile->name, BB_CATALOGPRODUCT_CROP_MOBILE_W, BB_CATALOGPRODUCT_CROP_MOBILE_H, $sErrorMsg, true)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = CatalogProduct::IMAGES_PATH . '/crop_mobile/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'crop_mobile';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            # make cms_thumb
            if (ImageManager::autoCropAndResizeImage(DOCUMENT_ROOT . $oImageFile->link, DOCUMENT_ROOT . CatalogProduct::IMAGES_PATH . '/cms_thumb/' . $oImageFile->name, 100, (100 * BB_CATALOGPRODUCT_CROP_OVERVIEW_H / BB_CATALOGPRODUCT_CROP_OVERVIEW_W), $sErrorMsg, true)) {
                $oImageFileThumb = clone $oImageFile;
                $oImageFileThumb->link = CatalogProduct::IMAGES_PATH . '/cms_thumb/' . $oImageFile->name;
                $oImageFileThumb->name = pathinfo($oImageFileThumb->link, PATHINFO_BASENAME);
                $oImageFileThumb->size = filesize(DOCUMENT_ROOT . $oImageFileThumb->link);
                $oImageFileThumb->reference = 'cms_thumb';

                $aImageFiles[] = clone $oImageFileThumb;
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload en automatische uitsnede(n) gemaakt';
            } else {
                $_SESSION['statusUpdate'] = 'Afbeelding geüpload maar automatische uitsnede(n) NIET gemaakt: ' . $sErrorMsg; //error cropping image
            }

            $oImage->setImageFiles($aImageFiles); //set imageFiles to Image object
            # save Image
            ImageManager::saveImage($oImage);

            # save object's Image relation
            CatalogProductManager::saveProductImageRelation($oProduct->catalogProductId, $oImage->imageId);

            # for SWFUpload
            if (http_post("SWFUpload")) {
                $oResObj->success = true;
                $oImageFileThumb->imageId = $oImage->imageId; // set for use in SWFUpload
                // add some extra values
                $oImageFileThumb->isOnlineChangeable = $oImage->isOnlineChangeable();
                $oImageFileThumb->isCropable = $oImage->isCropable();
                $oImageFileThumb->isEditable = $oImage->isEditable();
                $oImageFileThumb->isDeletable = $oImage->isDeletable();
                $oImageFileThumb->hasNeededImageFiles = $oImage->hasImageFiles(array('cms_thumb', 'crop_overview', 'crop_thumb', 'crop_detail', 'crop_mobile'));

                $oResObj->imageFile = $oImageFileThumb; // for adding image to list (last imageFile object)
                print json_encode($oResObj);
                die;
            }

            # back to edit
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProduct->catalogProductId);
        } else {
            # for SWFUpload
            if (http_post("SWFUpload")) {
                $oResObj->errorMsg = $oUpload->getErrorMessage();
                print json_encode($oResObj);
                die;
            }

            $_SESSION['statusUpdate'] = 'Afbeelding kon niet worden geüpload: ' . $oUpload->getErrorMessage(); //error uploading file
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProduct->catalogProductId);
        }
    }

    # save type specific property values
    if (http_post('action') == 'saveProductPropertyValues') {
        # set the CatalogProductPropertyValue objects
        $aPropertyValues = array();
        foreach (http_post('propertyTypes', array()) AS $iPropertyTypeId => $aPropertyType) {
            foreach ($aPropertyType AS $sPropertyValue) {
                $oPropertyValue = new CatalogProductPropertyValue(array('catalogProductId' => $oProduct->catalogProductId, 'catalogProductPropertyTypeId' => $iPropertyTypeId, 'value' => $sPropertyValue));

                if ($oPropertyValue->isValid())
                    $aPropertyValues[] = $oPropertyValue;
            }
        }
        $oProduct->setPropertyValues($aPropertyValues);

        # if object is valid, save
        if ($oProduct->isValid()) {
            CatalogProductManager::saveProduct($oProduct); //save object
            $_SESSION['statusUpdate'] = 'Product is opgeslagen'; //save status update into session
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProduct->catalogProductId);
        } else {
            Debug::logError("", "CatalogProduct module php validate error", __FILE__, __LINE__, "Tried to save CatalogProduct with wrong values despite javascript check.<br />" . _d($_POST, 1, 1), Debug::LOG_IN_EMAIL);
            $_SESSION['statusUpdate'] = 'Product is niet opgeslagen, niet alle velden zijn (juist) ingevuld';
        }

        CatalogProductPropertyValueManager::saveProductPropertyValues($oProduct);
        $_SESSION['statusUpdate'] = 'Product eigenschappen zijn opgeslagen'; //save status update into session
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProduct->catalogProductId);
    }

    // add Image to a color
    if (http_post('action') == 'addProductImageRelation') {
        $oCatalogProductImageRelation = new CatalogProductImageRelation();
        $oCatalogProductImageRelation->_load($_POST);
        $oCatalogProductImageRelation->catalogProductId = $oProduct->catalogProductId;


        if ($oCatalogProductImageRelation->isValid()) {
            CatalogProductImageRelationManager::saveCatalogProductImageRelation($oCatalogProductImageRelation);
            $_SESSION['statusUpdate'] = 'Voorraad toegevoegd'; //save status update into session
        } else {
            $_SESSION['statusUpdate'] = 'Voorraad <u>niet</u> toegevoegd, niet alle velden zijn (juist) ingevuld'; //save status update into session
        }

        http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProduct->catalogProductId);
    }

    // add stock record
    if (http_post('action') == 'addSizeColorRelation') {
        $oCatalogProductSizeColorRelation = new CatalogProductSizeColorRelation();
        $oCatalogProductSizeColorRelation->_load($_POST);
        $oCatalogProductSizeColorRelation->catalogProductId = $oProduct->catalogProductId;

        $oCatalogProductSizeColorRelationDuplicate = CatalogProductSizeColorRelationManager::getCatalogProductSizeColorRelation($oCatalogProductSizeColorRelation->catalogProductId, $oCatalogProductSizeColorRelation->catalogProductSizeId, $oCatalogProductSizeColorRelation->catalogProductColorId);
        if ($oCatalogProductSizeColorRelationDuplicate) {
            $_SESSION['statusUpdate'] = 'Voorraad <u>niet</u> toegevoegd, combinatie bestaat al!'; //save status update into session
        } else {

            if ($oCatalogProductSizeColorRelation->isValid()) {
                CatalogProductSizeColorRelationManager::saveCatalogProductSizeColorRelation($oCatalogProductSizeColorRelation);
                $_SESSION['statusUpdate'] = 'Voorraad toegevoegd'; //save status update into session
            } else {
                $_SESSION['statusUpdate'] = 'Voorraad <u>niet</u> toegevoegd, niet alle velden zijn (juist) ingevuld'; //save status update into session
            }
        }

        http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProduct->catalogProductId);
    }

    // get products for prev and next buttons
    if (!empty($oProduct->catalogProductId)) {
        $oProductNext = $oProduct->getNextProduct($aProductFilter, array('catalogProductId' => 'ASC'));
        $oProductPrev = $oProduct->getPrevProduct($aProductFilter, array('catalogProductId' => 'DESC'));
    }

    # set settings for image management
    $oImageManagerHTML = new ImageManagerHTML();
    $oImageManagerHTML->aImages = $oProduct->getImages('all');
    $oImageManagerHTML->cropLink = ADMIN_FOLDER . '/' . http_get('controller') . '/crop-image';
    $oImageManagerHTML->sUploadUrl = ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProduct->catalogProductId;
    $oImageManagerHTML->aNeededImageFileReferences = array('cms_thumb', 'crop_overview', 'crop_thumb', 'crop_detail', 'crop_mobile');
    $oImageManagerHTML->bShowCropAfterUploadOption = false;
    $oImageManagerHTML->sExtraUploadLine = 'Upload een afbeelding met een minimale breedte van <b>' . BB_CATALOGPRODUCT_CROP_MIN_W . 'px</b> en een minimale hoogte van <b>' . BB_CATALOGPRODUCT_CROP_MIN_H . 'px</b> voor het beste resultaat.';

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/catalogProduct_form.inc.php';
}

# crop Image
elseif (http_get("param1") == 'crop-image' && is_numeric(http_get("imageId"))) {
    $oImage = ImageManager::getImageById(http_get("imageId"));

    if (!$oImage || !$oImage->getImageFileByReference("original")) {
        $_SESSION['statusUpdate'] = 'Afbeelding niet gevonden of heeft geen origineel meer'; //error getting image
        http_redirect(http_session('cropReferrer'));
    }

    # set crop settings
    $oCropSettings = new CropSettings();
    $oCropSettings->iImageId = $oImage->imageId;
    $oCropSettings->sReferenceName = 'original';

    $oCropSettings->setAspectRatio(BB_CATALOGPRODUCT_CROP_OVERVIEW_W, BB_CATALOGPRODUCT_CROP_OVERVIEW_H);

    $oCropSettings->bShowPreview = true;
    $oCropSettings->iMaxPreviewWidth = BB_CATALOGPRODUCT_CROP_OVERVIEW_W;

    $oCropSettings->sReferrer = http_session('cropReferrer');
    $oCropSettings->sReferrerTekst = 'product bewerken';

    # create crop and show on crop form page
    $oCropSettings->addCrop(BB_CATALOGPRODUCT_CROP_THUMB_W, BB_CATALOGPRODUCT_CROP_THUMB_H, CatalogProduct::IMAGES_PATH . '/crop_thumb/' . $oImage->getImageFileByReference('original')->name, 'crop_thumb', true, true);
    $oCropSettings->addCrop(BB_CATALOGPRODUCT_CROP_OVERVIEW_W, BB_CATALOGPRODUCT_CROP_OVERVIEW_H, CatalogProduct::IMAGES_PATH . '/crop_overview/' . $oImage->getImageFileByReference('original')->name, 'crop_overview', false, true);
    $oCropSettings->addCrop(BB_CATALOGPRODUCT_CROP_DETAIL_W, BB_CATALOGPRODUCT_CROP_DETAIL_H, CatalogProduct::IMAGES_PATH . '/crop_detail/' . $oImage->getImageFileByReference('original')->name, 'crop_detail', false, true);
    $oCropSettings->addCrop(BB_CATALOGPRODUCT_CROP_MOBILE_W, BB_CATALOGPRODUCT_CROP_MOBILE_H, CatalogProduct::IMAGES_PATH . '/crop_mobile/' . $oImage->getImageFileByReference('original')->name, 'crop_mobile', false, true);

    # create crop but do not show on crop form page
    $oCropSettings->addCrop(100, (100 * BB_CATALOGPRODUCT_CROP_OVERVIEW_H / BB_CATALOGPRODUCT_CROP_OVERVIEW_W), $oImage->getImageFileByReference('cms_thumb')->link, 'cms_thumb', false, true);

    # add setting to session in an array
    $_SESSION['aCropSettings'][] = clone $oCropSettings;

    http_redirect(ADMIN_FOLDER . '/crop');
}

# set object online/offline
elseif (http_get("param1") == 'ajax-setOnline') {
    $bOnline = http_get("online", 0); //no value, set offline by default
    $bAjax = http_get("ajax", false); //controller requested by ajax
    $iProductId = http_get("param2");
    $oResObj = new stdClass(); //standard class for json feedback
    # update online for object
    if (is_numeric($iProductId)) {
        $oResObj->success = CatalogProductManager::updateOnlineByProductId($bOnline, $iProductId);
        $oResObj->catalogProductId = $iProductId;
        $oResObj->online = $bOnline;
    }

    # redirect to overview page if this isn't AJAX
    if (!$bAjax) {
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '');
    }

    die(json_encode($oResObj));
}

# Select image to match to a color
elseif (http_get("param1") == 'bewerken-afbeelding-kleur-relatie' && is_numeric(http_get("catalogProductId"))) {
    $oProduct = CatalogProductManager::getProductById(http_get("catalogProductId"));

    $aImages = CatalogProductManager::getImagesByFilter($oProduct->catalogProductId);

    // I take the color
    $oColor = CatalogProductColorManager::getProductColorById(http_get("colorId"));
    $aImageFile = array();
    foreach ($aImages AS $oImage) {
        // I only show the images wich don't have the selected color assigned
        $oProductImageRelation = CatalogProductImageRelationManager::getCatalogProductImageRelationById(http_get("catalogProductId"), $oImage->imageId);
        if (($oProductImageRelation->catalogProductColorId != $oColor->catalogProductColorId) && is_null($oProductImageRelation->catalogProductColorId)) {
            $oImageFile = $oImage->getImageFileByReference('detail');
            $aImageFile[] = $oImageFile;
        }
    }
    include ADMIN_PAGES_FOLDER . '/catalog/products/catalogProductImageColorRelation_form.inc.php';
    die;
}

# Reset(Set to null) a color in a Product-image relation
elseif (http_get("param1") == 'verwijder-afbeeldingen-kleur-relatie' && is_numeric(http_get("catalogProductId")) && is_numeric(http_get("catalogProductImageId"))) {
    if (is_numeric(http_get("catalogProductId")) && is_numeric(http_get("catalogProductImageId"))) {
        $oProductImageRelation = CatalogProductImageRelationManager::getCatalogProductImageRelationById(http_get("catalogProductId"), http_get("catalogProductImageId"));
    }
    print_r($oProductImageRelation);
    CatalogProductImageRelationManager::resetCatalogProductImageColorRelation($oProductImageRelation);
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("catalogProductId"));
    die;
}

# delete object
elseif (http_get("param1") == 'verwijderen' && is_numeric(http_get("param2"))) {
    if (is_numeric(http_get("param2")))
        $oProduct = CatalogProductManager::getProductById(http_get("param2"));

    if (!empty($oProduct) && CatalogProductManager::deleteProduct($oProduct)) {
        $_SESSION['statusUpdate'] = 'Product is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Product kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
}

# delete size color relation
elseif (http_get("param1") == 'verwijder-maat-kleur-relatie' && is_numeric(http_get("catalogProductId")) && is_numeric(http_get("catalogProductSizeId")) && is_numeric(http_get("catalogProductColorId"))) {
    $oCatalogProductSizeColorRelation = CatalogProductSizeColorRelationManager::getCatalogProductSizeColorRelation(http_get("catalogProductId"), http_get("catalogProductSizeId"), http_get("catalogProductColorId"));
    if (!empty($oCatalogProductSizeColorRelation) && CatalogProductSizeColorRelationManager::deleteCatalogProductSizeColorRelation($oCatalogProductSizeColorRelation)) {
        $_SESSION['statusUpdate'] = 'Voorraad is verwijderd'; //save status update into session
    } else {
        $_SESSION['statusUpdate'] = 'Voorraad kan niet worden verwijderd'; //save status update into session
    }
    http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("catalogProductId"));
}

# edit size color relation
elseif (http_get("param1") == 'bewerken-maat-kleur-relatie' && is_numeric(http_get("catalogProductId")) && is_numeric(http_get("catalogProductSizeId")) && is_numeric(http_get("catalogProductColorId"))) {
    $oCatalogProductSizeColorRelation = CatalogProductSizeColorRelationManager::getCatalogProductSizeColorRelation(http_get("catalogProductId"), http_get("catalogProductSizeId"), http_get("catalogProductColorId"));
    if (!empty($oCatalogProductSizeColorRelation)) {
        if (http_post('action') == 'saveSizeColorRelation') {
            $oCatalogProductSizeColorRelation->_load($_POST);
            if ($oCatalogProductSizeColorRelation->isValid()) {
                CatalogProductSizeColorRelationManager::saveCatalogProductSizeColorRelation($oCatalogProductSizeColorRelation);
                $_SESSION['statusUpdate'] = 'Voorraad is bijgewerkt'; //save status update into session
            } else {
                $_SESSION['statusUpdate'] = 'Voorraad kon <u>niet</u> worden bijgewerkt'; //save status update into session
            }
            http_redirect(ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get("catalogProductId"));
        }
        include ADMIN_PAGES_FOLDER . '/catalog/products/catalogProductSizeColorRelation_form.inc.php';
    }
    die;
} elseif (http_get('param1') == 'image-list') {
    $oProduct = CatalogProductManager::getProductById(http_get('param2'));

    if (!$oProduct)
        die;

    $aImages = $oProduct->getImages('all');
    $aImageFiles = array();
    $aLinks = array();
    foreach ($aImages AS $oImage) {
        $oImageFile = $oImage->getImageFileByReference('detail');
        if (!$oImageFile)
            continue;
        $oLink = new stdClass();
        $oLink->title = $oImageFile->title ? $oImageFile->title : $oImageFile->name;
        $oLink->value = $oImageFile->link;
        $aLinks[] = $oLink;
    }

    die(json_encode($aLinks));
}

# display overview
else {
    $iPerPage = http_session('productsPerPage', 10);
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if (!is_numeric($iCurrPage) || $iCurrPage <= 0) {
        http_redirect(ADMIN_FOLDER . '/' . http_get('controller'));
    }

    $aProducts = CatalogProductManager::getProductsByFilter($aProductFilter, $iPerPage, $iStart, $iFoundRows, array('catalogProductId' => 'DESC'));
    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;

    $oPageLayout->sPagePath = ADMIN_PAGES_FOLDER . '/catalog/products/catalogProducts_overview.inc.php';
}

# include template
include_once ADMIN_TEMPLATES_FOLDER . '/default.tmpl.php';
?>