<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <fieldset>
            <legend>Gebruiker <?= http_get("param1") ?></legend>
            <form method="POST" action="" class="validateForm">
                <input type="hidden" value="save" name="action" />
                <table class="withForm">
                    <tr>
                        <td class="withLabel" style="width: 120px;"><label for="name">Naam *</label></td>
                        <td><input id="name" class="{validate:{required:true}} autofocus" title="Vul de naam in van de gebruiker" type="text" autocomplete="off" name="name" value="<?= $oUser->name ?>" /></td>
                        <td><span class="error"><?= $oUser->isPropValid("name") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="username">Gebruikersnaam *</label></td>
                        <td><input id="username" class="{validate:{required:true,remote:'<?= ADMIN_FOLDER ?>/gebruikers/ajax-checkUsername?userId=<?= $oUser->userId ?>'}}" title="Vul een unieke gebruikersnaam in waarmee de gebruiker inlogt in de admin" type="text" autocomplete="off" name="username" value="<?= $oUser->username ?>" /></td>
                        <td><span class="error"><?= $oUser->isPropValid("username") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="password">Nieuw wachtwoord **</label></td>
                        <td><input id="password" class="<?= $oUser->userId ? '{validate:{password:true}}' : '{validate:{required:true,password:true}}' ?>" title="Vul een veilig password in (minimaal 8 tekens waarvan minimaal 1 kleine letter, 1 hoofdletter, 1 leesteken, 1 cijfer)" autocomplete="off" type="text" name="password" value="" /><em>&nbsp;(** leeg? -> oude wachtwoord wordt opgeslagen)</em></td>
                        <td><span class="error"><?= $oUser->isPropValid("password") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <? if ($oUser->userId == $oCurrentUser->userId && !$oCurrentUser->isAsideAdmin()) { ?>
                        <tr>
                            <td colspan="3"><b class="errorColor"><i>Let op, als je modules van jezelf uit zet, kun je ze niet zelf meer aanzetten!</i></b></td>
                        </tr>
                    <? } ?>
                    <tr>
                        <td>Modules</td>
                        <td>
                            <?
                            foreach ($aModules AS $oModule) {
                                echo '<input class="alignCheckbox" type="checkbox" id="module_' . $oModule->moduleId . '" name="moduleIds[' . $oModule->name . ']" ' . ($oUser->hasRightsFor($oModule->name) ? 'CHECKED' : '') . ' value="' . $oModule->moduleId . '" /> <label for="module_' . $oModule->moduleId . '">' . $oModule->linkName . '</label><br />';
                            }
                            ?>
                        </td>
                    </tr>
                    <? if ($oCurrentUser->isAdmin()) { ?>
                        <tr>
                            <td>admin rechten</td>
                            <td>
                                <input class="alignRadio" title="Gebruiker heeft alle adminrechten" type="radio" <?= $oUser->getAdministrator() ? 'CHECKED' : '' ?> id="administrator_1" name="administrator" value="1" /> <label for="administrator_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Gebruiker heeft alle adminrechten" type="radio" <?= !$oUser->getAdministrator() ? 'CHECKED' : '' ?> id="administrator_0" name="administrator" value="0" /> <label for="administrator_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oUser->isPropValid("administrator") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                    <? } ?>
                    <? if ($oCurrentUser->isAdmin() || $oCurrentUser->isSEO()) { ?>
                        <tr>
                            <td>seo rechten</td>
                            <td>
                                <input class="alignRadio" title="Gebruiker heeft alle seo rechten" type="radio" <?= $oUser->getSeo() ? 'CHECKED' : '' ?> id="seo_1" name="seo" value="1" /> <label for="seo_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Gebruiker heeft alle seo rechten" type="radio" <?= !$oUser->getSeo() ? 'CHECKED' : '' ?> id="seo_0" name="seo" value="0" /> <label for="seo_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oUser->isPropValid("seo") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                    <? } ?>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </form>
        </fieldset>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
# add ajax code for online/offline handling
$sRandomPasswordJavascript = <<<EOT
<script>
    // randompassword to password field
    $("#password").randomPass();
</script>
EOT;
$oPageLayout->addJavascriptBottom($sRandomPasswordJavascript);
?>