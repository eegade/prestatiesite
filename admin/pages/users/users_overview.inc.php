<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="4"><h2>Alle gebruikers</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Nieuwe gebruiker toevoegen" alt="Nieuwe gebruiker toevoegen">Gebruiker toevoegen</a></div></td>
        </tr>
        <tr>
            <th>#</th>
            <th>Naam</th>
            <th>Gebruikersnaam</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aUsers AS $oUser) {
            echo '<tr>';
            echo '<td>' . $oUser->userId . '</td>';
            echo '<td>' . $oUser->name . '</td>';
            echo '<td>' . $oUser->username . '</td>';
            echo '<td>';
            if ($oUser->isEditable())
                echo '<a class="action_icon edit_icon" title="Bewerk gebruiker" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oUser->userId . '"></a>';
            else
                echo '<span class="action_icon edit_icon grey"></span>';

            if ($oUser->isDeletable())
                echo '<a class="action_icon delete_icon" title="Verwijder gebruiker" onclick="return confirmChoice(\'gebruiker ' . $oUser->name . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oUser->userId . '"></a>';
            else
                echo '<span class="action_icon delete_icon grey"></span>';

            echo '</td>';
            echo '</tr>';
        }
        if (count($aUsers) == 0) {
            echo '<tr><td colspan="4"><i>Er zijn geen gebruikers weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>