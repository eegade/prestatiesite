<h1>Instellingen</h1> 
<form method="POST" action="" class="validateForm" enctype="multipart/form-data">
    <input type="hidden" value="save" name="action" />
    <table class="withForm">
        <tr>
            <td style="padding-top: 10px;" colspan="2"><b>Homepage</b></td>
        </tr>
        <? if (BB_WITH_CLICK_CALL) { ?>
            <tr>
                <td>Toon 'Klik of bel' op Homepage *</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Toon 'Klik of bel' op homepage `Ja`" type="radio" <?= Settings::get('showClickCallOnHome') ? 'CHECKED' : '' ?> id="showClickCallOnHome_1" name="settings[showClickCallOnHome]" value="1" /> <label for="showClickCallOnHome_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Toon 'Klik of bel' op homepage  `Nee`" type="radio" <?= !Settings::get('showClickCallOnHome') ? 'CHECKED' : '' ?> id="showClickCallOnHome_0" name="settings[showClickCallOnHome]" value="0" /> <label for="showClickCallOnHome_0">Nee</label>
                </td>
            </tr>
        <? } ?>
        <? if (BB_WITH_CALL_ME_BACK) { ?>
            <tr>
                <td>Toon 'Bel mij terug' op Homepage *</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Toon 'Bel mij terug' op homepage `Ja`" type="radio" <?= Settings::get('showCallMeBackOnHome') ? 'CHECKED' : '' ?> id="showCallMeBackOnHome_1" name="settings[showCallMeBackOnHome]" value="1" /> <label for="showCallMeBackOnHome_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Toon 'Bel mij terug' op homepage `Nee`" type="radio" <?= !Settings::get('showCallMeBackOnHome') ? 'CHECKED' : '' ?> id="showCallMeBackOnHome_0" name="settings[showCallMeBackOnHome]" value="0" /> <label for="showCallMeBackOnHome_0">Nee</label>
                </td>
            </tr>  
        <? } ?>
        <? if (BB_WITH_TWITTER_FEED) { ?>            
            <tr>
                <td>Toon Twitter feed op Homepage *</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Toon Twitter feed op homepage `Ja`" type="radio" <?= Settings::get('showTwitterFeedOnHome') ? 'CHECKED' : '' ?> id="showTwitterFeedOnHome_1" name="settings[showTwitterFeedOnHome]" value="1" /> <label for="showTwitterFeedOnHome_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Toon Twitter feed op homepage `Nee`" type="radio" <?= !Settings::get('showTwitterFeedOnHome') ? 'CHECKED' : '' ?> id="showTwitterFeedOnHome_0" name="settings[showTwitterFeedOnHome]" value="0" /> <label for="showTwitterFeedOnHome_0">Nee</label>
                </td>
            </tr> 
        <? } ?>
        <? if (BB_WITH_CONTINUOUS_SLIDER) { ?>            
            <tr>
                <td>Toon doorlopende slider op Homepage *</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Toon doorlopende slider op homepage `Ja`" type="radio" <?= Settings::get('showSliderOnHome') ? 'CHECKED' : '' ?> id="showSliderOnHome_1" name="settings[showSliderOnHome]" value="1" /> <label for="showSliderOnHome_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Toon doorlopende slider op homepage `Nee`" type="radio" <?= !Settings::get('showSliderOnHome') ? 'CHECKED' : '' ?> id="showSliderOnHome_0" name="settings[showSliderOnHome]" value="0" /> <label for="showSliderOnHome_0">Nee</label>
                </td>
            </tr> 
        <? } ?>            
        <? if (BB_WITH_NEWS) { ?>
            <tr>
                <td>Toon nieuws op Homepage *</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Toon nieuws op homepage `Ja`" type="radio" <?= Settings::get('showNewsOnHome') ? 'CHECKED' : '' ?> id="showNewsOnHome_1" name="settings[showNewsOnHome]" value="1" /> <label for="showNewsOnHome_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Toon nieuws op homepage  `Nee`" type="radio" <?= !Settings::get('showNewsOnHome') ? 'CHECKED' : '' ?> id="showNewsOnHome_0" name="settings[showNewsOnHome]" value="0" /> <label for="showNewsOnHome_0">Nee</label>
                </td>
            </tr>
        <? } ?>
        <tr>
            <td style="padding-top: 10px;" colspan="2"><b>Contact gegevens</b></td>
        </tr>
        <tr>
            <td class="withLabel" style="width: 220px;"><label for="clientName">Naam</label> *</td>
            <td>
                <input class="{validate:{required:true}} default" title="Vul uw naam in" type="text" id="clientName" name="settings[clientName]" value="<?= _e(Settings::get('clientName')) ?>" />
            </td>
        </tr>
        <tr>
            <td class="withLabel"><label for="clientStreet">Straat</label> *</td>
            <td>
                <input class="{validate:{required:true}}" title="Vul uw straat in" type="text" id="clientStreet" name="settings[clientStreet]" size="20" value="<?= _e(Settings::get('clientStreet')) ?>" />
            </td>
        </tr>
        <tr>
            <td class="withLabel"><label for="clientPostalCode">Postcode</label> *</td>
            <td>
                <input class="{validate:{required:true}}" title="Vul uw postcode in" type="text" id="clientPostalCode" name="settings[clientPostalCode]" size="6" value="<?= _e(Settings::get('clientPostalCode')) ?>" />
            </td>
        </tr>
        <tr>
            <td class="withLabel"><label for="clientCity">Plaats</label> *</td>
            <td>
                <input class="{validate:{required:true}}" title="Vul uw plaats in" type="text" id="clientCity" name="settings[clientCity]" size="20" value="<?= _e(Settings::get('clientCity')) ?>" />
            </td>
        </tr>
        <tr>
            <td class="withLabel"><label for="clientPhone">Telefoon</label> *</td>
            <td>
                <input class="{validate:{required:true}}" title="Vul uw telefoonnummer in" type="text" id="clientPhone" name="settings[clientPhone]" size="20" value="<?= _e(Settings::get('clientPhone')) ?>" />
            </td>
        </tr>
        <tr>
            <td class="withLabel"><label for="clientEmail">E-mail</label> *</td>
            <td>
                <input class="{validate:{required:true,email:true}}" title="Vul uw e-mailadres in" type="text" id="clientEmail" name="settings[clientEmail]" size="20" value="<?= _e(Settings::get('clientEmail')) ?>" />
            </td>
        </tr>
        <tr>
            <td class="withLabel"><label for="contactLatitude">Contact kaart latitude <div class="hasTooltip tooltip" title="Vul hier de latitude (breedtegraad) van de co&ouml;rdinaten in als u deze handmatig wilt bepalen.<br />- laat of maak dit veld leeg om deze waarde automatisch te laten (her)berekenen o.b.v. het adres">&nbsp;</div></label></td>
            <td>
                <input title="Vul de latitude in" type="text" id="contactLatitude" name="settings[contactLatitude]" size="20" value="<?= _e(Settings::get('contactLatitude')) ?>" />
            </td>
        </tr>
        <tr>
            <td class="withLabel"><label for="contactLongitude">Contact kaart longitude <div class="hasTooltip tooltip" title="Vul hier de longitude (lengtegraad) van de co&ouml;rdinaten in als u deze handmatig wilt bepalen.<br />- laat of maak dit veld leeg om deze waarde automatisch te laten (her)berekenen o.b.v. het adres">&nbsp;</div></label></td>
            <td>
                <input title="Vul de longitude in" type="text" id="contactLongitude" name="settings[contactLongitude]" size="20" value="<?= _e(Settings::get('contactLongitude')) ?>" />
            </td>
        </tr>
        <tr>
            <td style="padding-top: 10px;" colspan="2"><b>Social media</b></td>
        </tr>
        <tr>
            <td class="withLabel"><label for="socialFacebookLink">Facebook</label></td>
            <td>
                <input class="default" title="Vul de volledige link in naar uw Facebook account" type="text" id="socialFacebookLink" name="settings[socialFacebookLink]" value="<?= _e(Settings::get('socialFacebookLink')) ?>" />
            </td>
        </tr>
        <tr>
            <td class="withLabel"><label for="socialTwitterLink">Twitter</label></td>
            <td>
                <input class="default" title="Vul de volledige link in naar uw Twitter account" type="text" id="socialTwitterLink" name="settings[socialTwitterLink]" value="<?= _e(Settings::get('socialTwitterLink')) ?>" />
            </td>
        </tr>        
        <tr>
            <td class="withLabel"><label for="socialGooglePlusLink">Google+</label></td>
            <td>
                <input class="default" title="Vul de volledige link in naar uw Google+ account" type="text" id="socialGooglePlusLink" name="settings[socialGooglePlusLink]" value="<?= _e(Settings::get('socialGooglePlusLink')) ?>" />
            </td>
        </tr>
        <tr>
            <td class="withLabel"><label for="socialLinkedInLink">LinkedIn</label></td>
            <td>
                <input class="default" title="Vul de volledige link in naar uw LinkedIn account" type="text" id="socialLinkedInLink" name="settings[socialLinkedInLink]" value="<?= _e(Settings::get('socialLinkedInLink')) ?>" />
            </td>
        </tr>
        <tr>
            <td class="withLabel"><label for="socialPinterestLink">Pinterest</label></td>
            <td>
                <input class="default" title="Vul de volledige link in naar uw Pinterest account" type="text" id="socialPinterestLink" name="settings[socialPinterestLink]" value="<?= _e(Settings::get('socialPinterestLink')) ?>" />
            </td>
        </tr>

        <? if (BB_WITH_TWITTER_FEED) { ?>
            <tr>
                <td style="padding-top: 10px;" colspan="2"><b>Twitter feed instellingen</b></td>
            </tr>
            <tr>
                <td class="withLabel"><label for="maxNumTweets">Max. aantal tweets *</label>
                <td><select name="settings[maxNumTweets]" title="Maximum aantal tweets worden getoond">
                        <option value="0">-- onbeperkt --</option>
                        <option value="5" <?= intval(Settings::get('maxNumTweets')) === 5 ? 'selected' : '' ?> >5</option>
                        <option value="10" <?= intval(Settings::get('maxNumTweets')) === 10 ? 'selected' : '' ?>>10</option>
                        <option value="25" <?= intval(Settings::get('maxNumTweets')) === 25 ? 'selected' : '' ?>>25</option>
                        <option value="50" <?= intval(Settings::get('maxNumTweets')) === 50 ? 'selected' : '' ?>>50</option>
                    </select>
                </td>
            </tr>     
        <tr>
            <td class="withLabel"><label for="twitterConsumerKey">Consumer Key *</label></td>
            <td>
                <input class="default {validate:{required:true}}" title="Twitter consumer key" type="text" id="twitterConsumerKey" name="settings[twitterConsumerKey]" value="<?= _e(Settings::get('twitterConsumerKey')) ?>" />
            </td>
        </tr>  
        <tr>
            <td class="withLabel"><label for="twitterConsumerSecret">Consumer Secret *</label></td>
            <td>
                <input class="default {validate:{required:true}}" title="Twitter consumer secret" type="text" id="twitterConsumerSecret" name="settings[twitterConsumerSecret]" value="<?= _e(Settings::get('twitterConsumerSecret')) ?>" />
            </td>
        </tr>   
        <tr>
            <td class="withLabel"><label for="twitterAccessToken">Access Token</label></td>
            <td>
                <input class="default" title="Twitter access token" type="text" id="twitterAccessToken" name="settings[twitterAccessToken]" value="<?= _e(Settings::get('twitterAccessToken')) ?>" />
            </td>
        </tr>   
        <tr>
            <td class="withLabel"><label for="twitterAccessTokenSecret">Access Token Secret</label></td>
            <td>
                <input class="default" title="Twitter access token scret" type="text" id="twitterAccessTokenSecret" name="settings[twitterAccessTokenSecret]" value="<?= _e(Settings::get('twitterAccessTokenSecret')) ?>" />
            </td>
        </tr>             
        <? } ?>

        <? if ($oCurrentUser->isAdmin()) { ?>
            <tr>
                <td style="padding-top: 10px;" colspan="2"><b>Administrator instellingen</b></td>
            </tr>
            <tr>
                <td>Pagina niveaus</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Pagina niveaus `2`" type="radio" <?= Settings::get('pagesMaxLevels') == '2' ? 'CHECKED' : '' ?> id="pagesMaxLevels_2" name="settings[pagesMaxLevels]" value="2" /> <label for="pagesMaxLevels_2">2</label>
                    <input class="alignRadio {validate:{required:true}}" title="Pagina niveaus `3`" type="radio" <?= Settings::get('pagesMaxLevels') == '3' ? 'CHECKED' : '' ?> id="pagesMaxLevels_3" name="settings[pagesMaxLevels]" value="3" /> <label for="pagesMaxLevels_3">3</label>
                </td>
            </tr>

            <!-- MODULES -->
            <tr>
                <td colspan="2"><b><i>Modules</i></b></td>
            </tr>
            <tr>
                <td>Nieuws</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Nieuws module `Ja`" type="radio" <?= Settings::get('withNews') ? 'CHECKED' : '' ?> id="withNews_1" name="settings[withNews]" value="1" /> <label for="withNews_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Nieuws module `Nee`" type="radio" <?= !Settings::get('withNews') ? 'CHECKED' : '' ?> id="withNews_0" name="settings[withNews]" value="0" /> <label for="withNews_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Nieuws categorie&euml;n</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Nieuws categorie&euml;n module `Ja`" type="radio" <?= Settings::get('withNewsCategories') ? 'CHECKED' : '' ?> id="withNewsCategories_1" name="settings[withNewsCategories]" value="1" /> <label for="withNewsCategories_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Nieuws categorie&euml;n module `Nee`" type="radio" <?= !Settings::get('withNewsCategories') ? 'CHECKED' : '' ?> id="withNewsCategories_0" name="settings[withNewsCategories]" value="0" /> <label for="withNewsCategories_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Fotoalbums</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Fotoalbums module `Ja`" type="radio" <?= Settings::get('withPhotoAlbums') ? 'CHECKED' : '' ?> id="withPhotoAlbums_1" name="settings[withPhotoAlbums]" value="1" /> <label for="withPhotoAlbums_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Fotoalbums module `Nee`" type="radio" <?= !Settings::get('withPhotoAlbums') ? 'CHECKED' : '' ?> id="withPhotoAlbums_0" name="settings[withPhotoAlbums]" value="0" /> <label for="withPhotoAlbums_0">Nee</label>
                </td>
            </tr>

            <tr>
                <td>Catalogus</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Catalogus module `Ja`" type="radio" <?= Settings::get('withCatalog') ? 'CHECKED' : '' ?> id="withCatalog_1" name="settings[withCatalog]" value="1" /> <label for="withCatalog_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Catalogus module `Nee`" type="radio" <?= !Settings::get('withCatalog') ? 'CHECKED' : '' ?> id="withCatalog_0" name="settings[withCatalog]" value="0" /> <label for="withCatalog_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Klanten</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Klanten module `Ja`" type="radio" <?= Settings::get('withCustomers') ? 'CHECKED' : '' ?> id="withCustomers_1" name="settings[withCustomers]" value="1" /> <label for="withCustomers_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Klanten module `Nee`" type="radio" <?= !Settings::get('withCustomers') ? 'CHECKED' : '' ?> id="withCustomers_0" name="settings[withCustomers]" value="0" /> <label for="withCustomers_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Bestellingen</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Bestellingen module `Ja`" type="radio" <?= Settings::get('withOrders') ? 'CHECKED' : '' ?> id="withOrders_1" name="settings[withOrders]" value="1" /> <label for="withOrders_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Bestellingen module `Nee`" type="radio" <?= !Settings::get('withOrders') ? 'CHECKED' : '' ?> id="withOrders_0" name="settings[withOrders]" value="0" /> <label for="withOrders_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Kassa</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Kassa module `Ja`" type="radio" <?= Settings::get('withKassa') ? 'CHECKED' : '' ?> id="withKassa_1" name="settings[withKassa]" value="1" /> <label for="withKassa_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Kassa module `Nee`" type="radio" <?= !Settings::get('withKassa') ? 'CHECKED' : '' ?> id="withKassa_0" name="settings[withKassa]" value="0" /> <label for="withKassa_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Motoren</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Motoren module `Ja`" type="radio" <?= Settings::get('withMotoren') ? 'CHECKED' : '' ?> id="withMotoren_1" name="settings[withMotoren]" value="1" /> <label for="withMotoren_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Motoren module `Nee`" type="radio" <?= !Settings::get('withMotoren') ? 'CHECKED' : '' ?> id="withMotoren_0" name="settings[withMotoren]" value="0" /> <label for="withMotoren_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Sitemap uitgebreid</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Uitgebreide sitemap module `Ja`" type="radio" <?= Settings::get('withAdvancedSitemap') ? 'CHECKED' : '' ?> id="withAdvancedSitemap_1" name="settings[withAdvancedSitemap]" value="1" /> <label for="withAdvancedSitemap_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Uitgebreide sitemap module `Nee`" type="radio" <?= !Settings::get('withAdvancedSitemap') ? 'CHECKED' : '' ?> id="withAdvancedSitemap_0" name="settings[withAdvancedSitemap]" value="0" /> <label for="withAdvancedSitemap_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Agenda</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Uitgebreide agenda module `Ja`" type="radio" <?= Settings::get('withAgendaItems') ? 'CHECKED' : '' ?> id="withAgendaItems_1" name="settings[withAgendaItems]" value="1" /> <label for="withAgendaItems_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Uitgebreide agenda module `Nee`" type="radio" <?= !Settings::get('withAgendaItems') ? 'CHECKED' : '' ?> id="withAgendaItems_0" name="settings[withAgendaItems]" value="0" /> <label for="withAgendaItems_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Coupons</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Coupon module `Ja`" type="radio" <?= Settings::get('withCoupons') ? 'CHECKED' : '' ?> id="withCoupons_1" name="settings[withCoupons]" value="1" /> <label for="withCoupons_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Coupon module `Nee`" type="radio" <?= !Settings::get('withCoupons') ? 'CHECKED' : '' ?> id="withCoupons_0" name="settings[withCoupons]" value="0" /> <label for="withCoupons_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Trustpilot</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Trustpilot module `Ja`" type="radio" <?= Settings::get('withTrustpilot') ? 'CHECKED' : '' ?> id="withTrustpilot_1" name="settings[withTrustpilot]" value="1" /> <label for="withTrustpilot_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Trustpilot module `Nee`" type="radio" <?= !Settings::get('withTrustpilot') ? 'CHECKED' : '' ?> id="withTrustpilot_0" name="settings[withTrustpilot]" value="0" /> <label for="withTrustpilot_0">Nee</label>
                </td>
            </tr>

            <tr>
                <td>Facebook like Box</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Facebook like box module `Ja`" type="radio" <?= Settings::get('withFbLikeBox') ? 'CHECKED' : '' ?> id="withFbLikeBox_1" name="settings[withFbLikeBox]" value="1" /> <label for="withFbLikeBox_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Facebook like box module `Nee`" type="radio" <?= !Settings::get('withFbLikeBox') ? 'CHECKED' : '' ?> id="withFbLikeBox_0" name="settings[withFbLikeBox]" value="0" /> <label for="withFbLikeBox_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Klik of bel</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Klik of bel module `Ja`" type="radio" <?= Settings::get('withClickCall') ? 'CHECKED' : '' ?> id="withClickCall_1" name="settings[withClickCall]" value="1" /> <label for="withClickCall_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Klik of bel module `Nee`" type="radio" <?= !Settings::get('withClickCall') ? 'CHECKED' : '' ?> id="withClickCall_0" name="settings[withClickCall]" value="0" /> <label for="withClickCall_0">Nee</label>
                </td>
            </tr>   
            <tr>
                <td>Bel mij terug form</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Bel mij terug form `Ja`" type="radio" <?= Settings::get('withCallMeBack') ? 'CHECKED' : '' ?> id="withCallMeBack_1" name="settings[withCallMeBack]" value="1" /> <label for="withCallMeBack_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Bel mij terug form `Nee`" type="radio" <?= !Settings::get('withCallMeBack') ? 'CHECKED' : '' ?> id="withCallMeBack_0" name="settings[withCallMeBack]" value="0" /> <label for="withCallMeBack_0">Nee</label>
                </td>
            </tr>     
            <tr>
                <td>Twitter feed</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Twitter feed module `Ja`" type="radio" <?= Settings::get('withTwitterFeed') ? 'CHECKED' : '' ?> id="withTwitterFeed_1" name="settings[withTwitterFeed]" value="1" /> <label for="withTwitterFeed_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Twitter feed module `Nee`" type="radio" <?= !Settings::get('withTwitterFeed') ? 'CHECKED' : '' ?> id="withTwitterFeed_0" name="settings[withTwitterFeed]" value="0" /> <label for="withTwitterFeed_0">Nee</label>
                </td>
            </tr>   
            <tr>
                <td>Doorlopende slider</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Doorlopende slider module `Ja`" type="radio" <?= Settings::get('withContinuousSlider') ? 'CHECKED' : '' ?> id="withContinuousSlider_1" name="settings[withContinuousSlider]" value="1" /> <label for="withContinuousSlider_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Doorlopende slider module `Nee`" type="radio" <?= !Settings::get('withContinuousSlider') ? 'CHECKED' : '' ?> id="withContinuousSlider_0" name="settings[withContinuousSlider]" value="0" /> <label for="withContinuousSlider_0">Nee</label>
                </td>
            </tr>             
            
            <!-- Product Feed -->
            <tr>
                <td>Google</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Google Product Feed `Ja`" type="radio" <?= Settings::get('withGoogleFeed') ? 'CHECKED' : '' ?> id="withGoogleFeed_1" name="settings[withGoogleFeed]" value="1" /> <label for="withGoogleFeed_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Google Product Feed `Nee`" type="radio" <?= !Settings::get('withGoogleFeed') ? 'CHECKED' : '' ?> id="withGoogleFeed_0" name="settings[withGoogleFeed]" value="0" /> <label for="withGoogleFeed_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Beslist</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Beslist Product Feed `Ja`" type="radio" <?= Settings::get('withBeslistFeed') ? 'CHECKED' : '' ?> id="withBeslist_1" name="settings[withBeslistFeed]" value="1" /> <label for="withBeslist_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Beslist Product Feed `Nee`" type="radio" <?= !Settings::get('withBeslistFeed') ? 'CHECKED' : '' ?> id="withBeslist_0" name="settings[withBeslistFeed]" value="0" /> <label for="withBeslist_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Tradetracker</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Tradetracker Product Feed `Ja`" type="radio" <?= Settings::get('withTradetrackerFeed') ? 'CHECKED' : '' ?> id="withTradetracker_1" name="settings[withTradetrackerFeed]" value="1" /> <label for="withBeslist_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Tradetracker Product Feed `Nee`" type="radio" <?= !Settings::get('withTradetrackerFeed') ? 'CHECKED' : '' ?> id="withTradetracker_0" name="settings[withTradetrackerFeed]" value="0" /> <label for="withBeslist_0">Nee</label>
                </td>
            </tr>
        <? } ?>
        <!-- Product Feed -->
        <? if (BB_WITH_BESLIST_FEED || BB_WITH_GOOGLE_FEED || BB_WITH_TRADETRACKER_FEED) { ?>
            <tr>
                <td style="padding-top: 10px;"><b>Product Feed</b></td>
            </tr>
            <tr>
                <td class="withLabel"><label for="defaultDeliveryMethod">Standaard leveringswijze</label></td>
                <td>
                    <select id='defaultDeliveryMethod' name='settings[defaultDeliveryMethod]'>
                        <option value='0'>Selecteer een optie </option>
                        <? foreach (DeliveryMethodManager::getAllDeliveryMethods() as $oDeliveryMethod) { ?>
                            <option <?= (Settings::get('defaultDeliveryMethod') == $oDeliveryMethod->deliveryMethodId) ? 'SELECTED' : '' ?> value='<?= $oDeliveryMethod->deliveryMethodId ?>'><?= $oDeliveryMethod->name ?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="withLabel"><label for="defaultPaymentMethod">Standaard betaalmethode</label></td>
                <td>
                    <select id='defaultPaymentMethod' name='settings[defaultPaymentMethod]'>
                        <option value='0'>Selecteer een optie </option>
                        <? foreach (PaymentMethodManager::getPaymentMethodsByFilter() as $oPaymentMethod) { ?>
                            <option <?= (Settings::get('defaultPaymentMethod') == $oPaymentMethod->paymentMethodId) ? 'SELECTED' : '' ?> value='<?= $oPaymentMethod->paymentMethodId ?>'><?= $oPaymentMethod->name ?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
        <? } ?>
        <!-- Trustpilot URL's -->
        <? if ($oCurrentUser->isAdmin()) { ?>
            <tr>
                <td style="padding-top: 10px;"><b>Trustpilot</b></td>
            </tr>
            <? if (Settings::get('withTrustpilot')) { ?>
                <tr>
                    <td class="withLabel"><label for="socialTrustpilotLink">Trustpilot URL</label></td>
                    <td>
                        <input class="default" title="Vul de volledige link in naar uw Trustpilot account" type="text" id="socialTrustpilotLink" name="settings[socialTrustpilotLink]" value="<?= _e(Settings::get('socialTrustpilotLink')) ?>" />
                    </td>
                </tr>
            <? } ?>
            <!-- Facebook URL's -->
            <tr>
                <td style="padding-top: 10px;"><b>Facebook</b></td>
            </tr>
            <? if (Settings::get('withFbLikeBox')) { ?>
                <tr>
                    <td class="withLabel"><label for="fbAppId">Facebook like box App Id</label></td>
                    <td>
                        <input class="default" title="Vul de volledige link in naar uw Facebook Like Box" type="text" id="fbAppId" name="settings[fbAppId]" value="<?= _e(Settings::get('fbAppId')) ?>" />
                    </td>
                </tr>
            <? } ?>
        <? } ?>

        <!-- CATALOGUS MANAGEMENT -->
        <? if (BB_WITH_CATALOG) { ?>
            <tr>
                <td style="padding-top: 10px;"><b>Catalogus</b></td>
            </tr>
            <tr>
                <td>Toon ook de excl. BTW prijs *</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Toon ook de excl. BTW prijs `Ja`" type="radio" <?= Settings::get('showExclVAT') ? 'CHECKED' : '' ?> id="showExclVAT_1" name="settings[showExclVAT]" value="1" /> <label for="showExclVAT_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Toon ook de excl. BTW prijs `Nee`" type="radio" <?= !Settings::get('showExclVAT') ? 'CHECKED' : '' ?> id="showExclVAT_0" name="settings[showExclVAT]" value="0" /> <label for="showExclVAT_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Toon producten op de homepage *</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Toon producten op de homepage `Ja`" type="radio" <?= Settings::get('showProductsOnHome') ? 'CHECKED' : '' ?> id="showProductsOnHome_1" name="settings[showProductsOnHome]" value="1" /> <label for="showProductsOnHome_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Toon producten op de homepage `Nee`" type="radio" <?= !Settings::get('showProductsOnHome') ? 'CHECKED' : '' ?> id="showProductsOnHome_0" name="settings[showProductsOnHome]" value="0" /> <label for="showProductsOnHome_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Prijzen zijn inclusief BTW *</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Prijzen zijn inclusief BTW `Ja`" type="radio" <?= Settings::get('taxIncluded') ? 'CHECKED' : '' ?> id="taxIncluded_1" name="settings[taxIncluded]" value="1" /> <label for="taxIncluded_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Prijzen zijn inclusief BTW `Nee`" type="radio" <?= !Settings::get('taxIncluded') ? 'CHECKED' : '' ?> id="taxIncluded_0" name="settings[taxIncluded]" value="0" /> <label for="taxIncluded_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Toon alle opties in filter * <div class="hasTooltip tooltip" title="- Ja: alle filter opties worden getoond op de productcategorie detail pagina ongeacht of er<br> producten met meerdere producttypes met andere eigenschappen in de resultaten zitten<br>- Nee: als er producten van meer dan 1 producttype in de resultaten zit zal de klant eerst<br> een producttype moeten kiezen, pas daarna wordt het volledige filter weergegeven.">&nbsp;</div></td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Toon alle opties in filter `Ja`" type="radio" <?= Settings::get('catalogFilterShowAllOptions') ? 'CHECKED' : '' ?> id="catalogFilterShowAllOptions_1" name="settings[catalogFilterShowAllOptions]" value="1" /> <label for="catalogFilterShowAllOptions_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Toon alle opties in filter `Nee`" type="radio" <?= !Settings::get('catalogFilterShowAllOptions') ? 'CHECKED' : '' ?> id="catalogFilterShowAllOptions_0" name="settings[catalogFilterShowAllOptions]" value="0" /> <label for="catalogFilterShowAllOptions_0">Nee</label>
                </td>
            </tr>
            <tr>
                <td>Prijsdaling producten met korting * <div class="hasTooltip tooltip" title="Prijsdaling producten met korting">&nbsp;</div></td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Toon alle opties in filter `Ja`" type="radio" <?= Settings::get('reducedPriceWithDiscount') ? 'CHECKED' : '' ?> id="catalogFilterShowAllOptions_1" name="settings[reducedPriceWithDiscount]" value="1" /> <label for="reducedPriceWithDiscount">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Toon alle opties in filter `Nee`" type="radio" <?= !Settings::get('reducedPriceWithDiscount') ? 'CHECKED' : '' ?> id="catalogFilterShowAllOptions_0" name="settings[reducedPriceWithDiscount]" value="0" /> <label for="reducedPriceWithDiscount">Nee</label>
                </td>
            </tr>
            <!-- Email orders -->
            <tr>
                <td class="withLabel"><label for="defaultEmailOrders">Standaard e-mail bestellingen *</label> <div class="hasTooltip tooltip" title="E-mailadres waar orderstatus wijzigingen naar gestuurd worden">&nbsp;</div></td>
                <td>
                    <input class="default {validate:{required:true}}" title="Voer standaard e-mailadres" type="text" id="defaultEmailOrders" name="settings[defaultEmailOrders]" value="<?= _e(Settings::get('defaultEmailOrders')) ?>" />
                </td>
            </tr>  
            <tr>
                <td class="withLabel"><label for="newOrderClientBCC">BCC nieuwe order klant *</label> <div class="hasTooltip tooltip" title="E-mailadres waar een BCC van `de email naar de klant` wordt gestuurd">&nbsp;</div></td>
                <td>
                    <input class="default" type="text" id="newOrderClientBCC" name="settings[newOrderClientBCC]" value="<?= _e(Settings::get('newOrderClientBCC')) ?>" />
                </td>
            </tr>  

            <tr>
                <td class="withLabel"><label for="catalogFilterMaxChars">Max aantal tekens filter</label> <div class="hasTooltip tooltip" title="Vul hier een aantal tekens in waarbij de namen van de opties in het filter moeten worden afgebroken.<br>Soms zijn de namen te lang en is het mooier om deze af te kappen.<br>Normaliter zou een standaard waarde van 17 voldoende kort moeten zijn.">&nbsp;</div></td>
                <td>
                    <input class="default {validate:{required:true,digits:true,min:1}}" title="Vul een heel getal in van tenminste 1 of hoger" type="text" id="catalogFilterMaxChars" name="settings[catalogFilterMaxChars]" value="<?= _e(Settings::get('catalogFilterMaxChars')) ?>" />
                </td>
            </tr>            
            <tr>
                <td class="withLabel" ><label for="logoImageId">Order logo</label></td>
                <td>
                    <?
                    $oLogoIdSetting = SettingManager::getSettingByName('orderLogoImage');

                    if (!empty($oLogoIdSetting) && !empty($oLogoIdSetting->value)) {
                        $iLogoId = intval($oLogoIdSetting->value);
                        $oImage = ImageManager::getImageById($iLogoId);
                        ?>
                        <div class="images">
                            <div class="placeholder">
                                <?
                                if (!$oImage->hasImageFiles(array('original', 'cms_thumb', 'resize'))) {
                                    echo '<img class="notAllCrops" src="' . ADMIN_FOLDER . '/images/layout/icons/exclamation_icon.png" alt="Let op: niet alle uitsnedes zijn gemaakt" title="Let op: niet alle uitsnedes zijn gemaakt" />';
                                }
                                ?>
                                <div class="imagePlaceholder">
                                    <div class="centered">
                                        <img src="<?= $oImage->getImageFileByReference('cms_thumb')->link . '?' . time() ?>" alt="<?= $oImage->getImageFileByReference('cms_thumb')->title ?>" <?= $oImage->getImageFileByReference('cms_thumb')->imageSizeAttr ?> title="<?= $oImage->getImageFileByReference('cms_thumb')->title ?>" />
                                    </div>
                                </div>
                                <div class="actionsPlaceholder">
                                    <a class="action_icon delete_icon" onclick="return confirmChoice('dit logo');" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/delete-order-logo'; ?>"></a>
                                </div>
                            </div>
                        </div>
                    <? } else { ?>
                        <input type="file" title="Kies een bestand met een van de volgende extensies: jpg, jpeg, png, gif" class="{validate:{accept:'jpg,jpeg,png,gif'}}" name="logo" />
                    <? } ?>
                </td>
            </tr>
        <? } ?>

        <!-- MOTOREN -->
        <? if (BB_WITH_MOTOREN && $oCurrentUser->isAdmin()) { ?>
            <tr>
                <td style="padding-top: 10px;" colspan="2"><b>Motoren</b></td>
            </tr>
            <tr>
                <td>API key *</td>
                <td>
                    <input class="{validate:{required:true}} default" title="Vul de API key in" type="text" id="motorenAPIKey" name="settings[motorenAPIKey]" value="<?= _e(Settings::get('motorenAPIKey')) ?>" />
                </td>
            </tr>
            <tr>
                <td>Company ID *</td>
                <td>
                    <input class="{validate:{required:true, digits:true}} default" title="Vul de Company ID" type="text" id="motorenCompanyId" name="settings[motorenCompanyId]" value="<?= _e(Settings::get('motorenCompanyId')) ?>" />
                </td>
            </tr>
            <tr>
                <td>Toon motoren op de homepage *</td>
                <td>
                    <input class="alignRadio {validate:{required:true}}" title="Toon motoren op de homepage `Ja`" type="radio" <?= Settings::get('showBikesOnHome') ? 'CHECKED' : '' ?> id="showBikesOnHome_1" name="settings[showBikesOnHome]" value="1" /> <label for="showBikesOnHome_1">Ja</label>
                    <input class="alignRadio {validate:{required:true}}" title="Toon motoren op de homepage `Nee`" type="radio" <?= !Settings::get('showBikesOnHome') ? 'CHECKED' : '' ?> id="showBikesOnHome_0" name="settings[showBikesOnHome]" value="0" /> <label for="showBikesOnHome_0">Nee</label>
                </td>
            </tr>
            <? if (BB_WITH_CATALOG) { ?>
                <tr>
                    <td>Toon motoren boven producten *</td>
                    <td>
                        <input class="alignRadio {validate:{required:true}}" title="Toon motoren op de homepage boven de producten `Ja`" type="radio" <?= Settings::get('showBikesFirst') ? 'CHECKED' : '' ?> id="showBikesFirst_1" name="settings[showBikesFirst]" value="1" /> <label for="showBikesFirst_1">Ja</label>
                        <input class="alignRadio {validate:{required:true}}" title="Toon motoren op de homepage boven de producten `Nee`" type="radio" <?= !Settings::get('showBikesFirst') ? 'CHECKED' : '' ?> id="showBikesFirst_0" name="settings[showBikesFirst]" value="0" /> <label for="showBikesFirst_0">Nee</label>
                    </td>
                </tr>
            <? } ?>
        <? } ?>
        <? if (BB_WITH_KASSA && $oCurrentUser->isAdmin()) { ?>
            <tr>
                <td style="padding-top: 10px;" colspan="2"><b>Mollie</b></td>
            </tr>
            <tr>
                <td>API key *</td>
                <td>
                    <input class="{validate:{required:true}} default" title="Vul de API key in" type="text" id="mollieAPIKey" name="settings[mollieAPIKey]" value="<?= _e(Settings::get('mollieAPIKey')) ?>" />
                </td>
            </tr>
        <? } ?>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">
                <input type="submit" value="Opslaan" name="save" />
            </td>
        </tr>
    </table>
</form>