<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="5"><h2>Alle tweets</h2><a style="float: right;" href="?executeCron">Exectute cron</a></td>
        </tr>
        <tr>
            <th>tweetID</th>
            <th>user</th>
            <th>text</th>
            <th>retweet</th>
            <th>created @ twitter</th>
        </tr>
    </thead>
    <tbody>
        <?
        $aTweets = TweetManager::getAllTweets();
        foreach ($aTweets AS $oTweet) {
            echo '<tr>';
            echo '<td>' . $oTweet->tweetId . '</td>';
            echo '<td>' . $oTweet->screen_name . '</td>';
            echo '<td>' . $oTweet->text . '</td>';
            echo '<td>' . $oTweet->retweet . '</td>';
            echo '<td>' . $oTweet->created_at . '</td>';
            echo '</tr>';
        }
        if (empty($aTweets)) {
            echo '<tr><td colspan="5"><i>Er zijn geen tweets weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>