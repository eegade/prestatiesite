<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="6"><h2>Alle fotoalbums</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Nieuw fotoalbum toevoegen" alt="Nieuw fotoalbum toevoegen">Fotoalbum toevoegen</a></div></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted onlineOffline">&nbsp;</th>
            <th class="">Date</th>
            <th>Titel</th>
            <th>Aantal foto's</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aPhotoAlbums AS $oPhotoAlbum) {
            echo '<tr>';
            echo '<td>';

            # online offline button
            echo '<a id="photoAlbum_' . $oPhotoAlbum->photoAlbumId . '_online_1" title="Fotoalbum offline zetten" class="action_icon ' . ($oPhotoAlbum->online ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oPhotoAlbum->photoAlbumId . '/?online=0"></a>';
            echo '<a id="photoAlbum_' . $oPhotoAlbum->photoAlbumId . '_online_0" title="Fotoalbum online zetten" class="action_icon ' . ($oPhotoAlbum->online ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oPhotoAlbum->photoAlbumId . '/?online=1"></a>';

            echo '</td>';
            echo '<td>' . Date::strToDate($oPhotoAlbum->date)->format('%d-%m-%Y') . '</td>';
            echo '<td>' . _e($oPhotoAlbum->title) . '</td>';
            echo '<td>' . count($oPhotoAlbum->getImages('all')) . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk fotoalbum" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oPhotoAlbum->photoAlbumId . '"></a>';
            echo '<a class="action_icon delete_icon" title="Verwijder fotoalbum" onclick="return confirmChoice(\'' . _e($oPhotoAlbum->title) . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oPhotoAlbum->photoAlbumId . '"></a>';
            echo '</td>';
            echo '</tr>';
        }
        if (count($aPhotoAlbums) == 0) {
            echo '<tr><td colspan="6"><i>Er zijn geen fotoalbums weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>
<?
# add ajax code for online/offline handling
$sOnlineOfflineJavascript = <<<EOT
<script>
    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: this.href,
            data: "ajax=1",
            async: true,
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if(dataObj.success == true){
                    $("#photoAlbum_"+dataObj.photoAlbumId+"_online_0").hide(); // hide offline button
                    $("#photoAlbum_"+dataObj.photoAlbumId+"_online_1").hide(); // hide online button
                    $("#photoAlbum_"+dataObj.photoAlbumId+"_online_"+dataObj.online).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.online == 0)
                        showStatusUpdate("Fotoalbum offline gezet");
                    if(dataObj.online == 1)
                        showStatusUpdate("Fotoalbum online gezet");
                }else{
                        showStatusUpdate("Fotoalbum niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sOnlineOfflineJavascript);
?>