<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar fotoalbum overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Fotoalbum</legend>
                <table class="withForm">
                    <tr>
                        <td>Online *</td>
                        <td>
                            <input class="alignRadio {validate:{required:true}}" title="Zet het fotoalbum online OF offline" type="radio" <?= $oPhotoAlbum->online ? 'CHECKED' : '' ?> id="online_1" name="online" value="1" /> <label for="online_1">Ja</label>
                            <input class="alignRadio {validate:{required:true}}" title="Zet het fotoalbum online OF offline" type="radio" <?= !$oPhotoAlbum->online ? 'CHECKED' : '' ?> id="online_0" name="online" value="0" /> <label for="online_0">Nee</label>
                        </td>
                        <td><span class="error"><?= $oPhotoAlbum->isPropValid("online") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel" style="width: 116px;"><label for="title">Titel *</label></td>
                        <td><input id="title" class="{validate:{required:true}} autofocus default" title="Vul de titel in van het fotoalbum" type="text" autocomplete="off" name="title" value="<?= _e($oPhotoAlbum->title) ?>" /></td>
                        <td><span class="error"><?= $oPhotoAlbum->isPropValid("title") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="shortTitle">Menu link tekst</label></td>
                        <td><input class="default" id="shortTitle" type="text" name="shortTitle" value="<?= _e($oPhotoAlbum->shortTitle) ?>" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="date">Datum *</label></td>
                        <td>
                            <input title="Geef een geldige datum in (dd-mm-jjjj)" size="9" id="date" class="datePickerDefault hasDatePicker {validate:{required:true, dateNL:true}}" type="text" name="date" value="<?= $oPhotoAlbum->date ? Date::strToDate($oPhotoAlbum->date)->format("%d-%m-%Y") : Date::strToDate('NOW')->format("%d-%m-%Y") ?>" />
                        </td>
                        <td><span class="error"><?= $oPhotoAlbum->isPropValid("date") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="content">Album omschrijving</label></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3"><textarea name="content" id="content" class="tiny_MCE_default tiny_MCE"><?= $oPhotoAlbum->content ?></textarea></td>
                    </tr>
                    <tr>
                        <td colspan="3"><h2>SEO</h2></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="windowTitle">Window titel</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - De titel moet een goed leesbare zin zijn, met het belangrijkste zoekwoord van deze pagina zoveel mogelijk vooraan.<br /> - De titel moet tussen de 8 en 69 karakters zijn (incl. spaties).<br /> - Gebruik niet enkel de bedrijfsnaam in de titel.<br /> - Gebruik geen domeinnaam.">&nbsp;</div></td>
                        <td colspan="2">
                            <div class="inline-block">
                                <input class="default charCounterWindowTitle" id="windowTitle" type="text" name="windowTitle" value="<?= _e($oPhotoAlbum->windowTitle) ?>" />
                                <div id="windowTitleCounter" style="text-align: right;"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="metaDescription">Description</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - Omschrijving die Google op kan nemen in de zoekresultaten.<br /> - Deze zin moet aantrekkelijk en wervend genoeg zijn om zoekers te stimuleren om op het resultaat te klikken.<br /> - Tussen de 46 en 156 karakters (incl. spaties).">&nbsp;</div></td>
                        <td colspan="2">
                            <div class="inline-block">
                                <textarea class="default charCounterMetaDescription" id="metaDescription" name="metaDescription"><?= _e($oPhotoAlbum->metaDescription) ?></textarea>
                                <div id="metaDescriptionCounter" style="text-align: right;"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="metaKeywords">Keywords</label> <div class="hasTooltip tooltip" title="Plaats hier voor deze pagina, een aantal relevante woorden (maximaal 10 woorden).<br />Scheidt de woorden van elkaar met een komma en een spatie.<br />- zoekmachines gebruiken keywords vooral om misbruik op te sporen<br />- goed gebruik van keywords zal geen of een hele kleine bijdrage leveren aan de vindbaarheid van deze pagina">&nbsp;</div></td>
                        <td colspan="2"><input class="default" id="metaKeywords" type="text" name="metaKeywords" value="<?= _e($oPhotoAlbum->metaKeywords) ?>" /></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
    <?
    ?>
    <div class="contentColumn">
        <fieldset>
            <legend>Afbeeldingen</legend>
            <?
            if ($oPhotoAlbum->photoAlbumId !== null) {
                $oImageManagerHTML->includeTemplate();
            } else {
                echo '<p><i>Afbeeldingen kunnen worden geüpload nadat het fotoalbum eerst is opgeslagen</i></p>';
            }
            ?>
        </fieldset>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar fotoalbum overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
$oPageLayout->addJavascriptBottom('
<script>
    initTinyMCE(".tiny_MCE_default", "/admin/paginas/link-list", "/admin/fotoalbums/image-list/' . $oPhotoAlbum->photoAlbumId . '");
</script>
');
?>