<table class="sorted withActionIcons">
    <thead>
        <tr class="topRow">
            <td colspan="3"><h2>Alle brandbox items</h2>
                <div class="right">
                    <a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Brandbox item toevoegen" alt="Brandbox item toevoegen">Brandbox item toevoegen</a><br />
                    <a class="changeOrderBtn textLeft right" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/volgorde-wijzigen&lang=1" title="Volgorde wijzigen" alt="Volgorde wijzigen">Volgorde wijzigen</a>
                </div>
            </td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted onlineOffline"></th>
            <th>Naam</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody id="tableContents">
        <?
        foreach ($aAllBrandboxItems AS $oBrandboxItem) {
            echo '<tr data-id="' . $oBrandboxItem->brandboxItemId . '" class="movable">';
            echo '<td>';
            # online offline button
            echo '<a id="brandboxItem_' . $oBrandboxItem->brandboxItemId . '_online_1" title="Brandbox item offline zetten" class="action_icon ' . ($oBrandboxItem->online ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oBrandboxItem->brandboxItemId . '/?online=0"></a>';
            echo '<a id="brandboxItem_' . $oBrandboxItem->brandboxItemId . '_online_0" title="Brandbox item online zetten" class="action_icon ' . ($oBrandboxItem->online ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oBrandboxItem->brandboxItemId . '/?online=1"></a>';
            echo '</td>';
            echo '<td>' . $oBrandboxItem->name . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk brandbox item" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oBrandboxItem->brandboxItemId . '"></a>';
            echo '<a class="action_icon delete_icon" title="Verwijder brandbox item" onclick="return confirmChoice(\'' . $oBrandboxItem->getName() . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oBrandboxItem->brandboxItemId . '"></a>';
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aAllBrandboxItems)) {
            echo '<tr><td colspan="3"><i>Er zijn geen brandbox items om weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>

<?
# create necessary javascript
$sController = http_get('controller');
$sBottomJavascript = <<<EOT
<script type="text/javascript">
    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            data: 'ajax=1',
            async: true,
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $("#brandboxItem_"+dataObj.brandboxItemId+"_online_0").hide(); // hide offline button
                    $("#brandboxItem_"+dataObj.brandboxItemId+"_online_1").hide(); // hide online button
                    $("#brandboxItem_"+dataObj.brandboxItemId+"_online_"+dataObj.online).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.online == 0)    
                        showStatusUpdate("Brandbox item offline gezet");
                    if(dataObj.online == 1)    
                        showStatusUpdate("Brandbox item online gezet");                            
                } else {
                    showStatusUpdate("Brandbox item niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>