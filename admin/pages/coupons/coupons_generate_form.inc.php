<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCouponRule->couponRuleId ?>">Terug naar couponregel</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="generate" name="action" />
            <fieldset>
                <legend>Coupons genereren</legend>
                <table class="withForm">
                    <tr>
                        <td class="withLabel" style="width: 116px;"><label for="couponAmount">Aantal *</label> <div class="hasTooltip tooltip" title="Het aantal coupons dat gegenereerd moet worden">&nbsp;</div></td>
                        <td><input autocomplete="off" class="{validate:{required:true,number:true,min:1}} numbersOnly" id="couponAmount" name="couponAmount" style="width: 40px;" title="Vul een geldige aantal in" type="text" value="<?= _e(http_post('couponAmount')) ?>" /></td>
                        <td><span class="error"><?= http_post("action") == 'generate' && (!is_numeric(http_post('couponAmount')) || http_post('couponAmount') < 1) ? 'Veld niet (juist) ingevuld' : '' ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCouponRule->couponRuleId ?>">Terug naar couponregel</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
# create necessary javascript
$sAdminJsFolder = ADMIN_JS_FOLDER;
$sBottomJavascript = <<<EOT
<script src="{$sAdminJsFolder}/autoNumeric-1.7.5.js"></script>
<script>
    // force numbers on specific input fuelds
    $('input.floatOnly').autoNumeric({
        aSep: '',
        altDec: ',',
        mDec: 0,
        vMin: 1
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>