<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar couponregels overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf" style="margin-bottom: 50px;">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Couponregel</legend>
                <table class="withForm">                    
                    <tr>
                        <td>Actief *</td>
                        <td>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de couponregel actief OF inactief" type="radio" <?= $oCouponRule->active ? 'CHECKED' : '' ?> id="active_1" name="active" value="1" /> <label for="active_1">Ja</label>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de couponregel actief OF inactief" type="radio" <?= !$oCouponRule->active ? 'CHECKED' : '' ?> id="active_0" name="active" value="0" /> <label for="active_0">Nee</label>
                        </td>
                        <td><span class="error"><?= $oCouponRule->isPropValid("active") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>  
                    <tr>
                        <td class="withLabel" style="width: 126px;"><label for="title">Titel *</label> <div class="hasTooltip tooltip" title="De titel van de coupon">&nbsp;</div></td>
                        <td><input id="title" class="{validate:{required:true}} autofocus default" title="Vul de titel in van de couponregel" type="text" autocomplete="off" name="title" value="<?= _e($oCouponRule->title) ?>" /></td>
                        <td><span class="error"><?= $oCouponRule->isPropValid("title") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="reference">Referentie *</label> <div class="hasTooltip tooltip" title="De referentie van de coupon. Probeer deze zo uniek mogelijk te maken.">&nbsp;</div></td>
                        <td><input id="reference" class="{validate:{required:true}} default" title="Vul de referentie in van de couponregel" type="text" autocomplete="off" name="reference" value="<?= _e($oCouponRule->reference) ?>" /></td>
                        <td><span class="error"><?= $oCouponRule->isPropValid("reference") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="discountType">Kortingstype</label> <div class="hasTooltip tooltip" title="De kortingstype van deze couponregel.">&nbsp;</div></td>
                        <td>
                            <select class="{validate:{required:true}}" id="discountType" title="Kies een kortingstype" name="discountType">
                                <option value="">Maak een keuze</option>
                                <?
                                foreach (CouponRuleManager::getAllDiscountTypes() as $sDiscountType) {
                                    echo '<option ' . ($oCouponRule->discountType == $sDiscountType ? 'SELECTED' : '') . ' value="' . $sDiscountType . '">' . CouponRuleManager::getLabelByDiscountType($sDiscountType) . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td><span class="error"><?= $oCouponRule->isPropValid("discountType") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="discountAmount">Korting <span class="taxLabel <?= $oCouponRule->discountType != CouponRule::DISCOUNTTYPE_ORDER_FIXED_PRICE ? 'hide' : '' ?>">(<?= Settings::get('taxIncluded') ? 'incl' : 'excl'; ?>. BTW)</span> *</label> <div class="hasTooltip tooltip" title="De hoeveelheid korting">&nbsp;</div></td>
                        <td><input size="6" id="discountAmount" class="{validate:{required:true}} floatOnly" title="Vul een geldige korting in" type="text" autocomplete="off" name="discountAmount" value="<?= _e($oCouponRule->discountAmount) ?>" /></td>
                        <td><span class="error"><?= $oCouponRule->isPropValid("discountAmount") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="timesRedeemable">Aantal x bruikbaar *</label> <div class="hasTooltip tooltip" title="Aantal keer dat een code gebruikt mag worden">&nbsp;</div></td>
                        <td><input size="7" id="timesRedeemable" class="{validate:{required:true}} numbersOnly" title="Vul een geldige kortingsaantal in" type="text" autocomplete="off" name="timesRedeemable" value="<?= _e($oCouponRule->timesRedeemable) ?>" /></td>
                        <td><span class="error"><?= $oCouponRule->isPropValid("timesRedeemable") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="activeFromDate">Aktief vanaf *</label> <div class="hasTooltip tooltip" title="Geef aan vanaf wanneer deze couponregel aktief mag staan">&nbsp;</div></td>
                        <td>
                            <input title="Geef een geldige datum in (dd-mm-jjjj)" size="9" id="activeFromDate" class="datePickerDefault hasDatePicker {validate:{required:true, dateNL:true}}" type="text" name="activeFromDate" value="<?= $oCouponRule->activeFrom ? Date::strToDate($oCouponRule->activeFrom)->format("%d-%m-%Y") : Date::strToDate('NOW')->format("%d-%m-%Y") ?>" />
                            <input title="Geef een geldige tijd in (hh:mm)" size="3" id="activeFromTime" class="timePickerDefault hasTimePicker {validate:{required:true, time:true}}" type="text" name="activeFromTime" value="<?= $oCouponRule->activeFrom ? Date::strToDate($oCouponRule->activeFrom)->format("%H:%M") : Date::strToDate('NOW')->format("%H:%M") ?>" />
                        </td>
                        <td><span class="error"><?= $oCouponRule->isPropValid("activeFrom") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="activeToDate">Aktief tot</label> <div class="hasTooltip tooltip" title="Geef aan tot wanneer deze couponregel aktief mag zijn<br />- Leeg laten voor oneindig">&nbsp;</div></td>
                        <td colspan="2">
                            <input title="Geef een geldige datum in (dd-mm-jjjj)" size="9" id="activeToDate" class="datePickerDefault hasDatePicker {validate:{required: '#activeToTime:filled', dateNL:true}}" type="text" name="activeToDate" value="<?= $oCouponRule->activeTo ? Date::strToDate($oCouponRule->activeTo)->format("%d-%m-%Y") : '' ?>" />
                            <input title="Geef een geldige tijd in (hh:mm)" size="3" id="activeToTime" class="timePickerDefault hasTimePicker {validate:{required: '#activeToDate:filled', time:true}}" type="text" name="activeToTime" value="<?= $oCouponRule->activeTo ? Date::strToDate($oCouponRule->activeTo)->format("%H:%M") : '' ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>

    <div class="contentColumn">
        <fieldset style="padding-bottom: 10px;">
            <legend>Gerelateerde coupons</legend>
            <?
            if (is_numeric($oCouponRule->couponRuleId)) {
                ?>
                <form action="<?= getCurrentUrlPath() ?>" method="POST">
                    <input type="hidden" name="filterForm" value="1" />
                    <fieldset style="margin-bottom: 20px;">
                        <legend>Filter</legend>
                        <table class="withForm">
                            <tr>
                                <td class="withLabel" style="width: 116px;">Code <div class="hasTooltip tooltip" title="Zoek op de code of een gedeelte daarvan">&nbsp;</div></td>
                                <td><input type="text" name="couponFilter[code]" value="<?= _e($aCouponFilter['code']) ?>" /></td>
                            </tr>
                            <tr>
                                <td class="withLabel" style="width: 116px;">Nog bruikbaar <div class="hasTooltip tooltip" title="Filter op bruikbaarheid">&nbsp;</div></td>
                                <td>
                                    <select name="couponFilter[isRedeemable]">
                                        <option value="">Geen voorkeur</option>
                                        <option <?= ($aCouponFilter['isRedeemable'] == '1' ? 'SELECTED' : '') ?> value="1">Ja</option>
                                        <option <?= ($aCouponFilter['isRedeemable'] == '0' ? 'SELECTED' : '') ?> value="0">Nee</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><input type="submit" name="filterCoupons" value="Filter coupons" /> <input type="submit" name="resetCouponFilter" value="Reset filter" /></td>
                            </tr>
                        </table>
                    </fieldset>
                </form>
                <table class="sorted withActionIcons" style="width: 100%;">
                    <thead>
                        <tr class="topRow">
                            <td colspan="5">
                                <h2>Gerelateerde coupons (<?= (count($aCoupons) != 0 ? (db_int($iStart) + 1) : db_int($iStart)) . '-' . (db_int($iStart) + count($aCoupons)) ?>/<?= $iFoundRows ?>)</h2>
                                <div class="right">
                                    <a class="exportExcelBtn textLeft right" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get('param2') . '/export' ?>" title="Exporteer coupons" alt="Exporteer coupons">Exporteer coupons</a><br />
                                    <a class="addBtn textLeft right" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get('param2') . '/toevoegen' ?>" title="Coupon handmatig toevoegen voor deze couponregel" alt="Coupon handmatig toevoegen voor deze couponregel">Coupon toevoegen</a><br />
                                    <a class="addBtn textLeft right" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . http_get('param2') . '/genereren' ?>" title="Coupons genereren voor deze couponregel" alt="Coupons genereren voor deze couponregel">Coupons genereren</a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="{sorter:false} nonSorted onlineOffline">&nbsp;</th>
                            <th class="{sorter:false} nonSorted">Code</th>
                            <th class="{sorter:false} nonSorted">Aantal x gebruikt</th>
                            <th class="{sorter:false} nonSorted" style="width: 35px;">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        foreach ($aCoupons as $oCoupon) {
                            echo '<tr>';
                            echo '<td>';
                            # active inactive button
                            echo '<a id="coupon_' . $oCoupon->couponId . '_active_1" title="Coupon inactief zetten" class="action_icon ' . ($oCoupon->active ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setCouponActive/' . $oCoupon->couponId . '/0"></a>';
                            echo '<a id="coupon_' . $oCoupon->couponId . '_active_0" title="Coupon actief zetten" class="action_icon ' . ($oCoupon->active ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setCouponActive/' . $oCoupon->couponId . '/1"></a>';
                            echo '</td>';
                            echo '<td>' . _e($oCoupon->code) . '</td>';
                            echo '<td>' . _e($oCoupon->timesRedeemed . '/' . $oCouponRule->timesRedeemable) . '</td>';

                            echo '<td>';
                            if ($oCoupon->isDeletable()) {
                                echo '<a class="action_icon delete_icon" title="Verwijder coupon" onclick="return confirmChoice(\'' . _e($oCoupon->code) . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/coupon-verwijderen/' . $oCoupon->couponId . '"></a>';
                            } else {
                                echo '<a class="action_icon delete_icon grey" href="#" title="Deze coupon kan niet verwijderd worden"></a>';
                            }
                            echo '</td>';
                            echo '</tr>';
                        }
                        if (empty($aCoupons)) {
                            echo '<tr><td colspan="4"><i>Er zijn geen coupons om weer te geven</i></td></tr>';
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr class="bottomRow">
                            <td colspan="5">
                                <form method="POST">
                                    <?= generatePaginationHTML($iPageCount, $iCurrPage) ?>
                                    <input type="hidden" name="setCouponsPerPage" value="1" />
                                    <select name="perPage" onchange="$(this).closest('form').submit();">
                                        <option value="">alle</option>
                                        <option <?= $iPerPage == 10 ? 'SELECTED' : '' ?> value="10">10</option>
                                        <option <?= $iPerPage == 25 ? 'SELECTED' : '' ?> value="25">25</option>
                                        <option <?= $iPerPage == 50 ? 'SELECTED' : '' ?> value="50">50</option>
                                        <option <?= $iPerPage == 100 ? 'SELECTED' : '' ?> value="100">100</option>
                                    </select> per pagina
                                </form>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <?
            } else {
                echo '<p><i>Coupons kunnen worden toegevoegd nadat de couponregel is opgeslagen</i></p>';
            }
            ?>
        </fieldset>
    </div>
</div>

<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar couponregels overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
# create necessary javascript
$sAdminJsFolder = ADMIN_JS_FOLDER;
$sDISCOUNTTYPE_ORDER_FIXED_PRICE = CouponRule::DISCOUNTTYPE_ORDER_FIXED_PRICE;
$sBottomJavascript = <<<EOT
<script src="{$sAdminJsFolder}/autoNumeric-1.7.5.js"></script>
<script>
    // force floats on specific input fuelds
    $('input.floatOnly').autoNumeric({
        aSep: '',
        altDec: ',',
        vMin: 0.00
    });
    
    // force numbers on specific input fuelds
    $('input.numbersOnly').autoNumeric({
        aSep: '',
        altDec: ',',
        mDec: 0,
        vMin: 0
    });

    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            data: 'ajax=1',
            async: true,
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $("#coupon_"+dataObj.couponId+"_active_0").hide(); // hide offline button
                    $("#coupon_"+dataObj.couponId+"_active_1").hide(); // hide online button
                    $("#coupon_"+dataObj.couponId+"_active_"+dataObj.active).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.active == 0)    
                        showStatusUpdate("Coupon inactief gezet");
                    if(dataObj.active == 1)    
                        showStatusUpdate("Coupon actief gezet");                            
                } else {
                    showStatusUpdate("Coupon niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });

    $('#discountType').change(function(){
        if($(this).val() == '$sDISCOUNTTYPE_ORDER_FIXED_PRICE'){
            $('.taxLabel').show();
        }else{
            $('.taxLabel').hide();
        }
    });


</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>