
<form action="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>" method="POST">
    <input type="hidden" name="filterForm" value="1" />
    <fieldset style="margin-bottom: 20px;">
        <legend>Filter</legend>
        <table class="withForm">
            <tr>
                <td class="withLabel" style="width: 116px;">Titel <div class="hasTooltip tooltip" title="Zoek op de titel">&nbsp;</div></td>
                <td><input type="text" class="default" name="couponRuleFilter[title]" value="<?= _e($aCouponRuleFilter['title']) ?>" /></td>
            </tr>
            <tr>
                <td class="withLabel">Referentie <div class="hasTooltip tooltip" title="Zoek op de referentie of een gedeelte daarvan">&nbsp;</div></td>
                <td><input type="text" class="default" name="couponRuleFilter[reference]" value="<?= _e($aCouponRuleFilter['reference']) ?>" /></td>
            </tr>
            <tr>
                <td class="withLabel">Kortingstype</td>
                <td>
                    <select name="couponRuleFilter[discountType]">
                        <option value="">Alle kortingstype's</option>
                        <?
                        foreach (CouponRuleManager::getAllDiscountTypes() as $sDiscountType) {
                            echo '<option ' . ($aCouponRuleFilter['discountType'] == $sDiscountType ? 'SELECTED' : '') . ' value="' . $sDiscountType . '">' . CouponRuleManager::getLabelByDiscountType($sDiscountType) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="withLabel">Min. korting</td>
                <td>
                    <input class="floatOnly" size="6" type="text" name="couponRuleFilter[minDiscountAmount]" value="<?= _e($aCouponRuleFilter['minDiscountAmount']) ?>" />
                </td>
            </tr>
            <tr>
                <td class="withLabel">Max. korting</td>
                <td>
                    <input class="floatOnly" size="6" type="text" name="couponRuleFilter[maxDiscountAmount]" value="<?= _e($aCouponRuleFilter['maxDiscountAmount']) ?>" />
                </td>
            </tr>            
            <tr>
                <td class="withLabel">Toon inactief</td>                
                <td>
                    <input <?= !empty($aCouponRuleFilter['showAll']) ? 'checked' : '' ?> type="checkbox" name="couponRuleFilter[showAll]" value="1" />
                </td>
            </tr>           
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="filterCouponRules" value="Filter couponregels" /> <input type="submit" name="resetCouponRuleFilter" value="Reset filter" /></td>
            </tr>
        </table>
    </fieldset>
</form>
<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="10"><h2>Alle couponregels</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Nieuwe couponregel toevoegen" alt="Nieuw couponregel toevoegen">Couponregel toevoegen</a></div></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted onlineOffline">&nbsp;</th>
            <th class="{sorter:false} nonSorted">Titel</th>
            <th class="{sorter:false} nonSorted">Referentie</th>
            <th class="{sorter:false} nonSorted">Kortingstype</th>
            <th class="{sorter:false} nonSorted">Korting</th>
            <th class="{sorter:false} nonSorted">Aantal x bruikbaar</th>
            <th class="{sorter:false} nonSorted">Aktief vanaf</th>
            <th class="{sorter:false} nonSorted">Aktief tot</th>
            <th class="{sorter:false} nonSorted">Aantal coupons</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aCouponRules as $oCouponRule) {
            echo '<tr>';
            echo '<td>';

            # online offline button
            echo '<a id="couponRule_' . $oCouponRule->couponRuleId . '_active_1" title="Couponregel inactief zetten" class="action_icon ' . ($oCouponRule->active ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setCouponRuleActive/' . $oCouponRule->couponRuleId . '/0"></a>';
            echo '<a id="couponRule_' . $oCouponRule->couponRuleId . '_active_0" title="Couponregel actief zetten" class="action_icon ' . ($oCouponRule->active ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setCouponRuleActive/' . $oCouponRule->couponRuleId . '/1"></a>';

            echo '</td>';
            echo '<td>' . _e($oCouponRule->title) . '</td>';
            echo '<td>' . _e($oCouponRule->reference) . '</td>';
            echo '<td>' . CouponRuleManager::getLabelByDiscountType($oCouponRule->discountType) . '</td>';
            echo '<td>' . $oCouponRule->getDiscountAmount(true) . '</td>';
            echo '<td>' . $oCouponRule->timesRedeemable . '</td>';
            echo '<td>' . ($oCouponRule->activeFrom ? Date::strToDate($oCouponRule->activeFrom)->format('%d-%m-%Y %H:%M') : '' ) . '</td>';
            echo '<td>' . ($oCouponRule->activeTo ? Date::strToDate($oCouponRule->activeTo)->format('%d-%m-%Y %H:%M') : '') . '</td>';
            echo '<td>' . count($oCouponRule->getCoupons()) . '</td>';
            echo '<td>';

            echo '<a class="action_icon edit_icon" title="Bewerk couponregel" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCouponRule->couponRuleId . '"></a>';
            if ($oCouponRule->isDeletable()) {
                echo '<a class="action_icon delete_icon" title="Verwijder couponregel" onclick="return confirmChoice(\'' . $oCouponRule->title . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/couponregel-verwijderen/' . $oCouponRule->couponRuleId . '"></a>';
            } else {
                echo '<a class="action_icon delete_icon grey" href="#" title="Verwijder eerst alle gerelateerde coupons en maak deze couponregel inactief"></a>';
            }

            echo '</td>';
            echo '</tr>';
        }
        if (count($aCouponRules) == 0) {
            echo '<tr><td colspan="10"><i>Er zijn geen couponregels weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr class="bottomRow">
            <td colspan="10">
                <form method="POST">
                    <?= generatePaginationHTML($iPageCount, $iCurrPage) ?>
                    <input type="hidden" name="setCouponRulesPerPage" value="1" />
                    <select name="perPage" onchange="$(this).closest('form').submit();">
                        <option value="">alle</option>
                        <option <?= $iPerPage == 10 ? 'SELECTED' : '' ?> value="10">10</option>
                        <option <?= $iPerPage == 25 ? 'SELECTED' : '' ?> value="25">25</option>
                        <option <?= $iPerPage == 50 ? 'SELECTED' : '' ?> value="50">50</option>
                        <option <?= $iPerPage == 100 ? 'SELECTED' : '' ?> value="100">100</option>
                    </select> per pagina
                </form>
            </td>
        </tr>
    </tfoot>
</table>
<?
# create necessary javascript
$sAdminJsFolder = ADMIN_JS_FOLDER;
$sBottomJavascript = <<<EOT
<script src="{$sAdminJsFolder}/autoNumeric-1.7.5.js"></script>
<script>
    // force floats on specific input fuelds
    $('input.floatOnly').autoNumeric({
        aSep: '',
        altDec: ','
    });

    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            async: false,
            data: 'ajax=1',
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */ 
                if (dataObj.success == true) {
                    $("#couponRule_"+dataObj.couponRuleId+"_active_0").hide(); // hide offline button
                    $("#couponRule_"+dataObj.couponRuleId+"_active_1").hide(); // hide online button
                    $("#couponRule_"+dataObj.couponRuleId+"_active_"+dataObj.active).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.active == 0)    
                        showStatusUpdate("Couponregel inactief gezet");
                    if(dataObj.active == 1)    
                        showStatusUpdate("Couponregel actief gezet");                     
                } else {
                    showStatusUpdate("Couponregel niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>