<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCouponRule->couponRuleId ?>">Terug naar couponregel</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Coupon</legend>
                <table class="withForm">
                    <tr>
                        <td>Actief *</td>
                        <td>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de coupon inactief of actief" type="radio" <?= $oCoupon->active ? 'CHECKED' : '' ?> id="active_1" name="active" value="1" /> <label for="active_1">Ja</label>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de coupon actief of inactief" type="radio" <?= !$oCoupon->active ? 'CHECKED' : '' ?> id="active_0" name="active" value="0" /> <label for="active_0">Nee</label>
                        </td>                      
                        <td><span class="error"><?= $oCoupon->isPropValid("active") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>  
                    <tr>
                        <td class="withLabel" style="width: 116px;"><label for="code">Code *</label> <div class="hasTooltip tooltip" title="De code van de coupon">&nbsp;</div></td>
                        <td><input id="code" class="{validate:{required:true,remote:'<?= ADMIN_FOLDER . '/' . http_get('controller') ?>?ajax=checkCode<?= $oCoupon->couponId === null ? '' : '&couponId=' . $oCoupon->couponId ?>',messages:{required:'Vul een geldig code in',remote:'De opgegeven code is al in gebruik'}}} autofocus default" title="Vul een unieke code in" type="text" autocomplete="off" name="code" value="<?= _e($oCoupon->code) ?>" /></td>
                        <td><span class="error"><?= $oCoupon->isPropValid("code") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>

<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCouponRule->couponRuleId ?>">Terug naar couponregel</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>