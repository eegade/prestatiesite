<form action="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>" method="POST">
    <input type="hidden" name="filterForm" value="1" />
    <fieldset style="margin-bottom: 20px;">
        <legend>Filter</legend>
        <table class="withForm">
            <tr>
                <td class="withLabel" style="width: 116px;">Zoekwoord</td>
                <td><input type="text" name="formBackupFilter[q]" value="<?= _e($aFormBackupFilter['q']) ?>" /></td>
            </tr>
            <tr>
                <td class="withLabel">Datum</td>
                <td><input class="datePickerDefault hasDatePicker" type="text" name="formBackupFilter[created]" value="<?= $aFormBackupFilter['created'] ? Date::strToDate($aFormBackupFilter['created'])->format('%d-%m-%Y') : '' ?>" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="filterFormBackups" value="Filter form backups" /> <input type="submit" name="resetFilter" value="Reset filter" /></td>
            </tr>
        </table>
    </fieldset>
</form>
<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="3"><h2>Alle form backups</h2></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted">Datum/tijd</th>
            <th class="{sorter:false} nonSorted">Naam</th>
            <th class="{sorter:false} nonSorted" style="width: 30px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aFormBackups AS $oFormBackup) {
            echo '<tr>';
            echo '<td>' . Date::strToDate($oFormBackup->created)->format('%d-%m-%Y %H:%M:%S') . '</td>';
            echo '<td>' . $oFormBackup->name . '</td>';
            echo '<td><a class="action_icon fancyBoxLink fancybox.ajax magnifying_glass_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-getDetails/' . $oFormBackup->formBackupId . '"></a></td>';
            echo '</tr>';
        }
        if (count($aFormBackups) == 0) {
            echo '<tr><td colspan="3"><i>Er zijn geen form backups weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr class="bottomRow">
            <td colspan="6">
                <form method="POST">
                    <?= generatePaginationHTML($iPageCount, $iCurrPage) ?>
                    <input type="hidden" name="setPerPage" value="1" />
                    <select name="perPage" onchange="$(this).closest('form').submit();">
                        <option value="">alle</option>
                        <option <?= $iPerPage == 10 ? 'SELECTED' : '' ?> value="10">10</option>
                        <option <?= $iPerPage == 25 ? 'SELECTED' : '' ?> value="25">25</option>
                        <option <?= $iPerPage == 50 ? 'SELECTED' : '' ?> value="50">50</option>
                        <option <?= $iPerPage == 100 ? 'SELECTED' : '' ?> value="100">100</option>
                    </select> per pagina
                </form>
            </td>
        </tr>
    </tfoot>
</table>