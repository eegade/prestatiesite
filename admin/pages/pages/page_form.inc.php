<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar pagina's overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <fieldset>
            <legend>Pagina gegevens</legend>
            <form method="POST" action="" class="validateForm">
                <input type="hidden" value="save" name="action" />
                <input type="hidden" value="<?= $oPage->parentPageId ?>" name="parentPageId" />
                <table class="withForm">
                    <? if ($oPage->isOnlineChangeable()) { ?>
                        <tr>
                            <td>Online *</td>
                            <td>
                                <input class="alignRadio {validate:{required:true}}" title="Zet de pagina online OF offline" type="radio" <?= $oPage->online ? 'CHECKED' : '' ?> id="online_1" name="online" value="1" /> <label for="online_1">Ja</label>
                                <input class="alignRadio {validate:{required:true}}" title="Zet de pagina online OF offline" type="radio" <?= !$oPage->online ? 'CHECKED' : '' ?> id="online_0" name="online" value="0" /> <label for="online_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("online") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                    <? } ?>
                    <tr>
                        <td class="withLabel" style="width: 116px;"><label for="title">Titel *</label></td>
                        <td><input id="title" class="{validate:{required:true}} autofocus default" title="Vul de pagina titel in" type="text" autocomplete="off" name="title" value="<?= _e($oPage->title) ?>" /></td>
                        <td><span class="error"><?= $oPage->isPropValid("title") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="shortTitle">Menu link tekst <div class="hasTooltip tooltip" title="Vul dit veld in, om in plaats van de titel, de ingevulde tekst te gebruiken in het menu. Bij leeglaten wordt de titel gebruikt.">&nbsp;</div></label></td>
                        <td><input class="default" id="shortTitle" type="text" name="shortTitle" value="<?= _e($oPage->shortTitle) ?>" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3"><label for="content">Content</label></td>
                    </tr>
                    <tr>
                        <td colspan="3"><textarea name="content" id="content" class="tiny_MCE_page tiny_MCE"><?= $oPage->content ?></textarea></td>
                    </tr>
                    <? if ($oCurrentUser->isSEO()) { ?>
                        <tr>
                            <td colspan="3" style="padding-top: 10px;"><h2>SEO</h2></td>
                        </tr>
                        <? if ($oPage->pageId) { ?>
                            <tr>
                                <td>Huidige url</td>
                                <td><?= $oPage->getUrlPath() ?></td>
                            </tr>
                        <? } ?>
                        <tr>
                            <td class="withLabel"><label for="windowTitle">Window titel</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - De titel moet een goed leesbare zin zijn, met het belangrijkste zoekwoord van deze pagina zoveel mogelijk vooraan.<br /> - De titel moet tussen de 8 en 69 karakters zijn (incl. spaties).<br /> - Gebruik niet enkel de bedrijfsnaam in de titel.<br /> - Gebruik geen domeinnaam.">&nbsp;</div></td>
                            <td colspan="2">
                                <div class="inline-block">
                                    <input class="default charCounterWindowTitle" id="windowTitle" type="text" name="windowTitle" value="<?= _e($oPage->windowTitle) ?>" />
                                    <div id="windowTitleCounter" style="text-align: right;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="metaDescription">Description</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - Omschrijving die Google op kan nemen in de zoekresultaten.<br /> - Deze zin moet aantrekkelijk en wervend genoeg zijn om zoekers te stimuleren om op het resultaat te klikken.<br /> - Tussen de 46 en 156 karakters (incl. spaties).">&nbsp;</div></td>
                            <td colspan="2">
                                <div class="inline-block">
                                    <textarea class="charCounterMetaDescription default" id="metaDescription" name="metaDescription"><?= _e($oPage->metaDescription) ?></textarea>
                                    <div id="metaDescriptionCounter" style="text-align: right;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="withLabel"><label for="metaKeywords">Keywords</label> <div class="hasTooltip tooltip" title="Plaats hier voor deze pagina, een aantal relevante woorden (maximaal 10 woorden).<br />Scheidt de woorden van elkaar met een komma en een spatie.<br />- zoekmachines gebruiken keywords vooral om misbruik op te sporen<br />- goed gebruik van keywords zal geen of een hele kleine bijdrage leveren aan de vindbaarheid van deze pagina">&nbsp;</div></td>
                            <td colspan="2"><input class="default" id="metaKeywords" type="text" name="metaKeywords" value="<?= _e($oPage->metaKeywords) ?>" /> (optioneel)</td>
                        </tr>
                        <? if (!$oPage->getLockUrlPath()) { ?>
                            <tr>
                                <td class="withLabel"><label for="urlPart">Url tekst</label> <div class="hasTooltip tooltip" title="Kies zelf de tekst op basis waarvan de url moet worden gegenereerd<br />Bijvoorbeeld: <?= CLIENT_HTTP_URL ?><?= $oPage->parentPageId ? $oPage->getParent()->getUrlPath() : '' ?>/<b>[url tekst]</b><br />- alleen letters, cijfers en een `-` is toegestaan<br />- alle tekens anders dan aangegeven worden vervangen door een `-`<br />- vb: `cms systeem` wordt `cms-systeem`">&nbsp;</div></td>
                                <td><input  class="default" id="urlPart" type="text" name="urlPart" value="<?= _e($oPage->getUrlPart()) ?>" /></td>
                                <td></td>
                            </tr>
                        <? } ?>
                        <tr>
                            <td class="withLabel"><label for="urlParameters">Url parameters</label> <div class="hasTooltip tooltip" title="Kies zelf de tekst op basis waarvan de url moet worden gegenereerd<br />Bijvoorbeeld: <?= CLIENT_HTTP_URL ?><?= $oPage->parentPageId ? $oPage->getParent()->getUrlPath() : '' ?>/<b>[url tekst]?[url parameters]</b><br />">&nbsp;</div></td>
                            <td><input  class="default" id="urlParameters" type="text" name="urlParameters" value="<?= _e($oPage->getUrlParameters()) ?>" /></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>In menu tonen</td>
                            <td>
                                <input class="alignRadio" title="Pagina tonen in menu?" type="radio" <?= $oPage->getInMenu() ? 'CHECKED' : '' ?> id="inMenu_1" name="inMenu" value="1" /> <label for="inMenu_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Pagina tonen in menu?" type="radio" <?= !$oPage->getInMenu() ? 'CHECKED' : '' ?> id="inMenu_0" name="inMenu" value="0" /> <label for="inMenu_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("inMenu") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                    <? } ?>
                    <? if ($oCurrentUser->isAdmin()) { ?>
                        <tr>
                            <td colspan="3" style="padding-top: 10px;"><h2>Admin settings</h2></td>
                        </tr>
                        <tr>
                            <td class="withLabel"><label for="controllerPath">controller path *</label></td>
                            <td><input class="{validate:{required:true}} default" id="controllerPath" name="controllerPath" type="text" value="<?= _e($oPage->getControllerPath()) ?>" /></td>
                            <td><span class="error"><?= $oPage->isPropValid("controllerPath") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <tr>
                            <td>online changeable</td>
                            <td>
                                <input class="alignRadio" title="Kan de pagina online/offline gezet worden?" type="radio" <?= $oPage->getOnlineChangeable() ? 'CHECKED' : '' ?> id="onlineChangeable_1" name="onlineChangeable" value="1" /> <label for="onlineChangeable_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Kan de pagina online/offline gezet worden?" type="radio" <?= !$oPage->getOnlineChangeable() ? 'CHECKED' : '' ?> id="onlineChangeable_0" name="onlineChangeable" value="0" /> <label for="onlineChangeable_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("online") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <tr>
                            <td>editable</td>
                            <td>
                                <input class="alignRadio" title="Kan de pagina bewerken?" type="radio" <?= $oPage->getEditable() ? 'CHECKED' : '' ?> id="editable_1" name="editable" value="1" /> <label for="editable_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Kan de pagina bewerken?" type="radio" <?= !$oPage->getEditable() ? 'CHECKED' : '' ?> id="editable_0" name="editable" value="0" /> <label for="editable_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("editable") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <tr>
                            <td>deletable</td>
                            <td>
                                <input class="alignRadio" title="Kan de pagina verwijderen?" type="radio" <?= $oPage->getDeletable() ? 'CHECKED' : '' ?> id="deletable_1" name="deletable" value="1" /> <label for="deletable_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Kan de pagina verwijderen?" type="radio" <?= !$oPage->getDeletable() ? 'CHECKED' : '' ?> id="deletable_0" name="deletable" value="0" /> <label for="deletable_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("deletable") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <tr>
                            <td>may have sub</td>
                            <td>
                                <input class="alignRadio" title="Pagina mag sub pagina hebben?" type="radio" <?= $oPage->getMayHaveSub() ? 'CHECKED' : '' ?> id="mayHaveSub_1" name="mayHaveSub" value="1" /> <label for="mayHaveSub_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Pagina mag sub pagina hebben?" type="radio" <?= !$oPage->getMayHaveSub() ? 'CHECKED' : '' ?> id="mayHaveSub_0" name="mayHaveSub" value="0" /> <label for="mayHaveSub_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("mayHaveSub") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <tr>
                            <td>lock url path</td>
                            <td>
                                <input class="alignRadio" title="Pagina heeft vaste url?" type="radio" <?= $oPage->getLockUrlPath() ? 'CHECKED' : '' ?> id="lockUrlPath_1" name="lockUrlPath" value="1" /> <label for="lockUrlPath_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Pagina heeft vaste url?" type="radio" <?= !$oPage->getLockUrlPath() ? 'CHECKED' : '' ?> id="lockUrlPath_0" name="lockUrlPath" value="0" /> <label for="lockUrlPath_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("lockUrlPath") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <tr>
                            <td>lock parent</td>
                            <td>
                                <input class="alignRadio" title="Pagina heeft hoofdpagina?" type="radio" <?= $oPage->getLockParent() ? 'CHECKED' : '' ?> id="lockParent_1" name="lockParent" value="1" /> <label for="lockParent_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Pagina heeft hoofdpagina?" type="radio" <?= !$oPage->getLockParent() ? 'CHECKED' : '' ?> id="lockParent_0" name="lockParent" value="0" /> <label for="lockParent_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("lockUrlPath") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <tr>
                            <td>hide image management</td>
                            <td>
                                <input class="alignRadio" title="Pagina heeft geen afbeeldingen beheer?" type="radio" <?= $oPage->getHideImageManagement() ? 'CHECKED' : '' ?> id="hideImageManagement_1" name="hideImageManagement" value="1" /> <label for="hideImageManagement_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Pagina heeft geen afbeeldingen beheer?" type="radio" <?= !$oPage->getHideImageManagement() ? 'CHECKED' : '' ?> id="hideImageManagement_0" name="hideImageManagement" value="0" /> <label for="hideImageManagement_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("hideImageManagement") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <tr>
                            <td>hide header image management</td>
                            <td>
                                <input class="alignRadio" title="Pagina heeft geen header afbeeldingen beheer?" type="radio" <?= $oPage->getHideHeaderImageManagement() ? 'CHECKED' : '' ?> id="hideHeaderImageManagement_1" name="hideHeaderImageManagement" value="1" /> <label for="hideHeaderImageManagement_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Pagina heeft geen header afbeeldingen beheer?" type="radio" <?= !$oPage->getHideHeaderImageManagement() ? 'CHECKED' : '' ?> id="hideHeaderImageManagement_0" name="hideHeaderImageManagement" value="0" /> <label for="hideHeaderImageManagement_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("hideHeaderImageManagement") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <tr>
                            <td>hide file management</td>
                            <td>
                                <input class="alignRadio" title="Pagina heeft geen bestanden beheer?" type="radio" <?= $oPage->getHideFileManagement() ? 'CHECKED' : '' ?> id="hideFileManagement_1" name="hideFileManagement" value="1" /> <label for="hideFileManagement_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Pagina heeft geen bestanden beheer?" type="radio" <?= !$oPage->getHideFileManagement() ? 'CHECKED' : '' ?> id="hideFileManagement_0" name="hideFileManagement" value="0" /> <label for="hideFileManagement_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("hideFileManagement") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <tr>
                            <td>hide link management</td>
                            <td>
                                <input class="alignRadio" title="Pagina heeft geen link beheer?" type="radio" <?= $oPage->getHideLinkManagement() ? 'CHECKED' : '' ?> id="hideLinkManagement_1" name="hideLinkManagement" value="1" /> <label for="hideLinkManagement_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Pagina heeft geen link beheer?" type="radio" <?= !$oPage->getHideLinkManagement() ? 'CHECKED' : '' ?> id="hideLinkManagement_0" name="hideLinkManagement" value="0" /> <label for="hideLinkManagement_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("hideLinkManagement") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <tr>
                            <td>hide youtube link management</td>
                            <td>
                                <input class="alignRadio" title="Pagina heeft geen youtube link beheer?" type="radio" <?= $oPage->getHideYoutubeLinkManagement() ? 'CHECKED' : '' ?> id="hideYoutubeLinkManagement_1" name="hideYoutubeLinkManagement" value="1" /> <label for="hideYoutubeLinkManagement_1">Ja</label>
                                <input style="margin-left: 5px;" class="alignRadio" title="Pagina heeft geen youtube link beheer?" type="radio" <?= !$oPage->getHideYoutubeLinkManagement() ? 'CHECKED' : '' ?> id="hideYoutubeLinkManagement_0" name="hideYoutubeLinkManagement" value="0" /> <label for="hideYoutubeLinkManagement_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oPage->isPropValid("hideYoutubeLinkManagement") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                    <? } ?>
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
        </fieldset>
        </form>
    </div>
    <? if (!$oPage->hideHeaderImageManagement()) { ?>
        <div class="contentColumn">
            <fieldset>
                <legend>Header afbeelding</legend>
                <?
                if ($oPage->pageId !== null) {
                    $oHeaderImageManagerHTML->includeTemplate();
                } else {
                    echo '<p><i>De header afbeelding kan worden geupload nadat de pagina is opgeslagen</i></p>';
                }
                ?>
            </fieldset>
        </div>
    <? } ?>
    <? if (!$oPage->hideImageManagement()) { ?>
        <div class="contentColumn">
            <fieldset>
                <legend>Afbeeldingen</legend>
                <?
                if ($oPage->pageId !== null) {
                    $oImageManagerHTML->includeTemplate();
                } else {
                    echo '<p><i>Afbeeldingen kunnen worden geüpload nadat de pagina eerst is opgeslagen</i></p>';
                }
                ?>
            </fieldset>
        </div>
        <?
    }
    if (!$oPage->hideFileManagement()) {
        ?>
        <div class="contentColumn">
            <fieldset>
                <legend>Bestanden</legend>
                <?
                if ($oPage->pageId !== null) {
                    $oFileManagerHTML->includeTemplate();
                } else {
                    echo '<p><i>Bestanden kunnen worden geüpload nadat de pagina eerst is opgeslagen</i></p>';
                }
                ?>
            </fieldset>
        </div>
        <?
    }
    if (!$oPage->hideLinkManagement()) {
        ?>

        <div class="contentColumn">
            <fieldset>
                <legend>Links</legend>
                <?
                if ($oPage->pageId !== null) {
                    $oLinkManagerHTML->includeTemplate();
                } else {
                    echo '<p><i>Links kunnen worden toegevoegd nadat de pagina eerst is opgeslagen</i></p>';
                }
                ?>
            </fieldset>
        </div>
        <?
    }
    if (!$oPage->hideYoutubeLinkManagement()) {
        ?>
        <div class="contentColumn">
            <fieldset>
                <legend>Youtube video links</legend>
                <?
                if ($oPage->pageId !== null) {
                    $oYoutubeLinkManagerHTML->includeTemplate();
                } else {
                    echo '<p><i>Youtube video links kunnen worden toegevoegd nadat de pagina eerst is opgeslagen</i></p>';
                }
                ?>
            </fieldset>
        </div>
        <?
    }
    ?>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar pagina's overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
$oPageLayout->addJavascriptBottom('
<script>
    initTinyMCE(".tiny_MCE_page", "/admin/paginas/link-list", "/admin/paginas/image-list/' . $oPage->pageId . '");
</script>
');
?>