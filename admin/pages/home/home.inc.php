<h1>Contactgegevens A-side media B.V.</h1>
<p>
    Voor vragen of opmerkingen over het CMS of de website kunt u contact opnemen met A-side media B.V.
</p>
<p>
    <b>Adres</b><br />
    A-side media B.V.<br />
    Industrieweg 15<br />
    3641 RK Mijdrecht<br />
<p>
    <b>Telefoon</b><br />
    Tel. 0297 - 38 52 52<br />
    Fax. 0297 - 38 52 53<br />
</p>
<p>
    <b>Email</b><br />
    <a href="mailto:info@a-side.nl">info@a-side.nl</a><br />
    <b>Website</b><br />
    <a href="http://www.a-side.nl" onclick="target='_blank';">www.a-side.nl</a>
</p>