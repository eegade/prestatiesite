<form class="validateForm" action="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>" method="POST">
    <input type="hidden" name="filterForm" value="1" />
    <fieldset style="margin-bottom: 20px;">
        <legend>Filter</legend>
        <table class="withForm">
            <!-- Search words -->
            <tr>
                <td class="withLabel" style="width: 116px;">Zoekwoord</td>
                <td><input type="text" name="agendaItemFilter[q]" value="<?= _e($aAgendaItemFilter['q']) ?>" /></td>
            </tr>
            
            <!-- Begin date of the filter -->
            <tr>
                <td class="withLabel">
                    <label for="fromDate">Begin datum</label>
                    <div class="hasTooltip tooltip" title="Dit veld kan gebruikt worden om items te vinden die plaats vinden tijdens een bepaalde periode.<br />Dit is de start datum van die periode.">&nbsp;</div>
                </td>
                <td>
                    <input class="datePickerDefault hasDatePicker" type="text" name="agendaItemFilter[dateFrom]" value="<?= $aAgendaItemFilter['dateFrom'] ? Date::strToDate($aAgendaItemFilter['dateFrom'])->format('%d-%m-%Y') : '' ?>" />
                </td>
            </tr>
            
            <!-- End date of the filter -->
            <tr>
                <td class="withLabel">
                    <label for="toDate">Eind datum</label>
                    <div class="hasTooltip tooltip" title="Dit veld kan gebruikt worden om items te vinden die plaats vinden tijdens een bepaalde periode.<br />Dit is de eind datum van die periode.">&nbsp;</div>
                </td>
                <td>
                    <input class="datePickerDefault hasDatePicker" type="text" name="agendaItemFilter[dateTo]" value="<?= $aAgendaItemFilter['dateTo'] ? Date::strToDate($aAgendaItemFilter['dateTo'])->format('%d-%m-%Y') : '' ?>" />
                </td>
            </tr>
            
            <tr>
                <td>&nbsp;</td>
                <td><input id="filterAgendaItems" type="submit" name="filterAgendaItems" value="Filter agendaitems" /> <input type="submit" name="resetFilter" value="Reset filter" /></td>
            </tr>
        </table>
    </fieldset>
</form>

<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="5"><h2>Alle agenda items</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Agenda item toevoegen" alt="Agenda item toevoegen">Agenda item toevoegen</a></div></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted onlineOffline"> </th>
            <th>Begin</th>
            <th>Eind</th>
            <th>Titel</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aAgendaItems AS $oAgendaItem) {
            echo '<tr>';
            echo '<td>';

            # online offline button
            echo '<a id="agendaItem_' . $oAgendaItem->agendaItemId . '_online_1" title="Agendaitem offline zetten" class="action_icon ' . ($oAgendaItem->online ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oAgendaItem->agendaItemId . '/?online=0"></a>';
            echo '<a id="agendaItem_' . $oAgendaItem->agendaItemId . '_online_0" title="Agendaitem online zetten" class="action_icon ' . ($oAgendaItem->online ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oAgendaItem->agendaItemId . '/?online=1"></a>';

            echo '</td>';
            echo '<td>' . date("d-m-Y h:m", strtotime($oAgendaItem->dateTimeFrom)) . '</td>';
            echo '<td>';
            if (isset($oAgendaItem->dateTimeTo))
                 echo date("d-m-Y h:m", strtotime($oAgendaItem->dateTimeTo));

            echo '</td>';
            echo '<td>' . $oAgendaItem->title . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk agendaitem" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oAgendaItem->agendaItemId . '"></a>';
            echo '<a class="action_icon delete_icon" title="Verwijder agendaitem" onclick="return confirmChoice(\'item ' . $oAgendaItem->title . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/delete/' . $oAgendaItem->agendaItemId . '"></a>';

            echo '</td>';
            echo '</tr>';
        }
        if (count($aAgendaItems) == 0) {
            echo '<tr><td colspan="5"><i>There is not agenda items to show</i></td></tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr class="bottomRow">
            <td colspan="6">
                <form method="POST">
                    <?= generatePaginationHTML($iPageCount, $iCurrPage) ?>
                    <input type="hidden" name="setPerPage" value="1" />
                    <select name="perPage" onchange="$(this).closest('form').submit();">
                        <option value="">alle</option>
                        <option <?= $iPerPage == 10 ? 'SELECTED' : '' ?> value="10">10</option>
                        <option <?= $iPerPage == 25 ? 'SELECTED' : '' ?> value="25">25</option>
                        <option <?= $iPerPage == 50 ? 'SELECTED' : '' ?> value="50">50</option>
                        <option <?= $iPerPage == 100 ? 'SELECTED' : '' ?> value="100">100</option>
                    </select> per pagina
                </form>
            </td>
        </tr>
    </tfoot>
</table>

<?
# add ajax code for online/offline handling
$sOnlineOfflineJavascript = <<<EOT
<script>
    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: this.href,
            data: "ajax=1",
            async: true,
            success: function(data){    
                var dataObj = eval('(' + data + ')');

                /* On success */
                if(dataObj.success == true){
                    $("#agendaItem_"+dataObj.agendaItemId+"_online_0").hide(); // hide offline button
                    $("#agendaItem_"+dataObj.agendaItemId+"_online_1").hide(); // hide online button
                    $("#agendaItem_"+dataObj.agendaItemId+"_online_"+dataObj.online).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.online == 0)    
                        showStatusUpdate("AgendaItem offline gezet");
                    if(dataObj.online == 1)    
                        showStatusUpdate("AgendaItem online gezet");                            
                }else{
                        showStatusUpdate("AgendaItem niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });

</script>
EOT;
$oPageLayout->addJavascriptBottom($sOnlineOfflineJavascript);
?>