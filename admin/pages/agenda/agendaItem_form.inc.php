<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <fieldset>
            <legend>Gebruiker <?= http_get("param1") ?></legend>
            <form method="POST" action="" class="validateForm">
                <input type="hidden" value="save" name="action" />

                <table class="withForm">
                    <tr>
                        <td>Online *</td>
                        <td>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de agendaitem online OF offline" type="radio" <?= $oAgendaItem->online ? 'CHECKED' : '' ?> id="online_1" name="online" value="1" /> <label for="online_1">Ja</label>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de agendaitem online OF offline" type="radio" <?= !$oAgendaItem->online ? 'CHECKED' : '' ?> id="online_0" name="online" value="0" /> <label for="online_0">Nee</label>
                        </td>
                        <td><span class="error"><?= $oAgendaItem->isPropValid("online") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel" style="width: 120px;"><label for="title">Titel *</label> </td>
                        <td><input id="title" class="{validate:{required:true}} autofocus default" title="Vul de titel in van het agendaitem" type="text" name="title" value="<?= _e($oAgendaItem->title) ?>" /></td>
                        <td><span class="error"><?= $oAgendaItem->isPropValid("title") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="shortTitle">Menu link tekst <div class="hasTooltip tooltip" title="Vul dit veld in, om in plaats van de titel, de ingevulde tekst te gebruiken in het menu. Bij leeglaten wordt de titel gebruikt.">&nbsp;</div></label></td>
                        <td><input class="default" id="shortTitle" type="text" name="shortTitle" value="<?= _e($oAgendaItem->shortTitle) ?>" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="withLabel">
                            <label for="dateFrom">Begin *</label> 
                            <div class="hasTooltip tooltip" title="Datum en tijd dat het item begint">&nbsp;</div>
                        </td>
                        <td>
                            <input title="Geef een geldige datum in (dd-mm-jjjj)" size="9" id="dateFrom" class="datePickerDefault hasDatePicker {validate:{required:true, dateNL:true}}" type="text" name="dateFrom" value="<?= $oAgendaItem->dateTimeFrom ? Date::strToDate($oAgendaItem->dateTimeFrom)->format("%d-%m-%Y") : Date::strToDate('NOW')->format("%d-%m-%Y") ?>" />
                            <input title="Geef een geldige tijd in (hh:mm)" size="3" id="timeFrom" class="timePickerDefault hasTimePicker {validate:{required:true, time:true}}" type="text" name="timeFrom" value="<?= $oAgendaItem->dateTimeFrom ? Date::strToDate($oAgendaItem->dateTimeFrom)->format("%H:%M") : Date::strToDate('NOW')->format("%H:%M") ?>" <?= $oAgendaItem->allDay ? "disabled='disabled'" : '' ?>/>
                            <input class="alignCheckbox" onclick="handleAllDay(this.checked);" type="checkbox" name="allDay" id="allDay" value="1" <?= $oAgendaItem->allDay ? 'CHECKED' : '' ?>>
                            <label for="allDay">Hele dag</label>
                        </td>
                        <td><span class="error"><?= $oAgendaItem->isPropValid("onlineFrom") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="dateTo">Eind </label> <div class="hasTooltip tooltip" title="Datum en tijd dat het item eindigt">&nbsp;</div></td>
                        <td>
                            <input title="Geef een geldige datum in (dd-mm-jjjj)" size="9" id="dateTo" class="datePickerDefault hasDatePicker {validate:{dateNL:true}}" type="text" name="dateTo" value="<?= $oAgendaItem->dateTimeTo ? Date::strToDate($oAgendaItem->dateTimeTo)->format("%d-%m-%Y") : "" ?>" <?= $oAgendaItem->allDay ? "disabled='disabled'" : '' ?>/>
                            <input title="Geef een geldige tijd in (hh:mm)" size="3" id="timeTo" class="timePickerDefault hasTimePicker {validate:{ time:true}}" type="text" name="timeTo" value="<?= $oAgendaItem->dateTimeTo ? Date::strToDate($oAgendaItem->dateTimeTo)->format("%H:%M") : "" ?>" <?= $oAgendaItem->allDay ? "disabled='disabled'" : '' ?>/>
                        </td>
                        <td><span class="error"><?= $oAgendaItem->isPropValid("onlineFrom") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="padding-top: 20px;"><label for="content">Intro</label> <div class="hasTooltip tooltip" title="Vul hier een korte introductie tekst in.<br />Deze tekst wordt getoond de overzichten">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <textarea name="intro" id="intro" class="tiny_MCE_default tiny_MCE intro"><?= $oAgendaItem->intro ?></textarea></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="padding-top: 20px;"><label for="content">Omschrijving</label> <div class="hasTooltip tooltip" title="Vul hier de omschrijving van uw item in">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <textarea name="content" id="content" class="tiny_MCE_default tiny_MCE content"><?= $oAgendaItem->content ?></textarea></td>
                    </tr>

                    <!-- SEO information -->
                    <? if ($oCurrentUser->isSEO()) { ?>
                        <tr>
                            <td colspan="3"><h2>SEO</h2></td>
                        </tr>
                        <tr>
                            <td class="withLabel"><label for="windowTitle">Window titel</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - De titel moet een goed leesbare zin zijn, met het belangrijkste zoekwoord van deze pagina zoveel mogelijk vooraan.<br /> - De titel moet tussen de 8 en 69 karakters zijn (incl. spaties).<br /> - Gebruik niet enkel de bedrijfsnaam in de titel.<br /> - Gebruik geen domeinnaam.">&nbsp;</div></td>
                            <td colspan="2">
                                <div class="inline-block">
                                    <input class="default charCounterWindowTitle" id="windowTitle" type="text" name="windowTitle" value="<?= _e($oAgendaItem->windowTitle) ?>" />
                                    <div id="windowTitleCounter" style="text-align: right;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="metaDescription">Description</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - Omschrijving die Google op kan nemen in de zoekresultaten.<br /> - Deze zin moet aantrekkelijk en wervend genoeg zijn om zoekers te stimuleren om op het resultaat te klikken.<br /> - Tussen de 46 en 156 karakters (incl. spaties).">&nbsp;</div></td>
                            <td colspan="2">
                                <div class="inline-block">
                                    <textarea class="default charCounterMetaDescription" id="metaDescription" name="metaDescription"><?= _e($oAgendaItem->metaDescription) ?></textarea>
                                    <div id="metaDescriptionCounter" style="text-align: right;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="withLabel"><label for="metaKeywords">Keywords</label> <div class="hasTooltip tooltip" title="Plaats hier voor deze pagina, een aantal relevante woorden (maximaal 10 woorden).<br />Scheidt de woorden van elkaar met een komma en een spatie.<br />- zoekmachines gebruiken keywords vooral om misbruik op te sporen<br />- goed gebruik van keywords zal geen of een hele kleine bijdrage leveren aan de vindbaarheid van deze pagina">&nbsp;</div></td>
                            <td colspan="2"><input  class="default" id="metaKeywords" type="text" name="metaKeywords" value="<?= _e($oAgendaItem->metaKeywords) ?>" /></td>
                        </tr>   
                    <? } ?>
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>

            </form>
        </fieldset>
    </div>
    <div class="contentColumn">
        <fieldset>
            <legend>Afbeeldingen</legend>
            <?
            if ($oAgendaItem->agendaItemId !== null) {

                $oImageManagerHTML->includeTemplate();
            } else {
                echo '<p><i>Afbeeldingen kunnen worden geüpload nadat het agendaitem eerst is opgeslagen</i></p>';
            }
            ?>
        </fieldset>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>

<?
$sJavascript = <<<EOT
<script>
    initTinyMCE(".tiny_MCE_default.intro", "/admin/paginas/link-list");
    initTinyMCE(".tiny_MCE_default", "/admin/paginas/link-list", "/admin/agenda/image-list/' . $oAgendaItem->agendaItemId . '");

    function handleAllDay(checked){
        if(checked){
            $('#timeFrom,#dateTo,#timeTo').val('').attr('disabled', true);
            $('#timeFrom').val('00:00');
        }else{
            $('#timeFrom,#dateTo,#timeTo').val('').attr('disabled', false);
            $('#timeFrom').val('00:00');
        }
    }
</script>        
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);
?>