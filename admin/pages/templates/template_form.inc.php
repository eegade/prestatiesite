<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="contentColumn">
    <form method="POST" action="" class="validateForm">
        <input type="hidden" value="save" name="action" />
        <fieldset>
            <legend>Template <?= http_get("param1") ?></legend>
            <table class="withForm">
                <tr>
                    <td class="withLabel" style="width: 120px;"><label for="description">Omschrijving/naam *</label></td>
                    <td><input id="description" class="{validate:{required:true}} autofocus default" title="Vul een omschrijving/naam in van de template" type="text" name="description" value="<?= $oTemplate->description ?>" /></td>
                    <td><span class="error"><?= $oTemplate->isPropValid("description") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                </tr>
                <tr>
                    <td class="withLabel"><label for="templateGroupId">Template groep *</label></td>
                    <td>
                        <select onchange="setTemplateVariables(this.value);" id="templateGroupId" class="{validate:{required:true}}" title="Kies een template groep" name="templateGroupId">
                            <?
                            foreach (TemplateManager::getAllTemplateGroups() AS $oTemplateGroup) {
                                echo '<option ' . ($oTemplate->templateGroupId == $oTemplateGroup->templateGroupId ? 'SELECTED' : '') . ' value="' . $oTemplateGroup->templateGroupId . '">' . $oTemplateGroup->templateGroupName . '</option>';
                            }
                            ?>
                        </select>
                    </td>
                    <td><span class="error"><?= $oTemplate->isPropValid("templateGoupId") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                </tr>
                <tr>
                    <td class="withLabel"><label for="subject">Onderwerp</label></td>
                    <td><input id="subject" size="46" type="text" name="subject" value="<?= $oTemplate->subject ?>" /></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3"><label for="template">Email</label></td>
                </tr>
                <tr>
                    <td colspan="3"><textarea name="template" id="template" class="tiny_MCE_default tiny_MCE"><?= $oTemplate->template ?></textarea></td>
                </tr>
                <? if ($oCurrentUser->isAdmin()) { ?>
                    <tr>
                        <td colspan="3" style="padding-top: 10px;"><h2>Admin settings</h2></td>
                    </tr>
                    <tr>
                        <td class="withLabel" ><label for="name">Unieke naam</label></td>
                        <td><input id="description" class="{validate:{remote:'<?= ADMIN_FOLDER ?>/templates-beheer/ajax-checkName?templateId=<?= $oTemplate->templateId ?>'}} default" title="Vul een unieke naam in voor de template" type="text" name="name" value="<?= $oTemplate->name ?>" /></td>
                        <td><span class="error"><?= $oTemplate->isPropValid("name") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td>editable</td>
                        <td>
                            <input class="alignRadio" title="Kan de template bewerken?" type="radio" <?= $oTemplate->getEditable() ? 'CHECKED' : '' ?> id="editable_1" name="editable" value="1" /> <label for="editable_1">Ja</label>
                            <input style="margin-left: 5px;" class="alignRadio" title="Kan de template bewerken?" type="radio" <?= !$oTemplate->getEditable() ? 'CHECKED' : '' ?> id="editable_0" name="editable" value="0" /> <label for="editable_0">Nee</label>
                        </td>
                        <td><span class="error"><?= $oTemplate->isPropValid("editable") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td>deletable</td>
                        <td>
                            <input class="alignRadio" title="Kan de template verwijderen?" type="radio" <?= $oTemplate->getDeletable() ? 'CHECKED' : '' ?> id="deletable_1" name="deletable" value="1" /> <label for="deletable_1">Ja</label>
                            <input style="margin-left: 5px;" class="alignRadio" title="Kan de template verwijderen?" type="radio" <?= !$oTemplate->getDeletable() ? 'CHECKED' : '' ?> id="deletable_0" name="deletable" value="0" /> <label for="deletable_0">Nee</label>
                        </td>
                        <td><span class="error"><?= $oTemplate->isPropValid("deletable") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                <? } ?>
                <tr>
                    <td colspan="3">
                        <input type="submit" value="Opslaan" name="save" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>
<div class="contentColumn">
    <fieldset>
        <legend>Template variabelen <div class="tooltip hasTooltip" title="Gebruik deze variabelen in uw tekst/onderwerp en ze zullen worden vervangen<br /> door de echte waarde bij het versturen van de template">&nbsp;</div></legend>
        <p id="templateVariables">

        </p>
    </fieldset>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
$sAdminFolder = ADMIN_FOLDER;
$sController = http_get('controller');
$sJavascript = <<<EOT
<script>
    function setTemplateVariables(templateGroupId){
        if(!templateGroupId){
            templateGroupId = $('#templateGroupId').val();
        }

        $.ajax({
            url: '$sAdminFolder/$sController/ajax-getTemplateVariables?templateGroupId='+templateGroupId,
            success: function(data){
                var dataObj = eval('('+data+')');
                if(dataObj.success){
                    $('#templateVariables').html(dataObj.sHtml);
                }else{
                    alert('Kon template variabelen niet ophalen');
                }
            }
        });
    }
    
    setTemplateVariables('{$oTemplate->templateGroupId}');
    initTinyMCE(".tiny_MCE_default", "/admin/paginas/link-list");
</script>
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);
?>