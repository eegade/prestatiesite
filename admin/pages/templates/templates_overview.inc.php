<form action="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>" method="POST">
    <input type="hidden" name="filterForm" value="1" />
    <fieldset style="margin-bottom: 20px;">
        <legend>Filter</legend>
        <table class="withForm">
            <tr>
                <td class="withLabel" style="width: 116px;">Naam <div class="hasTooltip tooltip" title="Zoek op de omschrijving/naam van de template">&nbsp;</div></td>
                <td><input type="text" name="templateFilter[description]" value="<?= $aTemplateFilter['description'] ?>" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="filterTemplates" value="Filter templates" /> <input type="submit" name="resetFilter" value="Reset filter" /></td>
            </tr>
        </table>
    </fieldset>
</form>
<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="6"><h2>Gevonden templates (<?= (count($aTemplates) != 0 ? (db_int($iStart) + 1) : db_int($iStart)) . '-' . (db_int($iStart) + count($aTemplates)) ?>/<?= $iFoundRows ?>)</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Nieuwe template toevoegen" alt="Nieuwe template toevoegen">Template toevoegen</a></div></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted">#</th>
            <th class="{sorter:false} nonSorted">Omschrijving/naam</th>
            <th class="{sorter:false} nonSorted">Groep</th>
            <th class="{sorter:false} nonSorted">Type</th>
            <th class="{sorter:false} nonSorted">Technische naam</th>
            <th class="{sorter:false} nonSorted" style="width: 120px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aTemplates AS $oTemplate) {
            echo '<tr>';
            echo '<td>' . $oTemplate->templateId . '</td>';
            echo '<td>' . $oTemplate->description . '</td>';
            echo '<td>' . ($oTemplate->getTemplateGroup() ? $oTemplate->getTemplateGroup()->templateGroupName : 'Onbekend') . '</td>';
            echo '<td>' . $oTemplate->type . '</td>';
            echo '<td>' . $oTemplate->name . '</td>';
            echo '<td>';
            if ($oTemplate->isEditable()) {
                echo '<a class="action_icon edit_icon" title="Bewerk template" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oTemplate->templateId . '"></a>';
            } else {
                echo '<span class="action_icon edit_icon grey"></span>';
            }
            if ($oTemplate->isDeletable()) {
                echo '<a class="action_icon delete_icon" title="Verwijder template" onclick="return confirmChoice(\'template ' . $oTemplate->description . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oTemplate->templateId . '"></a>';
            } else {
                echo '<span class="action_icon delete_icon grey"></span>';
            }
            echo '<a class="action_icon copy_icon" title="Kopieer template" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/toevoegen?copyFrom=' . $oTemplate->templateId . '"></a>';
            echo '<a class="action_icon mail_icon fancyBoxLink" onclick="$(\'#sendTestTemplateId\').val(\'' . $oTemplate->templateId . '\');" title="Verstuur een test" href="#sendTestForm"></a>';
            echo '</td>';
            echo '</tr>';
        }
        if (count($aTemplates) == 0) {
            echo '<tr><td colspan="5"><i>Er zijn geen templates om weer te geven met dit filter</i></td></tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr class="bottomRow">
            <td colspan="6">
                <form method="POST">
                    <?= generatePaginationHTML($iPageCount, $iCurrPage) ?>
                    <input type="hidden" name="setPerPage" value="1" />
                    <select name="perPage" onchange="$(this).closest('form').submit();">
                        <option value="">alle</option>
                        <option <?= $iPerPage == 10 ? 'SELECTED' : '' ?> value="10">10</option>
                        <option <?= $iPerPage == 25 ? 'SELECTED' : '' ?> value="25">25</option>
                        <option <?= $iPerPage == 50 ? 'SELECTED' : '' ?> value="50">50</option>
                        <option <?= $iPerPage == 100 ? 'SELECTED' : '' ?> value="100">100</option>
                    </select> per pagina
                </form>
            </td>
        </tr>
    </tfoot>
</table>
<div class="hide">
    <div id="sendTestForm">
        <h2>Test template versturen</h2>
        <form onsubmit="sendTest(this);
                return false;" method="POST">
            <input type="hidden" id="sendTestTemplateId" name="templateId" value="" />
            <table class="withForm">
                <tr>
                    <td style="width: 60px;" class="withLabel"><label for="to">Naar</label></td>
                    <td><input type="text" size="30" name="to" id="to" value="" /></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Test verzenden" /></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<?
$sJavascript = <<<EOT
<script>
    function sendTest(form){
        $.ajax({
            type: "POST",
            url: '/admin/templates-beheer/ajax-sendTest',
            data: $(form).serialize(),
            success: function(data){
                var dataObj = eval('('+data+')');
                if(dataObj.success){
                    alert('Test email is verzonden naar: `' + dataObj.to + '`');
                    $.fancybox.close();
                }else{
                    alert('Test email kon niet worden verzonden naar: `' + dataObj.to + '`');
                }
            }
        });
    }
</script>
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);
?>