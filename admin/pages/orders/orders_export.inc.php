<?    
$sExport = '<table>';
$sExport .= '<tr>'
            . '<td>Betaaldatum</td>'
            . '<td>Ordernummer</td>'
            . '<td>Naam (Factuuradres)</td>'
            . '<td>Straat (Factuuradres)</td>'
            . '<td>Huisnummer (Factuuradres)</td>'
            . '<td>Postcode (Factuuradres)</td>'
            . '<td>Plaats (Factuuradres)</td>'
            . '<td>Naam (Factuuradres)</td>'
            . '<td>Straat (Bezorgadres) </td>'
            . '<td>Huisnummer (Bezorgadres)</td>'
            . '<td>Postcode (Bezorgadres)</td>'
            . '<td>Plaats (Bezorgadres)</td>'
            . '<td>E-mailadres</td>'
            . '<td>Betalingsmethode</td>'
            . '<td>Betaalstatus</td>'
            . '<td>Verzendkosten'.($bWithTax ? '(incl BTW)' : ' (excl BTW)').'</td>'
            . '<td>Factuurbedrag</td>'   
            . '<td>Korting</td>'
        
            // Product information
            . '<td>Merk</td>' 
            . '<td>Artikelnaam</td>'
            . '<td>Artikelnummer</td>'
            . '<td>Maat</td>'
            . '<td>Aantal</td>'
            . '<td>Prijs per stuk'.($bWithTax ? '(incl BTW)' : ' (excl BTW)').'</td>'
            . '<td>Totaal'.($bWithTax ? '(incl BTW)' : ' (excl BTW)').'</td>'
            . '<td>Inkoopprijs'.($bWithTax ? '(incl BTW)' : ' (excl BTW)').'</td>'
            . '<td>Marge'.($bWithTax ? '(incl BTW)' : ' (excl BTW)').'</td>'
        . '</tr>';

foreach ($aOrders as $oOrder) {
    $new_order = true;
    foreach (OrderProductManager::getProductsByOrderId($oOrder->orderId) as $oOrderProduct){
        $sExport .= '<tr>';
        // Order header details, only if it's the first product of the order
        if ($new_order){
           $sExport .=  '<td></td>'
                        . '<td>'. $oOrder->orderId .'</td>'
                        . '<td>'. $oOrder->invoice_firstName .' '. $oOrder->invoice_lastName .'</td>'
                        . '<td>'. $oOrder->invoice_address .'</td>'
                        . '<td>'. $oOrder->invoice_houseNumber .''. $oOrder->invoice_houseNumberAddition .'</td>'
                        . '<td>'. $oOrder->invoice_postalCode .'</td>'
                        . '<td>'. $oOrder->invoice_city .'</td>'
                        . '<td>'. $oOrder->delivery_firstName .' '. $oOrder->delivery_lastName .'</td>'
                        . '<td>'. $oOrder->delivery_address .'</td>'
                        . '<td>'. $oOrder->delivery_houseNumber .''. $oOrder->delivery_houseNumberAddition .'</td>'
                        . '<td>'. $oOrder->delivery_postalCode .'</td>'
                        . '<td>'. $oOrder->delivery_city .'</td>'
                        . '<td>'. $oOrder->email .'</td>'
                        . '<td>'. $oOrder->deliveryMethodName .'</td>'
                        . '<td>'. OrderStatusManager::getLabelByStatus($oOrder->status) .'</td>'
                        . '<td>'. $oOrder->getDeliveryPrice() .'</td>'
                        . '<td>'. $oOrderProduct->brandName .'</td>'
                        . '<td>'. $oOrder->totalDiscount .'</td>';          
        }else{
            $sExport .=  '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>'
                        . '<td></td>';
        }
        // Order product details
            $sExport .=   '<td>'. $oOrderProduct->brandName .'</td>'
                        . '<td>'. $oOrderProduct->productName .'</td>'
                        . '<td>'. $oOrderProduct->catalogProductId .'</td>'
                        . '<td>'. $oOrderProduct->productSizeName .'</td>'
                        . '<td>'. $oOrderProduct->amount .'</td>'
                        . '<td>'. EuroFormat($oOrderProduct->getSalePrice($bWithTax)) .'</td>'
                        . '<td>'. EuroFormat(EuroFormat($oOrderProduct->getSalePrice($bWithTax)) * $oOrderProduct->amount) .'</td>'
                        . '<td>'. EuroFormat($oOrderProduct->getPurchasePrice($bWithTax)) .'</td>'
                        . '<td>'. EuroFormat($oOrderProduct->getSalePrice($bWithTax) - ($oOrderProduct->getPurchasePrice($bWithTax))) .'</td>';
        $new_order = false;
        $sExport .= '</tr>';
    }
}
$sExport .= '</table>';

$sFileName = $oOrder->orderId . '-' . prettyUrlPart($oOrder->customerId) . '-' . date('Ymd_His');
$sFileName .= $bWithTax ? '_incl BTW' : '_excl BTW ';
    
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename=' . $sFileName . '.xls');
header('Pragma: no-cache');
header('Expires: 0');

?>

