<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het verzendmethoden overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Verzendmethode</legend>
                <table class="withForm">
                    <tr>
                        <td class="withLabel" style="width: 116px;"><label for="name">Verzendmethode *</label></td>
                        <td><input id="name" class="{validate:{required:true}} default" title="Vul de naam in" type="text" name="name" value="<?= _e($oDeliveryMethod->name) ?>" /></td>
                        <td><span class="error"><?= $oDeliveryMethod->isPropValid("name") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="priceWithTax">Prijs (incl. BTW)</label> <div class="hasTooltip tooltip" title="Vul hier de verzendkosten in. De verzendkosten zijn altijd incl. BTW omdat<br />de BTW na ratio wordt verdeeld op basis van de BTW percentages van de gekochte producten.">&nbsp;</div></td>
                        <td colspan="2">
                            &euro; <input size="6" id="priceWithTax" class="{validate:{required:true,number:true}}" name="price" type="text" value="<?= (is_numeric($oDeliveryMethod->price) ? number_format($oDeliveryMethod->price, 2, '.', '') : '') ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="freeFromPrice">Gratis vanaf (<?= (Settings::get('taxIncluded') ? 'incl' : 'excl') ?>. BTW)</label></td>
                        <td colspan="2">
                            &euro; <input size="6" id="freeFromPrice" name="freeFromPrice" class="{validate:{number:true}}" type="text" value="<?= (is_numeric($oDeliveryMethod->freeFromPrice) ? number_format($oDeliveryMethod->freeFromPrice, 2, '.', '') : '') ?>" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="withLabel"><label for="deliveryTime">Levertijd</label></td>
                        <td>
                            <input id="deliveryTime" name="deliveryTime" class="default" type="text" value="<?= $oDeliveryMethod->deliveryTime ?>" /> 
                        </td>
                    </tr>

                    <?
                    $aPaymentMethods = PaymentMethodManager::getPaymentMethodsByFilter();
                    if (!empty($aPaymentMethods)) {
                        ?>
                        <tr>
                            <td colspan="3"><h2>Geaccepteerde betaalmethoden voor deze verzendwijze</h2></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <?
                                # set current categories in an array to check whether to select/check the options
                                $aPaymentMethodIds = array();
                                foreach ($oDeliveryMethod->getPaymentMethods('all') AS $oPaymentMethod) {
                                    $aPaymentMethodIds[] = $oPaymentMethod->paymentMethodId;
                                }

                                echo '<ul style="list-style: none; margin: 0; padding: 0;">';
                                foreach ($aPaymentMethods as $oPaymentMethod) {
                                    echo '<li><input class="alignCheckbox {validate:{required:true}}" title="Kies tenminste 1 betaalwijze" id="paymentMethod_' . $oPaymentMethod->paymentMethodId . '" type="checkbox" name="paymentMethodIds[]" value="' . $oPaymentMethod->paymentMethodId . '" ' . (in_array($oPaymentMethod->paymentMethodId, $aPaymentMethodIds) ? 'CHECKED' : '') . ' /> <label for="paymentMethod_' . $oPaymentMethod->paymentMethodId . '">' . _e($oPaymentMethod->name) . '</label>';
                                }
                                echo '</ul>';
                                ?>
                            </td>
                            <td><span class="error"><?= $oDeliveryMethod->isPropValid("categories") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                        <?
                    }
                    ?>

                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het verzendmethoden overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>