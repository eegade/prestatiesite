<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="6"><h2>Alle verzendmethoden</h2>
                <div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Verzendmethode toevoegen" alt="Verzendmethode toevoegen">Verzendmethode toevoegen</a><br /><a class="changeOrderBtn textLeft right" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/volgorde-wijzigen" title="Volgorde wijzigen" alt="Volgorde wijzigen">Volgorde wijzigen</a></div>
            </td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted">Verzendmethode</th>
            <th class="{sorter:false} nonSorted">Prijs</th>
            <th class="{sorter:false} nonSorted">Gratis vanaf</th>
            <th class="{sorter:false} nonSorted">Levertijd</th>
            <th class="{sorter:false} nonSorted">Betaalwijzen</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aDeliveryMethods AS $oDeliveryMethod) {
            echo '<tr>';
            echo '<td>' . _e($oDeliveryMethod->name) . '</td>';
            echo '<td>' . decimal2valuta(_e($oDeliveryMethod->price)) . '</td>';
            echo '<td>' . ($oDeliveryMethod->freeFromPrice ? decimal2valuta(_e($oDeliveryMethod->freeFromPrice)) . ' ' . (Settings::get('taxIncluded') ? '(incl. BTW)' : '(excl. BTW)') : '') . '</td>';
            echo '<td>' . ($oDeliveryMethod->deliveryTime ? $oDeliveryMethod->deliveryTime : '') . '</td>';
            echo '<td>| ';
            foreach ($oDeliveryMethod->getPaymentMethods('all') AS $oPaymentMethod) {
                echo $oPaymentMethod->name . ' | ';
            }
            echo '</td>';
            echo '<td>';
            if ($oDeliveryMethod->isEditable()) {
                echo '<a class="action_icon edit_icon" title="Bewerk verzendmethode" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oDeliveryMethod->deliveryMethodId . '"></a>';
            } else {
                echo '<span class="action_icon edit_icon" title="Verzendmethode kan niet bewerkt worden"></span>';
            }

            if ($oDeliveryMethod->isDeletable()) {
                echo '<a class="action_icon delete_icon" onclick="return confirmChoice(\'deze verzendmethode\');" title="Verwijder verzendmethode" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oDeliveryMethod->deliveryMethodId . '"></a>';
            } else {
                echo '<span class="action_icon delete_icon" title="Verzendmethode kan niet verwijderd worden"></span>';
            }

            echo '</td>';
            echo '</tr>';
        }
        if (empty($aDeliveryMethods)) {
            echo '<tr><td colspan="5"><i>Er zijn geen verzendmethoden om weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>