<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het bestellingen overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Product</legend>
                <table class="withForm">
                    <tr>
                        <td style="width: 123px;"><label for="orderNumber">Ordernummer *</label></td>
                        <td colspan="2"><input id="orderNumber" type="text" value="<?= $oOrder->orderId ?>" disabled /></td>
                    </tr>
                    <tr>
                        <td><label for="email">Email *</label></td>
                        <td><input id="email" class="autofocus {validate:{required:true,email:true,messages:{required:'Vul een e-mailadres in',email:'Vul een geldig e-mailadres in'}}} default" title="Vul een geldig emailadres in" type="text" autocomplete="off" name="email" value="<?= _e($oOrder->email) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("email") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="3"><h2>Factuurgegevens</h2></td>
                    </tr>
                    <tr>
                        <td><label for="invoice_companyName">Bedrijfsnaam</label></td>
                        <td colspan="2"><input class="default" id="invoice_companyName" type="text" name="invoice_companyName" value="<?= _e($oOrder->invoice_companyName) ?>" /></td>
                    </tr>
                    <tr>
                        <td>Geslacht *</td>
                        <td>
                            <input class="alignRadio {validate:{required:true}}" title="Selecteer een geslacht (bij de factuurgegevens)" type="radio" <?= $oOrder->invoice_gender == 'M' ? 'CHECKED' : '' ?> id="invoice_gender_M" name="invoice_gender" value="M" /> <label for="invoice_gender_M">Man</label>
                            <input class="alignRadio {validate:{required:true}}" title="Selecteer een geslacht (bij de factuurgegevens)" type="radio" <?= $oOrder->invoice_gender == 'F' ? 'CHECKED' : '' ?> id="invoice_gender_F" name="invoice_gender" value="F" /> <label for="invoice_gender_F">Vrouw</label>
                        </td>
                        <td><span class="error"><?= $oOrder->isPropValid("invoice_gender") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="invoice_firstName">Voornaam *</label></td>
                        <td><input id="invoice_firstName" class="{validate:{required:true}} default" title="Vul de voornaam in (bij de factuurgegevens)" type="text" name="invoice_firstName" value="<?= _e($oOrder->invoice_firstName) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("invoice_firstName") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="invoice_lastName">Achternaam *</label></td>
                        <td><input id="invoice_lastName" class="{validate:{required:true}} default" title="Vul de achternaam in (bij de factuurgegevens)" type="text" name="invoice_lastName" value="<?= _e($oOrder->invoice_lastName) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("invoice_lastName") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="invoice_address">Adres *</label></td>
                        <td><input id="invoice_address" class="{validate:{required:true}} default" title="Vul het adres in (bij de factuurgegevens)" type="text" name="invoice_address" value="<?= _e($oOrder->invoice_address) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("invoice_address") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="invoice_houseNumber">Huisnummer *</label></td>
                        <td><input id="invoice_houseNumber" class="{validate:{required:true, number:true}} default" title="Vul een geldig huisnummer in met alleen nummers (bij de factuurgegevens)" type="text" name="invoice_houseNumber" value="<?= _e($oOrder->invoice_houseNumber) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("invoice_houseNumber") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="invoice_houseNumberAddition">Toevoeging</label></td>
                        <td colspan="2"><input class="default" id="invoice_houseNumberAddition" type="text" name="invoice_houseNumberAddition" value="<?= _e($oOrder->invoice_houseNumberAddition) ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="invoice_postalCode">Postcode *</label></td>
                        <td><input id="invoice_postalCode" class="{validate:{required:true}} default" title="Vul de postcode in (bij de factuurgegevens)" type="text" name="invoice_postalCode" value="<?= _e($oOrder->invoice_postalCode) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("invoice_postalCode") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="invoice_city">Plaats *</label></td>
                        <td><input id="invoice_city" class="{validate:{required:true}} default" title="Vul de plaats in (bij de factuurgegevens)" type="text" name="invoice_city" value="<?= _e($oOrder->invoice_city) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("invoice_city") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="invoice_phone">Telefoon</label></td>
                        <td colspan="2"><input class="default" id="invoice_phone" type="text" name="invoice_phone" value="<?= _e($oOrder->invoice_phone) ?>" /></td>
                    </tr>
                    <tr>
                        <td colspan="3"><h2>Aflevergegevens</h2></td>
                    </tr>
                    <tr>
                        <td><label for="delivery_companyName">Bedrijfsnaam</label></td>
                        <td colspan="2"><input class="default" id="delivery_companyName" type="text" name="delivery_companyName" value="<?= _e($oOrder->delivery_companyName) ?>" /></td>
                    </tr>
                    <tr>
                        <td>Geslacht *</td>
                        <td>
                            <input class="alignRadio {validate:{required:true}}" title="Selecteer een geslacht (bij de aflevergegevens)" type="radio" <?= $oOrder->delivery_gender == 'M' ? 'CHECKED' : '' ?> id="delivery_gender_M" name="delivery_gender" value="M" /> <label for="delivery_gender_M">Man</label>
                            <input class="alignRadio {validate:{required:true}}" title="Selecteer een geslacht (bij de aflevergegevens)" type="radio" <?= $oOrder->delivery_gender == 'F' ? 'CHECKED' : '' ?> id="delivery_gender_F" name="delivery_gender" value="F" /> <label for="delivery_gender_F">Vrouw</label>
                        </td>
                        <td><span class="error"><?= $oOrder->isPropValid("delivery_gender") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="delivery_firstName">Voornaam *</label></td>
                        <td><input id="delivery_firstName" class="{validate:{required:true}} default" title="Vul de voornaam in (bij de aflevergegevens)" type="text" name="delivery_firstName" value="<?= _e($oOrder->delivery_firstName) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("delivery_firstName") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="delivery_lastName">Achternaam *</label></td>
                        <td><input id="delivery_lastName" class="{validate:{required:true}} default" title="Vul de achternaam in (bij de aflevergegevens)" type="text" name="delivery_lastName" value="<?= _e($oOrder->delivery_lastName) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("delivery_lastName") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="delivery_address">Adres *</label></td>
                        <td><input id="delivery_address" class="{validate:{required:true}} default" title="Vul het adres in (bij de aflevergegevens)" type="text" name="delivery_address" value="<?= _e($oOrder->delivery_address) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("delivery_address") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="delivery_houseNumber">Huisnummer *</label></td>
                        <td><input id="delivery_houseNumber" class="{validate:{required:true, number:true}} default" title="Vul een geldig huisnummer in met alleen nummers (bij de factuurgegevens)" type="text" name="delivery_houseNumber" value="<?= _e($oOrder->delivery_houseNumber) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("delivery_houseNumber") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="delivery_houseNumberAddition">Toevoeging</label></td>
                        <td colspan="2"><input class="default" id="delivery_houseNumberAddition" type="text" name="delivery_houseNumberAddition" value="<?= _e($oOrder->delivery_houseNumberAddition) ?>" /></td>
                    </tr>
                    <tr>
                        <td><label for="delivery_postalCode">Postcode *</label></td>
                        <td><input id="delivery_postalCode" class="{validate:{required:true}} default" title="Vul de postcode in (bij de aflevergegevens)" type="text" name="delivery_postalCode" value="<?= _e($oOrder->delivery_postalCode) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("delivery_postalCode") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="delivery_city">Plaats *</label></td>
                        <td><input id="delivery_city" class="{validate:{required:true}} default" title="Vul de plaats in (bij de aflevergegevens)" type="text" name="delivery_city" value="<?= _e($oOrder->delivery_city) ?>" /></td>
                        <td><span class="error"><?= $oOrder->isPropValid("delivery_city") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label>Locatie</label></td>
                        <td colspan="2"><a href="https://maps.google.com/maps?q=<?= urlencode($oOrder->delivery_lat . ', ' . $oOrder->delivery_lng) ?>&hl=nl" target="_blank">Klik hier om de locatie te bekijken in Google Maps</a></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />                            
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>

    </div>
    <div class="contentColumn" style="margin-top:21px !important;">
        <form id="downloadPdf" method="POST" action="">
            <input type="hidden" value="getPdf" name="action" />
            <input type="submit" value="Download PDF" name="orderPdf"/>
        </form>
    </div>
    <div class="contentColumn">
        <fieldset>
            <legend>Producten</legend>
            <table class="sorted" style="width: 100%; margin-bottom: 10px;">
                <thead>
                    <tr>
                        <th class="{sorter:false} nonSorted">Productnaam</th>
                        <th class="{sorter:false} nonSorted">Merknaam</th>
                        <th class="{sorter:false} nonSorted">Aantal</th>
                        <th class="{sorter:false} nonSorted">Afgeboekt <div class="hasTooltip tooltip" title="Aantal producten dat al afgeboekt is van de voorraad.<br />- wordt automatisch gedaan als de bestelling de status `betaald` krijgt<br />- status `geanuleerd` boekt de voorraad automatisch bij">&nbsp;</div></th>
                <th class="{sorter:false} nonSorted" style="width: 60px;">Prijs</th>
                </tr>
                </thead>
                <tbody>
                    <?
                    foreach ($oOrder->getProducts() as $oProduct) {
                        ?>
                        <tr>
                            <td>
                                <?= (is_numeric($oProduct->catalogProductId) ? '<a title="Bekijk het originele product" href="' . ADMIN_FOLDER . '/catalogus/bewerken/' . $oProduct->catalogProductId . '">' . $oProduct->productName . '</a>' : $oProduct->productName) ?>
                                <?
                                if ($oProduct->productSizeName || $oProduct->productColorName) {
                                    echo $oProduct->productSizeName ? ' | ' . $oProduct->productSizeName : '';
                                    echo $oProduct->productColorName ? ' | ' . $oProduct->productColorName : '';
                                    echo ' |';
                                }
                                ?>
                            </td>
                            <td><?= $oProduct->brandName ?></td>
                            <td><?= $oProduct->amount ?></td>
                            <td><?= $oProduct->substractedFromStock ?></td>
                            <td style="text-align: right;"><?= decimal2valuta($oProduct->getSubTotalPrice(true)) ?></td>
                        </tr>
                        <?
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4">Subtotaal</td>
                        <td style="text-align: right;"><?= decimal2valuta($oOrder->getSubtotalProducts(true)) ?></td>
                    </tr>
                    <tr>
                        <td colspan="4">Korting</td>
                        <td style="text-align: right;"><?= decimal2valuta($oOrder->getDiscount(true)) ?></td>
                    </tr>
                    <tr>
                        <td colspan="4">Aflevermethode: <?= $oOrder->deliveryMethodName ?></td>
                        <td style="text-align: right;"><?= decimal2valuta($oOrder->getDeliveryPrice()) ?></td>
                    </tr>
                    <tr>
                        <td colspan="4">Betaalmethode: <?= $oOrder->paymentMethodName ?></td>
                        <td style="text-align: right;"><?= decimal2valuta($oOrder->getPaymentPrice()) ?></td>
                    </tr>
                    <tr>
                        <td colspan="4"><b>Totaal</b></td>
                        <td style="text-align: right;"><b><?= decimal2valuta($oOrder->getTotal(true)) ?></b></td>
                    </tr>
                    <tr>
                        <td colspan="4">BTW</td>
                        <td style="text-align: right;"><?= decimal2valuta($oOrder->getBTW()) ?></td>
                    </tr>
                    <tr>
                        <td colspan="4">Totaal (excl. BTW)</td>
                        <td style="text-align: right;"><?= decimal2valuta($oOrder->getTotal(false)) ?></td>
                    </tr>

                </tfoot>
            </table>
        </fieldset>
    </div>
    <div class="contentColumn">
        <fieldset>
            <legend>Bestellingstatus</legend>
            <form method="POST" action="" class="validateForm">
                <input type="hidden" name="action" value="saveStatus" />
                <table class="withForm">
                    <tr>
                        <td class="withLabel" style="width: 116px;"><label for="status">Status <div class="hasTooltip tooltip" title="Wijzig hier de status om de statusgeschiedenis bij te werken<br />- `betaald`: voorraad wordt afgeboekt als dat nog niet gedaan is<br />- `geannuleerd`: voorraad wordt bijgeboekt als dat nog niet gedaan is">&nbsp;</div></label></td>
                        <td>
                            <select id="status" class="{validate:{required:true}} default" title="Kies een status" name="status">
                                <option value="">Maak een keuze</option>
                                <?
                                foreach (OrderStatusManager::getAllStatuses() as $iStatus) {
                                    echo '<option value="' . $iStatus . '" ' . ($oOrder->status == $iStatus ? 'selected' : '') . '>' . OrderStatusManager::getLabelByStatus($iStatus) . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td><span class="error"><?= $oStatus->isPropValid("status") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="notifyCustomer">Notificatie <div class="hasTooltip tooltip" title="Vink deze optie aan om de klant een email te sturen over de status wijziging">&nbsp;</div></label></td>
                        <td>
                            <input class="alignCheckbox" id="notifyCustomer" name="notifyCustomer" onclick="this.checked ? $('.commentRow').show() : $('.commentRow').hide()" type="checkbox" value="1" /> <label for="notifyCustomer">stuur notificatie</label>
                        </td>
                    </tr>
                    <tr class="hide commentRow">
                        <td colspan="2"><label for="comment">Eventuele opmerking over status <div class="hasTooltip tooltip" title="Vul een eventuele opmerking om mee te sturen met de status notificatie">&nbsp;</div></label></td>
                    </tr>
                    <tr class="hide commentRow">
                        <td colspan="2">
                            <textarea class="default" name="comment"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><input type="submit" value="Status bewerken" name="saveStatus" /></td>
                    </tr>
                </table>
            </form>
            <hr />
            <h3>Statusgeschiedenis</h3>
            <table class="sorted" style="margin-bottom: 10px;">
                <thead>
                    <tr>
                        <th class="{sorter:'dateNL'}">Datum/tijd</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?
                    foreach ($oOrder->getStatusHistory() as $i => $oStatus) {
                        echo '<tr>';
                        echo '<td>' . ($i == 0 ? '<b>' : '') . strftime('%d-%m-%Y %H:%M:%S', strtotime($oStatus->timestamp)) . ($i == 0 ? '</b>' : '') . '</td>';
                        echo '<td>' . ($i == 0 ? '<b>' : '') . $oStatus->getLabel() . ($i == 0 ? '</b>' : '') . '</td>';
                        echo '</tr>';
                    }
                    ?>
                </tbody>
            </table>
        </fieldset>
    </div>
    <div class="contentColumn">
        <fieldset>
            <legend>Online betalingen</legend>
            <?
            if (count($oOrder->getPaymentHistory()) > 0) {
                ?>
                <table class="sorted" style="margin-bottom: 10px;">
                    <thead>
                        <tr>
                            <th class="{sorter:'dateNL'}">Betaling gestart</th>
                            <th class="{sorter:'dateNL'}">Laatste update</th>
                            <th>Betaalmethode</th>
                            <th>Prijs</th>
                            <th>Status</th>
                            <th>Status extern</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        foreach ($oOrder->getPaymentHistory() as $i => $oPayment) {
                            echo '<tr>';
                            echo '<td>' . ($i == 0 ? '<b>' : '') . strftime('%d-%m-%Y %H:%M:%S', strtotime($oPayment->created)) . ($i == 0 ? '</b>' : '') . '</td>';
                            echo '<td>' . ($i == 0 ? '<b>' : '') . (empty($oPayment->modified) ? '' : strftime('%d-%m-%Y %H:%M:%S', strtotime($oPayment->modified))) . ($i == 0 ? '</b>' : '') . '</td>';
                            echo '<td>' . ($i == 0 ? '<b>' : '') . $oPayment->paymentMethodName . ($i == 0 ? '</b>' : '') . '</td>';
                            echo '<td>' . ($i == 0 ? '<b>' : '') . decimal2valuta($oPayment->price) . ($i == 0 ? '</b>' : '') . '</td>';
                            echo '<td>' . ($i == 0 ? '<b>' : '') . $oPayment->getStatusLabel() . ($i == 0 ? '</b>' : '') . '</td>';
                            echo '<td>' . ($i == 0 ? '<b>' : '') . $oPayment->externalStatus . ($i == 0 ? '</b>' : '') . '</td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
                <?
            } else {
                echo '<p>Er zijn geen online betalingen voor deze bestelling.</p>';
            }
            ?>
        </fieldset>
    </div>

</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het bestellingen overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>

