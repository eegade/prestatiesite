<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het betaalmethoden overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Betaalmethode</legend>
                <table class="withForm">
                    <tr>
                        <td class="withLabel" style="width: 180px;"><label for="name">Betaalmethode *</label></td>
                        <td><input id="name" class="{validate:{required:true}} default" title="Vul de naam in" type="text" name="name" value="<?= _e($oPaymentMethod->name) ?>" /></td>
                        <td><span class="error"><?= $oPaymentMethod->isPropValid("name") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="priceWithTax">Prijs (incl. BTW)</label> <div class="hasTooltip tooltip" title="Vul hier de betaalkosten in. De betaalkosten zijn altijd incl. BTW omdat<br />de BTW na ratio wordt verdeeld op basis van de BTW percentages van de gekochte producten.">&nbsp;</div></td>
                        <td colspan="2">                            
                            &euro; <input size="6" id="priceWithTax" class="{validate:{required:true,number:true}}" name="price" type="text" value="<?= (is_numeric($oPaymentMethod->price) ? number_format($oPaymentMethod->price, 2, '.', '') : '') ?>" />
                        </td>
                    </tr>                    
                    <tr>
                        <td><label>Is online betaalmethode *</label></td>
                        <td>
                            <input class="alignRadio" id="isOnlinePaymentMethod_1" name="isOnlinePaymentMethod" <?= $oPaymentMethod->isOnlinePaymentMethod ? 'checked' : '' ?> type="radio" value="1" /> <label for="isOnlinePaymentMethod_1">ja</label> <input class="alignRadio" id="isOnlinePaymentMethod_0" name="isOnlinePaymentMethod" <?= !$oPaymentMethod->isOnlinePaymentMethod ? 'checked' : '' ?> style="margin-left: 10px;" type="radio" value="0" /> <label for="isOnlinePaymentMethod_0">nee</label>
                        </td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="redirectPage">Betaalmethode url *</label></td>
                        <td><input id="redirectPage" class="{validate:{required:true}} default" title="Vul de url in waar de gebruiker na toe gaat na het bevestigen van de bestelling" type="text" name="redirectPage" value="<?= _e($oPaymentMethod->redirectPage) ?>" /></td>
                        <td><span class="error"><?= $oPaymentMethod->isPropValid("redirectPage") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het betaalmethoden overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>