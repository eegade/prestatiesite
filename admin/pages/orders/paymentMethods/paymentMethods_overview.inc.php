<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="5"><h2>Alle betaalmethoden</h2>
                <div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Betaalmethode toevoegen" alt="Betaalmethode toevoegen">Betaalmethode toevoegen</a><br /><a class="changeOrderBtn textLeft right" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/volgorde-wijzigen" title="Volgorde wijzigen" alt="Volgorde wijzigen">Volgorde wijzigen</a></div>
            </td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted">Betaalmethode</th>
            <th class="{sorter:false} nonSorted">Prijs</th>
            <th class="{sorter:false} nonSorted">Online methode</th>
            <th class="{sorter:false} nonSorted">Pagina na bestellen</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aPaymentMethods AS $oPaymentMethod) {
            echo '<tr>';
            echo '<td>' . _e($oPaymentMethod->name) . '</td>';
            echo '<td>' . decimal2valuta(_e($oPaymentMethod->price)) . '</td>';
            echo '<td>' . ($oPaymentMethod->isOnlinePaymentMethod ? 'ja' : 'nee') . '</td>';
            echo '<td>' . _e($oPaymentMethod->redirectPage) . '</td>';
            echo '<td>';
            if ($oPaymentMethod->isEditable()) {
                echo '<a class="action_icon edit_icon" title="Bewerk betaalmethode" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oPaymentMethod->paymentMethodId . '"></a>';
            } else {
                echo '<span class="action_icon edit_icon" title="Betaalmethode kan niet bewerkt worden"></span>';
            }

            if ($oPaymentMethod->isDeletable()) {
                echo '<a class="action_icon delete_icon" onclick="return confirmChoice(\'deze betaalmethode\');" title="Verwijder betaalmethode" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oPaymentMethod->paymentMethodId . '"></a>';
            } else {
                echo '<span class="action_icon delete_icon" title="Betaalmethode kan niet verwijderd worden"></span>';
            }

            echo '</td>';
            echo '</tr>';
        }
        if (empty($aPaymentMethods)) {
            echo '<tr><td colspan="5"><i>Er zijn geen betaalmethoden om weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>