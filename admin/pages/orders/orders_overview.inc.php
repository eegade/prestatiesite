<form action="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>" method="POST">
    <input type="hidden" name="filterForm" value="1" />
    <fieldset style="margin-bottom: 20px;">
        <legend>Filter</legend>
        <table class="withForm">
            <tr>
                <td class="withLabel" style="width: 116px;">Ordernummer <div class="hasTooltip tooltip" title="Zoek op het ordernummer van een bestelling">&nbsp;</div></td>
                <td><input type="text" name="orderFilter[orderId]" value="<?= $aOrderFilter['orderId'] ?>" /></td>
            </tr>
            <tr>
                <td class="withLabel">Status <div class="hasTooltip tooltip" title="Zoek op een of meer statussen van bestellingen<br />- houd de Ctrl-toets ingedrukt om meerdere statussen aan te kunnen klikken">&nbsp;</div></td>
                <td>
                    <select multiple size="6" name="orderFilter[statuses][]">
                        <?
                        foreach (OrderStatusManager::getAllStatuses() as $iStatus) {
                            echo '<option value="' . $iStatus . '" ' . (in_array($iStatus, $aOrderFilter['statuses']) ? 'selected' : '') . '>' . OrderStatusManager::getLabelByStatus($iStatus) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="filterOrders" value="Filter bestellingen" /> <input type="submit" name="resetFilter" value="Reset filter" /></td>
            </tr>
        </table>
    </fieldset>
</form>
<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="10">
                <h2>Alle bestellingen</h2>
                <div class="float-right"><a class="exportExcelBtn textLeft right" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/export?withtax=1' ?>" title="Exporteer bestellingen (incl.BTW)" alt="Exporteer bestellingen (incl.BTW)">Exporteer bestellingen (incl.BTW)</a><br /></div>
                <div class="float-right"><a class="exportExcelBtn textLeft right" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/export?withtax=0' ?>" title="Exporteer bestellingen (excl.BTW)" alt="Exporteer bestellingen (excl.BTW)">Exporteer bestellingen (excl.BTW)</a><br /></div>
            </td>
        </tr>
        <tr>
            <th>#</th>
            <th class="{sorter:'dateNL'}">Aangemaakt</th>
            <th class="{sorter:'dateNL'}">Bijgewerkt</th>
            <th class="{sorter:false} nonSorted">Voornaam</th>
            <th class="{sorter:false} nonSorted">Achternaam</th>
            <th class="{sorter:false} nonSorted">Status</th>
            <th class="{sorter:false} nonSorted">Betaalwijze</th>
            <th class="{sorter:false} nonSorted">Prijs</th>
            <th class="{sorter:false} nonSorted" style="width: 30px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aOrders AS $oOrder) {
            echo '<tr>';
            echo '<td>' . $oOrder->orderId . '</td>';
            echo '<td>' . strftime('%d-%m-%Y %H:%M:%S', strtotime($oOrder->created)) . '</td>';
            echo '<td>' . strftime('%d-%m-%Y %H:%M:%S', strtotime($oOrder->modified)) . '</td>';
            echo '<td>' . _e($oOrder->invoice_firstName) . '</td>';
            echo '<td>' . _e($oOrder->invoice_lastName) . '</td>';
            echo '<td>' . OrderStatusManager::getLabelByStatus($oOrder->status) . '</td>';
            echo '<td>' . $oOrder->getPaymentMethod()->name . '</td>';
            echo '<td>' . decimal2valuta($oOrder->getTotal(true)) . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk bestelling" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oOrder->orderId . '"></a>';
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aOrders)) {
            echo '<tr><td colspan="9"><i>Er zijn geen bestellingen om weer te geven met dit filter</i></td></tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr class="bottomRow">
            <td colspan="10">
                <form method="POST">
                    <?= generatePaginationHTML($iPageCount, $iCurrPage) ?>
                    <input type="hidden" name="setPerPage" value="1" />
                    <select name="perPage" onchange="$(this).closest('form').submit();">
                        <option value="">alle</option>
                        <option <?= $iPerPage == 10 ? 'SELECTED' : '' ?> value="10">10</option>
                        <option <?= $iPerPage == 25 ? 'SELECTED' : '' ?> value="25">25</option>
                        <option <?= $iPerPage == 50 ? 'SELECTED' : '' ?> value="50">50</option>
                        <option <?= $iPerPage == 100 ? 'SELECTED' : '' ?> value="100">100</option>
                    </select> per pagina
                </form>
            </td>
        </tr>
    </tfoot>
</table>