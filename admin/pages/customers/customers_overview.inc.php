<form action="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>" method="POST">
    <input type="hidden" name="filterForm" value="1" />
    <fieldset style="margin-bottom: 20px;">
        <legend>Filter</legend>
        <table class="withForm">
            <tr>
                <td class="withLabel" style="width: 116px;">Naam <div class="hasTooltip tooltip" title="Zoek op de naam van de klant of een gedeelte daarvan">&nbsp;</div></td>
                <td><input type="text" name="customerFilter[name]" value="<?= $aCustomerFilter['name'] ?>" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="filterCustomers" value="Filter klanten" /> <input type="submit" name="resetFilter" value="Reset filter" /></td>
            </tr>
        </table>
    </fieldset>
</form>
<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="4"><h2>Gevonden klanten (<?= (count($aCustomers) != 0 ? (db_int($iStart) + 1) : db_int($iStart)) . '-' . (db_int($iStart) + count($aCustomers)) ?>/<?= $iFoundRows ?>)</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Klant toevoegen" alt="Klant toevoegen">Klant toevoegen</a></div></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted onlineOffline"></th>
            <th>Voornaam</th>
            <th>Achternaam</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aCustomers AS $oCustomer) {
            echo '<tr>';
            echo '<td>';
            # online offline button
            echo '<a id="customer_' . $oCustomer->customerId . '_online_1" title="Klant offline zetten" class="action_icon ' . ($oCustomer->online ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oCustomer->customerId . '/?online=0"></a>';
            echo '<a id="customer_' . $oCustomer->customerId . '_online_0" title="Klant online zetten" class="action_icon ' . ($oCustomer->online ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oCustomer->customerId . '/?online=1"></a>';
            echo '</td>';
            echo '<td>' . _e($oCustomer->firstName) . '</td>';
            echo '<td>' . _e($oCustomer->lastName) . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk klant" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oCustomer->customerId . '"></a>';
            echo '<a class="action_icon delete_icon" title="Verwijder klant" onclick="return confirmChoice(\'' . _e($oCustomer->firstName . ' ' . $oCustomer->lastName) . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oCustomer->customerId . '"></a>';
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aCustomers)) {
            echo '<tr><td colspan="4"><i>Er zijn geen klanten om weer te geven met dit filter</i></td></tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr class="bottomRow">
            <td colspan="6">
                <form method="POST">
                    <?= generatePaginationHTML($iPageCount, $iCurrPage) ?>
                    <input type="hidden" name="setPerPage" value="1" />
                    <select name="perPage" onchange="$(this).closest('form').submit();">
                        <option value="">alle</option>
                        <option <?= $iPerPage == 10 ? 'SELECTED' : '' ?> value="10">10</option>
                        <option <?= $iPerPage == 25 ? 'SELECTED' : '' ?> value="25">25</option>
                        <option <?= $iPerPage == 50 ? 'SELECTED' : '' ?> value="50">50</option>
                        <option <?= $iPerPage == 100 ? 'SELECTED' : '' ?> value="100">100</option>
                    </select> per pagina
                </form>
            </td>
        </tr>
    </tfoot>
</table>
<?
# create necessary javascript
$sController = http_get('controller');
$sBottomJavascript = <<<EOT
<script type="text/javascript">
    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            data: 'ajax=1',
            async: true,
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $("#customer_"+dataObj.customerId+"_online_0").hide(); // hide offline button
                    $("#customer_"+dataObj.customerId+"_online_1").hide(); // hide online button
                    $("#customer_"+dataObj.customerId+"_online_"+dataObj.online).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.online == 0)    
                        showStatusUpdate("Klant offline gezet");
                    if(dataObj.online == 1)    
                        showStatusUpdate("Klant online gezet");                            
                } else {
                    showStatusUpdate("Klant niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>
