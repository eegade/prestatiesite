<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het doorlopende slider overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <fieldset>
            <legend>Doorlopende slider item</legend>
            <form method="POST" action="" class="validateForm">
                <input type="hidden" value="save" name="action" />
                <table class="withForm">
                    <tr>
                        <td>Online *</td>
                        <td>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de doorlopende slider item online OF offline" type="radio" <?= $oContinuousSliderItem->online ? 'CHECKED' : '' ?> id="online_1" name="online" value="1" /> <label for="online_1">Ja</label>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de doorlopende slider item online OF offline" type="radio" <?= !$oContinuousSliderItem->online ? 'CHECKED' : '' ?> id="online_0" name="online" value="0" /> <label for="online_0">Nee</label>
                        </td>
                        <td><span class="error"><?= $oContinuousSliderItem->isPropValid("online") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="name">Naam * </label> <div class="hasTooltip tooltip" title="Deze titel word alleen gebruikt in het CMS.<br/>Deze wordt nergens op de website vertoond.<br/> De titel is alleen bedoelt om de verschillende items uit elkaar te halen d.m.v. een goede titel.">&nbsp;</div></td>
                        <td><input class="{validate:{required:true}} default" id="name" type="text" autocomplete="off" name="name" value="<?= $oContinuousSliderItem->name ?>" /> <span style="font-size: 11px; font-style: italic;">(word niet getoond)</span></td>
                        <td><span class="error"><?= $oContinuousSliderItem->isPropValid("name") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label>Link pagina</label> <div class="hasTooltip tooltip" title="U kunt het Doorlopende slider item koppelen aan een pagina of/een nieuwsbericht of een externe link.">&nbsp;</div></td>
                        <td>
                            <select name="pageId" class="linkChoice default">
                                <option value="">Maak een keuze</option>
                                <?

                                function generateOptions($aPages, &$oContinuousSliderItem) {
                                    foreach ($aPages as $oPage) {
                                        $sLeadingChars = '';
                                        for ($iC = $oPage->level; $iC > 1; $iC--) {
                                            $sLeadingChars .= '--';
                                        }
                                        echo '<option value="' . $oPage->pageId . '" ' . ($oPage->pageId == $oContinuousSliderItem->pageId ? 'selected' : '') . '>' . $sLeadingChars . $oPage->getShortTitle() . '</option>';
                                        generateOptions($oPage->getSubPages('online-all'), $oContinuousSliderItem);
                                    }
                                }

                                generateOptions(PageManager::getPagesByFilter(array('showAll' => 1, 'online' => 1, 'level' => 1)), $oContinuousSliderItem);
                                ?>
                            </select>
                        </td>
                    </tr>
                    <? if (BB_WITH_NEWS) { ?>
                        <tr>
                            <td class="withLabel"><label>Link nieuwsbericht</label> <div class="hasTooltip tooltip" title="U kunt het Doorlopende slider item koppelen aan een pagina of/een nieuwsbericht of een externe link.">&nbsp;</div></td>
                            <td>
                                <select class="linkChoice default" name="newsItemId" id="newsItemId">
                                    <option value="">Maak een keuze</option>
                                    <?
                                    if (NewsItemManager::getNewsItemsByFilter()) {
                                        foreach (NewsItemManager::getNewsItemsByFilter() as $oNewsItem) {
                                            echo '<option value="' . $oNewsItem->newsItemId . '" ' . ($oNewsItem->newsItemId == $oContinuousSliderItem->newsItemId ? 'selected' : '') . '>' . $oNewsItem->title . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    <? } ?>
                    <tr>
                        <td class="withLabel"><label for="link">Link</label> <div class="hasTooltip tooltip" title="U kunt het Doorlopende slider item koppelen aan een pagina of/een nieuwsbericht of een externe link.">&nbsp;</div></td>
                        <td>
                            <input class="linkChoice default" title="Typ hier een link" name="link" type="text" id="link" value="<?= $oContinuousSliderItem->link ?>" />
                        </td>
                        <td><span class="error"><?= $oContinuousSliderItem->isPropValid('link') ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="name">Regel 1</label></td>
                        <td colspan="2"><input class="default" id="title" type="text" autocomplete="off" name="line1" value="<?= $oContinuousSliderItem->line1 ?>" /></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="name">Regel 2</label></td>
                        <td colspan="2"><input class="default" id="title" type="text" autocomplete="off" name="line2" value="<?= $oContinuousSliderItem->line2 ?>" /></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </form>
        </fieldset>
    </div>
    <!-- Insert image at agenda item -->
    <div class="contentColumn">
        <fieldset id="continuousSliderImages">
            <legend>Afbeeldingen</legend>
            <?
            if ($oContinuousSliderItem->continuousSliderItemId !== null) {
                $oImageManagerHTML->includeTemplate();
            } else {
                echo '<p><i>Afbeeldingen kunnen worden geüpload nadat het referentie item eerst is opgeslagen</i></p>';
            }
            ?>
        </fieldset>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het doorlopende slider overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
# define and add javascript to bottom
$sBottomJavascript = <<<EOT
<script type="text/javascript">
        $('.linkChoice').change(function(){
            var filled = null;
            $('.linkChoice').each(function(index, element){
                if($(element).val() != ''){
                    filled = element;
                }
            });
            if(filled === null){
                $('.linkChoice').attr('disabled', false);
            }else{
                $('.linkChoice').not(filled).attr('disabled', true);
            }
        });
        
        $('#link').keyup(function(){
            $(this).change();
        });
        
        $('#link').change();
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>