<table class="sorted withActionIcons">
    <thead>
        <tr class="topRow">
            <td colspan="3"><h2>Alle doorlopende slider items</h2>
                <div class="right">
                    <a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Doorlopende slider item toevoegen" alt="Doorlopende slider item toevoegen">Doorlopende slider item toevoegen</a><br />
                    <a class="changeOrderBtn textLeft right" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/volgorde-wijzigen&lang=1" title="Volgorde wijzigen" alt="Volgorde wijzigen">Volgorde wijzigen</a>
                </div>
            </td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted onlineOffline"></th>
            <th>Naam</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody id="tableContents">
        <?
        foreach ($aAllContinuousSliderItems AS $oContinuousSliderItem) {
            echo '<tr data-id="' . $oContinuousSliderItem->continuousSliderItemId . '" class="movable">';
            echo '<td>';
            # online offline button
            echo '<a id="continuousSliderItem_' . $oContinuousSliderItem->continuousSliderItemId . '_online_1" title="Doorlopende slider item offline zetten" class="action_icon ' . ($oContinuousSliderItem->online ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oContinuousSliderItem->continuousSliderItemId . '/?online=0"></a>';
            echo '<a id="continuousSliderItem_' . $oContinuousSliderItem->continuousSliderItemId . '_online_0" title="Doorlopende slider item online zetten" class="action_icon ' . ($oContinuousSliderItem->online ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oContinuousSliderItem->continuousSliderItemId . '/?online=1"></a>';
            echo '</td>';
            echo '<td>' . $oContinuousSliderItem->name . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk doorlopende slider item" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oContinuousSliderItem->continuousSliderItemId . '"></a>';
            echo '<a class="action_icon delete_icon" title="Verwijder doorlopende slider item" onclick="return confirmChoice(\'' . $oContinuousSliderItem->getName() . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oContinuousSliderItem->continuousSliderItemId . '"></a>';
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aAllContinuousSliderItems)) {
            echo '<tr><td colspan="3"><i>Er zijn geen doorlopende slider items om weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>

<?
# create necessary javascript
$sController = http_get('controller');
$sBottomJavascript = <<<EOT
<script type="text/javascript">
    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            data: 'ajax=1',
            async: true,
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $("#continuousSliderItem_"+dataObj.continuousSliderItemId+"_online_0").hide(); // hide offline button
                    $("#continuousSliderItem_"+dataObj.continuousSliderItemId+"_online_1").hide(); // hide online button
                    $("#continuousSliderItem_"+dataObj.continuousSliderItemId+"_online_"+dataObj.online).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.online == 0)    
                        showStatusUpdate("Doorlopende slider item offline gezet");
                    if(dataObj.online == 1)    
                        showStatusUpdate("Doorlopende slider item online gezet");                            
                } else {
                    showStatusUpdate("Doorlopende slider item niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>