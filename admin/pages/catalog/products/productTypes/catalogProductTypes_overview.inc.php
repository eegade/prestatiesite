<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="5"><h2>Alle product types</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Product type toevoegen" alt="Product type toevoegen">Product type toevoegen</a><br /><a class="changeOrderBtn textLeft right" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/volgorde-wijzigen" title="Volgorde wijzigen" alt="Volgorde wijzigen">Volgorde wijzigen</a></div></td>
        </tr>
        <tr>
            <th>Product type</th>
            <th>Met maten</th>
            <th>Met kleuren</th>
            <th>Met geslacht</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aAllProductTypes AS $oProductType) {
            echo '<tr>';
            echo '<td>' . _e($oProductType->title) . '</td>';
            echo '<td>' . ($oProductType->withSizes ? 'ja' : 'nee') . '</td>';
            echo '<td>' . ($oProductType->withColors ? 'ja' : 'nee') . '</td>';
            echo '<td>' . ($oProductType->withGenders ? 'ja' : 'nee') . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk merk" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductType->catalogProductTypeId . '"></a>';

            if ($oProductType->isDeletable()) {
                echo '<a class="action_icon delete_icon" title="Verwijder product type" onclick="return confirmChoice(\'' . _e($oProductType->title) . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oProductType->catalogProductTypeId . '"></a>';
            } else {
                echo '<span class="action_icon delete_icon grey" title="Aan dit type hangen nog producten"></span>';
            }
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aAllProductTypes)) {
            echo '<tr><td colspan="5"><i>Er zijn geen product types om weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>