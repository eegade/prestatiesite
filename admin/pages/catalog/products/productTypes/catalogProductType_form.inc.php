<?
$bHasProducts = CatalogProductTypeManager::getNumberOfProductsByProductTypeId($oProductType->catalogProductTypeId) > 0;
?>
<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het product types overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Product type</legend>
                <table class="withForm">
                    <tr>
                        <td class="withLabel" style="width: 116px;"><label for="title">Product type *</label></td>
                        <td><input class="{validate:{required:true}} default" name="title" title="Vul het product type in" type="text" value="<?= _e($oProductType->title) ?>" /></td>
                        <td><span class="error"><?= $oProductType->isPropValid("title") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="withSizes">Met maten *</label> <div class="hasTooltip tooltip" title="Dit type product heeft meerdere maten per toegevoegd product (niet meer te wijzigen na toevoegen eerste product)">&nbsp;</div></td>
                        <td>
                            <? if (!$bHasProducts) { ?>
                                <input class="alignRadio {validate:{required:true}}" title="Maak gebruik van maten voor dit type product" type="radio" <?= $oProductType->withSizes ? 'CHECKED' : '' ?> id="withSizes_1" name="withSizes" value="1" /> <label for="withSizes_1">Ja</label>
                                <input class="alignRadio {validate:{required:true}}" title="Maak geen gebruik van maten voor dit type product" type="radio" <?= !$oProductType->withSizes ? 'CHECKED' : '' ?> id="withSizes_0" name="withSizes" value="0" /> <label for="withSizes_0">Nee</label>
                            <? } else { ?>
                                <?= $oProductType->withSizes ? 'ja' : 'nee' ?> <div class="hasTooltip tooltip" title="Zolang er aan dit type product, producten hangen kan deze waarde niet gewijzigd worden">&nbsp;</div>
                            <? } ?>
                        </td>
                        <td><span class="error"><?= $oProductType->isPropValid("withSizes") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>  
                    <tr>
                        <td><label for="withColors">Met kleuren *</label> <div class="hasTooltip tooltip" title="Dit type product heeft meerdere kleuren per toegevoegd product (niet meer te wijzigen na toevoegen eerste product)">&nbsp;</div></td>
                        <td>
                            <? if (!$bHasProducts) { ?>
                                <input class="alignRadio {validate:{required:true}}" title="Maak gebruik van kleuren voor dit type product" type="radio" <?= $oProductType->withColors ? 'CHECKED' : '' ?> id="withColors_1" name="withColors" value="1" /> <label for="withColors_1">Ja</label>
                                <input class="alignRadio {validate:{required:true}}" title="Maak geen gebruik van kleuren voor dit type product" type="radio" <?= !$oProductType->withColors ? 'CHECKED' : '' ?> id="withColors_0" name="withColors" value="0" /> <label for="withColors_0">Nee</label>
                            <? } else { ?>
                                <?= $oProductType->withColors ? 'ja' : 'nee' ?> <div class="hasTooltip tooltip" title="Zolang er aan dit type product, producten hangen kan deze waarde niet gewijzigd worden">&nbsp;</div>
                            <? } ?>
                        </td>
                        <td><span class="error"><?= $oProductType->isPropValid("withColors") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td><label for="withGenders">Met geslacht *</label> <div class="hasTooltip tooltip" title="Dit type product heeft meerdere geslacht per toegevoegd product (niet meer te wijzigen na toevoegen eerste product)">&nbsp;</div></td>
                        <td>
                            <? if (!$bHasProducts) { ?>
                                <input class="alignRadio {validate:{required:true}}" title="Maak gebruik van geslacht voor dit type product" type="radio" <?= $oProductType->withGenders ? 'CHECKED' : '' ?> id="withGenders" name="withGenders" value="1" /> <label for="withGenders_1">Ja</label>
                                <input class="alignRadio {validate:{required:true}}" title="Maak geen gebruik van geslacht voor dit type product" type="radio" <?= !$oProductType->withGenders ? 'CHECKED' : '' ?> id="withGenders_0" name="withGenders" value="0" /> <label for="withGenders_0">Nee</label>
                            <? } else { ?>
                                <?= $oProductType->withGenders ? 'ja' : 'nee' ?> <div class="hasTooltip tooltip" title="Zolang er aan dit type product, producten hangen kan deze waarde niet gewijzigd worden">&nbsp;</div>
                            <? } ?>
                        </td>
                        <td><span class="error"><?= $oProductType->isPropValid("withGenders") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>  
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het produc typen overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>