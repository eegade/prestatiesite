<div class="cf" id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het producten overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
    <?
    if (!empty($oProductPrev) || !empty($oProductNext)) {
        echo '<div style="float: right; line-height: 1.5em;">';
        if ($oProductPrev)
            echo '<a href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductPrev->catalogProductId . '">&laquo; vorige</a> | ';
        echo '<b style="font-size: 1.5em;">' . $oProduct->catalogProductId . '</b>';
        if ($oProductNext)
            echo ' | <a href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductNext->catalogProductId . '">volgende &raquo;</a>';
        echo '</div>';
    }
    ?>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Product</legend>
                <table class="withForm">
                    <tr>
                        <td style="width: 120px;">Online *</td>
                        <td>
                            <input class="alignRadio {validate:{required:true}}" title="Zet het product online OF offline" type="radio" <?= $oProduct->online ? 'CHECKED' : '' ?> id="online_1" name="online" value="1" /> <label for="online_1">Ja</label>
                            <input class="alignRadio {validate:{required:true}}" title="Zet het product online OF offline" type="radio" <?= !$oProduct->online ? 'CHECKED' : '' ?> id="online_0" name="online" value="0" /> <label for="online_0">Nee</label>
                        </td>
                        <td><span class="error"><?= $oProduct->isPropValid("online") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <? if (Settings::get('showProductsOnHome')) { ?>
                        <tr>
                            <td>Op homepage *</td>
                            <td>
                                <input class="alignRadio {validate:{required:true}}" title="Zet het product zichtbaar OF onzichtbaar op de homepage" type="radio" <?= $oProduct->showOnHome ? 'CHECKED' : '' ?> id="showOnHome_1" name="showOnHome" value="1" /> <label for="showOnHome_1">Ja</label>
                                <input class="alignRadio {validate:{required:true}}" title="Zet het product zichtbaar OF onzichtbaar op de homepage" type="radio" <?= !$oProduct->showOnHome ? 'CHECKED' : '' ?> id="showOnHome_0" name="showOnHome" value="0" /> <label for="showOnHome_0">Nee</label>
                            </td>
                            <td><span class="error"><?= $oProduct->isPropValid("online") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>
                    <? } ?>
                    <tr>
                        <td class="withLabel"><label for="catalogProductTypeId">Producttype *</label></td>
                        <td>
                            <select id="catalogProductTypeId" <?= $oProduct->catalogProductId && $oProduct->catalogProductTypeId ? 'DISABLED' : '' ?> class="{validate:{required:true}} default" title="Kies een producttype" name="catalogProductTypeId">
                                <option value="">Maak een keuze</option>
                                <?
                                foreach (CatalogProductTypeManager::getAllProductTypes() as $oProductType) {
                                    echo '<option data-withgenders="' . $oProductType->withGenders . '" value="' . $oProductType->catalogProductTypeId . '"' . ($oProductType->catalogProductTypeId == $oProduct->catalogProductTypeId ? ' selected' : '') . '>' . _e($oProductType->title) . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td><span class="error"><?= $oProduct->isPropValid("catalogProductTypeId") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="catalogBrandId">Merk *</label></td>
                        <td>
                            <select id="catalogBrandId" class="{validate:{required:true}} default" title="Kies een merk" name="catalogBrandId">
                                <option value="">Maak een keuze</option>
                                <?
                                foreach (CatalogBrandManager::getAllBrands() as $oBrand) {
                                    echo '<option value="' . $oBrand->catalogBrandId . '"' . ($oBrand->catalogBrandId == $oProduct->catalogBrandId ? ' selected' : '') . '>' . _e($oBrand->name) . '</option>' . "\n";
                                }
                                ?>
                            </select>
                        </td>
                        <td><span class="error"><?= $oProduct->isPropValid("catalogBrandId") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>

                    <!-- Name -->
                    <tr>
                        <td class="withLabel"><label for="name">Naam *</label></td>
                        <td><input id="name" class="{validate:{required:true}} default" title="Vul de naam in" type="text" name="name" value="<?= _e($oProduct->name) ?>" /></td>
                        <td><span class="error"><?= $oProduct->isPropValid("name") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>

                    <!-- Gender -->
                        <tr id="catalogProductGender">
                            <td style="width: 120px;">Geslacht *</td>
                            <td>
                                <?
                                foreach (CatalogProductManager::getAllGenders() as $key => $sGender) {
                                    echo '<input class="alignRadio {validate:{required:true}}" title="Geslacht" type="radio"' . ($oProduct->gender == $sGender ? "CHECKED" : "") . ' id="gender_' . $key . '" name="gender" value="' . $sGender . '" />';
                                    echo ' <label for="gender_' . $key . '" style="margin-right: 5px;">' . CatalogProductManager::getLabelByGender($sGender) . '</label> ';
                                }
                                ?>
                            </td>
                            <td><span class="error"><?= $oProduct->isPropValid("online") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                        </tr>

                    <!-- MPN -->
                    <tr>
                        <td class="withLabel">MPN General <div class="hasTooltip tooltip" title="Manufacter product number">&nbsp;</div></td>
                        <td><input type="text" class="default" name="catalogProductMPN" size="50"  value="<?= $oProduct->catalogProductMPN ?>" /></td>
                    </tr>

                    <!-- Sale Price -->
                    <tr>
                        <td class="withLabel"><label for="salePriceWithTax">Prijs *</label></td>
                        <td colspan="2">
                            &euro; <input <?= intval(Settings::get('taxIncluded')) ? '' : 'disabled' ?> size="6" id="salePriceWithTax" name="salePrice"  class="priceFloatOnly {validate:{required:true}} " type="text" value="<?= number_format($oProduct->getSalePrice(true, null, null, false, false), 2, '.', '') ?>" />
                            <span>(incl. BTW)</span>
                        </td>
                    </tr>

                    <tr>
                        <td class="withLabel"><label for="salePrice">Prijs *</label></td>
                        <td>
                            &euro; <input <?= intval(Settings::get('taxIncluded')) ? 'disabled' : '' ?> size="6" id="salePriceWithoutTax" name="salePrice" class="priceFloatOnly {validate:{required:true}}" title="Vul de prijs in"  type="text" name="salePriceWithoutTax" value="<?= number_format($oProduct->getSalePrice(false, null, null, false, false), 2, '.', '') ?>" />                            
                            <span>(excl. BTW)</span>
                        </td>
                        <td><span class="error"><?= $oProduct->isPropValid("salePrice") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>

                    <!-- Reduced Price -->
                    <tr>
                        <td class="withLabel"><label for="reducedPriceWithTax">Actie prijs </label></td>
                        <td colspan="2">                           
                            &euro; <input <?= intval(Settings::get('taxIncluded')) ? '' : 'disabled' ?> size="6" id="reducedPriceWithTax" name="reducedPrice" class="priceFloatOnly" type="text" value="<?= ($oProduct->getReducedPrice(true) !== null) ? number_format($oProduct->getReducedPrice(true), 2, '.', '') : '' ?>" />                            
                            <span>(incl. BTW)</span>
                        </td>
                    </tr>

                    <tr>
                        <td class="withLabel"><label for="reducedPrice">Actie prijs </label></td>
                        <td>
                            &euro; <input <?= intval(Settings::get('taxIncluded')) ? 'disabled' : '' ?> size="6" id="reducedPriceWithoutTax"  name="reducedPrice" class="priceFloatOnly {validate:{min:0.001}}" title="Vul de prijs in"  type="text" name="formattedReducedPrice" value="<?= ($oProduct->getReducedPrice(false) !== null) ? number_format($oProduct->getReducedPrice(false), 2, '.', '') : '' ?>" />                            
                            <span>(excl. BTW)</span>
                        </td>
                        <td><span class="error"><?= $oProduct->isPropValid("reducedPrice") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>

                    <!-- Pruchase Price -->
                    <tr>
                        <td class="withLabel"><label for="purchasePriceWithTax">Inkoopprijs *</label></td>
                        <td colspan="2">
                            &euro; <input <?= intval(Settings::get('taxIncluded')) ? '' : 'disabled' ?> size="6" id="purchasePriceWithTax" name="purchasePrice"  class="priceFloatOnly {validate:{required:true}} " type="text" value="<?= number_format($oProduct->getPurchasePrice(true, null, null, false, false), 2, '.', '') ?>" />
                            <span>(incl. BTW)</span>
                        </td>
                    </tr>

                    <tr>
                        <td class="withLabel"><label for="purchasePrice">Inkoopprijs *</label></td>
                        <td>
                            &euro; <input <?= intval(Settings::get('taxIncluded')) ? 'disabled' : '' ?> size="6" id="purchasePriceWithoutTax" name="purchasePrice" class="priceFloatOnly {validate:{required:true}}" title="Vul de prijs in"  type="text" name="purchasePriceWithoutTax" value="<?= number_format($oProduct->getPurchasePrice(false, null, null, false, false), 2, '.', '') ?>" />                            
                            <span>(excl. BTW)</span>
                        </td>
                        <td><span class="error"><?= $oProduct->isPropValid("purchasePrice") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>

                    <!-- Tax percent -->
                    <tr>
                        <td class="withLabel"><label for="taxPercentageId">BTW percentage *</label></td>
                        <td>
                            <select id="taxPercentageId" class="{validate:{required:true}}" title="Kies een BTW percentage" name="taxPercentageId">
                                <?
                                foreach (TaxManager::getAllPercentageIds() as $iTaxPercentageId) {
                                    echo '<option value="' . $iTaxPercentageId . '"' . ($iTaxPercentageId == $oProduct->taxPercentageId ? ' selected' : '') . '>' . TaxManager::getPercentageById($iTaxPercentageId, true) . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td><span class="error"><?= $oProduct->isPropValid("reducedPrice") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>

                    <!-- Google Category -->
                    <tr>
                        <td class="withLabel"><label for="googleCategory">Google categorie <?= Settings::get("withGoogleFeed") ? '*' : '' ?></label></td>
                        <td>
                            <input class="default <?= Settings::get("withGoogleFeed") ? 'required' : '' ?>" type="text" id="googleCategory" name="googleCategory" title="<?= !empty($oProduct->googleCategory) ? $oProduct->googleCategory : 'Kies een Google categories' ?>"  value="<?= !empty($oProduct->googleCategory) ? $oProduct->googleCategory : '' ?>"/>
                        </td>
                    </tr>

                    <!-- Description -->
                    <tr>
                        <td colspan="3"><label for="description">Omschrijving</label></td>
                    </tr>
                    <tr>
                        <td colspan="3"><textarea name="description" id="description" class="tiny_MCE_default tiny_MCE"><?= $oProduct->description ?></textarea></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <b>Categorie&euml;n *</b> <span class="error"><?= $oProduct->isPropValid("catalogProductCategory") ? '' : 'Veld niet (juist) ingevuld' ?></span>
                            <div class="autoCheckParent">
                                <?

                                function makeCategoryListTree(array $aProductCategories = array(), array $aProductCategoryIds = array()) {
                                    echo '<ul class="nestedCheckboxes">';
                                    foreach ($aProductCategories AS $oProductCategory) {
                                        echo '<li><input id="productCategory_' . $oProductCategory->catalogProductCategoryId . '"' . ($oProductCategory->level == 1 ? ' class="{validate:{required:true}}"' : '') . ' title="Selecteer minstens 1 categorie"  type="checkbox" name="catalogProductCategoryIds[]" value="' . $oProductCategory->catalogProductCategoryId . '" ' . (in_array($oProductCategory->catalogProductCategoryId, $aProductCategoryIds) ? 'CHECKED' : '') . ' /> <label for="productCategory_' . $oProductCategory->catalogProductCategoryId . '">' . _e($oProductCategory->name) . '</label>';
                                        makeCategoryListTree($oProductCategory->getSubCategories('all'), $aProductCategoryIds); //call function recursive
                                        echo '</li>';
                                    }
                                    echo '</ul>';
                                }

                                # set current categories in an array to check whether to select/check the options
                                $aProductCategoryIds = array();
                                foreach ($oProduct->getCategories('all') AS $oProductCategory) {
                                    $aProductCategoryIds[] = $oProductCategory->catalogProductCategoryId;
                                }

                                makeCategoryListTree(CatalogProductCategoryManager::getProductCategoriesByFilter(array('showAll' => true, 'level' => 1)), $aProductCategoryIds);
                                ?>
                            </div>
                        </td>
                    </tr>
<? if ($oCurrentUser->isSEO()) { ?>
                        <tr>
                            <td colspan="3" style="padding-top: 10px;"><h2>SEO</h2></td>
                        </tr>
    <? if ($oProduct->catalogProductId) { ?>
                            <tr>
                                <td>Huidige url</td>
                                <td><?= $oProduct->getUrlPath() ?></td>
                            </tr>
    <? } ?>
                        <tr>
                            <td class="withLabel"><label for="windowTitle">Window titel</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - De titel moet een goed leesbare zin zijn, met het belangrijkste zoekwoord van deze pagina zoveel mogelijk vooraan.<br /> - De titel moet tussen de 8 en 69 karakters zijn (incl. spaties).<br /> - Gebruik niet enkel de bedrijfsnaam in de titel.<br /> - Gebruik geen domeinnaam.">&nbsp;</div></td>
                            <td colspan="2">
                                <div class="inline-block">
                                    <input class="default charCounterWindowTitle" id="windowTitle" type="text" name="windowTitle" value="<?= _e($oProduct->windowTitle) ?>" />
                                    <div id="windowTitleCounter" style="text-align: right;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="metaDescription">Description</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - Omschrijving die Google op kan nemen in de zoekresultaten.<br /> - Deze zin moet aantrekkelijk en wervend genoeg zijn om zoekers te stimuleren om op het resultaat te klikken.<br /> - Tussen de 46 en 156 karakters (incl. spaties).">&nbsp;</div></td>
                            <td colspan="2">
                                <div class="inline-block">
                                    <textarea class="charCounterMetaDescription default" id="metaDescription" name="metaDescription"><?= _e($oProduct->metaDescription) ?></textarea>
                                    <div id="metaDescriptionCounter" style="text-align: right;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="withLabel"><label for="metaKeywords">Keywords</label> <div class="hasTooltip tooltip" title="Plaats hier voor deze pagina, een aantal relevante woorden (maximaal 10 woorden).<br />Scheidt de woorden van elkaar met een komma en een spatie.<br />- zoekmachines gebruiken keywords vooral om misbruik op te sporen<br />- goed gebruik van keywords zal geen of een hele kleine bijdrage leveren aan de vindbaarheid van deze pagina">&nbsp;</div></td>
                            <td colspan="2"><input class="default" id="metaKeywords" type="text" name="metaKeywords" value="<?= _e($oProduct->metaKeywords) ?>" /> (optioneel)</td>
                        </tr>
                        <tr>
                            <td class="withLabel"><label for="urlPart">Url tekst</label> <div class="hasTooltip tooltip" title="Kies zelf de tekst op basis waarvan de url moet worden gegenereerd<br />Bijvoorbeeld: <?= CLIENT_HTTP_URL ?>/producten/1/<b>[url tekst]</b><br />- alleen letters, cijfers en een `-` is toegestaan<br />- alle tekens anders dan aangegeven worden vervangen door een `-`<br />- vb: `cms systeem` wordt `cms-systeem`">&nbsp;</div></td>
                            <td><input  class="default" id="urlPart" type="text" name="urlPart" value="<?= _e($oProduct->getUrlPart()) ?>" /></td>
                            <td></td>
                        </tr>
<? } ?>
                    <tr>
                        <td colspan="3">
                            <input id="saveProductButton" type="button" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>

    <!-- Images -->
    <div class="contentColumn">
        <fieldset>
            <legend>Afbeeldingen</legend>
            <?
            if ($oProduct->catalogProductId !== null) {
                $oImageManagerHTML->includeTemplate();
            } else {
                echo '<p><i>Afbeeldingen kunnen worden geüpload nadat het product eerst is opgeslagen</i></p>';
            }
            ?>
        </fieldset>
    </div>

    <!-- Proguct-Images-Color managament -->
    <? $aCatalogProductColors = CatalogProductColorManager::getProductColorsByFilter() ?>
<? if ($oProduct->catalogProductId !== null && $oProduct->getProductType()->withColors && !empty($aCatalogProductColors)) { ?>
    <? if ($oProduct->getProductType()->withColors) { ?>
            <div class="contentColumn">
                <fieldset>
                    <legend>Afbeelding/kleur koppeling <div class="hasTooltip tooltip" title="Koppel hier een kleur aan een specifieke afbeelding.<br />Dit is nodig voor de google product feed om aan te geven welke afbeelding bij welke kleur hoort">&nbsp;</div></legend>

                    <form name="productImageColorManagement" method="POST" class="validateForm">
                        <input type="hidden" name="action" value="addProductImageRelation" />
                        <table style="margin-top: 5px;" class="withForm">
                            <tr>
                                <td class="withLabel">Kleur <div class="hasTooltip tooltip" title="Kies hier de kleur waarvoor u wilt toevoegen">&nbsp;</div></td>
                                <td>
                                    <select name="catalogProductColorId" class="catalogProductColor">
                                        <?
                                        foreach ($aCatalogProductColors AS $key => $oCatalogProductColor) {
                                            if ($key == 0) {
                                                $colorId = $oCatalogProductColor->catalogProductColorId;
                                            }
                                            echo '<option value="' . $oCatalogProductColor->catalogProductColorId . '">' . $oCatalogProductColor->name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="fancyBoxLink fancybox.ajax" href=<?= ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken-afbeelding-kleur-relatie?catalogProductId=' . $oProduct->catalogProductId . '&colorId=' . $colorId ?>>Selecteer een afbeelding</a>  
                                </td>
                            </tr>
                            <tr id="catalogProductImageColorImage" class="hide">
                                <td>
                                    <div class='images'>
                                        <div class='placeholder'>
                                            <div class='imagePlaceholder'>
                                                <div class='centered'>
                                                    <img />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="catalogProductImageColorImageId" name="imageId" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><input type="submit" name="addImageColorBtn" value="Afbeeldingen en kleur koppelen" /></td>
                            </tr>
                        </table>

                    </form>

                    <hr>
                    <table class="sorted" style="margin-top: 5px; margin-bottom: 10px; width: 100%;">
                        <thead>
                            <tr class="topRow">
                                <td colspan="6"><h2>Gekoppelde afbeeldingen en kleuren<div style="float: right;"></div></h2></td>
                            </tr>
                            <tr>
                                <th>Kleur</th>
                                <th>Afbeeldingen</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?
                            $aFilter = array();
                            $aFilter['notNullColors'] = '1';
                            $aFilter['catalogProductId'] = $oProduct->catalogProductId;
                            $aImageProductRelationsGroup = array();

                            // I get all the Product-Image relations and I group them by color in an Array
                            foreach (CatalogProductImageRelationManager::getCatalogProductImageRelationsByFilter($aFilter) AS $key => $oImageProductRelation) {
                                $oImage = ImageManager::getImageById($oImageProductRelation->imageId);
                                $oImageFile = $oImage->getImageFileByReference('detail');

                                $aImageProductRelationsGroup[$oImageProductRelation->catalogProductColorId][$key] = $oImageFile;
                            }

                            // I show all the colors and images
                            if (count($aImageProductRelationsGroup)) {
                                foreach ($aImageProductRelationsGroup as $catalogProductColorId => $oImageProductRelationGroup) {
                                    ?>
                                    <tr>
                                        <td> <?= CatalogProductColorManager::getProductColorById($catalogProductColorId)->name ?></td>
                                        <td>
                <? foreach ($oImageProductRelationGroup as $oImageProductRelation) { ?>
                                                <div class='images-50'>
                                                    <div class='placeholder-50'>
                                                        <div class='imagePlaceholder-50'>
                                                            <div class='centered-50'>
                                                                <img src="<?= CatalogProduct::IMAGES_PATH . '/cms_thumb/' . $oImageProductRelation->name ?>" title="<?= $oImageProductRelation->title ?>" />
                                                            </div>
                                                        </div>
                                                        <div class='actionsPlaceholder'>
                                                            <a onclick="return confirmChoice('deze koppeling');" class="action_icon delete_icon" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/verwijder-afbeeldingen-kleur-relatie?catalogProductId=' . $oProduct->catalogProductId . '&catalogProductImageId=' . $oImageProductRelation->imageId ?>"></a>    
                                                        </div>
                                                    </div>
                                                </div>
                                    <? } ?>
                                        </td>
                                    </tr>
                                <? } ?> 
                                <?
                            } else {
                                echo '<tr><td colspan="5"><i>Er zijn nog geen afbeeldingen toegevoegd. </i></td></tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                </fieldset>
            </div>
    <? } ?>
<? } else { ?>
        <div class="contentColumn">
            <fieldset>
                <legend>Afbeelding/kleur koppeling <div class="hasTooltip tooltip" title="Koppel hier een kleur aan een specifieke afbeelding.<br />Dit is nodig voor de google product feed om aan te geven welke afbeelding bij welke kleur hoort">&nbsp;</div></legend>
                <p><i>Afbeeldingen en kleuren koppelen kan nadat het product eerst is opgeslagen</i></p>
            </fieldset>
        </div>
<? } ?>


    <!-- Stock managament -->
    <div class="contentColumn">
        <fieldset>
            <legend>Voorraad beheer <div class="hasTooltip tooltip" title="Beheer hier de voorraden van dit product">&nbsp;</div></legend>
<? if ($oProduct->catalogProductId !== null) { ?>
                        <? if (!(!$oProduct->getProductType()->withSizes && !$oProduct->getProductType()->withColors && count($oProduct->getColorSizeRelations('all')) > 0)) { ?>
                    <form method="POST" class="validateForm">
                        <input type="hidden" name="action" value="addSizeColorRelation" />
                        <table style="margin-top: 5px;" class="withForm">
        <? if ($oProduct->getProductType()->withSizes) { ?>
                                <tr>
                                    <td class="withLabel" style="width: 90px;">Maat <div class="hasTooltip tooltip" title="Kies hier de maat waarvoor u voorraad wilt toevoegen">&nbsp;</div></td>
                                    <td>
                                        <select name="catalogProductSizeId">
                                            <?
                                            foreach (CatalogProductSizeManager::getProductSizesByFilter() AS $oCatalogProductSize) {
                                                echo '<option value="' . $oCatalogProductSize->catalogProductSizeId . '">' . $oCatalogProductSize->name . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
        <? } ?>
        <? if ($oProduct->getProductType()->withColors) { ?>
                                <tr>
                                    <td class="withLabel">Kleur <div class="hasTooltip tooltip" title="Kies hier de kleur waaraan u een afbeelding wilt koppelen">&nbsp;</div></td>
                                    <td>
                                        <select name="catalogProductColorId">
                                            <?
                                            foreach (CatalogProductColorManager::getProductColorsByFilter() AS $oCatalogProductColor) {
                                                echo '<option value="' . $oCatalogProductColor->catalogProductColorId . '">' . $oCatalogProductColor->name . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
        <? } ?>
                            <tr>
                                <td class="withLabel">Voorraad <div class="hasTooltip tooltip" title="Vul hier het aantal producten dat voorradig is in, als de voorraad onbeperkt is, laat dit veld dan leeg en druk op `voorraad toevoegen`">&nbsp;</div></td>
                                <td><input class="numbersOnly" type="text" name="stock" size="3" value="" /></td>
                            </tr>
        <? if ($oProduct->getProductType()->withSizes || $oProduct->getProductType()->withColors) { ?>
                                <tr>
                                    <td class="withLabel">Meerprijs (incl. BTW)<div class="hasTooltip tooltip" title="Vul hier het bedrag in dat het product meer kost door een bepaalde maat en/of kleur (combinatie). Als er geen meerprijs is, vul 0.00 in">&nbsp;</div></td>
                                    <td><input class="priceFloatOnly" type="text" name="extraPrice" size="3" value="0.00" /></td>
                                </tr>
        <? } ?>
        <? if (($oProduct->getProductType()->withColors) || ($oProduct->getProductType()->withSizes)) { ?>
                                <tr>
                                    <td class="withLabel">MPN <div class="hasTooltip tooltip" title="Manufacter product number">&nbsp;</div></td>
                                    <td><input type="text" name="catalogProductSizeColorMPN" size="50"   /></td>
                                </tr>
        <? } ?>
                            <tr>
                                <td colspan="2"><input type="submit" name="addStockBtn" value="Voorraad toevoegen" /></td>
                            </tr>
                        </table>
                    </form>
                    <hr />
    <? } ?>
                <table class="sorted" style="margin-top: 5px; margin-bottom: 10px; width: 100%;">
                    <thead>
                        <tr class="topRow">
                            <td colspan="6"><h2>Toegevoegde voorraad<div style="float: right;"></div></h2></td>
                        </tr>
                        <tr>
                            <? if ($oProduct->getProductType()->withSizes) { ?><th>Maat</th><? } ?>
                            <? if ($oProduct->getProductType()->withColors) { ?><th>Kleur</th><? } ?>
                            <th>Voorraad</th>
    <? if ($oProduct->getProductType()->withSizes || $oProduct->getProductType()->withColors) { ?><th>Meerprijs (incl. BTW)</th><? } ?>
    <? if ($oProduct->getProductType()->withSizes || $oProduct->getProductType()->withColors) { ?><th>MPN</th><? } ?>
                            <th class="{sorter: false} nonSorted" style="width: 60px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        foreach ($oProduct->getColorSizeRelations('all') AS $oColorSizeRelation) {
                            echo '<tr>';
                            if ($oProduct->getProductType()->withSizes)
                                echo '<td>' . ($oColorSizeRelation->catalogProductSizeId ? $oColorSizeRelation->getSize()->name : '' ) . '</td>';
                            if ($oProduct->getProductType()->withColors)
                                echo '<td>' . ($oColorSizeRelation->catalogProductColorId ? $oColorSizeRelation->getColor()->name : '' ) . '</td>';
                            echo '<td>' . ($oColorSizeRelation->stock === null ? 'onbeperkt' : $oColorSizeRelation->stock) . '</td>';
                            if ($oProduct->getProductType()->withSizes || $oProduct->getProductType()->withColors)
                                echo '<td>' . $oColorSizeRelation->extraPrice . '</td>';
                            if ($oProduct->getProductType()->withSizes || $oProduct->getProductType()->withColors)
                                echo '<td>' . $oColorSizeRelation->catalogProductSizeColorMPN . '</td>';
                            echo '<td><a class="action_icon edit_icon fancyBoxLink fancybox.ajax" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken-maat-kleur-relatie?catalogProductId=' . $oColorSizeRelation->catalogProductId . '&catalogProductSizeId=' . $oColorSizeRelation->catalogProductSizeId . '&catalogProductColorId=' . $oColorSizeRelation->catalogProductColorId . '"></a><a onclick="return confirmChoice(\'deze regel\');" class="action_icon delete_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijder-maat-kleur-relatie?catalogProductId=' . $oColorSizeRelation->catalogProductId . '&catalogProductSizeId=' . $oColorSizeRelation->catalogProductSizeId . '&catalogProductColorId=' . $oColorSizeRelation->catalogProductColorId . '"></a></td>';
                            echo '</tr>';
                        }
                        if (count($oProduct->getColorSizeRelations('all')) == 0) {
                            echo '<tr><td colspan="5"><i>Er zijn nog geen voorraden toegevoegd. Dit product wordt dan ook <u>niet</u> getoond in de webshop</i></td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
                <?
            } else {
                echo '<p><i>Voorraad kan beheerd worden nadat het product eerst is opgeslagen</i></p>';
            }
            ?>
        </fieldset>
    </div>

    <div class="contentColumn">
        <form method="POST">
            <input type="hidden" name="action" value="saveProductPropertyValues" />
            <fieldset>
                <legend>Type-specifieke eigenschappen</legend>
                <?
                if ($oProduct->catalogProductId === null || $oProduct->catalogProductTypeId === null) {
                    echo '<p><i>Type-specifieke eigenschappen kunnen worden toegevoegd nadat het product eerst is opgeslagen</i></p>';
                } else {
                    if (count($oProduct->getPropertyTypes()) > 0) {
                        # create the HTML for each CatalogProductPropertyTypeGroup related to this CatalogProduct
                        foreach ($oProduct->getPropertyTypeGroups() as $oPropertyTypeGroup) {
                            if (count($oPropertyTypeGroup->getPropertyTypes()) > 0) {
                                ?>
                                <h3><?= $oPropertyTypeGroup->title ?></h3>
                                <table class="withForm " style="margin-bottom: 10px;">
                                    <?
                                    # create HTML for each related CatalogProductPropertyType
                                    foreach ($oPropertyTypeGroup->getPropertyTypes() AS $oPropertyType) {
                                        # define the values to check for
                                        $aPropertyTypeValues = array();
                                        if (count($oPropertyType->getValuesByProductId($oProduct->catalogProductId)) > 0) {
                                            foreach ($oPropertyType->getValuesByProductId($oProduct->catalogProductId) AS $oPropertyValue) {
                                                $aPropertyTypeValues[] = $oPropertyValue->value;
                                            }
                                        }

                                        # create the HTML for a single text-input
                                        if ($oPropertyType->type == 'text') {
                                            ?>
                                            <tr>
                                                <td style="width: 140px;" class="withLabel"><label for="propertyType-<?= $oPropertyType->catalogProductPropertyTypeId ?>"><?= $oPropertyType->title ?></label></td>
                                                <td colspan="2"><input class="default" id="propertyType-<?= $oPropertyType->catalogProductPropertyTypeId ?>" type="text" name="propertyTypes[<?= $oPropertyType->catalogProductPropertyTypeId ?>][]" value="<?= (empty($aPropertyTypeValues[0]) ? '' : _e($aPropertyTypeValues[0])) ?>" /></td>
                                            </tr>
                                            <?
                                        }

                                        # create the HTML for checkboxes
                                        elseif ($oPropertyType->type == 'checkbox' && count($oPropertyType->getPossibleValues()) > 0) {
                                            ?>
                                            <tr>
                                                <td style="width: 140px;"><?= $oPropertyType->title ?></td>
                                                <td colspan="3">
                                                    <?
                                                    foreach ($oPropertyType->getPossibleValues() AS $i => $oPossibleValue) {
                                                        ?>
                                                        <input class="alignCheckbox" id="propertyType-<?= $oPropertyType->catalogProductPropertyTypeId . '-' . $i ?>" type="checkbox" name="propertyTypes[<?= $oPropertyType->catalogProductPropertyTypeId ?>][]" value="<?= $oPossibleValue->value ?>"<?= (in_array($oPossibleValue->value, $aPropertyTypeValues) ? ' checked' : '') ?> /> <label for="propertyType-<?= $oPropertyType->catalogProductPropertyTypeId . '-' . $i ?>"><?= _e($oPossibleValue->value) ?></label><br />
                            <?
                        }
                        ?>
                                                </td>
                                            </tr>
                                            <?
                                        }

                                        # create the HTML for a select field
                                        elseif ($oPropertyType->type == 'select' && count($oPropertyType->getPossibleValues()) > 0) {
                                            ?>
                                            <tr>
                                                <td style="width: 140px;" class="withLabel"><label for="propertyType-<?= $oPropertyType->catalogProductPropertyTypeId ?>"><?= _e($oPropertyType->title) ?></label></td>
                                                <td colspan="2">
                                                    <select class="default" id="propertyType-<?= $oPropertyType->catalogProductPropertyTypeId ?>" name="propertyTypes[<?= $oPropertyType->catalogProductPropertyTypeId ?>][]">
                                                        <option value="">Maak een keuze</option>
                                                        <?
                                                        foreach ($oPropertyType->getPossibleValues() as $oPossibleValue) {
                                                            echo '<option value="' . $oPossibleValue->value . '"' . (in_array($oPossibleValue->value, $aPropertyTypeValues) ? ' selected' : '') . '>' . _e($oPossibleValue->value) . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <?
                                        }
                                    }
                                    ?>
                                </table>
                                <?
                            }
                        }
                        ?>
                        <input type="submit" value="Opslaan" name="save" style="margin-bottom: 5px;" />
                        <?
                    } else {
                        echo '<p><i>Er zijn geen type-specifieke eigenschappen voor dit product</i></p>';
                    }
                }
                ?>
            </fieldset>
        </form>
    </div>

    <!-- Related Products -->
    <div class="contentColumn">
        <form method="POST">
            <input type="hidden" name="action" value="relatedProducts" />
            <fieldset>
                <legend>Gerelateerde producten</legend>
                <?
                if ($oProduct->catalogProductId === null || $oProduct->catalogProductTypeId === null) {
                    echo '<p><i>Gerelateerde producten kunnen worden toegevoegd omdat het product eerst is opgeslagen</i></p>';
                } else {
                    ?>
                    <table class="withForm " style="margin-bottom: 10px;">
                        <tr>
                            <td class="withLabel"><label for="relatedProduct">Gerelateerde product</label></td>
                            <td>
                                <input class="default" type="text" id="relatedProduct" name="relatedProduct" title="Gerelateerde product"  />
                                <input type="hidden" id="relatedProductId" name="relatedProductId" />
                            </td>
                        </tr>
                    </table>
                    <hr>
                    <div  id="relatedProducts">
                        <?
                        $aRelatedProducts = $oProduct->getRelatedProducts();
                        include(ADMIN_TEMPLATES_FOLDER . '/elements/catalogProduct/catalogProductRelated.inc.php');
                    }
                    ?>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het producten overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
$sAdminJsFolder = ADMIN_JS_FOLDER;
$sBottomJavascript = <<<EOT
<script src="{$sAdminJsFolder}/autoNumeric-1.7.5.js"></script>
<script>
    $('input.priceFloatOnly').autoNumeric({
        aSep: '',
        altDec: ','
    });

    $('input.numbersOnly').autoNumeric({
        aSep: '',
        mDec: '0'
    });

    //-------------------------------------------------------------------
    // update the salePriceWithTax if the salePriceWithoutTax is updated
    //-------------------------------------------------------------------
    function onChangeSalePriceWithoutTax() {
        $.ajax({
            type: "GET",
            url: '?ajax=addTax',
            data: 'salePrice=' + $('#salePriceWithoutTax').val() + '&taxPercentageId=' + $('select#taxPercentageId').val(),
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $('input#salePriceWithoutTax').val(dataObj.formattedSalePriceWithoutTax);
                    $('input#salePriceWithTax').val(dataObj.formattedSalePriceWithTax);
                } else{
                    $('input#salePriceWithTax').val('');
                    $('input#salePriceWithoutTax').val('');
                }
            }
        });
    }
    $('input#salePriceWithoutTax:enabled').change(onChangeSalePriceWithoutTax);

    //-------------------------------------------------------------------
    // update the salePriceWithoutTax if the salePriceWithTax is updated
    //-------------------------------------------------------------------
    function onChangeSalePriceWithTax() {
        $.ajax({
            type: "GET",
            url: '?ajax=subtractTax',
            data: 'salePrice=' + $('#salePriceWithTax').val() + '&taxPercentageId=' + $('select#taxPercentageId').val(),
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $('input#salePriceWithoutTax').val(dataObj.formattedSalePriceWithoutTax);
                    $('input#salePriceWithTax').val(dataObj.formattedSalePriceWithTax);
                } else{
                    $('input#salePriceWithTax').val('');
                    $('input#salePriceWithoutTax').val('');
                }
            }
        });
    }
    $('input#salePriceWithTax:enabled').change(onChangeSalePriceWithTax);

    //---------------------------------------------------------------------------
    // update the purchasePriceWithTax if the purchasePriceWithoutTax is updated
    //---------------------------------------------------------------------------
    function onChangePurchasePriceWithoutTax() {
        $.ajax({
            type: "GET",
            url: '?ajax=addTax',
            data: 'salePrice=' + $('#purchasePriceWithoutTax').val() + '&taxPercentageId=' + $('select#taxPercentageId').val(),
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $('input#purchasePriceWithoutTax').val(dataObj.formattedSalePriceWithoutTax);
                    $('input#purchasePriceWithTax').val(dataObj.formattedSalePriceWithTax);
                } else{
                    $('input#purchasePriceWithTax').val('');
                    $('input#purchasePriceWithoutTax').val('');
                }
            }
        });
    }
    $('input#purchasePriceWithoutTax:enabled').change(onChangePurchasePriceWithoutTax);

    //---------------------------------------------------------------------------
    // update the purchasePriceWithoutTax if the purchasePriceWithTax is updated
    //---------------------------------------------------------------------------
    function onChangePurchasePriceWithTax() {
        $.ajax({
            type: "GET",
            url: '?ajax=subtractTax',
            data: 'salePrice=' + $('#purchasePriceWithTax').val() + '&taxPercentageId=' + $('select#taxPercentageId').val(),
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $('input#purchasePriceWithoutTax').val(dataObj.formattedSalePriceWithoutTax);
                    $('input#purchasePriceWithTax').val(dataObj.formattedSalePriceWithTax);
                } else{
                    $('input#purchasePriceWithTax').val('');
                    $('input#purchasePriceWithoutTax').val('');
                }
            }
        });
    }
    $('input#purchasePriceWithTax:enabled').change(onChangePurchasePriceWithTax);

    //---------------------------------------------------------------------------
    // update the reducedPriceWithTax if the reducedPriceWithoutTax is updated
    //----------------------------------------------------------------------------
    function onChangeReducedPriceWithoutTax() {
        $.ajax({
            type: "GET",
            url: '?ajax=addTax',
            data: 'salePrice=' + $('#reducedPriceWithoutTax').val() + '&taxPercentageId=' + $('select#taxPercentageId').val(),
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $('input#reducedPriceWithoutTax').val(dataObj.formattedSalePriceWithoutTax);
                    $('input#reducedPriceWithTax').val(dataObj.formattedSalePriceWithTax);
                } else{
                    $('input#reducedPriceWithTax').val('');
                    $('input#reducedPriceWithoutTax').val('');
                }
            }
        });
    }
    $('input#reducedPriceWithoutTax:enabled').change(onChangeReducedPriceWithoutTax);

    //--------------------------------------------------------------------------
    // update the reducedPriceWithoutTax if the reducedPriceWithTax is updated
    //--------------------------------------------------------------------------
    function onChangeReducedPriceWithTax() {
        $.ajax({
            type: "GET",
            url: '?ajax=subtractTax',
            data: 'salePrice=' + $('#reducedPriceWithTax').val() + '&taxPercentageId=' + $('select#taxPercentageId').val(),
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $('input#reducedPriceWithoutTax').val(dataObj.formattedSalePriceWithoutTax);
                    $('input#reducedPriceWithTax').val(dataObj.formattedSalePriceWithTax);
                } else{
                    $('input#reducedPriceWithTax').val('');
                    $('input#reducedPriceWithoutTax').val('');
                }
            }
        });
    }
    $('input#reducedPriceWithTax:enabled').change(onChangeReducedPriceWithTax);

    //-----------------------------------------------------------
    // update prices info if the tax percentage has been changed
    //-----------------------------------------------------------
    $('select#taxPercentageId').change(function() {
        if ($('input#salePriceWithoutTax').prop('disabled')) {
            onChangeSalePriceWithTax();
            onChangeReducedPriceWithTax();
            onChangePurchasePriceWithTax();
        }
        else {
            onChangeSalePriceWithoutTax();
            onChangeReducedPriceWithoutTax();
            onChangePurchasePriceWithoutTax();
        }
    });

    //-------------------------------------
    // auto check and uncheck parent/child
    //-------------------------------------
    $('.autoCheckParent input[type="checkbox"]').click(function(){
        var element = this;
        if($(element).attr('checked')){
            while($(element).closest('ul').parent().find('> input[type="checkbox"]').size() == 1){
                element = $(element).closest('ul').parent().find('> input[type="checkbox"]');
                $(element).attr('checked', true);
            }
        }else{
            $(element).closest('li').find('input[type="checkbox"]').attr('checked', false);
        }
   });

    //--------------------------------
    // Submit form
    //--------------------------------
    $('input#saveProductButton').click(function() {
        var thisForm = $(this).closest('form');
        thisForm.submit();
    });

$( document ).ready(function() {
    //--------------------------------
    // Google Categories    
    //--------------------------------
    var googleCategories = [];
    $.ajax({
        type: "POST",
        url: '?ajax=googleCategories',
        dataType: "json",
        success: function( data ) {
            googleCategories = data;
            $('#googleCategory').autocomplete({
                source: data
            });
        
            // When I get the Categories, I add a rule to check if the category is correct
            $.validator.addMethod("googleCategory",
            function(value,element,params){
                return this.optional(element) || params.indexOf(value) > 0;
            });
        
            $("#googleCategory").rules("add",{
                    googleCategory: googleCategories,
                    messages: {
                        googleCategory: "Dit is geen geldige Google Categorie"
                    }
            });

            $("#googleCategory").blur(function(){
                $(this).attr('title',$(this).val());
            });
        }
    });

    //-------------------------------- 
    // Gender     
    //-------------------------------- 
    function setWithGenders(){
            var withGenders = $("#catalogProductTypeId option:selected").data("withgenders");
            if (withGenders) {
                $('#catalogProductGender').show();
            }else{
                $('#catalogProductGender').hide();
            }
    }
    
    setWithGenders();
    $('#catalogProductTypeId').change(function(){  
       setWithGenders();
    });
    
    //--------------------------------   
    // Product-color-image href change
    //-------------------------------- 
    $('form[name="productImageColorManagement"] select.catalogProductColor').change(function(o){
      $('form[name="productImageColorManagement"] .fancyBoxLink').attr('href', function(i,a){
        return a.replace( /(colorId=)[0-9]+/ig, "colorId="+o.target.value);
      });
    });
});

//-------------------------------- 
// Related Products Autocomplete   
//-------------------------------- 
function relatedProductAutocomplete(){
    if(!$('#relatedProduct').size())
        return false;
    $.ajax({
        type: "POST",
        url: '?ajax=related-products-autoComplete',
        dataType: "json",
        success: function( data ) {  
                $( "#relatedProduct" ).autocomplete({
                        minLength: 2,
                        source: data,
                        focus: function( event, ui ) {
                                $( "#relatedProduct" ).val( ui.item.value );
                                $( "#relatedProductId" ).val( ui.item.relatedProductid );
                                return false;
                        },
                        select: function( event, ui ) {
                                $( "#relatedProduct" ).val( ui.item.value);
                                $( "#relatedProductId" ).val( ui.item.relatedProductid);
                                saveRelatedProduct();
                                return false;
                        }
                })
                .data( "autocomplete" )._renderItem = function( ul, item ) {
                        return $( "<li></li>" )
                                .data( "item.autocomplete", item )
                                .append( "<a><table><tr><td style='width:65px'><img src='" + item.link + "' width='50px' /></td><td style='vertical-align:middle'>" + item.value + "</td></tr></table></a>" )      
                                .appendTo( ul );
                };
        }
    });     
}
relatedProductAutocomplete();

//-------------------------------- 
// Related Products List   
//-------------------------------- 
function getRelatedProducts(){
    $.ajax({
        type: "POST",
        async: false,
        url: '?ajax=relatedProducts&catalogProductId=' + '$oProduct->catalogProductId',
        dataType: "json",
        success: function( data ) {
            $('#relatedProducts').html(data.html) ;
            relatedProductAutocomplete();
        }
    });
}

//--------------------------------   
// Delete related product
//-------------------------------- 
function deleteRelatedProduct(relatedProductId){
    if(confirmChoice('deze koppeling')){
        $.ajax({
            type: "POST",
            url: '?ajax=related-product-delete&relatedProductId=' + relatedProductId,
            dataType: "json",
            success: function( data ) {
                getRelatedProducts();
                relatedProductAutocomplete();
                showStatusUpdate('Het product werd succesvol verwijderd');
            }
        });
    }
}

//--------------------------------   
// Save related product
//-------------------------------- 
function saveRelatedProduct(){
    $.ajax({
        type: "POST",
        url: '?ajax=related-product-save&relatedProductId=' + $( "#relatedProductId" ).val(),
        dataType: "json",
        success: function( data ) {
            getRelatedProducts();
            showStatusUpdate('Het product werd toegevoegd');
        }
    });
}

</script>

EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);

$oPageLayout->addJavascriptBottom('
<script>
    //--------------------------------   
    // Add Tiny Editor
    //-------------------------------- 
    initTinyMCE(".tiny_MCE_default", "/admin/paginas/link-list", "/admin/catalogus/image-list/' . $oProduct->catalogProductId . '");
</script>
');
?>