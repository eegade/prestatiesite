<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<h1>Product categorie structuur wijzigen</h1>
<p>
    <i>Sleep de categorie titels om de structuur te veranderen</i>
</p>
<div id="nestedSortableContainer">
    <?
    # list categories in unordered list
    function makeListTree(array $aProductCategories = array(), $iLevel, $iMaxLevels) {
        if (count($aProductCategories) > 0 && $iLevel == 0)
            echo '<ol class="nestedSortable cursorMove level' . $iLevel . '">';
        elseif (count($aProductCategories) > 0)
            echo '<ol class="level' . $iLevel . '">';

        $iT = 0;
        foreach ($aProductCategories AS $oProductCategory) {
            $sLiClass = '';
            $sLocked = '';

            # main categories are locked by default
            if ($iLevel == 0) {
                //$sLiClass = 'locked';
                //$sLocked = ' (Vergrendeld)';
            }

            // lock parent if set in database
            $sPreventLevelChanging = '';
            if ($oProductCategory->lockParent()) {
                $oParent = $oProductCategory->getParent();
                if ($oParent) {
                    $sParentProductCategoryId = $oParent->catalogProductCategoryId;
                } else {
                    $sParentProductCategoryId = 'root';
                }
                $sPreventLevelChanging = 'data-parent="' . $sParentProductCategoryId . '"'; //prevent root from nesting
            }


            echo '<li class="' . $sLiClass . '" id="productCategory_' . $oProductCategory->catalogProductCategoryId . '" ' . $sPreventLevelChanging . '>';
            echo '<div class="no-action-icons ' . ($iT % 2 == 0 ? 'even' : 'odd') . '">';
            echo _e($oProductCategory->name) . '<span class="brackedComment"> ' . $sLocked . '</span>';
            echo '</div>';

            makeListTree($oProductCategory->getSubCategories('all'), $iLevel + 1, $iMaxLevels); //call function recursive
            echo '</li>';
            $iT++;
        }
        if (count($aProductCategories) > 0)
            echo '</ol>';
    }

    # start recursive displaying categories
    makeListTree($aLevel1ProductCategories, 0, $iMaxLevels);
    ?>
</div>
<form action="" method="POST" onsubmit="return setProductCategoryStructure();" id="productCategoryStructureForm">
    <input type="hidden" name="action" value="saveProductCategoryStructure" />
    <input type="hidden" name="productCategoryStructure" id="productCategoryStructure" value="" />
    <input type="submit" value="Opslaan" /> <input type="button" value="Reset" onclick="window.location.reload(); return false;" />
</form>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
# add nested sortable javascript source code
$oPageLayout->addJavascriptBottom('<script src="' . ADMIN_JS_FOLDER . '/jquery-ui-nestedSortable.min.js"></script>');

# add nested sortable javascript initiation code
$sNestedSortableJavascript = <<<EOT
<script>
    $('ol.nestedSortable').nestedSortable({
        disableNesting: 'no-nesting',
        forcePlaceholderSize: true,
        handle: 'div',
        helper: 'clone',
        listType: 'ol',
        items: 'li:not(.locked)',
        cancel: '.not-sortable',
        maxLevels: $iMaxLevels,
        opacity: .6,
        placeholder: 'placeholder',
        revert: 250,
        tabSize: 25,
        tolerance: 'pointer',
        toleranceElement: '> div',
        rootClass: 'root-item'
        }).disableSelection();

    function setProductCategoryStructure(){
        serialized = $('ol.nestedSortable').nestedSortable('serialize');
        $("#productCategoryStructure").val(serialized);
        return true;
    }
</script>
EOT;
$oPageLayout->addJavascriptBottom($sNestedSortableJavascript);
?>