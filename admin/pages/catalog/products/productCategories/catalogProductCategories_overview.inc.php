<div class="likeTopRow cf">
    <h2>Alle product categorieën</h2>
    <a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen">Hoofdcategorie toevoegen</a><br />
    <a class="changeOrderBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/structuur-wijzigen">Structuur wijzigen</a>
</div>
<?
# list categories in unordered list

function makeListTree(array $aProductCategories = array(), $iLevel, $iMaxLevels) {
    if (count($aProductCategories) > 0 && $iLevel == 1)
        echo '<ol class="nestedSortable level' . $iLevel . '">';
    elseif (count($aProductCategories) > 0)
        echo '<ol class="level' . $iLevel . '">';

    $iT = 0;
    foreach ($aProductCategories AS $oProductCategory) {
        echo '<li id="productCategory_' . $oProductCategory->catalogProductCategoryId . '">';

        $sClasses = '';
        if ($iT > 0 && $iLevel == 1)
            $sClasses .= ' mainPage';
        if ($iT == 0 && $iLevel == 1)
            $sClasses .= ' first-mainPage';
        if ($iT > 0 && $iLevel > 1)
            $sClasses .= ' sub';
        if ($iT == 0 && $iLevel > 1)
            $sClasses .= ' first-sub';

        # add sub category
        echo '<div class="' . $sClasses . (($iLevel < $iMaxLevels) ? '"><a class="action_icon add_icon" alt="sub toevoegen" title="Sub categorie onder deze categorie toevoegen" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/toevoegen?parentCatalogProductCategoryId=' . $oProductCategory->catalogProductCategoryId . '"></a>' : ' no-action-icons">');

        echo _e($oProductCategory->name);

        echo '<div class="actionIconsHolder">';

        # online offline button
        echo '<a id="productCategory_' . $oProductCategory->catalogProductCategoryId . '_online_1" productCategoryTitle="' . _e($oProductCategory->name) . '" title="Product categorie offline zetten" class="action_icon ' . ($oProductCategory->online ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oProductCategory->catalogProductCategoryId . '/?online=0"></a>';
        echo '<a id="productCategory_' . $oProductCategory->catalogProductCategoryId . '_online_0" productCategoryTitle="' . _e($oProductCategory->name) . '" title="Product categorie online zetten" class="action_icon ' . ($oProductCategory->online ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oProductCategory->catalogProductCategoryId . '/?online=1"></a>';

        #edit button
        echo '<a title="Product categorie bewerken" class="action_icon edit_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductCategory->catalogProductCategoryId . '"></a>';

        # delete button
        if ($oProductCategory->isDeletable())
            echo '<a onclick="return confirmChoice(\'product categorie ' . _e($oProductCategory->name) . '\');" class="action_icon delete_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oProductCategory->catalogProductCategoryId . '"></a>';
        else
            echo '<a class="action_icon delete_icon grey" href="#" title="Verwijder eerst alle gerelateerde sub categoriën en producten"></a>';

        echo '</div>';
        echo '</div>';
        makeListTree($oProductCategory->getSubCategories('all'), $iLevel + 1, $iMaxLevels); //call function recursive
        echo '</li>';
        $iT++;
    }
    if (count($aProductCategories) > 0)
        echo '</ol>';
}

if (count($aLevel1ProductCategories) == 0) {
    echo '<div class="likeSorterTr"><i>Er zijn geen categorieën om weer te geven</i></div>';
}
# start recursive displaying categories
makeListTree($aLevel1ProductCategories, 1, $iMaxLevels);

# create necessary javascript
$sController = http_get('controller');
$sBottomJavascript = <<<EOT
<script>
    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            data: 'ajax=1',
            async: true,
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $("#productCategory_"+dataObj.catalogProductCategoryId+"_online_0").hide(); // hide offline button
                    $("#productCategory_"+dataObj.catalogProductCategoryId+"_online_1").hide(); // hide online button
                    $("#productCategory_"+dataObj.catalogProductCategoryId+"_online_"+dataObj.online).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.online == 0)    
                        showStatusUpdate("Categorie offline gezet");
                    if(dataObj.online == 1)    
                        showStatusUpdate("Categorie online gezet");
                } else {
                    showStatusUpdate("Categorie niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>
