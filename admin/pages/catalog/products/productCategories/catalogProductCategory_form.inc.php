<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het categorieën overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Categorie</legend>
                <table class="withForm">
                    <tr>
                        <td>Online *</td>
                        <td>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de categorie online OF offline" type="radio" <?= $oProductCategory->online ? 'CHECKED' : '' ?> id="online_1" name="online" value="1" /> <label for="online_1">Ja</label>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de categorie online OF offline" type="radio" <?= !$oProductCategory->online ? 'CHECKED' : '' ?> id="online_0" name="online" value="0" /> <label for="online_0">Nee</label>
                        </td>
                        <td><span class="error"><?= $oProductCategory->isPropValid("online") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel" style="width: 116px;"><label for="name">Categorienaam *</label></td>
                        <td><input id="name" class="{validate:{required:true}} autofocus default" title="Vul de naam in" type="text" name="name" value="<?= _e($oProductCategory->name) ?>" /></td>
                        <td><span class="error"><?= $oProductCategory->isPropValid("name") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="3"><label for="content">Content</label></td>
                    </tr>
                    <tr>
                        <td colspan="3"><textarea name="content" id="content" class="tiny_MCE_default tiny_MCE"><?= $oProductCategory->content ?></textarea></td>
                    </tr>                    
                    <? if ($oCurrentUser->isSEO()) { ?>
                        <tr>
                            <td colspan="3" style="padding-top: 10px;"><h2>SEO</h2></td>
                        </tr>
                        <? if ($oProductCategory->catalogProductCategoryId) { ?>
                            <tr>
                                <td>Huidige url</td>
                                <td><?= $oProductCategory->getUrlPath() ?></td>
                            </tr>
                        <? } ?>
                        <tr>
                            <td class="withLabel"><label for="windowTitle">Window titel</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - De titel moet een goed leesbare zin zijn, met het belangrijkste zoekwoord van deze pagina zoveel mogelijk vooraan.<br /> - De titel moet tussen de 8 en 69 karakters zijn (incl. spaties).<br /> - Gebruik niet enkel de bedrijfsnaam in de titel.<br /> - Gebruik geen domeinnaam.">&nbsp;</div></td>
                            <td colspan="2">
                                <div class="inline-block">
                                    <input class="default charCounterWindowTitle" id="windowTitle" type="text" name="windowTitle" value="<?= _e($oProductCategory->windowTitle) ?>" />
                                    <div id="windowTitleCounter" style="text-align: right;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="metaDescription">Description</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - Omschrijving die Google op kan nemen in de zoekresultaten.<br /> - Deze zin moet aantrekkelijk en wervend genoeg zijn om zoekers te stimuleren om op het resultaat te klikken.<br /> - Tussen de 46 en 156 karakters (incl. spaties).">&nbsp;</div></td>
                            <td colspan="2">
                                <div class="inline-block">
                                    <textarea class="charCounterMetaDescription default" id="metaDescription" name="metaDescription"><?= _e($oProductCategory->metaDescription) ?></textarea>
                                    <div id="metaDescriptionCounter" style="text-align: right;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="withLabel"><label for="metaKeywords">Keywords</label> <div class="hasTooltip tooltip" title="Plaats hier voor deze pagina, een aantal relevante woorden (maximaal 10 woorden).<br />Scheidt de woorden van elkaar met een komma en een spatie.<br />- zoekmachines gebruiken keywords vooral om misbruik op te sporen<br />- goed gebruik van keywords zal geen of een hele kleine bijdrage leveren aan de vindbaarheid van deze pagina">&nbsp;</div></td>
                            <td colspan="2"><input class="default" id="metaKeywords" type="text" name="metaKeywords" value="<?= _e($oProductCategory->metaKeywords) ?>" /> (optioneel)</td>
                        </tr>
                        <tr>
                            <td class="withLabel"><label for="urlPart">Url tekst</label> <div class="hasTooltip tooltip" title="Kies zelf de tekst op basis waarvan de url moet worden gegenereerd<br />Bijvoorbeeld: <?= CLIENT_HTTP_URL ?><?= $oProductCategory->parentCatalogProductCategoryId ? $oProductCategory->getParent()->getUrlPath() : '' ?>/<b>[url tekst]</b><br />- alleen letters, cijfers en een `-` is toegestaan<br />- alle tekens anders dan aangegeven worden vervangen door een `-`<br />- vb: `cms systeem` wordt `cms-systeem`">&nbsp;</div></td>
                            <td><input  class="default" id="urlPart" type="text" name="urlPart" value="<?= _e($oProductCategory->getUrlPart()) ?>" /></td>
                            <td></td>
                        </tr>
                    <? } ?>                    
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het categorieën overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
$oPageLayout->addJavascriptBottom('
<script>
    initTinyMCE(".tiny_MCE_default", "/admin/paginas/link-list");
</script>
');
?>