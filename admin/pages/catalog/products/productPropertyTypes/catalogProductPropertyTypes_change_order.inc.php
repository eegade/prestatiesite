<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<h1>Producteigenschappen volgorde wijzigen</h1>
<p>
    <i>Sleep de namen om de volgorde te veranderen</i>
</p>
<div id="sortableContainer">
    <?
# list pages in unordered list

    if (count($aProductPropertyTypes) > 0)
        echo '<ol id="productPropertyTypeSorter" class="sortable cursorMove">';
    $iT = 0;
    foreach ($aProductPropertyTypes AS $oProductPropertyType) {
        echo '<li data-productpropertytypeid="' . $oProductPropertyType->catalogProductPropertyTypeId . '">';
        echo '<div>';
        echo _e($oProductPropertyType->title) . ' (' . ($oProductPropertyType->getProductType() ? _e($oProductPropertyType->getProductType()->title) : '') . ')';
        echo '</div>';
        echo '</li>';
        $iT++;
    }
    if (count($aProductPropertyTypes) > 0)
        echo '</ol>';
    ?>
</div>
<form action="" method="POST" onsubmit="return setOrder();" id="changeOrderForm">
    <input type="hidden" name="action" value="saveOrder" />
    <input type="hidden" name="order" id="order" value="" />
    <input type="submit" value="Opslaan" /> <input type="button" value="Reset" onclick="window.location.reload(); return false;" /></a>
</form>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
# add nested sortable javascript initiation code
$sSortableJavascript = <<<EOT
<script>
    $('ol.sortable').sortable({
        axis: 'y',
        forcePlaceholderSize: true,
        helper: 'clone',
        items: 'li',
        cancel: '.not-sortable',
        opacity: .6,
        placeholder: 'placeholder',
        revert: 250,
        tolerance: 'pointer',
        stop: function(){
            makeZebra('#productPropertyTypeSorter');
        }
        }).disableSelection();

    function setOrder(){
        var order = '';
        $('#productPropertyTypeSorter li').each(function(index, element){
            order += (order == '' ? '' : '|')+$(element).data('productpropertytypeid');
        });
        $("#order").val(order);
        return true;
    }

    makeZebra('#productPropertyTypeSorter');
</script>
EOT;
$oPageLayout->addJavascriptBottom($sSortableJavascript);
?>