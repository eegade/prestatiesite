<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het producteigenschappen overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Producteigenschap <div class="hasTooltip tooltip" title="Maak hier een nieuwe producttype specifieke producteigenschap aan. Hier wordt alleen de naam aangemaakt.<br />Voorbeelden voor een `bed`:<br />- Voetbord breedte<br />- voetbord hoogte<br />- hoofdbord breedte x diepte x hoogte<br />- etc">&nbsp;</div></legend>
                <table class="withForm">
                    <tr>
                        <td class="withLabel" style="width: 140px;"><label for="title">Product eigenschap *</label></td>
                        <td><input id="title" class="{validate:{required:true}} autofocus default" name="title" title="Vul de producteigenschap in" type="text" value="<?= _e($oProductPropertyType->title) ?>" /></td>
                        <td><span class="error"><?= $oProductPropertyType->isPropValid("title") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="type">Type invoer *</label> <div class="hasTooltip tooltip" title="De manier van het invoeren van de gegevens bij een product<br />SELECT: kies 1 vooraf gedefinieerde waarde uit een dropdown/selectbox<br />CHECKBOX: vink meerdere vooraf gedefinieerde waarden aan<br />TEXT: vul vrij een waarde in in een tekst veld">&nbsp;</div></td>
                        <td>
                            <select name="type" id="type" class="{validate:{required:true}} default" title="Kies een invoer type">
                                <option value="">Maak een keuze</option>
                                <option <?= $oProductPropertyType->type == CatalogProductPropertyType::TYPE_SELECT ? 'SELECTED' : '' ?> value="<?= CatalogProductPropertyType::TYPE_SELECT ?>">SELECT</option>
                                <option <?= $oProductPropertyType->type == CatalogProductPropertyType::TYPE_CHECKBOX ? 'SELECTED' : '' ?> value="<?= CatalogProductPropertyType::TYPE_CHECKBOX ?>">CHECKBOX</option>
                                <option <?= $oProductPropertyType->type == CatalogProductPropertyType::TYPE_TEXT ? 'SELECTED' : '' ?> value="<?= CatalogProductPropertyType::TYPE_TEXT ?>">TEXT</option>
                            </select>
                        </td>
                        <td><span class="error"><?= $oProductPropertyType->isPropValid("type") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="type">Type filter</label> <div class="hasTooltip tooltip" title="Kies een type weergave voor het filter<br />- TEXT: zoeken op een (deel) van de ingevoerde waarde<br />- CHECKBOX: meerdere opties kunnen aanvinken<br />- SELECT: selecteer 1 waarde van een eigenschap<br />- MIN-MAX: filteren tussen min en max waarde (getallen)">&nbsp;</div></td>
                        <td>
                            <select name="filterType" id="filterType" class="default" title="Kies een filter type">
                                <option value="">Geen filter</option>
                                <option <?= $oProductPropertyType->filterType == 'text' ? 'SELECTED' : '' ?> value="text">TEXT</option>
                                <option <?= $oProductPropertyType->filterType == 'checkbox' ? 'SELECTED' : '' ?> value="checkbox">CHECKBOX</option>
                                <option <?= $oProductPropertyType->filterType == 'select' ? 'SELECTED' : '' ?> value="select">SELECT</option>
                                <option <?= $oProductPropertyType->filterType == 'min-max' ? 'SELECTED' : '' ?> value="min-max">MIN-MAX</option>
                            </select>
                        </td>
                        <td><span class="error"><?= $oProductPropertyType->isPropValid("type") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="catalogProductTypeId">Product type *</label> <div class="hasTooltip tooltip" title="Het producttype waarvoor deze eigenschap van toepasssing is">&nbsp;</div></td>
                        <td>
                            <select id="catalogProductTypeId" class="{validate:{required:true}} default" title="Kies een producttype">
                                <option value="">Maak een keuze</option>
                                <?
                                foreach ($aProductTypes as $oProductType) {
                                    echo '<option value="' . $oProductType->catalogProductTypeId . '"' . ($oProductType->catalogProductTypeId == $iProductTypeId ? ' selected' : '') . '>' . _e($oProductType->title) . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td><span class="error"><?= $oProductPropertyType->isPropValid("catalogProductTypeId") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="catalogProductPropertyTypeGroupId">Eigenschapgroep *</label> <div class="hasTooltip tooltip" title="De eigenschapgroep waarvoor deze eigenschap van toepasssing is">&nbsp;</div></td>
                        <td>
                            <select class="{validate:{required:true}} default catalogProductPropertyTypeGroupId" data-productpropertytypeid="" title="Kies een eigenschapgroep" <?= !is_numeric($iProductTypeId) ? '' : 'DISABLED style="display: none"' ?>>
                                <option value="" SELECTED>Kies eerst een product type</option>
                            </select>
                            <?
                            foreach ($aProductTypes AS $oProductType) {
                                ?>
                                <select class="{validate:{required:true}} catalogProductPropertyTypeGroupId default" data-productpropertytypeid="<?= $oProductType->catalogProductTypeId ?>" title="Kies een eigenschapgroep" name="catalogProductPropertyTypeGroupId" <?= $iProductTypeId == $oProductType->catalogProductTypeId ? '' : 'DISABLED style="display: none"' ?>>
                                    <option value="">Maak een keuze</option>
                                    <?
                                    foreach (CatalogProductPropertyTypeGroupManager::getProductPropertyTypeGroupsByProductTypeId($oProductType->catalogProductTypeId) AS $oProductPropertyTypeGroup) {
                                        echo '<option ' . ($oProductPropertyType->catalogProductPropertyTypeGroupId == $oProductPropertyTypeGroup->catalogProductPropertyTypeGroupId ? 'SELECTED' : '') . ' value="' . $oProductPropertyTypeGroup->catalogProductPropertyTypeGroupId . '">' . _e($oProductPropertyTypeGroup->title) . '</option>';
                                    }
                                    ?>
                                </select>
                                <?
                            }
                            ?>
                        </td>
                        <td><span class="error"><?= $oProductPropertyType->isPropValid("catalogProductPropertyTypeGroupId") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
    <div class="contentColumn">
        <fieldset>
            <legend>Mogelijke waarden <div class="hasTooltip tooltip" title="Als er wordt gekozen voor SELECT of CHECKBOX kunnen hier waarden worden toegevoegd die dan gekozen kunnen worden">&nbsp;</div></legend>
            <?
            if ($oProductPropertyType->type !== null && $oProductPropertyType->type !== CatalogProductPropertyType::TYPE_TEXT) {
                ?>
                <div id="possibleValues">
                    <form method="POST" class="validateForm">
                        <input type="hidden" name="action" value="savePossibleValue" />
                        <input type="hidden" name="catalogProductPropertyTypeId" value="<?= $oProductPropertyType->catalogProductPropertyTypeId ?>" />
                        <table class="withForm">
                            <tr>
                                <td class="withLabel" style="width: 116px;"><label for="value">Waarde *</label></td>
                                <td><input class="{validate:{required:true}} default" title="Vul de waarde in" id="possibleValueValue" type="text" name="value" value="" /></td>
                            </tr>
                            <tr>
                                <td colspan="2"><input type="submit" value="Waarde opslaan" name="savePossibleValue" /></td>
                            </tr>
                        </table>
                    </form>
                    <hr />
                    <h3>Reeds toegevoegde waarden <div class="hasTooltip tooltip" title="Sleep de regels om de volgorde aan te passen">&nbsp;</div></h3>
                    <ul class="possibleValues sortable">
                        <?
                        foreach ($oProductPropertyType->getPossibleValues() as $oProductPropertyTypePossibleValue) {
                            ?>
                            <li id="placeholder-<?= $oProductPropertyTypePossibleValue->catalogProductPropertyTypePossibleValueId ?>" data-catalogproductpropertytypepossiblevalueid="<?= $oProductPropertyTypePossibleValue->catalogProductPropertyTypePossibleValueId ?>" class="placeholder">
                                <div class="possibleValuePlaceholder cf">
                                    <div class="value"><?= $oProductPropertyTypePossibleValue->value ?></div>
                                </div>
                                <div class="actionsPlaceholder">
                                    <a class="action_icon edit_icon" title="Bewerk waarde" onclick="showEditPossibleValue(this);
                                                    return false;" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-editPossibleValue' ?>"></a>
                                    <a class="action_icon delete_icon" title="Verwijder waarde" onclick="deletePossibleValue(this);
                                                    return false;" href="<?= ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-deletePossibleValue' ?>"></a>
                                </div>
                            </li>
                            <?
                        }
                        ?>
                    </ul>
                </div>
                <?
            } else {
                echo '<p><i>Mogelijke waarden kunnen worden toegevoegd nadat de product eigenschap eerst is opgeslagen en alleen voor type SELECT en CHECKBOX</i></p>';
            }
            ?>
        </fieldset>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het producteigeschappen overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="hide">
    <div id="editPossibleValuesForm">
        <h2>Waarde wijzigen</h2>
        <form onsubmit="savePossibleValue(this);
                return false;" method="POST" action="#">
            <input type="hidden" name="catalogProductPropertyTypePossibleValueId" value="" />
            <label for="form_possibleValueValue">Waarde</label><br />
            <input type="text" class="default" id="form_possibleValueValue" name="value" value="" /><br />
            <input type="submit" name="" value="Waarde opslaan" />
        </form>
    </div>
    <a id="editPossibleValuesFormLink" class="fancyBoxLink" href="#editPossibleValuesForm"></a>
</div>
<?
# add sortable javascript initiation code
$sSaveOrderLink = ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-savePossibleValueOrder';
$sOpeningHoursManagerJavascript = <<<EOT
// change the catalogProductTypeId-select when catalogProductPropertyTypeGroupId-select changes
$('select#catalogProductTypeId').change(function() {
    $('select.catalogProductPropertyTypeGroupId').hide();
    $('select.catalogProductPropertyTypeGroupId').attr('disabled', true);
    $('select.catalogProductPropertyTypeGroupId').val('');
    $('select.catalogProductPropertyTypeGroupId[data-productpropertytypeid=' + $(this).val() + ']').removeAttr('disabled');
    $('select.catalogProductPropertyTypeGroupId[data-productpropertytypeid=' + $(this).val() + ']').show();
});

// sort the possible values
$( "#possibleValues ul.possibleValues.sortable").sortable({
    items: '> li',
    placeholder: 'ui-state-highlight placeholder',
    forcePlaceholderSize: true,
    tolerance: 'pointer',
    update: function(event, ui) {
        var catalogProductPropertyTypePossibleValueIds = new Array();
        ui.item.closest('ul.possibleValues').find('> li').each(function(index, value){
            catalogProductPropertyTypePossibleValueIds[index] = $(value).data('catalogproductpropertytypepossiblevalueid');
        });

        $.ajax({
            type : 'POST',
            url : '$sSaveOrderLink',
            data : 'catalogProductPropertyTypePossibleValueIds=' + catalogProductPropertyTypePossibleValueIds.join(','),
            success : function(data){
                var dataObj = eval("(" + data + ")");
                if(dataObj.success === true) {
                    $("#possibleValues ul.possibleValues > li.placeholder").effect('highlight', 1500);
                    showStatusUpdate('Waarde volgorde gewijzigd');
                } else {
                    showStatusUpdate('Waarde kon niet worden gewijzigd');
                }
            }
        });
    }
});

/**
 * edit a PossibleValues
 * @param element to get data from
 */
function showEditPossibleValue(element){
    jElement = $(element);
    jPlaceholder = $(element).closest(".placeholder");

    //fill the form and show it
    $('#editPossibleValuesForm form').attr('action', jElement.attr('href'));
    $('#editPossibleValuesForm input[name="value"]').val(jPlaceholder.find('.value').html());
    $('#editPossibleValuesForm input[name="catalogProductPropertyTypePossibleValueId"]').val(jPlaceholder.data("catalogproductpropertytypepossiblevalueid"));
    $('#editPossibleValuesFormLink').click();
}

/**
 * save PossibleValues from form
 * @param element (form element)
 */
function savePossibleValue(element){
    var jForm = $(element);

    $.ajax({
        type : 'POST',
        url : jForm.attr("action"),
        data : jForm.serialize(),
        success : function(data){
            var dataObj = eval("(" + data + ")");
            if(dataObj.success === true) {
                //change value from list
                $("ul.possibleValues #placeholder-" + dataObj.catalogProductPropertyTypePossibleValueId + " .possibleValuePlaceholder .value").html(dataObj.value);
                $.fancybox.close(); //close fancybox after saving
                setTimeout(function() {
                    $("ul.possibleValues #placeholder-" + dataObj.catalogProductPropertyTypePossibleValueId).effect('highlight', 1500);
                    showStatusUpdate('Waarde opgeslagen');
                }, 800);// timeout for closing fancybox first
            } else {
                showStatusUpdate('Waarde kon niet worden gewijzigd');
            }
        }
    });
}

/**
 * delete a PossibleValues and on success its placeholder
 * @param element clicked <a> element
 */
function deletePossibleValue(element){
    // confirm deleting PossibleValue
    if(confirmChoice('deze waarde') !== true) {
        return false;
    }

    jElement = $(element);

    $.ajax({
        type : 'POST',
        url : jElement.attr("href"),
        data : 'catalogProductPropertyTypePossibleValueId=' + jElement.closest('.placeholder').data("catalogproductpropertytypepossiblevalueid"),
        success : function(data){
            var dataObj = eval("(" + data + ")");
            if(dataObj.success === true){
                $("ul.possibleValues #placeholder-" + dataObj.catalogProductPropertyTypePossibleValueId).hide(750, function() {
                    $(this).remove();
                });
                showStatusUpdate('Waarde succesvol verwijderd');
            } else {
                showStatusUpdate('Waarde kon niet worden verwijderd');
            }
        }
    });
}
EOT;
$oPageLayout->addJavascriptBottom('<script>' . $sOpeningHoursManagerJavascript . '</script>');
?>