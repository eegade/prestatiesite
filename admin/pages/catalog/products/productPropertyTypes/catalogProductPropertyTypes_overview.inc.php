<form action="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>" method="POST">
    <input type="hidden" name="filterForm" value="1" />
    <fieldset style="margin-bottom: 20px;">
        <legend>Filter</legend>
        <table class="withForm">
            <tr>
                <td class="withLabel" style="width: 116px;">Product type</td>
                <td>
                    <select class="default" id="catalogProductTypeId" name="productPropertyTypeFilter[catalogProductTypeId]">
                        <option value="-1">Maak een keuze</option>
                        <?
                        foreach ($aProductTypes AS $oProductType) {
                            echo '<option ' . ($aProductPropertyTypeFilter['catalogProductTypeId'] == $oProductType->catalogProductTypeId ? 'SELECTED' : '') . ' value="' . $oProductType->catalogProductTypeId . '">' . _e($oProductType->title) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="withLabel" style="width: 116px;">Eigenschapgroep</td>
                <td>
                    <select class="catalogProductPropertyTypeGroupId default" data-productpropertytypeid="-1" <?= $aProductPropertyTypeFilter['catalogProductTypeId'] == -1 ? '' : 'DISABLED style="display: none"' ?>>
                        <option value="-1">Kies eerst een product type</option>
                    </select>
                    <?
                    foreach ($aProductTypes AS $oProductType) {
                        ?>
                        <select class="catalogProductPropertyTypeGroupId default" data-productpropertytypeid="<?= $oProductType->catalogProductTypeId ?>" name="productPropertyTypeFilter[catalogProductPropertyTypeGroupId]" <?= $aProductPropertyTypeFilter['catalogProductTypeId'] == $oProductType->catalogProductTypeId ? '' : 'DISABLED style="display: none"' ?>>
                            <option value="">Alle eigenschapgroepen</option>
                            <?
                            foreach (CatalogProductPropertyTypeGroupManager::getProductPropertyTypeGroupsByProductTypeId($oProductType->catalogProductTypeId) AS $oProductPropertyTypeGroup) {
                                echo '<option ' . ($aProductPropertyTypeFilter['catalogProductPropertyTypeGroupId'] == $oProductPropertyTypeGroup->catalogProductPropertyTypeGroupId ? 'SELECTED' : '') . ' value="' . $oProductPropertyTypeGroup->catalogProductPropertyTypeGroupId . '">' . _e($oProductPropertyTypeGroup->title) . '</option>';
                            }
                            ?>
                        </select>
                        <?
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="filterProductPropertyTypes" value="Filter producteigenschappen" /> <input type="submit" name="resetFilter" value="Reset filter" /></td>
            </tr>
        </table>
    </fieldset>
</form>
<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="5"><h2>Gevonden producteigenschappen (<?= (count($aProductPropertyTypes) != 0 ? (db_int($iStart) + 1) : db_int($iStart)) . '-' . (db_int($iStart) + count($aProductPropertyTypes)) ?>/<?= $iFoundRows ?>)</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Producteigenschap toevoegen" alt="Producteigenschap toevoegen">Producteigenschap toevoegen</a><br /><a class="changeOrderBtn textLeft right" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/volgorde-wijzigen" title="Volgorde wijzigen" alt="Volgorde wijzigen">Volgorde wijzigen</a></div></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted">Producteigenschap</th>
            <th class="{sorter:false} nonSorted">Product type</th>
            <th class="{sorter:false} nonSorted">Type</th>
            <th class="{sorter:false} nonSorted">Filter type</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aProductPropertyTypes AS $oProductPropertyType) {
            echo '<tr>';
            echo '<td>' . _e($oProductPropertyType->title) . '</td>';
            echo '<td>' . ($oProductPropertyType->getProductType() ? _e($oProductPropertyType->getProductType()->title) : '') . '</td>';
            echo '<td>' . $oProductPropertyType->type . '</td>';
            echo '<td>' . $oProductPropertyType->filterType . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk merk" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductPropertyType->catalogProductPropertyTypeId . '"></a>';
            echo '<a class="action_icon delete_icon" title="Verwijder producteigenschap (deze wordt ook verwijderd van de producten die deze hebben)" onclick="return confirmChoice(\'' . _e($oProductPropertyType->title) . ', en alle koppelingen met producten\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oProductPropertyType->catalogProductPropertyTypeId . '"></a>';
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aProductPropertyTypes)) {
            echo '<tr><td colspan="5"><i>Er zijn geen producteigenschappen om weer te geven met dit filter</i></td></tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr class="bottomRow">
            <td colspan="5">
                <form method="POST">
                    <?= generatePaginationHTML($iPageCount, $iCurrPage) ?>
                    <input type="hidden" name="setPerPage" value="1" />
                    <select name="perPage" onchange="$(this).closest('form').submit();">
                        <option value="">alle</option>
                        <option <?= $iPerPage == 10 ? 'SELECTED' : '' ?> value="10">10</option>
                        <option <?= $iPerPage == 25 ? 'SELECTED' : '' ?> value="25">25</option>
                        <option <?= $iPerPage == 50 ? 'SELECTED' : '' ?> value="50">50</option>
                        <option <?= $iPerPage == 100 ? 'SELECTED' : '' ?> value="100">100</option>
                    </select> per pagina
                </form>
            </td>
        </tr>
    </tfoot>
</table>
<?
$sBottomJavascript = <<<EOT
<script>
    // change the catalogProductTypeId-select when catalogProductPropertyTypeGroupId-select changes
    $('select#catalogProductTypeId').change(function() {
        $('select.catalogProductPropertyTypeGroupId').hide();
        $('select.catalogProductPropertyTypeGroupId').attr('disabled', true);
        $('select.catalogProductPropertyTypeGroupId').val('-1');
        $('select.catalogProductPropertyTypeGroupId[data-productpropertytypeid=' + $(this).val() + ']').removeAttr('disabled');
        $('select.catalogProductPropertyTypeGroupId[data-productpropertytypeid=' + $(this).val() + ']').show();
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>