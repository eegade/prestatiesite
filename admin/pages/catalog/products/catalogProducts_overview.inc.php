<form action="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>" method="POST">
    <input type="hidden" name="filterForm" value="1" />
    <fieldset style="margin-bottom: 20px;">
        <legend>Filter</legend>
        <table class="withForm">
            <tr>
                <td class="withLabel" style="width: 150px;">Naam <div class="hasTooltip tooltip" title="Zoek op de naam van het product of een gedeelte daarvan">&nbsp;</div></td>
                <td><input class="default" type="text" name="productFilter[q]" value="<?= _e($aProductFilter['q']) ?>" /></td>
            </tr>
            <tr>
                <td class="withLabel">Merk</td>
                <td>
                    <select class="default" name="productFilter[catalogBrandId]">
                        <option value="">alle merken</option>
                        <?
                        foreach (CatalogBrandManager::getAllBrands() AS $oBrand) {
                            echo '<option ' . ($aProductFilter['catalogBrandId'] == $oBrand->catalogBrandId ? 'SELECTED' : '') . ' value="' . $oBrand->catalogBrandId . '">' . _e($oBrand->name) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="withLabel">Type</td>
                <td>
                    <select class="default" name="productFilter[catalogProductTypeId]">
                        <option value="">alle typen</option>
                        <?
                        foreach (CatalogProductTypeManager::getAllProductTypes() AS $oProductType) {
                            echo '<option ' . ($aProductFilter['catalogProductTypeId'] == $oProductType->catalogProductTypeId ? 'SELECTED' : '') . ' value="' . $oProductType->catalogProductTypeId . '">' . _e($oProductType->title) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="withLabel">Categorie</td>
                <td>
                    <select class="default" name="productFilter[catalogProductCategoryId]">
                        <option value="">alle categorieën</option>
                        <?
                        foreach (CatalogProductCategoryManager::getProductCategoriesByFilter() AS $oProductCategory) {
                            echo '<option ' . ($aProductFilter['catalogProductCategoryId'] == $oProductCategory->catalogProductCategoryId ? 'SELECTED' : '') . ' value="' . $oProductCategory->catalogProductCategoryId . '">' . _e($oProductCategory->name) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <? if (Settings::get('showProductsOnHome')) { ?>
                <tr>
                    <td>Toon op homepage <div class="hasTooltip tooltip" title="Zoek alleen producten die op de homepage worden weergegeven">&nbsp;</div></td>
                    <td>
                        <input <?= !empty($aProductFilter['showOnHome']) ? 'checked' : '' ?> class="alignCheckbox" name="productFilter[showOnHome]" id="showOnHome" type="checkbox" value="showOnHome" /> <label for="showOnHome">Ja</label>
                    </td>
                </tr>
            <? } ?>
            <tr>
                <td>Zonder google categorie <div class="hasTooltip tooltip" title="Zoek alleen producten die geen google categorie hebben">&nbsp;</div></td>
                <td>
                    <input <?= !empty($aProductFilter['emptyGoogleCategory']) ? 'checked' : '' ?> class="alignCheckbox" name="productFilter[emptyGoogleCategory]" id="emptyGoogleCategory" type="checkbox" value="emptyGoogleCategory" /> <label for="emptyGoogleCategory">Ja</label>
                </td>
            </tr>                
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="filterProducts" value="Filter producten" /> <input type="submit" name="resetFilter" value="Reset filter" /></td>
            </tr>
        </table>
    </fieldset>
</form>

<!-- Products -->
<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="9"><h2>Gevonden producten (<?= (count($aProducts) != 0 ? (db_int($iStart) + 1) : db_int($iStart)) . '-' . (db_int($iStart) + count($aProducts)) ?>/<?= $iFoundRows ?>)</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Product toevoegen" alt="Product toevoegen">Product toevoegen</a></div></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted onlineOffline">&nbsp;</th>
            <th class="{sorter:false} nonSorted">Naam</th>
            <th class="{sorter:false} nonSorted">Merk</th>
            <th class="{sorter:false} nonSorted">Type</th>
            <th class="{sorter:false} nonSorted"><?= Settings::get('taxIncluded') ? 'Prijs (incl. BTW)' : 'Prijs (excl. BTW)' ?></th>
            <th class="{sorter:false} nonSorted"><?= Settings::get('taxIncluded') ? 'Actie prijs (incl. BTW)' : 'Actie prijs (excl. BTW)' ?></th>
            <th class="{sorter:false} nonSorted"><?= Settings::get('taxIncluded') ? 'Inkoopprijs (incl. BTW)' : 'Inkoopprijs (excl. BTW)' ?></th>
            <th class="{sorter:false} nonSorted">Marge</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aProducts AS $oProduct) {
            echo '<tr>';
            echo '<td>';
            # online offline button
            echo '<a id="product_' . $oProduct->catalogProductId . '_online_1" title="Product offline zetten" class="action_icon ' . ($oProduct->online ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oProduct->catalogProductId . '/?online=0"></a>';
            echo '<a id="product_' . $oProduct->catalogProductId . '_online_0" title="Product online zetten" class="action_icon ' . ($oProduct->online ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oProduct->catalogProductId . '/?online=1"></a>';
            echo '</td>';
            echo '<td>' . _e($oProduct->name) . '</td>';
            echo '<td>' . ($oProduct->getBrand() ? _e($oProduct->getBrand()->name) : 'Onbekend') . '</td>';
            echo '<td>' . ($oProduct->getProductType() ? _e($oProduct->getProductType()->title) : 'Onbekend') . '</td>';
            echo '<td>' . decimal2valuta($oProduct->getSalePrice(Settings::get('taxIncluded'), null, null, true, false)) . '</td>';
            echo '<td>' . decimal2valuta($oProduct->getReducedPrice(Settings::get('taxIncluded'))) . '</td>';
            echo '<td>' . decimal2valuta($oProduct->getPurchasePrice(Settings::get('taxIncluded'))) . '</td>';
            echo '<td>' . decimal2valuta($oProduct->getSalePrice(Settings::get('taxIncluded'), null, null, true, false) - $oProduct->getPurchasePrice(Settings::get('taxIncluded'))) . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk product" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProduct->catalogProductId . '"></a>';
            echo '<a class="action_icon delete_icon" title="Verwijder product" onclick="return confirmChoice(\'' . _e($oProduct->name) . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oProduct->catalogProductId . '"></a>';
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aProducts)) {
            echo '<tr><td colspan="9"><i>Er zijn geen producten om weer te geven met dit filter</i></td></tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr class="bottomRow">
            <td colspan="9">
                <form method="POST">
                    <?= generatePaginationHTML($iPageCount, $iCurrPage) ?>
                    <input type="hidden" name="setPerPage" value="1" />
                    <select name="perPage" onchange="$(this).closest('form').submit();">
                        <option value="">alle</option>
                        <option <?= $iPerPage == 10 ? 'SELECTED' : '' ?> value="10">10</option>
                        <option <?= $iPerPage == 25 ? 'SELECTED' : '' ?> value="25">25</option>
                        <option <?= $iPerPage == 50 ? 'SELECTED' : '' ?> value="50">50</option>
                        <option <?= $iPerPage == 100 ? 'SELECTED' : '' ?> value="100">100</option>
                    </select> per pagina
                </form>
            </td>
        </tr>
    </tfoot>
</table>
<?
# create necessary javascript
$sController = http_get('controller');
$sBottomJavascript = <<<EOT
<script>
    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            data: 'ajax=1',
            async: true,
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $("#product_"+dataObj.catalogProductId+"_online_0").hide(); // hide offline button
                    $("#product_"+dataObj.catalogProductId+"_online_1").hide(); // hide online button
                    $("#product_"+dataObj.catalogProductId+"_online_"+dataObj.online).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.online == 0)    
                        showStatusUpdate("Product offline gezet");
                    if(dataObj.online == 1)    
                        showStatusUpdate("Product online gezet");                            
                } else {
                    showStatusUpdate("Product niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>