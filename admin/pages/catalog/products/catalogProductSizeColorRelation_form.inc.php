<div style="min-width: 450px;" class="cf">
    <h2>Voorraad bijwerken</h2>
    <form method="POST" action="<?= getCurrentUrl() ?>" class="validateForm">
        <input type="hidden" name="action" value="saveSizeColorRelation" />
        <table class="withForm">
            <tr>
                <td class="withLabel">Voorraad</td>
                <td><input class="numbersOnly" type="text" name="stock" size="3" value="<?= $oCatalogProductSizeColorRelation->stock ?>" /></td>
            </tr>
            <? if ($oCatalogProductSizeColorRelation->getProduct()->getProductType()->withSizes || $oCatalogProductSizeColorRelation->getProduct()->getProductType()->withColors) { ?>
                <tr>
                    <td class="withLabel">Meerprijs (incl. BTW)</td>
                    <td><input class="priceFloatOnly" type="text" name="extraPrice" size="3" value="<?= $oCatalogProductSizeColorRelation->extraPrice ?>" /></td>
                </tr>

                <tr>
                    <td class="withLabel">MPN </td>
                    <td><input type="text" name="catalogProductSizeColorMPN" size="50" value="<?= $oCatalogProductSizeColorRelation->catalogProductSizeColorMPN ?>"  /></td>
                </tr>
            <? } ?>
            <tr>
                <td colspan="3">
                    <input type="submit" value="Voorraad bijwerken" name="save" />
                </td>
            </tr>
        </table>
    </form>
</div>