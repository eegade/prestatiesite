<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het eigenschapgroepen overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Eigenschapgroep <div class="hasTooltip tooltip" title="Maak hier een nieuwe producttype specifieke eigenschapgroep aan. Hier wordt alleen de naam aangemaakt.<br />Voorbeelden voor een `TV`:<br />- Beeldscherm<br />- Aansluitingen<br />- Gewicht en omvang<br />- etc">&nbsp;</div></legend>
                <table class="withForm">
                    <tr>
                        <td class="withLabel" style="width: 125px;"><label for="title">Naam *</label></td>
                        <td><input id="title" class="{validate:{required:true}} autofocus default" name="title" title="Vul de naam in" type="text" value="<?= _e($oProductPropertyTypeGroup->title) ?>" /></td>
                        <td><span class="error"><?= $oProductPropertyTypeGroup->isPropValid("title") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="catalogProductTypeId">Product type *</label> <div class="hasTooltip tooltip" title="Het producttype waarvoor deze groep van toepasssing is">&nbsp;</div></td>
                        <td>
                            <select id="catalogProductTypeId" class="{validate:{required:true}} default" title="Kies een producttype" name="catalogProductTypeId">
                                <option value="">Maak een keuze</option>
                                <?
                                foreach (CatalogProductTypeManager::getAllProductTypes() as $oProductType) {
                                    echo '<option value="' . $oProductType->catalogProductTypeId . '"' . ($oProductType->catalogProductTypeId == $oProductPropertyTypeGroup->catalogProductTypeId ? ' selected' : '') . '>' . _e($oProductType->title) . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td><span class="error"><?= $oProductPropertyTypeGroup->isPropValid("type") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>   
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het eigenschapgroepen overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>