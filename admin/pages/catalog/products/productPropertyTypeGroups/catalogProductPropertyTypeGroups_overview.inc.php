<form action="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>" method="POST">
    <input type="hidden" name="filterForm" value="1" />
    <fieldset style="margin-bottom: 20px;">
        <legend>Filter</legend>
        <table class="withForm">
            <tr>
                <td class="withLabel" style="width: 116px;">Type</td>
                <td>
                    <select class="default" name="productPropertyTypeGroupFilter[catalogProductTypeId]">
                        <option value="-1">Maak een keuze</option>
                        <?
                        foreach (CatalogProductTypeManager::getAllProductTypes() AS $oProductType) {
                            echo '<option ' . ($aProductPropertyTypeGroupFilter['catalogProductTypeId'] == $oProductType->catalogProductTypeId ? 'SELECTED' : '') . ' value="' . $oProductType->catalogProductTypeId . '">' . _e($oProductType->title) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="filterProductPropertyTypeGroups" value="Filter eigenschapgroepen" /> <input type="submit" name="resetFilter" value="Reset filter" /></td>
            </tr>
        </table>
    </fieldset>
</form>
<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="3"><h2>Gevonden eigenschapgroepen (<?= (count($aProductPropertyTypeGroups) != 0 ? (db_int($iStart) + 1) : db_int($iStart)) . '-' . (db_int($iStart) + count($aProductPropertyTypeGroups)) ?>/<?= $iFoundRows ?>)</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Eigenschapgroep toevoegen" alt="Eigenschapgroep toevoegen">Eigenschapgroep toevoegen</a><br /><a class="changeOrderBtn textLeft right" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/volgorde-wijzigen" title="Volgorde wijzigen" alt="Volgorde wijzigen">Volgorde wijzigen</a></div></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted">Groepnaam</th>
            <th class="{sorter:false} nonSorted">Product type</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aProductPropertyTypeGroups AS $oProductPropertyTypeGroup) {
            echo '<tr>';
            echo '<td>' . $oProductPropertyTypeGroup->title . '</td>';
            echo '<td>' . ($oProductPropertyTypeGroup->getProductType() ? _e($oProductPropertyTypeGroup->getProductType()->title) : '') . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk eigenschapgroep" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductPropertyTypeGroup->catalogProductPropertyTypeGroupId . '"></a>';
            
            if ($oProductPropertyTypeGroup->isDeletable()) {
                echo '<a class="action_icon delete_icon" title="Verwijder eigenschapgroep (deze wordt ook verwijderd van de producten die deze hebben)" onclick="return confirmChoice(\'' . _e($oProductPropertyTypeGroup->title) . ', en alle koppelingen met producten\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oProductPropertyTypeGroup->catalogProductPropertyTypeGroupId . '"></a>';
            } else {
                echo '<span class="action_icon delete_icon grey" title="Aan deze groep hangen nog eigenschappen"></span>';
            }
            
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aProductPropertyTypeGroups)) {
            echo '<tr><td colspan="3"><i>Er zijn geen eigenschapgroepen om weer te geven met dit filter</i></td></tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr class="bottomRow">
            <td colspan="3">
                <form method="POST">
                    <?= generatePaginationHTML($iPageCount, $iCurrPage) ?>
                    <input type="hidden" name="setPerPage" value="1" />
                    <select name="perPage" onchange="$(this).closest('form').submit();">
                        <option value="">alle</option>
                        <option <?= $iPerPage == 10 ? 'SELECTED' : '' ?> value="10">10</option>
                        <option <?= $iPerPage == 25 ? 'SELECTED' : '' ?> value="25">25</option>
                        <option <?= $iPerPage == 50 ? 'SELECTED' : '' ?> value="50">50</option>
                        <option <?= $iPerPage == 100 ? 'SELECTED' : '' ?> value="100">100</option>
                    </select> per pagina
                </form>
            </td>
        </tr>
    </tfoot>
</table>