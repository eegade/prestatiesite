<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="3"><h2>Alle maten</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Maat toevoegen" alt="Maat toevoegen">Maat toevoegen</a><br /><a class="changeOrderBtn textLeft right" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/volgorde-wijzigen" title="Volgorde wijzigen" alt="Volgorde wijzigen">Volgorde wijzigen</a></div></td>
        </tr>
        <tr>
            <th>Maat</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aProductSizes AS $oProductSize) {
            echo '<tr>';
            echo '<td>' . _e($oProductSize->name) . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk maat" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductSize->catalogProductSizeId . '"></a>';
            
            if ($oProductSize->isDeletable()) {
                echo '<a class="action_icon delete_icon" title="Verwijder maat" onclick="return confirmChoice(\'' . _e($oProductSize->name) . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oProductSize->catalogProductSizeId . '"></a>';
            } else {
                echo '<span class="action_icon delete_icon grey" title="Aan deze maat hangen nog producten"></span>';
            }
            
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aProductSizes)) {
            echo '<tr><td colspan="3"><i>Er zijn geen maten om weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>