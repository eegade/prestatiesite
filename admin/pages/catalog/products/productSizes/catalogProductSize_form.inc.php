<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het maten overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Maat</legend>
                <table class="withForm">
                    <tr>
                        <td class="withLabel" style="width: 116px;"><label for="name">Maat *</label></td>
                        <td><input id="name" class="{validate:{required:true}} autofocus default" title="Vul de naam in" type="text" name="name" value="<?= _e($oProductSize->name) ?>" /></td>
                        <td><span class="error"><?= $oProductSize->isPropValid("name") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het maten overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>