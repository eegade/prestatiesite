<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="3"><h2>Alle kleuren</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Kleur toevoegen" alt="Kleur toevoegen">Kleur toevoegen</a><br /><a class="changeOrderBtn textLeft right" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/volgorde-wijzigen" title="Volgorde wijzigen" alt="Volgorde wijzigen">Volgorde wijzigen</a></div></td>
        </tr>
        <tr>
            <th>Kleur</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aProductColors AS $oProductColor) {
            echo '<tr>';
            echo '<td>' . _e($oProductColor->name) . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk kleur" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oProductColor->catalogProductColorId . '"></a>';
            
            if ($oProductColor->isDeletable()) {
                echo '<a class="action_icon delete_icon" title="Verwijder kleur" onclick="return confirmChoice(\'' . _e($oProductColor->name) . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oProductColor->catalogProductColorId . '"></a>';
            } else {
                echo '<span class="action_icon delete_icon grey" title="Aan deze kleur hangen nog producten"></span>';
            }
            
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aProductColors)) {
            echo '<tr><td colspan="3"><i>Er zijn geen kleuren om weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>