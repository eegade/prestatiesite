<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="3"><h2>Alle merken</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Merk toevoegen" alt="Merk toevoegen">Merk toevoegen</a><br /><a class="changeOrderBtn textLeft right" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/volgorde-wijzigen" title="Volgorde wijzigen" alt="Volgorde wijzigen">Volgorde wijzigen</a></div></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted onlineOffline">&nbsp;</th>
            <th>Merknaam</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aAllBrands AS $oBrand) {
            echo '<tr>';
            echo '<td>';
            # online offline button
            echo '<a id="brand_' . $oBrand->catalogBrandId . '_online_1" title="Merk offline zetten" class="action_icon ' . ($oBrand->online ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oBrand->catalogBrandId . '/?online=0"></a>';
            echo '<a id="brand_' . $oBrand->catalogBrandId . '_online_0" title="Merk online zetten" class="action_icon ' . ($oBrand->online ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oBrand->catalogBrandId . '/?online=1"></a>';
            echo '</td>';
            echo '<td>' . _e($oBrand->name) . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk merk" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oBrand->catalogBrandId . '"></a>';
            
            if ($oBrand->isDeletable()) {
                echo '<a class="action_icon delete_icon" title="Verwijder merk" onclick="return confirmChoice(\'' . _e($oBrand->name) . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oBrand->catalogBrandId . '"></a>';
            } else {
                echo '<span class="action_icon delete_icon grey" title="Aan dit merk hangen nog producten"></span>';
            }
            
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aAllBrands)) {
            echo '<tr><td colspan="3"><i>Er zijn geen merken om weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>
<?
# create necessary javascript
$sController = http_get('controller');
$sBottomJavascript = <<<EOT
<script>
    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            data: 'ajax=1',
            async: true,
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $("#brand_"+dataObj.catalogBrandId+"_online_0").hide(); // hide offline button
                    $("#brand_"+dataObj.catalogBrandId+"_online_1").hide(); // hide online button
                    $("#brand_"+dataObj.catalogBrandId+"_online_"+dataObj.online).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.online == 0)    
                        showStatusUpdate("Merk offline gezet");
                    if(dataObj.online == 1)    
                        showStatusUpdate("Merk online gezet");                            
                } else {
                    showStatusUpdate("Merk niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>