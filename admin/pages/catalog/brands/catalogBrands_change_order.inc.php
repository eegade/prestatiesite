<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<h1>Merken volgorde wijzigen</h1>
<p>
    <i>Sleep de merknamen om de volgorde te veranderen</i>
</p>
<div id="sortableContainer">
    <?
# list pages in unordered list

    if (count($aAllBrands) > 0)
        echo '<ol id="brandSorter" class="sortable cursorMove">';
    $iT = 0;
    foreach ($aAllBrands AS $oBrand) {
        echo '<li data-brandid="' . $oBrand->catalogBrandId . '">';
        echo '<div>';
        echo _e($oBrand->name);
        echo '</div>';
        echo '</li>';
        $iT++;
    }
    if (count($aAllBrands) > 0)
        echo '</ol>';
    ?>
</div>
<form action="" method="POST" onsubmit="return setOrder();" id="changeOrderForm">
    <input type="hidden" name="action" value="saveOrder" />
    <input type="hidden" name="order" id="order" value="" />
    <input type="submit" value="Opslaan" /> <input type="button" value="Reset" onclick="window.location.reload(); return false;" /></a>
</form>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
# add nested sortable javascript initiation code
$sSortableJavascript = <<<EOT
<script>
    $('ol.sortable').sortable({
        axis: 'y',
        forcePlaceholderSize: true,
        helper: 'clone',
        items: 'li',
        cancel: '.not-sortable',
        opacity: .6,
        placeholder: 'placeholder',
        revert: 250,
        tolerance: 'pointer',
        stop: function(){
            makeZebra('#brandSorter');
        }
        }).disableSelection();

    function setOrder(){
        var order = '';
        $('#brandSorter li').each(function(index, element){
            order += (order == '' ? '' : '|')+$(element).data('brandid');
        });
        $("#order").val(order);
        return true;
    }

    makeZebra('#brandSorter');
</script>
EOT;
$oPageLayout->addJavascriptBottom($sSortableJavascript);
?>