<table class="sorted withActionIcons">
    <thead>
        <tr class="topRow">
            <td colspan="3"><h2>Alle nieuws categorie&euml;n</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Nieuws categorie toevoegen" alt="Nieuws categorie toevoegen">Nieuws categorie toevoegen</a><br /><a class="changeOrderBtn textLeft right" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/volgorde-wijzigen" title="Volgorde wijzigen" alt="Volgorde wijzigen">Volgorde wijzigen</a></div></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted onlineOffline">&nbsp;</th>
            <th>Categorienaam</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aNewsItemCategories AS $oNewsItemCategory) {
            echo '<tr>';
            echo '<td>';
            # online offline button
            echo '<a id="newsItemCategory_' . $oNewsItemCategory->newsItemCategoryId . '_online_1" title="Nieuws categorie offline zetten" class="action_icon ' . ($oNewsItemCategory->online ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oNewsItemCategory->newsItemCategoryId . '/?online=0"></a>';
            echo '<a id="newsItemCategory_' . $oNewsItemCategory->newsItemCategoryId . '_online_0" title="Nieuws categorie online zetten" class="action_icon ' . ($oNewsItemCategory->online ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oNewsItemCategory->newsItemCategoryId . '/?online=1"></a>';
            echo '</td>';
            echo '<td>' . _e($oNewsItemCategory->name) . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk merk" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItemCategory->newsItemCategoryId . '"></a>';
            
            if ($oNewsItemCategory->isDeletable()) {
                echo '<a class="action_icon delete_icon" title="Verwijder merk" onclick="return confirmChoice(\'' . _e($oNewsItemCategory->name) . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oNewsItemCategory->newsItemCategoryId . '"></a>';
            } else {
                echo '<span class="action_icon delete_icon grey" title="Aan deze categorie hangen nog nieuwsitems"></span>';
            }
            
            echo '</td>';
            echo '</tr>';
        }
        if (empty($aNewsItemCategories)) {
            echo '<tr><td colspan="3"><i>Er zijn geen nieuws categorie&euml;n om weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>
<?
# create necessary javascript
$sController = http_get('controller');
$sBottomJavascript = <<<EOT
<script>
    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: $(this).attr('href'),
            data: 'ajax=1',
            async: true,
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if (dataObj.success == true){
                    $("#newsItemCategory_"+dataObj.newsItemCategoryId+"_online_0").hide(); // hide offline button
                    $("#newsItemCategory_"+dataObj.newsItemCategoryId+"_online_1").hide(); // hide online button
                    $("#newsItemCategory_"+dataObj.newsItemCategoryId+"_online_"+dataObj.online).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.online == 0)    
                        showStatusUpdate("Nieuws categorie offline gezet");
                    if(dataObj.online == 1)    
                        showStatusUpdate("Nieuws categorie online gezet");                            
                } else {
                    showStatusUpdate("Nieuws categorie niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sBottomJavascript);
?>