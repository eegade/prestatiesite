<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het nieuws categorie&euml;n overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Nieuws categorie</legend>
                <table class="withForm">
                    <tr>
                        <td>Online *</td>
                        <td>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de blogcategorie online OF offline" type="radio" <?= $oNewsItemCategory->online ? 'CHECKED' : '' ?> id="online_1" name="online" value="1" /> <label for="online_1">Ja</label>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de blogcategorie online OF offline" type="radio" <?= !$oNewsItemCategory->online ? 'CHECKED' : '' ?> id="online_0" name="online" value="0" /> <label for="online_0">Nee</label>
                        </td>
                        <td><span class="error"><?= $oNewsItemCategory->isPropValid("online") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel" style="width: 116px;"><label for="name">Naam *</label></td>
                        <td><input id="name" class="{validate:{required:true}} autofocus default" name="name" title="Vul de naam in" type="text" value="<?= _e($oNewsItemCategory->name) ?>" /></td>
                                      <td><span class="error"><?= $oNewsItemCategory->isPropValid("name") ? '' : 'Veld niet (juist) ingevuld of is al in gebruik' ?></span></td>
                        </tr>
                        <? if ($oCurrentUser->isSEO()) { ?>
                            <tr>
                                <td colspan="3" style="padding-top: 10px;"><h2>SEO</h2></td>
                            </tr>
                            <? if ($oNewsItemCategory->newsItemCategoryId) { ?>
                                <tr>
                                    <td>Huidige url</td>
                                    <td><?= $oNewsItemCategory->getUrlPath() ?></td>
                                </tr>
                            <? } ?>
                            <tr>
                                <td class="withLabel"><label for="windowTitle">Window titel</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - De titel moet een goed leesbare zin zijn, met het belangrijkste zoekwoord van deze pagina zoveel mogelijk vooraan.<br /> - De titel moet tussen de 8 en 69 karakters zijn (incl. spaties).<br /> - Gebruik niet enkel de bedrijfsnaam in de titel.<br /> - Gebruik geen domeinnaam.">&nbsp;</div></td>
                                <td colspan="2">
                                    <div class="inline-block">
                                        <input class="default charCounterWindowTitle" id="windowTitle" type="text" name="windowTitle" value="<?= _e($oNewsItemCategory->windowTitle) ?>" />
                                        <div id="windowTitleCounter" style="text-align: right;"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="metaDescription">Description</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - Omschrijving die Google op kan nemen in de zoekresultaten.<br /> - Deze zin moet aantrekkelijk en wervend genoeg zijn om zoekers te stimuleren om op het resultaat te klikken.<br /> - Tussen de 46 en 156 karakters (incl. spaties).">&nbsp;</div></td>
                                <td colspan="2">
                                    <div class="inline-block">
                                        <textarea cols="34" rows="5" class="charCounterMetaDescription default" id="metaDescription" name="metaDescription"><?= _e($oNewsItemCategory->metaDescription) ?></textarea>
                                        <div id="metaDescriptionCounter" style="text-align: right;"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="withLabel"><label for="metaKeywords">Keywords</label> <div class="hasTooltip tooltip" title="Plaats hier voor deze pagina, een aantal relevante woorden (maximaal 10 woorden).<br />Scheidt de woorden van elkaar met een komma en een spatie.<br />- zoekmachines gebruiken keywords vooral om misbruik op te sporen<br />- goed gebruik van keywords zal geen of een hele kleine bijdrage leveren aan de vindbaarheid van deze pagina">&nbsp;</div></td>
                                <td colspan="2"><input class="default" id="metaKeywords" type="text" name="metaKeywords" value="<?= _e($oNewsItemCategory->metaKeywords) ?>" /></td>
                            </tr>
                            <tr>
                                <td class="withLabel"><label for="urlPartText">Url tekst</label> <div class="hasTooltip tooltip" title="Kies zelf de tekst op basis waarvan de url moet worden gegenereerd<br />Bijvoorbeeld: <?= CLIENT_HTTP_URL ?>/nieuws/<b>[url tekst]</b>">&nbsp;</div></td>
                                <td><input class="default" id="urlPart" type="text" name="urlPartText" value="<?= _e($oNewsItemCategory->getUrlPartText()) ?>" /></td>
                                <td></td>
                            </tr>
                        <? } ?>
                        <tr>
                            <td colspan="3">
                                <input type="submit" value="Opslaan" name="save" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>
    </div>
    <div id="bottomOptions">
        <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar het nieuws categorie&euml;n overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
    </div>