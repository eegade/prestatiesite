<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar nieuws overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <form method="POST" action="" class="validateForm">
            <input type="hidden" value="save" name="action" />
            <fieldset>
                <legend>Nieuwsitem</legend>
                <table class="withForm">
                    <tr>
                        <td>Online *</td>
                        <td>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de Nieuwsitem online OF offline" type="radio" <?= $oNewsItem->online ? 'CHECKED' : '' ?> id="online_1" name="online" value="1" /> <label for="online_1">Ja</label>
                            <input class="alignRadio {validate:{required:true}}" title="Zet de Nieuwsitem online OF offline" type="radio" <?= !$oNewsItem->online ? 'CHECKED' : '' ?> id="online_0" name="online" value="0" /> <label for="online_0">Nee</label>
                        </td>
                        <td><span class="error"><?= $oNewsItem->isPropValid("online") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="date">Datum *</label> <div class="hasTooltip tooltip" title="De datum waarop dit artikel gearchiveerd wordt in de Nieuwsmodule">&nbsp;</div></td>
                        <td>
                            <input title="Geef een geldige datum in (dd-mm-jjjj)" size="9" id="date" class="datePickerDefault hasDatePicker {validate:{required:true, dateNL:true}}" type="text" name="date" value="<?= $oNewsItem->date ? Date::strToDate($oNewsItem->date)->format("%d-%m-%Y") : Date::strToDate('NOW')->format("%d-%m-%Y") ?>" />
                        </td>
                        <td><span class="error"><?= $oNewsItem->isPropValid("date") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel" style="width: 120px;"><label for="title">Titel *</label> <div class="hasTooltip tooltip" title="De titel van uw artikel">&nbsp;</div></td>
                        <td><input id="title" class="{validate:{required:true}} autofocus default" title="Vul de titel in van het Nieuwsitem" type="text" autocomplete="off" name="title" value="<?= _e($oNewsItem->title) ?>" /></td>
                        <td><span class="error"><?= $oNewsItem->isPropValid("title") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="onlineFromDate">Online vanaf *</label> <div class="hasTooltip tooltip" title="Geef aan vanaf wanneer uw artikel online mag staan">&nbsp;</div></td>
                        <td>
                            <input title="Geef een geldige datum in (dd-mm-jjjj)" size="9" id="onlineFromDate" class="datePickerDefault hasDatePicker {validate:{required:true, dateNL:true}}" type="text" name="onlineFromDate" value="<?= $oNewsItem->onlineFrom ? Date::strToDate($oNewsItem->onlineFrom)->format("%d-%m-%Y") : Date::strToDate('NOW')->format("%d-%m-%Y") ?>" />
                            <input title="Geef een geldige tijd in (hh:mm)" size="3" id="onlineFromTime" class="timePickerDefault hasTimePicker {validate:{required:true, time:true}}" type="text" name="onlineFromTime" value="<?= $oNewsItem->onlineFrom ? Date::strToDate($oNewsItem->onlineFrom)->format("%H:%M") : Date::strToDate('NOW')->format("%H:%M") ?>" />
                        </td>
                        <td><span class="error"><?= $oNewsItem->isPropValid("onlineFrom") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="onlineToDate">Online tot</label> <div class="hasTooltip tooltip" title="Geef aan tot wanneer uw artikel zichtbaar moet zijn op de website<br />- Leeg laten voor oneindig online">&nbsp;</div></td>
                        <td>
                            <input title="Geef een geldige datum in (dd-mm-jjjj)" size="9" id="onlineToDate" class="datePickerDefault hasDatePicker {validate:{required: '#onlineToTime:filled', dateNL:true}}" type="text" name="onlineToDate" value="<?= $oNewsItem->onlineTo ? Date::strToDate($oNewsItem->onlineTo)->format("%d-%m-%Y") : '' ?>" />
                            <input title="Geef een geldige tijd in (hh:mm)" size="3" id="onlineToTime" class="timePickerDefault hasTimePicker {validate:{required: '#onlineToDate:filled', time:true}}" type="text" name="onlineToTime" value="<?= $oNewsItem->onlineTo ? Date::strToDate($oNewsItem->onlineTo)->format("%H:%M") : '' ?>" />
                        </td>
                        <td></td>
                    </tr>
                    <?
                    if (BB_WITH_NEWS_CATEGORIES) {
                        $aNewsItemCategories = NewsItemCategoryManager::getNewsItemCategoriesByFilter(array('showAll' => true));
                        if (!empty($aNewsItemCategories)) {
                            ?>
                            <tr>
                                <td colspan="3"><h2>Gerelateerde categorie&euml;n</h2></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <?
                                    # set current categories in an array to check whether to select/check the options
                                    $aNewsItemCategoryIds = array();
                                    foreach ($oNewsItem->getCategories('all') AS $oNewsItemCategory) {
                                        $aNewsItemCategoryIds[] = $oNewsItemCategory->newsItemCategoryId;
                                    }

                                    echo '<ul style="list-style: none; margin: 0; padding: 0;">';
                                    foreach ($aNewsItemCategories as $oNewsItemCategory) {
                                        echo '<li><input class="alignCheckbox {validate:{required:true}}" title="Kies tenminste 1 nieuws categorie" id="newsItemCategory_' . $oNewsItemCategory->newsItemCategoryId . '" type="checkbox" name="newsItemCategoryIds[]" value="' . $oNewsItemCategory->newsItemCategoryId . '" ' . (in_array($oNewsItemCategory->newsItemCategoryId, $aNewsItemCategoryIds) ? 'CHECKED' : '') . ' /> <label for="newsItemCategory_' . $oNewsItemCategory->newsItemCategoryId . '">' . _e($oNewsItemCategory->name) . '</label>';
                                    }
                                    echo '</ul>';
                                    ?>
                                </td>
                                <td><span class="error"><?= $oNewsItem->isPropValid("categories") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                            </tr>
                            <?
                        }
                    }
                    ?>
                    <tr>
                        <td colspan="3" style="padding-top: 20px;"><label for="intro">Intro (korte intro tekst)</label> <div class="hasTooltip tooltip" title="Vul hier een korte introductie tekst in.<br />Deze tekst wordt getoond de overzichten en op de homepage">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td colspan="3"><textarea name="intro" id="intro" class="tiny_MCE_default tiny_MCE intro"><?= $oNewsItem->intro ?></textarea></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="padding-top: 20px;"><label for="content">Nieuwsbericht</label> <div class="hasTooltip tooltip" title="Vul hier uw Nieuwsbericht in.">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td colspan="3"><textarea name="content" id="content" class="tiny_MCE_default tiny_MCE"><?= $oNewsItem->content ?></textarea></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="source">Bron</label></td>
                        <td><input class="default" id="source" type="text" name="source" value="<?= _e($oNewsItem->source) ?>" /></td>
                        <td></td>
                    </tr>
                    
                    <!-- SEO information -->
                    <? if ($oCurrentUser->isSEO()) { ?>
                        <tr>
                            <td colspan="3"><h2>SEO</h2></td>
                        </tr>
                        <tr>
                            <td class="withLabel"><label for="windowTitle">Window titel</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - De titel moet een goed leesbare zin zijn, met het belangrijkste zoekwoord van deze pagina zoveel mogelijk vooraan.<br /> - De titel moet tussen de 8 en 69 karakters zijn (incl. spaties).<br /> - Gebruik niet enkel de bedrijfsnaam in de titel.<br /> - Gebruik geen domeinnaam.">&nbsp;</div></td>
                            <td colspan="2">
                                <div class="inline-block">
                                    <input class="default charCounterWindowTitle" id="windowTitle" type="text" name="windowTitle" value="<?= _e($oNewsItem->windowTitle) ?>" />
                                    <div id="windowTitleCounter" style="text-align: right;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="metaDescription">Description</label> <div class="hasTooltip tooltip" title="Bedoeld voor een goed resultaat in de zoekmachines.<br /> - Omschrijving die Google op kan nemen in de zoekresultaten.<br /> - Deze zin moet aantrekkelijk en wervend genoeg zijn om zoekers te stimuleren om op het resultaat te klikken.<br /> - Tussen de 46 en 156 karakters (incl. spaties).">&nbsp;</div></td>
                            <td colspan="2">
                                <div class="inline-block">
                                    <textarea class="default charCounterMetaDescription" id="metaDescription" name="metaDescription"><?= _e($oNewsItem->metaDescription) ?></textarea>
                                    <div id="metaDescriptionCounter" style="text-align: right;"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="withLabel"><label for="metaKeywords">Keywords</label> <div class="hasTooltip tooltip" title="Plaats hier voor deze pagina, een aantal relevante woorden (maximaal 10 woorden).<br />Scheidt de woorden van elkaar met een komma en een spatie.<br />- zoekmachines gebruiken keywords vooral om misbruik op te sporen<br />- goed gebruik van keywords zal geen of een hele kleine bijdrage leveren aan de vindbaarheid van deze pagina">&nbsp;</div></td>
                            <td colspan="2"><input  class="default" id="metaKeywords" type="text" name="metaKeywords" value="<?= _e($oNewsItem->metaKeywords) ?>" /></td>
                        </tr>
                    <? } ?>

                    <tr>
                        <td colspan="3">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
    </div>
    <? ?>
    <div class="contentColumn">
        <fieldset>
            <legend>Afbeeldingen</legend>
            <?
            if ($oNewsItem->newsItemId !== null) {

                $oImageManagerHTML->includeTemplate();
            } else {
                echo '<p><i>Afbeeldingen kunnen worden geüpload nadat het Nieuwsitem eerst is opgeslagen</i></p>';
            }
            ?>
        </fieldset>
    </div>
    <div class="contentColumn">
        <fieldset>
            <legend>Bestanden</legend>
            <?
            if ($oNewsItem->newsItemId !== null) {
                $oFileManagerHTML->includeTemplate();
            } else {
                echo '<p><i>Bestanden kunnen worden geüpload nadat het Nieuwsitem pagina eerst is opgeslagen</i></p>';
            }
            ?>
        </fieldset>
    </div>
    <div class="contentColumn">
        <fieldset>
            <legend>Links</legend>
            <?
            if ($oNewsItem->newsItemId !== null) {
                $oLinkManagerHTML->includeTemplate();
            } else {
                echo '<p><i>Links kunnen worden toegevoegd nadat het Nieuwsitem eerst is opgeslagen</i></p>';
            }
            ?>
        </fieldset>
    </div>
    <div class="contentColumn">
        <fieldset>
            <legend>Youtube video links</legend>
            <?
            if ($oNewsItem->newsItemId !== null) {
                $oYoutubeLinkManagerHTML->includeTemplate();
            } else {
                echo '<p><i>Youtube video links kunnen worden toegevoegd nadat het Nieuwsitem eerst is opgeslagen</i></p>';
            }
            ?>
        </fieldset>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar nieuws overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<?
$oPageLayout->addJavascriptBottom('
<script>
    initTinyMCE(".tiny_MCE_default.intro", "/admin/paginas/link-list");
    initTinyMCE(".tiny_MCE_default:not(.intro)", "/admin/paginas/link-list", "/admin/nieuws/image-list/' . $oNewsItem->newsItemId . '");
</script>
');
?>