<form action="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>" method="POST">
    <input type="hidden" name="filterForm" value="1" />
    <fieldset style="margin-bottom: 20px;">
        <legend>Filter</legend>
        <table class="withForm">
            <tr>
                <td class="withLabel" style="width: 116px;">Zoekwoord</td>
                <td><input type="text" name="newsItemFilter[q]" value="<?= _e($aNewsItemFilter['q']) ?>" /></td>
            </tr>
            <? if (BB_WITH_NEWS_CATEGORIES) { ?>
                <tr>
                    <td class="withLabel"><label>Categorie</label></td>
                    <td>
                        <select name="newsItemFilter[newsItemCategoryId]">
                            <option value="">-- alle categorie&euml;n --</option>
                            <?
                            foreach (NewsItemCategoryManager::getNewsItemCategoriesByFilter(array('showAll' => true)) AS $oNewsItemCategory) {
                                echo '<option ' . ($aNewsItemFilter['newsItemCategoryId'] == $oNewsItemCategory->newsItemCategoryId ? 'selected' : '') . ' value="' . $oNewsItemCategory->newsItemCategoryId . '">' . $oNewsItemCategory->name . '</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
            <? } ?>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="filterNewsItems" value="Filter nieuws" /> <input type="submit" name="resetFilter" value="Reset filter" /></td>
            </tr>
        </table>
    </fieldset>
</form>

<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="6"><h2>Alle Nieuwsitems</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Nieuw Nieuwsitem toevoegen" alt="Nieuw Nieuwsitem toevoegen">Nieuwsitem toevoegen</a></div></td>
        </tr>
        <tr>
            <th class="{sorter:false} nonSorted onlineOffline">&nbsp;</th>
            <th class="{sorter:false} nonSorted">Datum</th>
            <th class="{sorter:false} nonSorted">Titel</th>
            <th class="{sorter:false} nonSorted">Online vanaf</th>
            <th class="{sorter:false} nonSorted"">Online tot</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aNewsItems AS $oNewsItem) {
            echo '<tr>';
            echo '<td>';

            # online offline button
            echo '<a id="newsItem_' . $oNewsItem->newsItemId . '_online_1" title="Blogitem offline zetten" class="action_icon ' . ($oNewsItem->online ? '' : 'hide') . ' online_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oNewsItem->newsItemId . '/?online=0"></a>';
            echo '<a id="newsItem_' . $oNewsItem->newsItemId . '_online_0" title="Blogitem online zetten" class="action_icon ' . ($oNewsItem->online ? 'hide' : '') . ' offline_icon" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/ajax-setOnline/' . $oNewsItem->newsItemId . '/?online=1"></a>';

            echo '</td>';
            echo '<td>' . Date::strToDate($oNewsItem->date)->format('%d-%m-%Y') . '</td>';
            echo '<td>' . _e($oNewsItem->title) . '</td>';
            echo '<td>' . ($oNewsItem->onlineFrom ? Date::strToDate($oNewsItem->onlineFrom)->format('%d-%m-%Y %H:%M') : '') . '</td>';
            echo '<td>' . ($oNewsItem->onlineTo ? Date::strToDate($oNewsItem->onlineTo)->format('%d-%m-%Y %H:%M') : '') . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk Nieuwsitem" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oNewsItem->newsItemId . '"></a>';
            echo '<a class="action_icon delete_icon" title="Verwijder Nieuwsitem" onclick="return confirmChoice(\'' . $oNewsItem->title . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oNewsItem->newsItemId . '"></a>';
            echo '</td>';
            echo '</tr>';
        }
        if (count($aNewsItems) == 0) {
            echo '<tr><td colspan="6"><i>Er zijn geen Nieuwsitems weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
    <tfoot>
        <tr class="bottomRow">
            <td colspan="6">
                <form method="POST">
                    <?= generatePaginationHTML($iPageCount, $iCurrPage) ?>
                    <input type="hidden" name="setPerPage" value="1" />
                    <select name="perPage" onchange="$(this).closest('form').submit();">
                        <option value="">alle</option>
                        <option <?= $iPerPage == 10 ? 'SELECTED' : '' ?> value="10">10</option>
                        <option <?= $iPerPage == 25 ? 'SELECTED' : '' ?> value="25">25</option>
                        <option <?= $iPerPage == 50 ? 'SELECTED' : '' ?> value="50">50</option>
                        <option <?= $iPerPage == 100 ? 'SELECTED' : '' ?> value="100">100</option>
                    </select> per pagina
                </form>
            </td>
        </tr>
    </tfoot>
</table>

<?
# add ajax code for online/offline handling
$sOnlineOfflineJavascript = <<<EOT
<script>
    $("a.online_icon, a.offline_icon").click(function(e){
        $.ajax({
            type: "GET",
            url: this.href,
            data: "ajax=1",
            async: true,
            success: function(data){
                var dataObj = eval('(' + data + ')');

                /* On success */
                if(dataObj.success == true){
                    $("#newsItem_"+dataObj.newsItemId+"_online_0").hide(); // hide offline button
                    $("#newsItem_"+dataObj.newsItemId+"_online_1").hide(); // hide online button
                    $("#newsItem_"+dataObj.newsItemId+"_online_"+dataObj.online).css('display', 'inline-block'); // show button based on online value
                    if(dataObj.online == 0)    
                        showStatusUpdate("Blogitem offline gezet");
                    if(dataObj.online == 1)    
                        showStatusUpdate("Blogitem online gezet");                            
                }else{
                        showStatusUpdate("Blogitem niet gewijzigd");
                }
            }
        });
        e.preventDefault();
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sOnlineOfflineJavascript);
?>