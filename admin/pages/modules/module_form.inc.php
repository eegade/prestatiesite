<div id="topOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>
<div class="cf">
    <div class="contentColumn">
        <fieldset>
            <legend>Module <?= http_get("param1") ?></legend>
            <form method="POST" action="" class="validateForm">
                <input type="hidden" value="save" name="action" />
                <table class="withForm">
                    <tr>
                        <td class="withLabel" style="width: 160px;"><label for="name">Naam (URL) *</label></td>
                        <td><input id="name" class="{validate:{required:true}} autofocus" title="Vul de naam van de module" type="text" autocomplete="off" name="name" value="<?= $oModule->name ?>" /></td>
                        <td><span class="error"><?= $oModule->isPropValid("name") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="linkName">Link naam (tekst in menu) *</label></td>
                        <td><input id="linkName" class="{validate:{required:true}}" title="Vul de link naam (weergegeven in het menu)" type="text" autocomplete="off" name="linkName" value="<?= $oModule->linkName ?>" /></td>
                        <td><span class="error"><?= $oModule->isPropValid("linkName") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="parentModuleId">Hoofd module</label></td>
                        <td>
                            <select name="parentModuleId" id="parentModuleId">
                                <option value="">-- geen hoofd module --</option>
                                <?
                                foreach (ModuleManager::getModulesByFilter(array('showAll' => 1, 'parentModuleId' => -1)) AS $oParentModule) {
                                    if ($oParentModule->moduleId == $oModule->moduleId)
                                        continue; //skip current module
                                    echo '<option ' . ($oParentModule->moduleId == $oModule->parentModuleId ? 'SELECTED' : '') . ' value="' . $oParentModule->moduleId . '">' . $oParentModule->linkName . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="withLabel"><label for="order">Volgorde *</label></td>
                        <td><input id="order" class="{validate:{required:true}}" title="Vul de volgorde in" type="text" autocomplete="off" name="order" value="<?= $oModule->order ?>" /></td>
                        <td><span class="error"><?= $oModule->isPropValid("order") ? '' : 'Veld niet (juist) ingevuld' ?></span></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <input type="hidden" name="showInMenu" value="0" /><? // extra hidden field for workaround with checkboxes and _load function in controller        ?>
                            <input class="alignCheckbox" type="checkbox" id="showInMenu" name="showInMenu" <?= ($oModule->showInMenu ? 'CHECKED' : '') ?> value="1" /> <label for="showInMenu">Laat zien in het menu</label>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2">
                            <input type="submit" value="Opslaan" name="save" />
                        </td>
                    </tr>
                </table>
            </form>
        </fieldset>
    </div>
</div>
<div id="bottomOptions">
    <a class="backBtn" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>">Terug naar overzicht</a><span class="backBtnInfo"> (zonder opslaan)</span>
</div>