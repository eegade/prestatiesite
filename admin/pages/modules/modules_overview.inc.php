<table class="sorted">
    <thead>
        <tr class="topRow">
            <td colspan="8"><h2>Alle modules</h2><div class="right"><a class="addBtn textLeft" href="<?= ADMIN_FOLDER ?>/<?= http_get('controller') ?>/toevoegen" title="Nieuwe module toevoegen" alt="Nieuwe module toevoegen">Module toevoegen</a></div></td>
        </tr>
        <tr>
            <th>#</th>
            <th>Volgorde</th>
            <th>Link naam</th>
            <th>Naam</th>
            <th>Parent</th>
            <th>Show in menu</th>
            <th class="{sorter:false} nonSorted" style="width: 60px;">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?
        foreach ($aModules AS $oModule) {
            echo '<tr>';
            echo '<td>' . $oModule->moduleId . '</td>';
            echo '<td>' . $oModule->order . '</td>';
            echo '<td>' . $oModule->linkName . '</td>';
            echo '<td>' . $oModule->name . '</td>';
            echo '<td>' . ($oModule->getParent() ? $oModule->getParent()->linkName : '') . '</td>';
            echo '<td>' . ($oModule->showInMenu ? 'ja' : 'nee') . '</td>';
            echo '<td>';
            echo '<a class="action_icon edit_icon" title="Bewerk module" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/bewerken/' . $oModule->moduleId . '"></a>';
            echo '<a class="action_icon delete_icon" title="Verwijder module" onclick="return confirmChoice(\'module ' . $oModule->linkName . '\');" href="' . ADMIN_FOLDER . '/' . http_get('controller') . '/verwijderen/' . $oModule->moduleId . '"></a>';
            echo '</td>';
            echo '</tr>';
        }
        if (count($aModules) == 0) {
            echo '<tr><td colspan="8"><i>Er zijn geen modules weer te geven</i></td></tr>';
        }
        ?>
    </tbody>
</table>