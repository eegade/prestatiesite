<?php

# set document root
if (!defined('DOCUMENT_ROOT'))
    define("DOCUMENT_ROOT", '/home/prestatie/domains/dev.prestatiesite.nl/public_html');
require_once DOCUMENT_ROOT . '/inc/config.inc.php';

# create new TitterOAuth connection (edit the keys in the class)
$oTwitterOAuth = new TwitterOAuth();

# get the tweets from Twitter
$aTweets = $oTwitterOAuth->get('statuses/user_timeline', array());

# save the tweets
if (is_array($aTweets))
    TweetManager::saveTweets($aTweets);
?>
