<?php

set_time_limit(0);

// set docuemnt root
if (!defined('DOCUMENT_ROOT')) {
    define("DOCUMENT_ROOT", '/home/prestatie/domains/prestatiesite.nl/public_html');
}
include_once DOCUMENT_ROOT . '/inc/config.inc.php';

if (!BB_WITH_ADVANCED_SITEMAP) {
    Debug::logError('0', 'CRON ERROR', __FILE__, __LINE__, 'Kon `sitemap uitgebreid` cron niet uitvoeren omdat de module uit staat', Debug::LOG_IN_EMAIL);
    die;
}

$sReferenceName = 'advancedSitemap';

// check if cron lock is set
if (CronManager::isLocked($sReferenceName)) {
    if (DEBUG)
        echo 'Cron locked `' . $sReferenceName . '`';
    die;
}

// check if cron lock can be set
if (!CronManager::setCronLock($sReferenceName)) {
    if (DEBUG)
        echo 'Kon cron lock niet setten `' . $sReferenceName . '`';
    else
        Debug::logError('0', 'CRON ERROR', __FILE__, __LINE__, 'Kon cron lock niet setten `' . $sReferenceName . '`', Debug::LOG_IN_EMAIL);
    die;
}

// log cron starting
CronManager::log('cron started', $sReferenceName . '.log');

$aPages = PageManager::getPagesByFilter(array('lastHourOnly' => true), null, 0, $iFoundRows, array('IFNULL(`p`.`modified`, `p`.`created`)' => 'DESC'));
if (!empty($aPages)) {
    pingSitemap('pages');
}

if (BB_WITH_NEWS) {
    $aNewsItems = NewsItemManager::getNewsItemsByFilter(array('lastHourOnly' => true), null, 0, $iFoundRows, array('IFNULL(`ni`.`modified`, `ni`.`created`)' => 'DESC'));
    if (!empty($aNewsItems)) {
        pingSitemap('news');
    }
    if (BB_WITH_NEWS_CATEGORIES) {
        $aCategories = NewsItemCategoryManager::getNewsItemCategoriesByFilter(array('lastHourOnly' => true), null, 0, $iFoundRows, array('IFNULL(`nic`.`modified`, `nic`.`created`)' => 'DESC'));
        if (!empty($aCategories)) {
            pingSitemap('newscategories');
        }
    }
}

if (BB_WITH_PHOTO_ABLUMS) {
    $aPhotoAlbums = PhotoAlbumManager::getPhotoAlbumsByFilter(array('lastHourOnly' => true), null, 0, $iFoundRows, array('IFNULL(`pa`.`modified`, `pa`.`created`)' => 'DESC'));
    if (!empty($aPhotoAlbums)) {
        pingSitemap('photoalbums');
    }
}

if (BB_WITH_CATALOG) {
    $aProducts = CatalogProductManager::getProductsByFilter(array('lastHourOnly' => true), null, 0, $iFoundRows, array('IFNULL(`cp`.`modified`, `cp`.`created`)' => 'DESC'));
    if (!empty($aProducts)) {
        pingSitemap('catalog');
    }
    $aCategories = CatalogProductCategoryManager::getProductCategoriesByFilter(array('lastHourOnly' => true), null, 0, $iFoundRows, array('IFNULL(`cpc`.`modified`, `cpc`.`created`)' => 'DESC'));
    if (!empty($aCategories)) {
        pingSitemap('catalogcategories');
    }
}


if (BB_WITH_AGENDA_ITEMS) {
    $aAgendaItems = AgendaItemManager::getAgendaItemsByFilter(array('lastHourOnly' => true), null, 0, $iFoundRows, array('IFNULL(`ai`.`modified`, `ai`.`created`)' => 'DESC'));
    if (!empty($aAgendaItems)) {
        pingSitemap('agendaItems');
    }
}

/**
 * do request to google url to `ping` the specific sitemap and log results
 * @global string $sReferenceName
 * @param type $sModuleName
 */
function pingSitemap($sModuleName) {
    global $sReferenceName;
    $ch = curl_init('http://www.google.com/webmasters/sitemaps/ping?sitemap=' . CLIENT_HTTP_URL . '/sitemap-' . $sModuleName . '.xml.gz');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $sResponse = curl_exec($ch);

    if (curl_getinfo($ch, CURLINFO_HTTP_CODE) === 200) {
        if (DEBUG) {
            echo 'pinged `' . $sModuleName . '`<br />';
        }
        CronManager::log('pinged `' . $sModuleName . '`', $sReferenceName . '.log');
    } else {
        if (DEBUG) {
            echo 'ping error `' . $sModuleName . '`<br />';
            echo $sResponse;
        } else {
            Debug::logError('0', 'Kon module `' . $sModuleName . '` niet pingen voor `sitemap uitgebreid`', __FILE__, __LINE__, $sResponse, Debug::LOG_IN_EMAIL);
        }
        CronManager::log('ping error `' . $sModuleName . '`', $sReferenceName . '.log');
    }
}

// check if cron can be unlocked
if (!CronManager::unsetCronLock($sReferenceName)) {
    if (DEBUG)
        echo 'Kon cron lock niet verwijderen `' . $sReferenceName . '`';
    else
        Debug::logError('0', 'CRON ERROR', __FILE__, __LINE__, 'Kon cron lock niet verwijderen `' . $sReferenceName . '`', Debug::LOG_IN_EMAIL);
    die;
}

// log cron ending
CronManager::log('cron ended', $sReferenceName . '.log');
?>