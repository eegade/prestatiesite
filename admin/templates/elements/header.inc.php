<header>
    <div id="loggedinUser">
        <?= $oCurrentUser ? 'Ingelogd als: ' . $oCurrentUser->name : 'U dient eerst in te loggen' ?>
    </div>
    <div id="statusUpdate"><noscript><?= $oPageLayout->sStatusUpdate ?></noscript></div>
    <div id="logout">
        <? if ($oCurrentUser){ ?>
            <a href="/" title="Website" target="_blank">Website</a>
            <span> || </span>
            <a href="?logout=1">Uitloggen</a>
        <? } ?>
    </div>
</header>