<div class="hide">
    <div id="editFileForm">
        <h2>Bestand titel wijzigen</h2>
        <form onsubmit="saveFile(this); return false;" method="POST" action="#">
            <input type="hidden" name="mediaId" value="" />
            <label for="fileTitle">Titel</label><br />
            <input type="text" class="default" id="fileTitle" name="title" value="" /><br />
            <input type="submit" name="" value="Titel opslaan" />
        </form>
    </div>
    <a id="editFileFormLink" class="fancyBoxLink" href="#editFileForm"></a>
</div>