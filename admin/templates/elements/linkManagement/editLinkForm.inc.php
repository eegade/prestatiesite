<div class="hide">
    <div id="editLinkForm">
        <h2>Link titel wijzigen</h2>
        <form onsubmit="saveLink(this); return false;" method="POST" action="#">
            <input type="hidden" name="mediaId" value="" />
            <label for="linkTitle">Titel</label><br />
            <input type="text" class="default" id="linkTitle" name="title" value="" /><br />
            <label for="linkLink">URL</label><br />
            <input type="text" class="default" id="linkLink" name="link" value="" /><br />            
            <input type="submit" name="" value="Link opslaan" />
        </form>
    </div>
    <a id="editLinkFormLink" class="fancyBoxLink" href="#editLinkForm"></a>
</div>