<div class="hide">
    <div id="editYoutubeLinkForm">
        <h2>Youtube titel wijzigen</h2>
        <form onsubmit="saveYoutubeLink(this); return false;" method="POST" action="#">
            <input type="hidden" name="mediaId" value="" />
            <label for="youtubeLinkTitle">Titel</label><br />
            <input type="text" class="default" id="youtubeLinkTitle" name="title" value="" /><br />
            <label for="youtubeLinkLink">URL</label><br />
            <input type="text" class="default" id="youtubeLinkLink" name="link" value="" /><br />            
            <input type="submit" name="" value="Youtube video link opslaan" />
        </form>
    </div>
    <a id="editYoutubeLinkFormLink" class="fancyBoxLink" href="#editYoutubeLinkForm"></a>
</div>