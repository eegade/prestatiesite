<?php
/*
 * $aProducten
 * Alle producten als XML wegschrijven
 */
header("Content-Type:text/xml;charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<productFeed version="1.0" timestamp="<?= strftime("%Y-%m-%d %H:%M:%S") ?>">
    <? foreach ($aProducts as $oProduct) { ?>
        <product>
            <ID><?= $oProduct->catalogProductId; ?></ID>
            <name><![CDATA[<?= $oProduct->name . ' | ' . $oProduct->getBrand()->name; ?>]]></name>
            <? if(DeliveryMethodManager::getDeliveryMethodById(Settings::get('defaultDeliveryMethod'))) { ?>
                <deliveryCost><![CDATA[ <?= number_format(DeliveryMethodManager::getDeliveryMethodById(Settings::get('defaultDeliveryMethod'))->price,2,'.','') ?> ]]></deliveryCost>
            <? } ?>
            <lowestPrice><?= ($oProduct->getReducedPrice(true) > 0 ? number_format($oProduct->getReducedPrice(true),2,'.','') : number_format($oProduct->getSalePrice(true),2,'.','')); ?></lowestPrice>
            <description>
                <![CDATA[  <?= $oProduct->description; ?> ]]>
            </description>
            <productURL>
                <![CDATA[ <?= CLIENT_HTTP_URL  . $oProduct->getUrlPath(); ?> ]]>
            </productURL>
            <?
                foreach ($oProduct->getImages() as $key => $oProductImage) { 
                    if ($oProductImageFile = $oProductImage->getImageFileByReference('detail')) { 
                        if ($key==0) {
                            echo "<imageURL>";
                        }else{
                            echo "<additional>";
                        } 
                        ?>
                            <![CDATA[ <?= CLIENT_HTTP_URL . $oProductImageFile->link; ?> ]]>
                        <? 

                        if ($key==0) {
                            echo "</imageURL>";
                        }else{
                            echo "</additional>";
                        }
                    }
                }
            ?>

            <categories>
            <? 
                readSubcategoriesProduct($oProduct,1);
            ?>
            </categories>
                           
            <? foreach ($oProduct->getColorSizeRelations('inStock') as $oProductSizeColorRelation) { ?>
                <variation>
                    <? if ($oProduct->getProductType()->withSizes){ ?>
                        <size><![CDATA[ <?= $oProductSizeColorRelation->getSize()->name ?> ]]></size>
                    <? } ?>
                    <? if ($oProduct->getProductType()->withColors){ ?>
                        <color><![CDATA[ <?= $oProductSizeColorRelation->getColor()->name ?> ]]></color>
                    <? } ?>
                    <stock><![CDATA[ <?= $oProductSizeColorRelation->stock === null ? 9999 : $oProductSizeColorRelation->stock ?> ]]></stock>
                    <!-- Extra price is always with taxes -->
                    <price><?= ($oProduct->getReducedPrice(true) > 0 ? number_format(($oProduct->getReducedPrice(true) + $oProductSizeColorRelation->extraPrice),2,'.','') : number_format(($oProduct->getSalePrice(true) + $oProductSizeColorRelation->extraPrice),2,'.','')); ?></price>
                </variation>    
            <? }?>
        </product>
        <?
    ;}
    ?>
</productFeed>

<!-- Functions -->
<?php 
    function readSubcategoriesProduct(CatalogProduct $oProduct,$iLevel,$iParentCategoryId = NULL,$sText = NULL){
        $aFilter['level'] = $iLevel;
        $aFilter['parentCatalogProductCategoryId'] = $iParentCategoryId;
        $aCategories = CatalogProductManager::getCategoriesByFilter($oProduct->catalogProductId,$aFilter);

        $sTextPrint = '';
        // If I don't find more categories, It means that this it is the last subcategory and I have to print
        if (empty($aCategories)){
            return false;
        }else{
            foreach ($aCategories AS $oCategory){
                if ($iLevel === 1){
                    $sTextPrint = "<category><![CDATA[";
                }else{
                    $sTextPrint = $sText. ">";
                }
                    
                // I add the new category
                $sTextPrint .=  $oCategory->name;

                // If the function returns "false", I write the whole string
                if (!readSubcategoriesProduct($oProduct,$iLevel + 1,$oCategory->catalogProductCategoryId,$sTextPrint)){
                    $sTextPrint .= "]]></category>";
                    echo $sTextPrint;  
                }
            }
            return true;
        }
    }
?>