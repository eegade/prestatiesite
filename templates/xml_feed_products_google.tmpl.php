<?php
set_time_limit(0);
/* echt een XML van maken */
/*
 * $aProducten
 * Alle producten als XML wegschrijven
 */
header("Content-Type:text/xml;charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>

<channel version="1.0" timestamp="<?= strftime("%Y-%m-%d %H:%M:%S") ?>" xmlns:g="http://base.google.com/ns/1.0">
    <title><?= CLIENT_NAME ?></title>
    <link><?= CLIENT_HTTP_URL ?></link>
    <description>Alle producten voor <?= CLIENT_NAME ?></description>

    <? foreach ($aProducts as $oProduct) { ?>
        <!--- I write a new product for each available color/size combination -->
        <? foreach ($oProduct->getColorSizeRelations() as $oProductColorSize) { 
            $aFilter['catalogProductColorId'] = $oProductColorSize->catalogProductColorId;
            $aFilter['catalogProductId'] = $oProduct->catalogProductId;
            $aProductImageColorRelations = CatalogProductImageRelationManager::getCatalogProductImageRelationsByFilter($aFilter);
        ?>
            <item>
                <!--- Basic products information -->
                <g:id><![CDATA[ <?= $oProduct->catalogProductId ?><?= $oProduct->getProductType()->withColors ? '-' . $oProductColorSize->getColor()->name : '' ?><?= $oProduct->getProductType()->withSizes ? '-' . $oProductColorSize->getSize()->name : '' ?> ]]></g:id>
                <title><![CDATA[<?= $oProduct->name . ' | ' . $oProduct->getBrandName(); ?>]]></title>
                <description>
                    <![CDATA[ <?= $oProduct->getBrandName(); ?>
                    <?= "-" ?>
                    <?= strip_tags($oProduct->description); ?> ]]>
                </description>
                <g:google_product_category><![CDATA[ <?= $oProduct->googleCategory ?> ]]></g:google_product_category>
                
                <? 
                    readSubcategoriesProduct($oProduct,1);
                ?>
 
                <link>
                    <![CDATA[ <?= CLIENT_HTTP_URL . $oProduct->getUrlPath(); ?> ]]>
                </link>
                
                <!-- I set the main image and all the additional images -->
                <? if ($oProduct->getProductType()->withColors) {
                    // If there aren't ImageColor Relation objects, I set the firs image I get
                    if (!empty($aProductImageColorRelations)){
                        foreach ($aProductImageColorRelations as $key => $oProductImageColorRelation) { 
                            $oImage = $oProductImageColorRelation->getImage();
                            $oImageFile = $oImage->getImageFileByReference('detail');

                            if ($key==0) {
                                echo "<g:image_link>";
                            }else{
                                echo "<g:additional_image_link>";
                            } 
                            ?>
                                <![CDATA[ <?= CLIENT_HTTP_URL.$oImageFile->link ?> ]]>
                            <? 
                            if ($key==0) {
                                echo "</g:image_link>";
                            }else{
                                echo "</g:additional_image_link>";
                            }

                        }
                    } else {
                        // I use this, only if the product has at least one image
                        if($oProductImage = $oProduct->getImages('first-online')){

                            $oProductImageFile = $oProductImage->getImageFileByReference('detail');

                            echo "<g:image_link>";
                            ?>
                                <![CDATA[ <?= CLIENT_HTTP_URL.$oProductImageFile->link?> ]]>
                            <? 
                            echo "</g:image_link>";
                        }
                    }
                    
                // If the product doesn't have different colors, I show all the images  
                } else {                                   
                    foreach ($oProduct->getImages() as $key => $oProductImage) { 
                        if ($oProductImageFile = $oProductImage->getImageFileByReference('detail')) { 
                            if ($key==0) {
                                echo "<g:image_link>";
                            }else{
                                echo "<g:additional_image_link>";
                            } 
                            ?>
                                <![CDATA[ <?= CLIENT_HTTP_URL.$oProductImageFile->link?> ]]>
                            <? 

                            if ($key==0) {
                                echo "</g:image_link>";
                            }else{
                                echo "</g:additional_image_link>";
                            }
                        }
                    }
                } ?>
                                
                <g:condition>new</g:condition>   
                
                <!-- Availability and price -->
                <g:availability>in stock</g:availability>
                <g:price><?= number_format(($oProduct->getSalePrice(true) + $oProductColorSize->extraPrice), 2, '.', '').' EUR'; ?></g:price>
                <g:sale_price><?= $oProduct->getReducedPrice(true) ? number_format($oProduct->getReducedPrice(true), 2, '.', '').' EUR' : '' ?></g:sale_price>
                
                <!-- Unique product identifiers -->
                <g:brand><![CDATA[ <?= $oProduct->getBrandName(); ?> ]]></g:brand>
                <g:mpn><![CDATA[ <?= $oProductColorSize->getMPN() ?> ]]></g:mpn>
                
                <!-- Products clothing category -->
                <?
                    if($oProduct->getProductType()->withGenders)
                        echo  "<g:gender>".$oProduct->gender."</g:gender>";
                ?>
                
                <?
                /* switch {
                 *      case 'adult':
                 *          <g:age_group>adult</g:age_group>
                 *          break;
                 *      case 'girl':
                 *          <g:age_group>kids</g:age_group>
                 *          break;
                 * 
                 * }
                 */
                ?>
                <? 
                    if ($oProduct->getProductType()->withColors){
                        echo "<g:color><![CDATA[".$oProductColorSize->getColor()->name." ]]></g:color>";
                    }
                    if ($oProduct->getProductType()->withSizes){
                        echo "<g:size><![CDATA[ ". $oProductColorSize->getSize()->name ." ]]></g:size>";
                    }
                ?>
               
                <!-- Products Variants -->
                <g:item_group_id><?= $oProduct->catalogProductId ?></g:item_group_id>
                
                <!-- Delivery Prices -->
                <g:shipping>
                   <g:country>NL</g:country>
                   <g:service><![CDATA[ <?= DeliveryMethodManager::getDeliveryMethodById(Settings::get('defaultDeliveryMethod'))->name ?> ]]></g:service>
                   <g:price><![CDATA[ <?= number_format(DeliveryMethodManager::getDeliveryMethodById(Settings::get('defaultDeliveryMethod'))->price,2,'.','') ?> EUR ]]></g:price>
                </g:shipping>
            </item>
        <? }?>
    <? ;}?>
</channel>

<?php 
    function readSubcategoriesProduct(CatalogProduct $oProduct,$iLevel,$iParentCategoryId = NULL,$sText = NULL){
        $aFilter['level'] = $iLevel;
        $aFilter['parentCatalogProductCategoryId'] = $iParentCategoryId;
        $aCategories = CatalogProductManager::getCategoriesByFilter($oProduct->catalogProductId,$aFilter);

        $sTextPrint = '';
        // If I don't find more categories, It means that this it is the last subcategory and I have to print
        if (empty($aCategories)){
            return false;
        }else{
            foreach ($aCategories AS $oCategory){
                if ($iLevel === 1){
                    $sTextPrint = "<g:product_type><![CDATA[";
                }else{
                    $sTextPrint = $sText. ">";
                }
                    
                // I add the new category
                $sTextPrint .=  $oCategory->name;

                // If the function returns "false", I write the whole string
                if (!readSubcategoriesProduct($oProduct,$iLevel + 1,$oCategory->catalogProductCategoryId,$sTextPrint)){
                    $sTextPrint .= "]]></g:product_type>";
                    echo $sTextPrint;  
                }
            }
            return true;
        }
    }
?>