<nav class="column-25 float-left sub-navigation">
    <?
    $aUrl = array();
    $aUrl[1] = '/' . http_get('controller'); // set level 1 becaus of submenu only

    function makeSubListTree($aPages) {
        global $aUrl;
        if (count($aPages) > 0) {
            echo '<ul>';
            foreach ($aPages AS $oPage) {
                $iLevel = $oPage->level;
                $aUrl[$iLevel] = $aUrl[$iLevel - 1] . '/' . http_get('param' . ($iLevel - 1));
                $bIsActive = $aUrl[$iLevel] == $oPage->getUrlPath();

                // check if controller equals page urlPath be aware of special treathment for homepage!!
                echo '<li><a class="' . ($bIsActive ? 'active' : '' ) . (count($oPage->getSubPages()) > 0 ? ' withSubPages' : '') . '" href="' . $oPage->getUrlPath() . '">' . $oPage->getShortTitle() . '</a>';
                if ($bIsActive) {
                    makeSubListTree($oPage->getSubPages()); //call function recursive
                }
                echo '</li>';
            }
            echo '</ul>';
        }
    }

    makeSubListTree($oPageForMenu->getSubPages());
    ?>
</nav>
<div class="column-width float-left">&nbsp;</div>