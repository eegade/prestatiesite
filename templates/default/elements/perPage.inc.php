<div class="perPage">
    <form method="POST">
        <input type="hidden" name="setPerPage" value="1" />
        <select name="perPage" onchange="$(this).closest('form').submit();">
            <option value="">alle</option>
            <option <?= $iPerPage == 6 ? 'SELECTED' : '' ?> value="6">6</option>
            <option <?= $iPerPage == 9 ? 'SELECTED' : '' ?> value="9">9</option>
            <option <?= $iPerPage == 12 ? 'SELECTED' : '' ?> value="12">12</option>
            <option <?= $iPerPage == 15 ? 'SELECTED' : '' ?> value="15">15</option>
            <option <?= $iPerPage == 30 ? 'SELECTED' : '' ?> value="30">30</option>
            <option <?= $iPerPage == 60 ? 'SELECTED' : '' ?> value="60">60</option>
        </select> per pagina
    </form>
</div>