<!-- Products -->
<br class="clear">
<div class="module-container">
    <p class="module-producten module-container-title main-color"><a href="/producten">Artikelen</a></p>
    <?
    $iProductCounter = 0;
    foreach ($aProducts AS $oProduct) {
        $iProductCounter++;
        ?>

        <div class="product-block column-25 float-left cf">
            <div class="product-block-column">
                <div class="product-name"><a href="<?= $oProduct->getUrlPath() ?>"><?= _e($oProduct->name) ?></a></div>
                <div class="product-image"><?
                    $oImage = $oProduct->getImages('first-online');
                    if (!empty($oImage)) {
                        $oImageFileCrop = $oImage->getImageFileByReference('crop_overview');
                        $oImageFileOri = $oImage->getImageFileByReference('detail');
                        if (!empty($oImageFileCrop) && !empty($oImageFileOri))
                            echo '<a href="' . $oProduct->getUrlPath() . '"><img src="' . $oImageFileCrop->link . '" alt="' . _e($oImageFileCrop->title) . '" ' . $oImageFileCrop->imageSizeAttr . ' /></a>';
                    } else {
                        echo '<a href="' . $oProduct->getUrlPath() . '"><img src="/images/layout/shop-no-image.jpg" style="width:100%" alt="No image"></a>';
                    }
                    ?>
                    <? if ($oProduct->getReducedPrice(true, null, null, true)){ ?>
                        <div class="product-original-price">
                            <?= decimal2valuta($oProduct->getSalePrice(true, null, null, true,false)) ?>
                        </div>
                    <? } ?>
                    <a href="<?= $oProduct->getUrlPath() ?>">
                        <div class="product-price">
                            <?= decimal2valuta($oProduct->getSalePrice(true)) ?>
                        </div>
                    </a>
                </div>
                <div class="product-content">
                    <?= firstXCharacters($oProduct->description, 50) ?>
                </div>
                <a href="<?= $oProduct->getUrlPath() ?>" class="read-more">Meer informatie</a>
            </div>
        </div>
        <?
        if ($iProductCounter < 4) {
            echo '<div class="column-width float-left">&nbsp;</div>';
        }
    }
    ?>
</div>