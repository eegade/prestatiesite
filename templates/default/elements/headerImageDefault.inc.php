<section class="<?= BB_TYPE === 'wide' ? '' : 'page-header-container' ?>">
    <?
    if (!empty($oHeaderImage) && ($oCropHeader = $oHeaderImage->getImageFileByReference('crop_header')) && ($oCropSmall = $oHeaderImage->getImageFileByReference('crop_small'))) {
        ?>
        <div class="page-header-image">
            <span data-picture data-alt="<?= $oCropHeader->title ?>">
                <span data-src="<?= $oCropHeader->link ?>"></span>
                <span data-src="<?= $oCropSmall->link ?>" data-media="(max-width: 767px)"></span>
            </span>
        </div>
    <? } else { ?>
        <div class="page-header-noimage"></div>
    <? } ?>
</section>