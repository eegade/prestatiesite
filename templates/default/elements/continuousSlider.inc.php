<? if (!empty($aContinuousSliderItems)) { ?>
    <br class="clear"><br/>
    <div class="logos">
        <ul class="continuousSlider">
            <?
            foreach ($aContinuousSliderItems as $oContinuousSliderItem) {
                $oImage = $oContinuousSliderItem->getImage();
                ?>
                <?
                if (!empty($oImage)) {
                    $oImageFileCrop = $oImage->getImageFileByReference('crop_small');
                    $oImageFileOri = $oImage->getImageFileByReference('detail');

                    if (!empty($oContinuousSliderItem->link)) {
                        if (!empty($oImageFileCrop) && !empty($oImageFileOri))
                            echo '<li><a href="' . $oContinuousSliderItem->link . '" target="_blank"><img src = "' . $oImageFileCrop->link . '" alt = "' . _e($oImageFileCrop->title) . '" ' . $oImageFileCrop->imageSizeAttr . ' /></a></li>';
                    } else {
                        if (!empty($oImageFileCrop) && !empty($oImageFileOri))
                            echo '<li><img src = "' . $oImageFileCrop->link . '" alt = "' . _e($oImageFileCrop->title) . '" ' . $oImageFileCrop->imageSizeAttr . ' /></li>';
                    }
                }
                ?>
                <?
            }
            ?>
        </ul>
    </div>
<? } ?>