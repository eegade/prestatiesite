<!-- Nieuws -->
<br class="clear">
<div class="module-container clear">
    <p class="module-container-title main-color">Het laatste nieuws</p>
    <?
    $iNewsItemCounter = 0;
    foreach ($aNewsItems AS $oNewsItem) {
        $iNewsItemCounter++;
        if ($iNewsItemCounter != 1) {
            ?>
            <div class="column-width float-left">&nbsp;</div>
            <?
        }
        ?>
        <div class="newsitem-block column-25 float-left">
            <div class="newsitem-content">
                <h3><?= firstXCharacters($oNewsItem->title, 50) ?></h3>
                <div class="newsdate-category clear">
                    <span class="float-left"><?= strftime('%d %B %Y', strtotime($oNewsItem->date)) ?></span>
                </div>
                <div class="newsitem-summary">
                    <?= firstXCharacters($oNewsItem->intro, 200) ?>
                </div>
                <a href="<?= $oNewsItem->getUrlPath() ?>" title="<?= _e($oNewsItem->title) ?>" class="read-more">Lees meer</a>
            </div>
        </div>
    <? } ?>
</div>