<?
/*
 * Mini Account content
 */
if (BB_WITH_CUSTOMERS) {
    ?>
    <a class="inlog-button hide-on-mobile unselectable"><div style="float:left;"><img src="/images/layout/motoren/login-icon.png" width="35" height="25" alt="Login"></div><div class="inlog-button-content"><?= !empty($oCurrentCustomer) ? $oCurrentCustomer->firstName : 'Aanmelden' ?></div></a>
    <div class="login-column">
        <div class="login-column-head"><a href="/account">Uw account</a><a class="delete-button-small" onclick="closeMiniAccount();
                    return false;"></a></div>
        <div class="login-column-content">
            <div>
                <? if (empty($oCurrentCustomer)) { ?>
                    <form method="POST" action="/account" class="validateForm">
                        <input type="hidden" value="login" name="action" />
                        <? include TEMPLATES_FOLDER . '/elements/errorBox.inc.php'; ?>
                        <table style="width:100%">
                            <tr>
                                <td style="width: 100px; font-size:12px;"><label>E-mail *</label></td>
                                <td><input class="required" title="Vul uw e-mailadres in" id="mini-account-email" type="email" name="email" value="" /></td>
                            </tr>
                            <tr>
                                <td><label>Wachtwoord *</label></td>
                                <td><input class="required" title="Vul uw wachtwoord in" id="mini-account-loginPassword" type="password" name="password" value="" /></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <input class="read-more" type="submit" value="Inloggen" name="login" />
                                </td>
                            </tr>
                        </table>
                    </form>
                    <div><a href="/account/wachtwoord-vergeten">Wachtwoord vergeten?</a></div>
                    <div>Nog geen account? <a href="/account">Maak een account aan</a></div>
                <? } else { ?>
                    <a href="/account">Mijn gegevens</a><br />
                    <a href="/account/logout">Uitloggen</a>
                <? } ?>
            </div>
        </div>
    </div>
<? } ?>