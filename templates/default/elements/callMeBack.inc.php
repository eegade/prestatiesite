<div class="float-left clearfix call-me-back">    
    <div class="phone-icon"></div><h2>Wilt u worden teruggebeld</h2>
    <hr/>    

    <div class="statusUpdate callMeBackSatus"></div>  
    <? include TEMPLATES_FOLDER . '/elements/errorBox.inc.php'; ?>
    <p>Geef uw naam en telefoonnummer en wij bellen u snel terug.</p>

    <form class="validateForm" method="POST">
        <? include TEMPLATES_FOLDER . '/elements/errorBox.inc.php'; ?>
        <fieldset>           
            <input type="hidden" name="action" value="contact" />
            <input type="text" class="required" name="contactName" placeholder="Naam" title="Uw naam is niet (juist) ingevuld" value="" />
            <input type="text" class="required" name="contactPhone" placeholder="Telefoonnummer" title="Uw telefoonnummer is niet (juist) ingevuld" value="" />
            <input type="submit" class="read-more submit" name="send" value="Verzend"/>
            <div style="display: none;">
                <div>
                    <label for="rumpelstiltskin-empty">Dit veld niet wijzigen (SPAM check)</label>
                    <input autocomplete="off" type="text" name="rumpelstiltskin-empty" value="" />
                </div>
                <div>
                    <label for="rumpelstiltskin-filled">Dit veld niet wijzigen (SPAM check)</label>
                    <input autocomplete="off" type="text" name="rumpelstiltskin-filled" value="repelsteeltje" />
                </div>
            </div> 
        </fieldset>
        
    </form>
</div>
<?
$sStatusMsg = http_session('callMeBackSatus', '');
unset($_SESSION['callMeBackSatus']);
$sJavascript = <<<EOT
<script>
      var statusMsg = '$sStatusMsg';
      
      if (statusMsg.length > 0) {
        showStatusUpdate(statusMsg, $('.callMeBackSatus'));
      }
        
</script>
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);
?>