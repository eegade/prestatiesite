<div class="float-right clearfix twitter-feed"> 
    <div class="tweet-heading">
        <div class="tweet-icon"></div>
        <h2><span id="twitterClientName">Client 1</span> op Twitter @<span id="clientTwitter">client1</span></h2>
    </div> 
    <div id="twitter-slider">     
        <? foreach ($aTweets as $oTweet) { ?>
        <div class="tweet cf">            
            <span class="tweet-date"><?=$oTweet->created_at?></span>
            <p><?= $oTweet->text ?></p>        
        </div>
        <? } ?>       
    </div>
</div>

<?
$sTwitterClients = '';

foreach($aTweets as $oTweet) {
    $sTwitterClients .= ' "'.$oTweet->name.'", "'.$oTweet->screen_name.'",';
}
$sTwitterClients = substr($sTwitterClients, 0, strlen($sTwitterClients) - 1);
$sJavascript = <<<EOT
<script>
    var tweetIndex = 0; 
    var twitterClients = [$sTwitterClients];
           
    $('#twitter-slider').bxSlider({
        auto: false,
        random: false,
        pager: false
    });
    $('#twitterClientName').html(twitterClients[tweetIndex]);
        $('#clientTwitter').html(twitterClients[tweetIndex + 1]);
    $('.bx-next').click(function () {
        tweetIndex+=2;
        
        if (tweetIndex > twitterClients.length) {
            tweetIndex = 0;
        }
        
        $('#twitterClientName').html(twitterClients[tweetIndex]);
        $('#clientTwitter').html(twitterClients[tweetIndex + 1]);
    });
        
    $('.bx-prev').click(function () {
        tweetIndex-=2;
        
        if (tweetIndex < 0) {
            tweetIndex =  twitterClients.length - 2;
        }
        
        $('#twitterClientName').html(twitterClients[tweetIndex]);
        $('#clientTwitter').html(twitterClients[tweetIndex + 1]);
    });        
</script>
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);
?>