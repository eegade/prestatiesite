<!-- Bikes -->
<br class="clear">
<div class="module-container">
    <p class="module-container-title main-color"><a href="/motoren">Actuele aanbod motoren</a></p>
    <?
    $iBikeCounter = 0;
    foreach ($aBikes AS $oBike) {
        $iBikeCounter++;
        ?>
        <div class="column-33 float-left bike-column">
            <h3 class="bike-column-title"><?= $oBike->title ?></h3>
            <a title="<?= $oBike->title ?>" href="/motoren/<?= $oBike->bikeId ?>/<?= prettyUrlPart($oBike->title) ?>">
                <div class="bike-crop-homepage"><img class="bike-overview-column-image" style="width: 100%" alt="<?= $oBike->title ?>" src="<?= !empty($oBike->imageFancyboxImage) ? $oBike->imageFancyboxImage : '/images/layout/motoren/motoren-no-image.jpg' ?>"/></div>
                <span style="position: absolute; bottom: 0px; left:0; font-size: 12px;" class="read-more">Details bekijken</span>
                
            </a>
        </div>
        <?
        if ($iBikeCounter < 3) {
            echo '<div class="column-width float-left">&nbsp;</div>';
        }
    }
    ?>
</div>