<?
/*
 * the content for the mini Basket
 */

$iCountOrderProducts = (empty($_SESSION['oBasket']) ? 0 : $_SESSION['oBasket']->countProducts());
$fSubTotalPriceWithoutTaxForMiniBasket = $_SESSION['oBasket']->getSubtotalProducts(false);
$fSubTotalPriceWithTaxForMiniBasket = $_SESSION['oBasket']->getSubtotalProducts(true);
$fTaxPriceForMiniBasket = round($fSubTotalPriceWithTaxForMiniBasket, 2) - round($fSubTotalPriceWithoutTaxForMiniBasket, 2);
?>
<div id="shopping-cart">
    <div class="shopping-cart-head"><a href="/winkelwagen" title="Ga naar uw winkelwagen">Uw winkelwagen</a><a class="delete-button-small" onclick="closeMiniBasket();
            return false;"></a></div>
        <? if ($iCountOrderProducts > 0) { ?>
        <div class="shopping-cart-content">
            <div class="orderStatusUpdate statusUpdate"></div>
            <div id="miniBasketProducts">
                <?
                $i = 0;
                foreach ($_SESSION['oBasket']->getProducts() as $oOrderProduct) {
                    $i++;
                    ?>
                    <div class="product cf" <?= $i == 1 ? 'style="border: none;"' : '' ?>>
                        <?= $oOrderProduct->amount ?> x <a class="productName" href="<?= $oOrderProduct->getProduct()->getUrlPath() ?>"><?= firstXCharacters($oOrderProduct->brandName . ' ' . $oOrderProduct->productName, 37) ?></a>
                        <div>
                            <form style="float:right;" class="removeProductFromBasket" action="/winkelwagen" method="post">
                                <input type="hidden" name="catalogProductId" value="<?= $oOrderProduct->catalogProductId ?>" />
                                <input type="hidden" name="catalogProductSizeId" value="<?= $oOrderProduct->catalogProductSizeId ?>" />
                                <input type="hidden" name="catalogProductColorId" value="<?= $oOrderProduct->catalogProductColorId ?>" />
                                <input class="delete-button-small" type="submit" name="removeProductFromBasket" value="" />
                                <div class="orderStatusUpdate statusUpdate"></div>
                            </form>
                        </div>
                        <div>
                            <? if ($oOrderProduct->getProduct()->getProductType()->withSizes || $oOrderProduct->getProduct()->getProductType()->withColors) { ?>
                                <span class="size-color">
                                    <?= $oOrderProduct->getProduct()->getProductType()->withSizes ? '| ' . $oOrderProduct->productSizeName : '' ?>
                                    <?= $oOrderProduct->getProduct()->getProductType()->withColors ? '| ' . $oOrderProduct->productColorName : '' ?> |
                                </span>
                            <? } ?>
                            <span class="price"><?= decimal2valuta($oOrderProduct->getSubTotalPrice(true)) ?></span>
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>

        <div class="shopping-cart-price">
            <div>
                <span>Subtotaal <?= decimal2valuta($fSubTotalPriceWithTaxForMiniBasket) ?></span>
            </div>
            <div class="actions clearfix">
                <?
                if ($iCountOrderProducts > 0 && $fSubTotalPriceWithTaxForMiniBasket > 0) {
                    echo '<a style="width: 95%;" class="read-more" href="/winkelwagen">Bestellen</a>';
                }
                ?>
            </div>
        </div>
        <?
    } else {
        echo '<div class="shopping-cart-content">Geen producten in uw winkelmandje</div>';
    }
    ?>
</div>