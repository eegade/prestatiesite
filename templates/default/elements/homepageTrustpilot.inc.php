<?php
    // Get the JSON feed and gzunpack
    if(!function_exists('gzdecode')) {
            function gzdecode($data) {
                    return gzinflate(substr($data,10,-8));
            }
    }

    // I check if the information is online, otherwise, I exit from the document
    $file = @file_get_contents('http://s.trustpilot.com/tpelements/954151/f.json.gz') ;
    if($file != false){
        // JSON decode the string
        $json = json_decode($file);
    }else{
        return;
    }

?>
    <div class="column-50 float-left trustpilot">    
        <!-- Quality -->
        <div class="column-50 float-left quality">
            <div>
                <span class="trustpilot_title">
                    <?= $json->TrustScore->Human; ?>
                </span>
                <div class="stars">
                    <? for ($i=1;$i<=$json->TrustScore->Stars;$i++) { ?>
                       <div class=" star_green">
                        </div>
                    <? } ?>
                </div>
            </div>
        </div>

        <!-- Number of reviews-->  
        <div class="column-50 float-right number_reviews">
            <div class="text">
                <?= $json->ReviewCount->Total ?> klanten hebben een review geschreven op Trustpilot   
            </div>
                <img src="/images/trustpilot/trustpilot_logo.png" />

        </div>
        <!-- Reviews -->
        <div class="reviews">
            <ul class="bxslider">
            <?  foreach ($json->Reviews AS $review){ ?>
                <li>     
                    <!-- SHOWN ON-MOBILE AND TABLET -->
                    <div class="">                    
                        <div class="show-on-mobile show-on-tablet hide-on-desktop">           
                            <!-- Stars -->
                            <div class="float-left ">
                                    <? for($i=1; $i<=$review->TrustScore->Stars; $i++){ ?>
                                        <div class="float-left star_white">
                                        </div>
                                    <? } ?>
                            </div>

                            <!-- Date -->
                            <div  class="trustpilot_date clear">
                                <?= Date::strToDate($review->Created->UnixTime)-> format('%A %e %B %Y'); ?>
                            </div>
                        </div>  

                        <!-- Title -->
                        <div class="trustpilot_content float-left column-50">
                            <h3>
                                <?= $review->User->Name ?>
                            </h3>
                            <h2>
                                <?= $review->Title ?>
                            </h2>
                            <article>
                                <?= firstXWords($review->Content, 20) ?>
                            </article>
                            <a target="_blank" href="<?= $review->Url ?>">Lees meer</a>
                        </div>

                        <div class="hide-on-mobile hide-on-tablet show-on-desktop float-right">           
                            <!-- Stars -->
                            <div class="float-left ">
                                    <? for($i=1; $i<=$review->TrustScore->Stars; $i++){ ?>
                                        <div class="float-left star_white">

                                        </div>
                                    <? } ?>
                            </div>

                            <!-- Date -->
                            <div  class="trustpilot_date clear">
                                    <?= Date::strToDate($review->Created->UnixTime)-> format('%A %e %B %Y'); ?>
                                    <?//= date("l j F Y",$review->Created->UnixTime) ?>
                            </div>
                        </div>

                    </div>
                    <!-- /SHOWN ON-MOBILE AND TABLET -->
                </li>
            <? } ?>
            </ul>     
        </div>
    </div>
<? 
$oPageLayout->addJavascriptBottom('
<script>
    $(".bxslider").bxSlider({
      auto: false,
      autoControls: false,
      pager: true
    });
</script>');

?>