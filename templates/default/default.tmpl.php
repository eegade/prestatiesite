<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js ie ie6 lte9 lte8 lte7" lang="nl"> <![endif]-->
<!--[if IE 7]>     <html class="no-js ie ie7 lte9 lte8 lte7" lang="nl"> <![endif]-->
<!--[if IE 8]>     <html class="no-js ie ie8 lte9 lte8" lang="nl"> <![endif]-->
<!--[if IE 9]>     <html class="no-js ie ie9 lte9" lang="nl"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="nl"> <!--<![endif]-->
    <head>

        <meta charset="utf-8">

        <? #Uncomment this viewport metatag to make website responsive ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title><?= $oPageLayout->sWindowTitle ?> | <?= CLIENT_NAME ?></title>
        <?
        # add meta tags
        if (!empty($oPageLayout->sRobots))
            echo '<meta name="robots" content="' . $oPageLayout->sRobots . '">' . PHP_EOL;
        if (!empty($oPageLayout->sMetaDescription))
            echo '<meta name="description" content="' . $oPageLayout->sMetaDescription . '">' . PHP_EOL;
        if (!empty($oPageLayout->sMetaKeywords))
            echo '<meta name="keywords" content="' . $oPageLayout->sMetaKeywords . '">' . PHP_EOL;
        # add social tags
        if (!empty($oPageLayout->sOGTitle))
            echo '<meta property="og:title" content="' . $oPageLayout->sOGTitle . '"/>' . PHP_EOL;
        if (!empty($oPageLayout->sOGType))
            echo '<meta property="og:type" content="' . $oPageLayout->sOGType . '"/>' . PHP_EOL;
        if (!empty($oPageLayout->sOGUrl))
            echo '<meta property="og:url" content="' . $oPageLayout->sOGUrl . '"/>' . PHP_EOL;
        if (!empty($oPageLayout->sOGDescription))
            echo '<meta property="og:description" content="' . $oPageLayout->sOGDescription . '"/>' . PHP_EOL;
        if (!empty($oPageLayout->sOGImage))
            echo '<meta property="og:image" content="' . $oPageLayout->sOGImage . '"/>' . PHP_EOL;
        # add canonical
        if (!empty($oPageLayout->sCanonical) && $oPageLayout->sCanonical != getCurrentUrl())
            echo '<link rel="canonical" href="' . $oPageLayout->sCanonical . '">' . PHP_EOL;

        # add prev/next canonical
        if (!empty($oPageLayout->sRelPrev))
            echo '<link rel="prev" href="' . $oPageLayout->sRelPrev . '" />' . PHP_EOL;
        if (!empty($oPageLayout->sRelNext))
            echo '<link rel="next" href="' . $oPageLayout->sRelNext . '" />' . PHP_EOL;
        ?>


        <link rel="stylesheet" href="<?= CSS_FOLDER ?>/style.css">
        <link rel="stylesheet" href="<?= CSS_FOLDER ?>/style_<?= ltrim(PATH_PREFIX, '/') ?>.css">
        <link rel="stylesheet" href="<?= CSS_FOLDER ?>/style_responsive.css">
        <link rel="stylesheet" href="<?= CSS_FOLDER ?>/style_custom.css">
        <link rel="stylesheet" href="<?= CSS_FOLDER ?>/jquery.bxslider.css">
        <link rel="stylesheet" href="<?= CSS_FOLDER ?>/print.css" media="print">
        <link rel="stylesheet" href="<?= PLUGINS_FOLDER ?>/fancybox/jquery.fancybox.css">

        <meta name="author" content="<?= CLIENT_NAME ?>" />

        <link rel="icon" href="/favicon.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= IMAGE_FOLDER ?>/apple-touch-icons/apple-touch-icon-144x144-precomposed.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= IMAGE_FOLDER ?>/apple-touch-icons/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= IMAGE_FOLDER ?>/apple-touch-icons/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon" href="<?= IMAGE_FOLDER ?>/apple-touch-icons/apple-touch-icon-precomposed.png">

        <meta name="application-name" content="<?= CLIENT_NAME ?>"/>
        <meta name="msapplication-TileColor" content="#ffffff"/>
        <meta name="msapplication-square70x70logo" content="<?= IMAGE_FOLDER ?>/window-8-tiles/tiny.png"/>
        <meta name="msapplication-square150x150logo" content="<?= IMAGE_FOLDER ?>/window-8-tiles/square.jpg"/>
        <meta name="msapplication-wide310x150logo" content="<?= IMAGE_FOLDER ?>/window-8-tiles/wide.jpg"/>
        <meta name="msapplication-square310x310logo" content="<?= IMAGE_FOLDER ?>/window-8-tiles/large.jpg"/>

        <?
        # echo extra stylesheet stuff
        foreach ($oPageLayout->getStylesheets() AS $sStylesheet) {
            echo $sStylesheet . "\n";
        }

        if (empty($_SESSION['bDontShowBrowserWarning'])) {
            ?>
            <!--[if lte IE 7]>
                <script>window.location = '/browserwarning';</script>
            <![endif]-->
            <?
        }

        /* echo extra javascript stuff */
        foreach ($oPageLayout->getJavascriptsTop() AS $sJavascript) {
            echo $sJavascript . "\n";
        }
        ?>

        <script src="<?= JS_FOLDER ?>/modernizr-2.5.3.js"></script>
        <script src="<?= JS_FOLDER ?>/matchmedia.js"></script>
        <script src="<?= JS_FOLDER ?>/picturefill.js"></script>

        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-1120002-13']);
            _gaq.push(['_setDomainName', 'prestatiesite.nl']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
    </head>

    <body class="<?= BB_TYPE ?>">
        <div id="content-wrapper" class="<?= empty($sControllerRequest) ? 'home' : $sControllerRequest ?>">

            <!-- HEADER -->
            <div class="default-container-width no-padding overflow-visible">
                <nav id="main-menu">
                    <?

                    function makeListTree($aPages) {
                        if (count($aPages) > 0) {
                            echo '<ul class="main-menu-links">';
                            foreach ($aPages AS $oPage) {
                                $sAddClass = '';
                                if ($oPage->level == 2) {
                                    $sAddClass = 'hide-on-tablet hide-on-desktop';
                                }
                                // check if controller equals page urlPath be aware of special treathment for homepage!!
                                echo '<li class="' . $sAddClass . '"><a href="' . $oPage->getUrlPath() . '" class="' . ('/' . http_get('controller') == $oPage->getUrlPath() || getCurrentUrlPath() == $oPage->getUrlPath() ? 'active' : '' ) . '" title="' . $oPage->getShortTitle() . '">' . $oPage->getShortTitle() . '</a>';
                                makeListTree($oPage->getSubPages()); //call function recursive
                                echo '</li>';
                            }
                            echo '</ul>';
                        }
                    }

                    makeListTree(PageManager::getPagesByFilter(array('level' => 1)));
                    ?>
                </nav>
            </div>

            <header class="default-container-width">

                <div id="logo"><a href="/" title="<?= CLIENT_NAME ?>"><img src="/images/layout/logo.jpg" alt="<?= CLIENT_NAME ?>" width="250" height="60" /></a></div>
                <div class="mini-buttons">

                    <? if (BB_WITH_CUSTOMERS) { ?>
                        <? include TEMPLATES_FOLDER . '/elements/miniAccount.inc.php' ?>
                    <? } ?>
                    <? if (BB_WITH_ORDERS) { ?>
                        <div class="cart-button hide-on-mobile unselectable"><div class="cart-button-amount"><?= (empty($_SESSION['oBasket']) ? 0 : $_SESSION['oBasket']->countProducts()) ?></div></div>
                        <div id="miniBasket" class="shopping-cart unselectable">
                            <? include TEMPLATES_FOLDER . '/elements/miniBasketContent.inc.php' ?>
                        </div>

                    <? } ?>
                    <? if (BB_WITH_CLICK_CALL && Settings::get('showClickCallOnHome')) { ?>
                        <? include TEMPLATES_FOLDER . '/elements/clickCall.inc.php' ?> 
                    <? } ?>
                </div>

                <div class="show-on-mobile hide-on-tablet hide-on-desktop mobile-menu">
                    <span id="nav-button" class="show-on-mobile show-on-tablet"></span>
                    <div class="mini-buttons">
                         <? if (BB_WITH_CLICK_CALL && Settings::get('showClickCallOnHome')) { ?>
                            <a class="click-call-button" href="/contact"></a>
                        <? } ?> 

                        <? if (BB_WITH_ORDERS) { ?>
                            <div class="cart-button"><div class="cart-button-amount"><?= (empty($_SESSION['oBasket']) ? 0 : $_SESSION['oBasket']->countProducts()) ?></div></div>
                        <? } ?>
                            
                        <? if (BB_WITH_CUSTOMERS) { ?>
                            <span class="inlog-button"></span>
                        <? } ?>                            

                                              
                    </div>
                </div>
            </header>
            <!-- /HEADER -->


            <!-- CONTENT CONTAINER -->
            <div class="cf">
                <?
                # include the actual page with changable content
                include_once $oPageLayout->sPagePath;
                ?>
            </div>
            <!-- /CONTENT CONTAINER -->
            <div id="push-sticky-footer"></div>
            <!-- FOOTER -->
            <footer id="sticky-footer" class="clear">
                <div class="<?= BB_TYPE == 'wide' ? '' : 'default-container-width' ?> footer-background-color no-padding">
                    <div class="footer-padding <?= BB_TYPE == 'wide' ? 'default-container-width' : '' ?>">
                        <div class="footer-address">
                            <span class="address-company"><?= _e(Settings::get('clientName')) ?></span><br />
                            <?= _e(Settings::get('clientStreet')) ?><br />
                            <?= _e(Settings::get('clientPostalCode')) . ' ' . _e(Settings::get('clientCity')) ?><br />
                            <span class="address-phonenumber"><?= _e(Settings::get('clientPhone')) ?></span><br />
                            <a href="mailto:<?= _e(Settings::get('clientEmail')) ?>"><?= _e(Settings::get('clientEmail')) ?></a>
                        </div>
                        <div class="social-media-icons cf">
                            <? if (Settings::get('socialFacebookLink')) { ?>
                                <a href="<?= Settings::get('socialFacebookLink') ?>" target="_blank" class="facebook-icon"></a>
                                <?
                            }
                            if (Settings::get('socialTwitterLink')) {
                                ?>
                                <a href="<?= Settings::get('socialTwitterLink') ?>" target="_blank" class="twitter-icon"></a>
                                <?
                            }
                            if (Settings::get('socialGooglePlusLink')) {
                                ?>
                                <a href="<?= Settings::get('socialGooglePlusLink') ?>" target="_blank" class="google-icon"></a>
                                <?
                            }
                            if (Settings::get('socialLinkedInLink')) {
                                ?>
                                <a href="<?= Settings::get('socialLinkedInLink') ?>" target="_blank" class="linkedin-icon"></a>
                                <?
                            }
                            if (Settings::get('socialPinterestLink')) {
                                ?>
                                <a href="<?= Settings::get('socialPinterestLink') ?>" target="_blank" class="pinterest-icon"></a>
                            <? } ?>
                        </div>
                        <div class="aside">Webdesign &amp; realisatie: <a href="https://www.a-side.nl/websites" rel="nofollow" target="_blank">A-side media</a> (2014)</div>
                    </div>
                </div>
            </footer>
            <!-- /FOOTER -->            
        </div>
        <div id="fb-root"></div>
        <script src="<?= JS_FOLDER ?>/jquery.min.js"></script>
        <script src="<?= JS_FOLDER ?>/jquery-tooltip.pack.js"></script>
        <script src="<?= PLUGINS_FOLDER ?>/fancybox/jquery.fancybox.pack.js"></script>
        <script src="<?= PLUGINS_FOLDER ?>/autoNumeric.js"></script>
        <script src="<?= JS_FOLDER ?>/jquery.bxslider.min.js"></script>
        <script src="<?= JS_FOLDER ?>/jquery.validate.min.js"></script>
        <script src="<?= JS_FOLDER ?>/base_functions.js"></script>
        <script src="<?= JS_FOLDER ?>/order_functions.js"></script>
        <script src="<?= JS_FOLDER ?>/default.js"></script>
        <script type="text/javascript">
            //add tooltip to tooltip elements
            $(".tooltip").tooltip({
                delay: 0
            });
        </script>
        <?
        # echo extra javascript stuff
        foreach ($oPageLayout->getJavascriptsBottom() AS $sJavascript) {
            echo $sJavascript . "\n";
        }
        ?>
    </body>
</html>