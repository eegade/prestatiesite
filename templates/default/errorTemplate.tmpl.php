<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?= $oPageLayout->sWindowTitle ?></title>
        <link rel="stylesheet" href="<?= CSS_FOLDER ?>/error_style.css" />
        <link rel="shortcut icon" href="/favicon.png" />
    </head>
    <body>
        <div id="container">
            <div id="containerLeft">
                <img src="/images/layout/logo.jpg" alt="<?= CLIENT_NAME ?> - HTTP-statuscode: <?= $iErrorNr ?>" width="200" height="44" />
                <h1>HTTP-statuscode: <span><?= $iErrorNr ?></span></h1>
                <h2>&lsquo;<?= $iErrorMessage ?>&rsquo;</h2>
            </div>
            <div id="containerRight">
                <h3><?= $iErrorDescription ?></h3>
                <h4>Wij verzoeken u naar onze <a href="/" title="Ga naar de homepage van <?= CLIENT_NAME ?>">homepage</a> te gaan</h4>
            </div>
        </div>
    </body>
</html>