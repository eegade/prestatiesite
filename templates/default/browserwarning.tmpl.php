<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?= $oPageLayout->sWindowTitle ?></title>
        <link rel="stylesheet" href="/css/default/browserwarning_style.css" />
        <link rel="shortcut icon" href="/favicon.png" />
    </head>
    <body>
        <div id="container">
            <div id="leftColumn">
                <img id="warning" src="/images/layout/browserwarning/icon-warning.gif" alt="Waarschuwing" title="Waarschuwing" />
                <p>
                    U probeert deze website te openen. Helaas maakt u gebruik van een verouderde internetbrowser.
                </p>
                <p>
                    Wij adviseren u deze browser te updaten, zodat u weer veilig alle websites kunt openen. Maak hier rechts een keuze om de update van uw gewenste browser te downloaden.
                </p>
            </div>
            <div id="middleColumn">
                <div class="browserLogoContainer firstRow firstCol">
                    <a href="http://www.microsoft.com/windows/Internet-explorer/default.aspx" onClick="target='_blank'" titel="Download Internet Explorer">
                        <img class="browserLogo" src="/images/layout/browserwarning/icon-internet-explorer.gif" alt="Internet Explorer" /><br />
                        Internet Explorer
                    </a>
                </div>
                <div class="browserLogoContainer firstRow">
                    <a href="http://www.mozilla.com/firefox/" onClick="target='_blank'" titel="Download Mozilla Firefox">
                        <img class="browserLogo" src="/images/layout/browserwarning/icon-mozilla-firefox.gif" alt="Mozilla Firefox" /><br />
                        Mozilla Firefox
                    </a>
                </div>
                <div class="browserLogoContainer firstCol">
                    <a href="http://www.google.com/chrome" onClick="target='_blank'" titel="Download Google Chrome">
                        <img class="browserLogo" src="/images/layout/browserwarning/icon-google-chrome.gif" alt="Google Chrome" /><br />
                        Google Chrome
                    </a>
                </div>
                <div class="browserLogoContainer">
                    <a href="http://www.apple.com/safari/" onClick="target='_blank'" titel="Download Safari">
                        <img src="/images/layout/browserwarning/icon-safari.gif" alt="Safari" /><br />
                        Safari
                    </a>
                </div>
            </div>
            <div id="rightColumn">
                <a href="?action=dontShowBrowserWarning">
                    Site bezoeken met uw oude browser.<br />
                    <img src="/images/layout/browserwarning/icon-continue.jpg" alt="Ga verder met oude browser" title="Ga verder met oude browser" />
                </a>
            </div>
        </div>
        <script>
            var windowHeight = document.body.clientHeight;
            var containerHeight = 240;
            
            var containerMargin = (windowHeight-containerHeight)/2;
            
            /* set container to vertical middle */
            document.getElementById('container').style.marginTop = containerMargin;
        </script>
    </body>
</html>