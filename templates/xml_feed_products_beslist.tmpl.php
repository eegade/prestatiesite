<?php
set_time_limit(0);
/* echt een XML van maken */
/*
 * $aProducten
 * Alle producten als XML wegschrijven
 */
header("Content-Type:text/xml;charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<productFeed version="1.0" timestamp="<?= strftime("%Y-%m-%d %H:%M:%S") ?>">
    <? foreach ($aProducts as $oProduct) { ?>
        <!--- I write a new product for each available color/size combination -->
        <? foreach ($oProduct->getColorSizeRelations() as $oProductColorSize) { ?>
            <?
                $aFilter['catalogProductColorId'] = $oProductColorSize->catalogProductColorId;
                $aFilter['catalogProductId'] = $oProduct->catalogProductId;
                $aProductImageColorRelations = CatalogProductImageRelationManager::getCatalogProductImageRelationsByFilter($aFilter); 
            ?>
            <product id="<?= $oProduct->catalogProductId; ?>">

                <!--- Required records -->
                <title><![CDATA[<?= $oProduct->name . ' | ' . $oProduct->getBrandName(); ?>]]></title>
                <price><![CDATA[ <?= $oProduct->getReducedPrice(true) ? number_format(($oProduct->getReducedPrice(true) + $oProductColorSize->extraPrice), 2, '.', '') : number_format($oProduct->getSalePrice(true), 2, '.', ''); ?> ]]></price>
                <oldprice><![CDATA[ <?= !$oProduct->getReducedPrice(true) ? ' ' : number_format(($oProduct->getSalePrice(true) + $oProductColorSize->extraPrice), 2, '.', ''); ?> ]]></oldprice>
                <productURL>
                    <![CDATA[ <?= CLIENT_HTTP_URL  . $oProduct->getUrlPath(); ?> ]]>
                </productURL>
                
                <? if (DeliveryMethodManager::getDeliveryMethodById(Settings::get('defaultDeliveryMethod'))) { ?>
                    <deliveryCosts><![CDATA[ <?= DeliveryMethodManager::getDeliveryMethodById(Settings::get('defaultDeliveryMethod'))->price ?> ]]></deliveryCosts>
                    <deliveryTime><![CDATA[ <?= DeliveryMethodManager::getDeliveryMethodById(Settings::get('defaultDeliveryMethod'))->deliveryTime ?> ]]></deliveryTime>
                <? } ?>
                
                <!-- I set the main image and all the additional images -->
                <? if ($oProduct->getProductType()->withColors) {
                    // If there aren ImageColor Relation objects, I set the first image I get all the additional images
                    if (!empty($aProductImageColorRelation)){
                        foreach ($aProductImageColorRelation as $key => $oProductImageColorRelation) { 
                            $oImage = $oProductImageColorRelation->getImage();
                            $oImageFile = $oImage->getImageFileByReference('detail');

                            if ($key==0) {
                                echo "<imageURL>";
                            }else{
                                echo "<extraImageURL>";
                            } 
                            ?>
                               <![CDATA[ <?= CLIENT_HTTP_URL . $oImageFile->link; ?> ]]>
                            <? 
                            if ($key==0) {
                                echo "</imageURL>";
                            }else{
                                echo "</extraImageURL>";
                            }

                        }
                    } else {
                        // I use this, only if the product has at least one image
                        if($oProductImage = $oProduct->getImages('first-online')){
                            $oProductImageFile = $oProductImage->getImageFileByReference('detail');
                                                        
                            echo "<imageURL>";
                            ?>
                                <![CDATA[ <?= CLIENT_HTTP_URL . $oProductImageFile->link; ?> ]]>
                            <? 
                            echo "</imageURL>";
                        }
                    }
                    
                // If the product doesn't have different colors, I show all the images  
                } else {                                   
                    foreach ($oProduct->getImages() as $key => $oProductImage) { 
                        if ($oProductImageFile = $oProductImage->getImageFileByReference('detail')) { 
                            if ($key==0) {
                                echo "<imageURL>";
                            }else{
                                echo "<extraImageURL>";
                            } 
                            ?>
                                <![CDATA[ <?= CLIENT_HTTP_URL . $oProductImageFile->link; ?> ]]>
                            <? 

                            if ($key==0) {
                                echo "</imageURL>";
                            }else{
                                echo "</extraImageURL>";
                            }
                        }
                    }
                } ?>
                        
                <uniqueCode><![CDATA[ <?= $oProduct->catalogProductId ?><?= !empty($oProductColorSize->getColor()->name) ? '-' . $oProductColorSize->getColor()->name : '' ?><?= !empty($oProductColorSize->getSize()->name) ? '-' . $oProductColorSize->getSize()->name : '' ?> ]]></uniqueCode>
                <!--- I make a foreach as many times as possible levels. In this case, we have two levels -->
                <categories>
                    <? foreach (CatalogProductCategoryManager::getProductCategoriesByFilter() as $oCategory) {
                        if ($oCategory->level == 1) { ?>
                            <maincat id = "<?= $oCategory->catalogProductCategoryId ?>" name ="<?= $oCategory->name ?>">
                            <? foreach (CatalogProductCategoryManager::getProductCategoriesByFilter() as $oSubCategory){
                                if($oSubCategory->parentCatalogProductCategoryId == $oCategory->catalogProductCategoryId) {?>
                                    <level_1 id = "<?= $oSubCategory->catalogProductCategoryId ?>" name="<?= $oSubCategory->name ?>"></level_1> 
                            <?  
                                }
                            } ?>
                            </maincat>
                        <? }
                    } ?>
                </categories>
                <category>
                    <? 
                        $level_ant = 0;
                        foreach ($oProduct->getCategories() as $oProductCategory) { 
                            if ($oProductCategory->level > $level_ant){
                            $level_ant = $oProductCategory->level;
                            $Category = $oProductCategory->name;
                            } 
                        } 
                    ?>
                    <![CDATA[<?= $oProductCategory->name ?> ]]>
                </category>
                <size><![CDATA[ <?= $oProductColorSize->getSize()->name ?> ]]></size>
                <!--- If Color and Size are not empty, I write them both. Otherwise, I write Size, even if Size is empty -->
                <variantCode><![CDATA[ <?= $oProduct->catalogProductId ?><?= (!empty($oProductColorSize->getColor()->name) && !empty($oProductColorSize->getSize()->name)) ? '-' . $oProductColorSize->getColor()->name : $oProductColorSize->getSize()->name ?> ]]></variantCode>
                
                <!--- Optional records -->
                <description>
                    <![CDATA[ 
                        <?= $oProduct->getBrandName(); ?>
                        <?= "-" ?>
                        <?= strip_tags($oProduct->description); ?> 
                    ]]>
                </description>

                
                <!--- Price comparasion records--> 
                <ean><![CDATA[ <?= $oProduct->ean ?>]]></ean>
                <brand><![CDATA[ <?= $oProduct->getBrandName(); ?>]]></brand>
                <sku><![CDATA[ <?= $oProductColorSize->getMPN() ?> ]]></sku>
                
                <!-- Records to join differents products/colors into one product -->
                <modelCode><?= $oProduct->catalogProductId ?></modelCode>
                
                <color><![CDATA[<?= $oProductColorSize->getColor()->name ?> ]]></color>
                <stock><![CDATA[<?= $oProductColorSize->stock === null ? 9999 : $oProductColorSize->stock ?> ]]></stock>
                
                <?
                    if($oProduct->getProductType()->withGenders)
                        echo  "<gender><![CDATA[ ".$oProduct->gender." ]]></gender>";
                ?>
                <original><![CDATA[ Yes  ]]></original>
            </product>
        <?  }?>
    <?  } ?>
</productFeed>