/**
 * add a product to the basket
 */
$('form.addProductToBasket').submit(function(e) {
    var thisElement = $(this);
    $.ajax({
        type: "POST",
        url: thisElement.attr('action'),
        data: thisElement.serialize() + '&ajax=1',
        success: function(data) {
            var dataObj = eval('(' + data + ')');
            
            if (dataObj.success === true) {
                $('div#miniBasket').html(dataObj.miniBasketContent);
                openMiniBasket();
                $('.cart-button-amount').html(dataObj.countProducts);
            }
            if (dataObj.orderStatusUpdate) {
                showStatusUpdate(dataObj.orderStatusUpdate, thisElement.find('.orderStatusUpdate'));
            }
        }
    });
    e.preventDefault();
});

/**
 * remove a product from the basket
 */
$('form.removeProductFromBasket').live('submit', function(e) {
    var thisElement = $(this);
    $.ajax({
        type: "POST",
        url: thisElement.attr('action'),
        data: thisElement.serialize() + '&removeProductFromBasket=1&ajax=1',
        success: function(data) {
            var dataObj = eval('(' + data + ')');

            if (dataObj.success == true) {
                $('div#miniBasket').html(dataObj.miniBasketContent);
            }
            if (dataObj.orderStatusUpdate) {
                showStatusUpdate(dataObj.orderStatusUpdate, thisElement.find('.orderStatusUpdate'));
            }
            $('.cart-button-amount').html(dataObj.countProducts);
        }
    });
    e.preventDefault();
});