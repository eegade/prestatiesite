//add fancybox to fancyBoxLink elements
$(".fancyBoxLink").fancybox({
    helpers: {
        overlay: {
            locked: false
        }
    }
});

var ua = $.browser;
if (ua.msie && ua.version.slice(0, 1) == "8") {
    var slideCount = $('#slider .slide').length;
    $('.slide').css('height', 300);
} else if (ua.msie && ua.version.slice(0, 1) == "7") {
    var slideCount = $('#slider .slide').length;
    $('.slide').css('height', 300);
} else {
    var slideCount = $('#slider .slide').length;
}

$(window).load(function() {
    $("#brandbox-module").show();

    if (slideCount > 1) {
        $('#slider').bxSlider({
            mode: 'fade',
            auto: true,
            pager: true,
            controls: false
        });
    } else {
        $('#slider').bxSlider({
            auto: false,
            pager: false,
            controls: false
        });
    }

});

$('.continuousSlider').bxSlider({
    minSlides: 4,
    maxSlides: 4,
    slideWidth: 300,
    controls: false,
    pager: false,
    slideMargin: 10,
    auto: true
});

$('#nav-button').click(function() {
    if ($('nav#main-menu').hasClass('nav-opened')) {
        closeNav();
    } else {
        openNav();
    }
});

$('.cart-button').click(function() {
    if ($('.shopping-cart').hasClass('cart-opened')) {
        closeMiniBasket();
    } else {
        openMiniBasket();
    }
});

$('.inlog-button').click(function() {
    if ($('.login-column').hasClass('login-opened')) {
        closeMiniAccount();
    } else {
        openMiniAccount();
    }
});

function openNav() {
    closeMiniAccount(0);
    closeMiniBasket(0);
    $('nav#main-menu').slideDown('fast').addClass('nav-opened');
}

function closeNav(speed) {
    if ($('nav#main-menu').hasClass('nav-opened')) {
        $('nav#main-menu').slideUp((typeof speed != 'undefined' ? speed : 'slow'), function() {
            $(this).removeAttr('style');
        }).removeClass('nav-opened');
    }
}

function openMiniAccount() {
    closeMiniBasket(0);
    closeNav(0);
    $('.login-column').slideDown('fast').addClass('login-opened');
}

function closeMiniAccount(speed) {
    if ($('.login-column').hasClass('login-opened')) {
        $('.login-column').slideUp((typeof speed != 'undefined' ? speed : 'slow'), function() {
            $(this).removeAttr('style');
        }).removeClass('login-opened');
    }
}

function openMiniBasket() {
    closeMiniAccount(0);
    closeNav(0);
    $('.shopping-cart').slideDown('fast').addClass('cart-opened');
}

function closeMiniBasket(speed) {
    if ($('.shopping-cart').hasClass('cart-opened')) {
        $('.shopping-cart').slideUp((typeof speed != 'undefined' ? speed : 'slow'), function() {
            $(this).removeAttr('style');
        }).removeClass('cart-opened');
    }
}

// validate order form
$('form.validateForm').each(function(index, form) {
    var jForm = $(form);
    jForm.validate({
        focusInvalid: true,
        errorContainer: jForm.find('.errorBox'),
        errorLabelContainer: jForm.find('.errorBox ul'),
        wrapper: "li"
    });
});

$('input.priceFloatOnly').autoNumeric({
    aSep: '',
    altDec: ','
});

$('input.numbersOnly').autoNumeric({
    aSep: '',
    mDec: '0'
});



(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id))
        return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=140327209465999";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

!function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
    if (!d.getElementById(id)) {
        js = d.createElement(s);
        js.id = id;
        js.src = p + '://platform.twitter.com/widgets.js';
        fjs.parentNode.insertBefore(js, fjs);
    }
}(document, 'script', 'twitter-wjs');