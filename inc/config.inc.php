<?php

#timezone settings
date_default_timezone_set('Europe/Amsterdam');

# set locales to NL by default
setlocale(LC_TIME, 'nl_NL');

# set UTF-8 encoding with header
header('Content-Type:text/html; charset=UTF-8');

define("PATH_PREFIX", "/default");

# set image folder
define("IMAGE_FOLDER", '/images');

# set admin folder
define("ADMIN_FOLDER", '/admin');

# set include folder
define("INC_FOLDER", DOCUMENT_ROOT . '/inc');

# set classes folder
define("CLASSES_FOLDER", DOCUMENT_ROOT . '/classes');

# set include folder
define("PAGES_FOLDER", DOCUMENT_ROOT . '/pages' . PATH_PREFIX);

# set javascript folder
define("JS_FOLDER", '/js');

# set css folder
define("CSS_FOLDER", '/css' . PATH_PREFIX);

# set plugins folder
define("PLUGINS_FOLDER", '/plugins');

# set templates folder, general
define("TEMPLATES_FOLDER_BASE", DOCUMENT_ROOT . '/templates');

# set templates folder
define("TEMPLATES_FOLDER", TEMPLATES_FOLDER_BASE . PATH_PREFIX);

# admin images foler
define("ADMIN_IMAGE_FOLDER", ADMIN_FOLDER . '/images');

# set admin include folder
define("ADMIN_INC_FOLDER", DOCUMENT_ROOT . ADMIN_FOLDER . '/inc');

# set admin include folder
define("ADMIN_TEMPLATES_FOLDER", DOCUMENT_ROOT . ADMIN_FOLDER . '/templates');

# set admin paginas folder
define("ADMIN_PAGES_FOLDER", DOCUMENT_ROOT . ADMIN_FOLDER . '/pages');

# set admin javascript folder
define("ADMIN_JS_FOLDER", ADMIN_FOLDER . '/js');

# set admin css folder
define("ADMIN_CSS_FOLDER", ADMIN_FOLDER . '/css');

# set admin plugins folder
define("ADMIN_PLUGINS_FOLDER", ADMIN_FOLDER . '/plugins');

# include the base functions
require_once(INC_FOLDER . '/base_functions.inc.php');

include_once DOCUMENT_ROOT . '/libs/chromephp/ChromePhp.php';
include_once DOCUMENT_ROOT . '/libs/FirePHPCore/FirePHP.class.php';

# register the autoload function
spl_autoload_register('autoloadClass');

// SWF upload fix
if (http_post('PHPSESSID'))
    session_id(http_post('PHPSESSID'));

# start session
session_start();

// calculates the amount of days since the website went LIVE compared to today
$iDaysSinceLIVE = Date::strToDate('11-09-2014')->daysDiff(new Date('today')); // @todo enter the date the website went LIVE (first date)

# To echo query's and other debug stuff, true. Otherwise false
define("DEBUG", (isAside() ? 1 : 0));
define("DEBUG_QUERIES", 0);
define("DO_NOT_EMAIL_HTTP_ERRORS", $iDaysSinceLIVE >= 31); // optionally change the lenght of time, in days, of the http error emails

# error reporting
error_reporting(E_ALL | E_STRICT);
set_error_handler('error_handler', E_ALL | E_STRICT);
register_shutdown_function('shutdown_handler');

/*
 * Database connection details
 * To make a connection to an other container, make yourself some acces in 'Direct Admin' and
 * use the first letter of the user to connect. e.g. c.a-sidemedia.nl as host
 */
define("DB_HOST", "localhost");
define("DB_USER", "prestatie_dev");
define("DB_PASS", "JEFdW2xA");
define("DB_DATABASE", "prestatie_dbdev");

# client details
define("CLIENT_NAME", "Prestatiesite"); //@todo change name
define("CLIENT_URL", "dev.prestatiesite.nl"); //@todo change url
define("CLIENT_IP", "x.x.x.x"); // client IP(s) komma seperated (x.x.x.x,x.x.x.x)
define('CLIENT_HTTP_URL', 'http://dev.prestatiesite.nl');
define('CLIENT_HTTPS_URL', 'http://dev.prestatiesite.nl');
define("CLIENT_DEFAULT_EMAIL_TO", "arjan@a-side.nl");

define("DEFAULT_EMAIL_FROM", CLIENT_NAME . " <arjan@a-side.nl>"); //@todo change email
define("DEFAULT_EMAIL_REPLY_TO", CLIENT_NAME . " <arjan@a-side.nl>"); //@todo change email

define("DEVELOPER_NAME", "Arjan & Cornee"); //name of the maker(s)
define("DEFAULT_ERROR_EMAIL", "cornee@a-side.nl"); //error@a-side.nl @todo

define("ASIDE_LOG_FOLDER", DOCUMENT_ROOT . "/a-side_logs"); //level below public_html
# Current logged in user 
$oCurrentUser = http_session("oCurrentUser", null);

// Prestatiesite configuration
require_once(INC_FOLDER . '/BBConfig.inc.php');
?>
