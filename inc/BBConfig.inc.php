<?php

/* Prestatiesite settings */
define('BB_TYPE', http_get('type', 'small')); // wide | small

define('BB_WITH_NEWS', Settings::get('withNews')); // true | false
define('BB_WITH_NEWS_CATEGORIES', Settings::get('withNewsCategories')); // true | false
define('BB_WITH_PHOTO_ABLUMS', Settings::get('withPhotoAlbums')); // true | false
define('BB_WITH_CATALOG', Settings::get('withCatalog')); // true | false
define('BB_WITH_ORDERS', Settings::get('withOrders')); // true | false
define('BB_WITH_AGENDA_ITEMS', Settings::get('withAgendaItems')); // true | false
define('BB_WITH_COUPONS', Settings::get('withCoupons')); // true | false
define('BB_WITH_CUSTOMERS', Settings::get('withCustomers')); // true | false
define('BB_WITH_MOTOREN', Settings::get('withMotoren')); // true | false
define('BB_WITH_KASSA', Settings::get('withKassa')); // true | false
define('BB_WITH_ADVANCED_SITEMAP', Settings::get('withAdvancedSitemap')); // true | false
define('BB_WITH_BESLIST_FEED', Settings::get('withBeslistFeed')); // true | false
define('BB_WITH_GOOGLE_FEED', Settings::get('withGoogleFeed')); // true | false
define('BB_WITH_TRADETRACKER_FEED', Settings::get('withTradetrackerFeed')); // true | false
define('BB_WITH_CLICK_CALL', Settings::get('withClickCall')); // true | false
define('BB_WITH_CALL_ME_BACK', Settings::get('withCallMeBack')); // true | false
define('BB_WITH_TWITTER_FEED', Settings::get('withTwitterFeed')); // true | false
define('BB_WITH_CONTINUOUS_SLIDER', Settings::get('withContinuousSlider')); // true | false

/* default/core */
define('BB_HEADER_CROP_HEADER_W', '1903'); // width of header image crop_header
define('BB_HEADER_CROP_HEADER_H', '488'); // height of header image crop_header

define('BB_HEADER_CROP_SMALL_W', '750'); // width of header image crop_small
define('BB_HEADER_CROP_SMALL_H', '472'); // height of header image crop_small

define('BB_HEADER_CROP_MIN_W', BB_HEADER_CROP_HEADER_W); // preferred minimum width of header image
define('BB_HEADER_CROP_MIN_H', BB_HEADER_CROP_HEADER_H); // preferred minimum height of header image

define('BB_IMAGE_DETAIL_W', '960'); // width of default image detail
define('BB_IMAGE_DETAIL_H', '700'); // height of default image detail

define('BB_IMAGE_CROP_SMALL_W', '733'); // width of default image crop_small
define('BB_IMAGE_CROP_SMALL_H', '461'); // width of default image crop_small

define('BB_IMAGE_CROP_MIN_W', BB_IMAGE_CROP_SMALL_W);  // preferred minimum width of default image
define('BB_IMAGE_CROP_MIN_H', BB_IMAGE_CROP_SMALL_H);  // preferred minimum height of default image

define('BB_IMAGE_ORIGINAL_W', '2000');  // maximum width of original image
define('BB_IMAGE_ORIGINAL_H', '2000');  // maximum height of original image

/* brandbox */
define('BB_BRANDBOX_CROP_LARGE_W', '1903'); // width of brandbox image crop_large
define('BB_BRANDBOX_CROP_LARGE_H', '488'); // height of brandbox image crop_large

define('BB_BRANDBOX_CROP_SMALL_W', '750'); // width of brandbox image crop_small
define('BB_BRANDBOX_CROP_SMALL_H', '518'); // height of brandbox image crop_small

define('BB_BRANDBOX_CROP_MIN_W', BB_BRANDBOX_CROP_LARGE_H);  // preferred minimum width of brandbox image
define('BB_BRANDBOX_CROP_MIN_H', BB_BRANDBOX_CROP_SMALL_H);  // preferred minimum height of brandbox image

/* continuous slider */
define('BB_SLIDER_CROP_SMALL_W', '300'); // height of brandbox image crop_small
define('BB_SLIDER_CROP_SMALL_H', '250'); // width of brandbox image crop_small


/* news items */
define('BB_NEWS_ITEM_CROP_SMALL_W', '775'); // width of news item image crop_small
define('BB_NEWS_ITEM_CROP_SMALL_H', '401'); // height of news item crop_small

/* photo albums */
define('BB_PHOTO_ALBUM_CROP_SMALL_W', '223'); // width of photo album image crop_small
define('BB_PHOTO_ALBUM_CROP_SMALL_H', '167'); // height of photo album image crop_small

/* catalog */
define('BB_CATALOGPRODUCT_CROP_THUMB_W', '310'); // width of catalogProduct image crop_thumb
define('BB_CATALOGPRODUCT_CROP_THUMB_H', '310'); // height of catalogProduct image crop_thumb

define('BB_CATALOGPRODUCT_CROP_OVERVIEW_W', '615'); // width of catalogProduct image crop_overview
define('BB_CATALOGPRODUCT_CROP_OVERVIEW_H', '615'); // height of catalogProduct image crop_overview

define('BB_CATALOGPRODUCT_CROP_DETAIL_W', '468'); // width of catalogProduct image crop_detail
define('BB_CATALOGPRODUCT_CROP_DETAIL_H', '468'); // height of catalogProduct image crop_detail

define('BB_CATALOGPRODUCT_CROP_MOBILE_W', '638'); // width of catalogProduct image crop_mobile
define('BB_CATALOGPRODUCT_CROP_MOBILE_H', '638'); // height of catalogProduct image crop_mobile

define('BB_CATALOGPRODUCT_CROP_MIN_W', BB_CATALOGPRODUCT_CROP_MOBILE_W);  // preferred minimum width of catalogProduct image
define('BB_CATALOGPRODUCT_CROP_MIN_H', BB_CATALOGPRODUCT_CROP_MOBILE_H);  // preferred minimum height of catalogProduct image

/* Agenda items */
define('BB_AGENDA_ITEM_CROP_SMALL_W', '775'); // width of agenda item image crop_small
define('BB_AGENDA_ITEM_CROP_SMALL_H', '401'); // height of agenda item crop_small


if (BB_TYPE == 'wide') {
    define('BB_CONTACT_MAPS_H', '488'); // 
} else {
    define('BB_CONTACT_MAPS_H', '246'); // 
}
?>