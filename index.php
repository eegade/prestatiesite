<?php

# get micro time to check what time it takes to load the PHP
$iSMT = microtime(true);

// To see what the document root is you can use next line (not working with cron jobs)
//echo $_SERVER['DOCUMENT_ROOT'];
# set document root
define("DOCUMENT_ROOT", '/home/devgit/domains/devgit.a-test.nl/public_html');

include_once DOCUMENT_ROOT . '/inc/config.inc.php';

# get the controller from the $_GET
$sControllerRequest = strtolower(http_get("controller", ''));

/* save referrer for redirecting after browserwarning */
if ($sControllerRequest != 'browserwarning' && empty($_SESSION['bDontShowBrowserWarning'])) {
    $_SESSION['sBrowserWarningReferrer'] = getCurrentUrlPath(true, true);
} elseif (!empty($_SESSION['bDontShowBrowserWarning'])) {
    unset($_SESSION['sBrowserWarningReferrer']);
}

// get status update from session
$sOrderStatusUpdate = http_session("orderStatusUpdate", null);
unset($_SESSION['orderStatusUpdate']); //remove statusupdate, always show once
if (BB_WITH_CUSTOMERS) {
    // current logged in Customer
    $oCurrentCustomer = http_session('oCurrentCustomer', null);
}

# define the basket
if (empty($_SESSION['oBasket'])) {
    $_SESSION['oBasket'] = new Order();
}

// Key = 'controller name', Value = 'path to file' ()
$aControllers = array(
    "errors" => "/errors.cont.php",
    "browserwarning" => "/browserwarning.cont.php",
    "sitemap" => "/sitemap.cont.php",
    "product-feed" => "/productFeed.cont.php",
);

// with news? set controller
if (BB_WITH_AGENDA_ITEMS) {
    $aControllers['agenda'] = "/agendaItem.cont.php";
}

// with news? set controller
if (BB_WITH_NEWS) {
    $aControllers['nieuws'] = "/newsItem.cont.php";
}

// with photo albums? set controller
if (BB_WITH_PHOTO_ABLUMS) {
    $aControllers['fotoalbums'] = "/photoAlbum.cont.php";
}

// with catalog? set controller
if (BB_WITH_CATALOG) {
    $aControllers['productcategorieen'] = "/catalog/catalogProductCategory.cont.php";
    $aControllers['producten'] = "/catalog/catalogProduct.cont.php";
    // with orders? set controller
    if (BB_WITH_ORDERS) {
        $aControllers['winkelwagen'] = "/order.cont.php";
        // with kassa? set controller
        if (BB_WITH_KASSA) {
            $aControllers['kassa'] = "/kassa.cont.php";
        }
    }
}

// with customers? set controller
if (BB_WITH_CUSTOMERS) {
    $aControllers['account'] = "/customer.cont.php";
}

if (BB_WITH_MOTOREN) {
    $aControllers['motoren'] = "/motoren.cont.php";
}

# get the controller 'path to file' from the controller array
if (array_key_exists($sControllerRequest, $aControllers)) {
    $sControllerPath = $aControllers[$sControllerRequest];
} else {
    /*
     * Check if class could exists
     * Then search for a matching page in the Database
     * if a pageControllerPath is found, take this one for displaying
     */
    $sControllerPath = PageManager::getControllerPathByUrlPath(getCurrentUrlPath());

    # also try pageRedirects table
    if (empty($sControllerPath)) {
        $sPageRedirectUrlPath = PageManager::getRedirectUrlPathByUrlPath(getCurrentUrlPath());

        # redirect if there is a path
        if ($sPageRedirectUrlPath) {
            # 301 redirect
            http_redirect($sPageRedirectUrlPath, false, true);
        }
    }

    # Path not found? show 404 error
    if (empty($sControllerPath)) {
        # non existing page
        showHttpError(404);
    }
}

if (file_exists(DOCUMENT_ROOT . '/controllers' . PATH_PREFIX . $sControllerPath)) {
    include_once DOCUMENT_ROOT . '/controllers' . PATH_PREFIX . $sControllerPath;
} else {
    showHttpError(404);
}

# save referrer for redirecting after login
if (http_get('controller') != 'account') {
    $_SESSION['frontendLoginReferrer'] = getCurrentUrl();
}

# save referrer for redirecting from shop basket
if (http_get('controller') != 'winkelwagen') {
    $_SESSION['frontendBasketReferrer'] = getCurrentUrl();
}

# get micro time to check what time it takes to load the PHP
$iEMT = microtime(true);
if (DEBUG) {
    //echo "Time to load PHP: " . ($iEMT - $iSMT) . " sec";
}
?>