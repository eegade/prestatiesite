<?php

/*
 * controller to get all data for homepage
 */

# make pageLayout Object
$oPageLayout = new PageLayout();

# get Page by id
$oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

$oPageLayout->sWindowTitle = $oPage->getWindowTitle();
$oPageLayout->sMetaDescription = $oPage->getMetaDescription();
$oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
$oPageLayout->generateCustomCrumblePath($oPage->getCrumbles(), false);
$oPageLayout->sOGType = 'website';
$oPageLayout->sOGTitle = $oPage->getWindowTitle();
$oPageLayout->sOGDescription = $oPage->getMetaDescription();
$oPageLayout->sOGUrl = getCurrentUrl();
if (($oImage = $oPage->getImages('first-online')) && ($oImageFile = $oImage->getImageFileByReference('crop_small'))) {
    $oPageLayout->sOGImage = CLIENT_HTTP_URL . $oImageFile->link;
}

if (BB_WITH_NEWS && Settings::get('showNewsOnHome')) {
    $iFoundRows = false;
    $aNewsItems = NewsItemManager::getNewsItemsByFilter(array('checkOnline' => true), 4, 0, $iFoundRows, array('date' => 'DESC', 'newsItemId' => 'DESC'));
}
$aBrandboxItems = BrandboxItemManager::getBrandboxItemsByFilter();

if (BB_WITH_MOTOREN && Settings::get('showBikesOnHome')) {
    $iFoundRows = false;
    $aBikes = MotorenManager::getBikesByFilter(array(), 3);
}

if (BB_WITH_CATALOG && Settings::get('showProductsOnHome')) {
    $iFoundRows = false;
    $aProducts = CatalogProductManager::getProductsByFilter(array('showOnHome' => true), 4, 0, $iFoundRows, array('RAND()' => 'ASC'));
}

if (BB_WITH_CALL_ME_BACK && http_post('action', false) === 'contact') {
    if (!http_post('contactName') || !http_post('contactPhone')) {
        $_SESSION['callMeBackSatus'] = 'Velden niet (juist) ingevuld';
    } elseif (hasLinks(http_post('contactName')) || hasLinks(http_post('contactPhone'))) {
        $_SESSION['callMeBackSatus'] = 'Velden bevat een of meer links, dit is niet toegestaan. Verwijder deze en probeer nog eens.';
    } elseif (http_post('rumpelstiltskin-empty') || !http_post('rumpelstiltskin-filled')) { // temporary removed for testing trap || hasLinks(http_post('bericht'))
        $_SESSION['callMeBackSatus'] = 'Er is een fout opgetreden bij het versturen van de aanvraag. Probeer het nogmaals of neem op een andere manier contact met ons op.';
    } else {
        MailManager::sendMail(CLIENT_DEFAULT_EMAIL_TO, 'Bel mij terug', '<b>Naam:</b> ' . http_post('contactName') . '<br/><b>Telefoonnummer:</b> ' . http_post('contactPhone'));
        $_SESSION['callMeBackSatus'] = 'Dankjewel, we zullen binnenkort contact met u';
    }
}

if (BB_WITH_TWITTER_FEED && Settings::get('showTwitterFeedOnHome')) {
    $aTweets = TweetManager::getAllTweets(intval(Settings::get('maxNumTweets')));
}

if (BB_WITH_CONTINUOUS_SLIDER && Settings::get('showSliderOnHome')) {
    // Get participants
    $aContinuousSliderItems = ContinuousSliderItemManager::getContinuousSliderItemsByFilter();
}
$oPageLayout->sPagePath = PAGES_FOLDER . '/home/home.inc.php'; // special home page
$sJavascript = <<<EOT
<script>
    // code underneath is for prevending screenflickering due loadingtime images
    $(window).load(function(){
    });
</script>
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);

# include the error template
include_once TEMPLATES_FOLDER . '/default.tmpl.php';
?>