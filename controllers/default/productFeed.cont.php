<?php

/*
 * controller to get all data for homepage
 */

# Product-feed xml
if (http_get('param1') == 'beslist' && BB_WITH_BESLIST_FEED) {
    $aProducts = CatalogProductManager::getProductsByFilter();
    # include the prodcut xml template
    include_once TEMPLATES_FOLDER_BASE . '/xml_feed_products_beslist.tmpl.php';
}
elseif (http_get('param1') == 'google' && BB_WITH_GOOGLE_FEED) {
    $aProducts = CatalogProductManager::getProductsByFilter();
    # include the prodcut xml template
    include_once TEMPLATES_FOLDER_BASE . '/xml_feed_products_google.tmpl.php';
}
elseif (http_get('param1') == 'tradetracker' && BB_WITH_TRADETRACKER_FEED) {
    $aProducts = CatalogProductManager::getProductsByFilter();
    # include the prodcut xml template
    include_once TEMPLATES_FOLDER_BASE . '/xml_feed_products_tradetracker.tmpl.php';
}else{
    showHttpError('404');
}
?>