<?php

/*
 * controller to get all data for homepage
 */

# make pageLayout Object
$oPageLayout = new PageLayout();

$oPageForHeaderImage = PageManager::getPageByUrlPath('/' . http_get('controller'));
$oHeaderImage = $oPageForHeaderImage->getHeaderImages('first-online');

# photoalbum detail page
if (is_numeric(http_get('param1'))) {
    $oPhotoAlbum = PhotoAlbumManager::getPhotoAlbumById(http_get('param1'));

    if (empty($oPhotoAlbum) || !$oPhotoAlbum->online)
        showHttpError('404');

    $oPageLayout->sWindowTitle = $oPhotoAlbum->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPhotoAlbum->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPhotoAlbum->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPhotoAlbum->getCrumbles());
    $oPageLayout->sOGType = 'website';
    $oPageLayout->sOGTitle = $oPhotoAlbum->getWindowTitle();
    $oPageLayout->sOGDescription = $oPhotoAlbum->getMetaDescription();
    $oPageLayout->sOGUrl = getCurrentUrl();
    if (($oImage = $oPhotoAlbum->getImages('cover')) && ($oImageFile = $oImage->getImageFileByReference('crop_small'))) {
        $oPageLayout->sOGImage = CLIENT_HTTP_URL . $oImageFile->link;
    }

    $oPageLayout->sPagePath = PAGES_FOLDER . '/photoAlbums/photoAlbum_details.inc.php';
}

# agenda overview
else {
    # get page by urlPath (/photoalbums)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());
    $oPageLayout->sOGType = 'website';
    $oPageLayout->sOGTitle = $oPage->getWindowTitle();
    $oPageLayout->sOGDescription = $oPage->getMetaDescription();
    $oPageLayout->sOGUrl = getCurrentUrl();
    if (($oImage = $oPage->getImages('first-online')) && ($oImageFile = $oImage->getImageFileByReference('crop_small'))) {
        $oPageLayout->sOGImage = CLIENT_HTTP_URL . $oImageFile->link;
    }

    $aPhotoAlbums = PhotoAlbumManager::getPhotoAlbumsByFilter();
    $oPageLayout->sPagePath = PAGES_FOLDER . '/photoAlbums/photoAlbums_overview.inc.php';
}

# include the error template
include_once TEMPLATES_FOLDER . '/default.tmpl.php';
?>