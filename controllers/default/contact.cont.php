<?php

/*
 * Controller to handle contact pages
 */
$oPageLayout = new PageLayout();
$oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());
if (empty($oPage) || !$oPage->online) {
    showHttpError('404');
}

if ($oPage->level > 1)
    $oPageForMenu = PageManager::getPageByUrlPath('/' . http_get('controller'));
else
    $oPageForMenu = $oPage;

$oPageLayout->sWindowTitle = $oPage->getWindowTitle();
$oPageLayout->sMetaDescription = $oPage->getMetaDescription();
$oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
$oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

if (http_get('param1')) {
    $oPageLayout->sPagePath = PAGES_FOLDER . '/contact/contact_page_details.inc.php';
} else {
    if (http_post('action') == 'sendForm') {
        $aErrors = array();
        $bLogError = true;
        if (!http_post('naam')) {
            $aErrors['naam'] = 'Uw naam is niet (juist) ingevuld';
        }
        if (!http_post('email')) {
            $aErrors['email'] = 'Onjuist e-mail adres';
        }
        if (!http_post('bericht')) {
            $aErrors['bericht'] = 'U heeft geen bericht ingevuld';
        }

        // do SPAM checks
        if (hasLinks(http_post('bericht'))) {
            $aErrors['bericht'] = 'Uw bericht bevat een of meer links, dit is niet toegestaan. Verwijder deze en probeer nog eens.';
            $bLogError = false;
        }

        if (http_post('rumpelstiltskin-empty') || !http_post('rumpelstiltskin-filled')) { // temporary removed for testing trap || hasLinks(http_post('bericht'))
            $aErrors['S-P-A-M'] = 'Er is een fout opgetreden bij het versturen van de aanvraag. Probeer het nogmaals of neem op een andere manier contact met ons op.';
            $bLogError = false;
        }
        // end SPAM checks

        if (empty($aErrors)) {
            $sSubject = 'Contact formulier verstuurd';
            $sMail = '  <span style="font: 14px/22px Arial, sans-serif">
                            ' . _e(http_post('naam')) . ' heeft het contactformulier ingevuld en verstuurd. Hieronder lees je de verstuurde gegevens.<br />
                            <br />
                            <table style="font: 14px/22px Arial, sans-serif">
                                <tr>
                                    <td>Naam:</td>
                                    <td>' . _e(http_post('naam')) . '</td>
                                </tr>
                                <tr>
                                    <td>Organisatie:</td>
                                    <td>' . _e(http_post('organisatie')) . '</td>
                                </tr>
                                <tr>
                                    <td>Telefoon:</td>
                                    <td>' . _e(http_post('telefoon')) . '</td>
                                </tr>
                                <tr>
                                    <td>E-mail:</td>
                                    <td>' . _e(http_post('email')) . '</td>
                                </tr>
                                <tr>
                                    <td>Onderwerp:</td>
                                    <td>' . _e(http_post('onderwerp')) . '</td>
                                </tr>
                                <tr>
                                    <td>Bericht:</td>
                                    <td>' . nl2br(_e(http_post('bericht'))) . '</td>
                                </tr>
                              </table>
                            <br />
                            <br />
                            Neem zo spoedig mogelijk contact op voor de afhandeling.<br />
                        </span>
                        ';

            MailManager::sendMail(CLIENT_DEFAULT_EMAIL_TO, $sSubject, $sMail, http_post('email'), http_post('email'));
            FormBackupManager::makeFormBackup('contactformulier', $_POST);

            http_redirect('/' . http_get('controller') . '/bedankt?conversion=1');
        } else {
            if ($bLogError)
                Debug::logError("", "Contact form on frontend validate error", __FILE__, __LINE__, "Tried to post the Contact form with wrong values despite javascript check (or rumpelstiltkin values are incorrect)." . print_r($_POST, 1), Debug::LOG_IN_EMAIL);
        }
    }

    $oPageLayout->sPagePath = PAGES_FOLDER . '/contact/contact_form.inc.php';
}

# used for Google maps
$sLatitude = Settings::get('contactLatitude');
$sLongitude = Settings::get('contactLongitude');
$sClientName = _e(Settings::get('clientName'));
$sClientStreet = _e(Settings::get('clientStreet'));
$sClientPostalCode = _e(Settings::get('clientPostalCode'));
$sClientCity = _e(Settings::get('clientCity'));
$sClientStreetEncoded = _e(urlencode(Settings::get('clientStreet')));
$sClientCityEncoded = _e(urlencode(Settings::get('clientCity')));

$sJavascript = <<<EOT
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script>
    function initGoogleMaps() {
    
        /* Set google maps option draggable false on mobile*/
        if ($(document).width() <= 767) {
            var bMapoption = false;
        } else {
            var bMapoption = true;
        }
        
        /* lat/long used by Google Maps */
        var myLatLng = new google.maps.LatLng($sLatitude, $sLongitude);
        /* options for the map */
        var myOptions = {
            zoom: 15,
            center: myLatLng,
            draggable: bMapoption,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        /* placing the map object */
        var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
        /* placing the marker with his options */
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: '$sClientName',
            icon: '/images/layout/motoren/google-maps-icon.png'
        });
        /* for responsive layouts */
        google.maps.event.addDomListener(window, 'resize', function() {
            map.setCenter(myLatLng);
        });
        var infowindow = new google.maps.InfoWindow({
            content: '<p class="google-maps-window">$sClientName<br />$sClientStreet<br />$sClientPostalCode $sClientCity</p><a href="https://maps.google.nl/maps?q=$sClientStreetEncoded,+$sClientCityEncoded&hl=nl&t=m&z=17&iwloc=A" target="_blank">Routebeschrijving</a>'
        });
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });
    }
        
    $(window).load(function(){
        initGoogleMaps();
    });
    </script>    
EOT;
$oPageLayout->addJavascriptBottom($sJavascript);

include_once TEMPLATES_FOLDER . '/default.tmpl.php';
?>