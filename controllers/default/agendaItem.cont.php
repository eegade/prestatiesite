<?php

/*
 * controller to get all data for homepage
 */

# make pageLayout Object
$oPageLayout = new PageLayout();

$oPageForHeaderImage = PageManager::getPageByUrlPath('/' . http_get('controller'));
$oHeaderImage = $oPageForHeaderImage->getHeaderImages('first-online');

# agenda detail page
if (is_numeric(http_get('param1'))) {
    $oAgendaItem = AgendaItemManager::getAgendaItemById(http_get('param1'));

    if (empty($oAgendaItem) || !$oAgendaItem->isOnline()) {
        showHttpError('404');
    }

    $oPageLayout->sWindowTitle = $oAgendaItem->getWindowTitle();
    $oPageLayout->sMetaDescription = $oAgendaItem->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oAgendaItem->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oAgendaItem->getCrumbles($oAgendaItem->agendaItemId));
    $oPageLayout->sCanonical = $oAgendaItem->getUrlPath();
    $oPageLayout->sOGType = 'article';
    $oPageLayout->sOGTitle = $oAgendaItem->getWindowTitle();
    $oPageLayout->sOGDescription = $oAgendaItem->getMetaDescription();
    $oPageLayout->sOGUrl = getCurrentUrl();
    if (($oImage = $oAgendaItem->getImages('first-online')) && ($oImageFile = $oImage->getImageFileByReference('crop_small'))) {
        $oPageLayout->sOGImage = CLIENT_HTTP_URL . $oImageFile->link;
    }

    $aImages = $oAgendaItem->getImages();
    
    $sBackLink = '/agenda';

    $oPageLayout->sPagePath = PAGES_FOLDER . '/agendaItems/agendaItem_details.inc.php';
}
elseif (http_get('param1')=='agendaarchief'){
    # get page by urlPath (/agenda/agendaarchief)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    if ($oPage->level > 1)
        $oPageForMenu = PageManager::getPageByUrlPath('/' . http_get('controller'));
    else
        $oPageForMenu = $oPage;

    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

     // I only show the Agenda items which are expired in time
    $aFilter = array();
    $aFilter['showAll'] = '1';
    $aFilter['outOfDate'] = '1';
    
    $aNewsItems = NewsItemManager::getNewsItemsByFilter();
    $iPerPage = 6;
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if(!is_numeric($iCurrPage)){
        http_redirect($oPage->getUrlPath());
    }

    $aAgendaItems = AgendaItemManager::getAgendaItemsByFilter($aFilter, $iPerPage, $iStart, $iFoundRows);
    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;
    
    // Pagination
    // page is greater than max page count, redirect to main page
    if ($iPageCount > 0 && $iCurrPage > $iPageCount) {
        http_redirect($oPage->getUrlPath());
    }

    // pagecount is greater than 1 and not last page
    if ($iPageCount > 1 && $iCurrPage < $iPageCount) {
        $oPageLayout->sRelNext = CLIENT_HTTP_URL . $oPage->getUrlPath() . '?page=' . ($iCurrPage + 1);
    }

    // pagecount is greater than 1 and not first page
    if ($iPageCount > 1 && $iCurrPage > 1) {
        $oPageLayout->sRelPrev = CLIENT_HTTP_URL . $oPage->getUrlPath();
        // is second page, previous is url without page
        if (($iCurrPage - 1) > 1) {
            $oPageLayout->sRelPrev .= '?page=' . ($iCurrPage - 1);
        }
    }

    // is first page, set canonical in case ?page=1 is set
    if ($iCurrPage == 1) {
        $oPageLayout->sCanonical = CLIENT_HTTP_URL . $oPage->getUrlPath();
    }
    
    $oPageLayout->sPagePath = PAGES_FOLDER . '/agendaItems/agendaItems_archive.inc.php';
}
# agenda overview
else {
    # get page by urlPath (/agenda)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    if ($oPage->level > 1)
        $oPageForMenu = PageManager::getPageByUrlPath('/' . http_get('controller'));
    else
        $oPageForMenu = $oPage;

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $aImages = $oPage->getImages();
    
    $iPerPage = 3;
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if(!is_numeric($iCurrPage) || $iCurrPage <= 0){
        http_redirect($oPage->getUrlPath());
    }
    
    // I show all the valid Agenda Items
    $aFilter = array();

    $aAgendaItems = AgendaItemManager::getAgendaItemsByFilter($aFilter, $iPerPage,$iStart, $iFoundRows);
    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;
    
    // Pagination
    // page is greater than max page count, redirect to main page
    if ($iPageCount > 0 && $iCurrPage > $iPageCount) {
        http_redirect($oPage->getUrlPath());
    }

    // pagecount is greater than 1 and not last page
    if ($iPageCount > 1 && $iCurrPage < $iPageCount) {
        $oPageLayout->sRelNext = CLIENT_HTTP_URL . $oPage->getUrlPath() . '?page=' . ($iCurrPage + 1);
    }

    // pagecount is greater than 1 and not first page
    if ($iPageCount > 1 && $iCurrPage > 1) {
        $oPageLayout->sRelPrev = CLIENT_HTTP_URL . $oPage->getUrlPath();
        // is second page, previous is url without page
        if (($iCurrPage - 1) > 1) {
            $oPageLayout->sRelPrev .= '?page=' . ($iCurrPage - 1);
        }
    }

    // is first page, set canonical in case ?page=1 is set
    if ($iCurrPage == 1) {
        $oPageLayout->sCanonical = CLIENT_HTTP_URL . $oPage->getUrlPath();
    }
    
    $oPageLayout->sPagePath = PAGES_FOLDER . '/agendaItems/agendaItems_overview.inc.php';

}

# include the error template
include_once TEMPLATES_FOLDER . '/default.tmpl.php';
?>