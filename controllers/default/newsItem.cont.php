<?php

/*
 * controller to get all data for homepage
 */

# make pageLayout Object
$oPageLayout = new PageLayout();

$oPageForHeaderImage = PageManager::getPageByUrlPath('/' . http_get('controller'));
$oHeaderImage = $oPageForHeaderImage->getHeaderImages('first-online');

# news detail page
if (is_numeric(http_get('param1'))) {

    $oNewsItem = NewsItemManager::getNewsItemById(http_get('param1'));

    if (empty($oNewsItem) || !$oNewsItem->isOnline()) {
        showHttpError('404');
    }

    $oPageLayout->sWindowTitle = $oNewsItem->getWindowTitle();
    $oPageLayout->sMetaDescription = $oNewsItem->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oNewsItem->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oNewsItem->getCrumbles(http_get('categoryId')));
    $oPageLayout->sCanonical = $oNewsItem->getUrlPath();
    $oPageLayout->sOGType = 'article';
    $oPageLayout->sOGTitle = $oNewsItem->getWindowTitle();
    $oPageLayout->sOGDescription = $oNewsItem->getMetaDescription();
    $oPageLayout->sOGUrl = getCurrentUrl();
    if (($oImage = $oNewsItem->getImages('first-online')) && ($oImageFile = $oImage->getImageFileByReference('crop_small'))) {
        $oPageLayout->sOGImage = CLIENT_HTTP_URL . $oImageFile->link;
    }

    $aImages = $oNewsItem->getImages();
    $aFiles = $oNewsItem->getFiles();
    $aLinks = $oNewsItem->getLinks();
    $aYoutubeVideos = $oNewsItem->getYoutubeLinks();

    if (http_get('categoryId') && ($oNewsItemCategory = NewsItemCategoryManager::getNewsItemCategoryById(http_get('categoryId')))) {
        $sBackLink = $oNewsItemCategory->getUrlPath();
    } else {
        $sBackLink = '/nieuws';
    }

    $oPageLayout->sPagePath = PAGES_FOLDER . '/newsItems/newsItem_details.inc.php';
} elseif (http_get('param1') == 'nieuwsarchief') {

    # get page by urlPath (/nieuws/nieuwsarchief)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    if ($oPage->level > 1)
        $oPageForMenu = PageManager::getPageByUrlPath('/' . http_get('controller'));
    else
        $oPageForMenu = $oPage;

    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $iPerPage = 6;
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if(!is_numeric($iCurrPage) || $iCurrPage <= 0){
        http_redirect($oPage->getUrlPath());
    }

    $aNewsItems = NewsItemManager::getNewsItemsByFilter(array(), $iPerPage, $iStart, $iFoundRows);

    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;

    // Pagination
    // page is greater than max page count, redirect to main page
    if ($iPageCount > 0 && $iCurrPage > $iPageCount) {
        http_redirect($oPage->getUrlPath());
    }

    // pagecount is greater than 1 and not last page
    if ($iPageCount > 1 && $iCurrPage < $iPageCount) {
        $oPageLayout->sRelNext = CLIENT_HTTP_URL . $oPage->getUrlPath() . '?page=' . ($iCurrPage + 1);
    }

    // pagecount is greater than 1 and not first page
    if ($iPageCount > 1 && $iCurrPage > 1) {
        $oPageLayout->sRelPrev = CLIENT_HTTP_URL . $oPage->getUrlPath();
        // is second page, previous is url without page
        if (($iCurrPage - 1) > 1) {
            $oPageLayout->sRelPrev .= '?page=' . ($iCurrPage - 1);
        }
    }

    // always set canonical to first page
    $oPageLayout->sCanonical = CLIENT_HTTP_URL . $oPage->getUrlPath();


    $oPageLayout->sPagePath = PAGES_FOLDER . '/newsItems/newsItems_archive.inc.php';
} elseif (http_get('param1')) {

    if (!BB_WITH_NEWS_CATEGORIES) {
        http_redirect('/' . http_get('controller'));
    }

    $oNewsItemCategory = NewsItemCategoryManager::getNewsItemCategoryByUrlPart(http_get('param1'));
    if (!$oNewsItemCategory || !$oNewsItemCategory->isOnline()) {
        http_redirect('/' . http_get('controller'));
    }

    $iPerPage = 3;
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if(!is_numeric($iCurrPage) || $iCurrPage <= 0){
        http_redirect('/' . http_get('controller'));
    }

    $aNewsItems = NewsItemManager::getNewsItemsByFilter(array('newsItemCategoryId' => $oNewsItemCategory->newsItemCategoryId), $iPerPage, $iStart, $iFoundRows);

    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;

    // page is greater than max page count, redirect to main page
    if ($iPageCount > 0 && $iCurrPage > $iPageCount) {
        http_redirect($oNewsItemCategory->getUrlPath());
    }

    // pagecount is greater than 1 and not last page
    if ($iPageCount > 1 && $iCurrPage < $iPageCount) {
        $oPageLayout->sRelNext = CLIENT_HTTP_URL . $oNewsItemCategory->getUrlPath() . '?page=' . ($iCurrPage + 1);
    }

    // pagecount is greater than 1 and not first page
    if ($iPageCount > 1 && $iCurrPage > 1) {
        $oPageLayout->sRelPrev = CLIENT_HTTP_URL . $oNewsItemCategory->getUrlPath();
        // is second page, previous is url without page
        if (($iCurrPage - 1) > 1) {
            $oPageLayout->sRelPrev .= '?page=' . ($iCurrPage - 1);
        }
    }

    // always set canonical to first page
    $oPageLayout->sCanonical = CLIENT_HTTP_URL . $oNewsItemCategory->getUrlPath();

    $oPageLayout->sWindowTitle = $oNewsItemCategory->getWindowTitle();
    $oPageLayout->sMetaDescription = $oNewsItemCategory->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oNewsItemCategory->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oNewsItemCategory->getCrumbles());
    $oPageLayout->sPagePath = PAGES_FOLDER . '/newsItems/newsItemCategory_overview.inc.php';
}
# news overview
else {
    # get page by urlPath (/news)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    if ($oPage->level > 1)
        $oPageForMenu = PageManager::getPageByUrlPath('/' . http_get('controller'));
    else
        $oPageForMenu = $oPage;

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $aImages = $oPage->getImages();
    $aYoutubeVideos = $oPage->getYoutubeLinks();
    $aFiles = $oPage->getFiles();
    $aLinks = $oPage->getLinks();

    $aNewsItems = NewsItemManager::getNewsItemsByFilter(array(), 5);

    $oPageLayout->sPagePath = PAGES_FOLDER . '/newsItems/newsItems_overview.inc.php';
}

$aNewsItemCategoriesForMenu = array();
if (BB_WITH_NEWS_CATEGORIES) {
    $aNewsItemCategoriesForMenu = NewsItemCategoryManager::getNewsItemCategoriesByFilter();
}

# include the error template
include_once TEMPLATES_FOLDER . '/default.tmpl.php';
?>