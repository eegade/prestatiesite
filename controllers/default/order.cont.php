<?php

/*
 * controller to handle the shopping basket (order)
 */

# make PageLayout Object
$oPageLayout = new PageLayout();

# define the order settings in the SESSION
if (empty($_SESSION['aOrderSettings']))
    $_SESSION['aOrderSettings'] = array();

# define variable for checking if this is an AJAX request
$bAjax = http_post("ajax", false);


# order products
if (http_get('param1') == 'bestellen') {

    # return to basket if there are no products
    if (count($_SESSION['oBasket']->getProducts()) <= 0) {
        http_redirect('/' . http_get('controller'));
    }

    # go to signup page if the Customer is not logged in and if it's needed
    if (Settings::get('withCustomers') && Settings::get('loginRequiredBeforeOrder') && empty($oCurrentCustomer)) {
        $_SESSION['frontendLoginReferrer'] = getCurrentUrl();
        http_redirect('/account');
    }

    // if user is logged in
    if (!empty($oCurrentCustomer)) {
        # add Customer info to the Order (by default, the delivery is the same as invoice)
        $_SESSION['oBasket']->setCustomer($oCurrentCustomer);
        $_SESSION['oBasket']->setInvoiceToDelivery();
        $_SESSION['oBasket']->setLatLong();
    }
    // true by default
    $_SESSION['aOrderSettings']['delivery_same_as_invoice'] = true;

    # action saveOrder
    if (!empty($_POST['saveOrder'])) {

        $_SESSION['oBasket']->email = http_post('email');
        $_SESSION['oBasket']->invoice_companyName = http_post('invoice_companyName');
        $_SESSION['oBasket']->invoice_gender = http_post('invoice_gender');
        $_SESSION['oBasket']->invoice_firstName = http_post('invoice_firstName');
        $_SESSION['oBasket']->invoice_insertion = http_post('invoice_insertion');
        $_SESSION['oBasket']->invoice_lastName = http_post('invoice_lastName');
        $_SESSION['oBasket']->invoice_address = http_post('invoice_address');
        $_SESSION['oBasket']->invoice_houseNumber = http_post('invoice_houseNumber');
        $_SESSION['oBasket']->invoice_houseNumberAddition = http_post('invoice_houseNumberAddition');
        $_SESSION['oBasket']->invoice_postalCode = http_post('invoice_postalCode');
        $_SESSION['oBasket']->invoice_city = http_post('invoice_city');
        $_SESSION['oBasket']->invoice_phone = http_post('invoice_phone');

        if (http_post('delivery_same_as_invoice')) {
            $_SESSION['oBasket']->setInvoiceToDelivery();
        } else {
            $_SESSION['oBasket']->delivery_companyName = http_post('delivery_companyName');
            $_SESSION['oBasket']->delivery_gender = http_post('delivery_gender');
            $_SESSION['oBasket']->delivery_firstName = http_post('delivery_firstName');
            $_SESSION['oBasket']->delivery_insertion = http_post('delivery_insertion');
            $_SESSION['oBasket']->delivery_lastName = http_post('delivery_lastName');
            $_SESSION['oBasket']->delivery_address = http_post('delivery_address');
            $_SESSION['oBasket']->delivery_houseNumber = http_post('delivery_houseNumber');
            $_SESSION['oBasket']->delivery_houseNumberAddition = http_post('delivery_houseNumberAddition');
            $_SESSION['oBasket']->delivery_postalCode = http_post('delivery_postalCode');
            $_SESSION['oBasket']->delivery_city = http_post('delivery_city');
        }

        $_SESSION['oBasket']->setLatLong();

        if ((DeliveryMethodManager::isValidDeliveryMethodPaymentMethodRelation($_SESSION['oBasket']->deliveryMethodId, $_SESSION['oBasket']->paymentMethodId))) {
            $oDeliveryMethod = DeliveryMethodManager::getDeliveryMethodById($_SESSION['oBasket']->deliveryMethodId);
            $oPaymentMethod = PaymentMethodManager::getPaymentMethodById($_SESSION['oBasket']->paymentMethodId);
            $_SESSION['oBasket']->setDeliveryMethod($oDeliveryMethod);
            $_SESSION['oBasket']->setPaymentMethod($oPaymentMethod);
        } else {
            $_SESSION['oBasket']->deliveryMethodId = null;
            $_SESSION['oBasket']->paymentMethodId = null;
        }

        // if object is valid, save
        if ($_SESSION['oBasket']->isValid()) {

            OrderManager::saveOrder($_SESSION['oBasket']); //save object
            // process status new, handle status and set next status
            $_SESSION['oBasket']->processStatus(OrderStatus::STATUS_NEW, true, true, '', Settings::get('newOrderClientBCC'));

            // check if there needs to be an online payment
            if ($_SESSION['oBasket']->getPaymentMethod()->isOnlinePaymentMethod) {
                // define and save the new OrderPayment
                $oOrderPayment = new OrderPayment(array('price' => $_SESSION['oBasket']->getTotal(true), 'status' => OrderPayment::STATUS_AWAITING_PAYMENT, 'orderId' => $_SESSION['oBasket']->orderId, 'paymentMethodId' => $_SESSION['oBasket']->paymentMethodId));
                OrderPaymentManager::savePayment($oOrderPayment); //save the OrderPayment and OrderStatus
            }

            # check if the payment needs to be handles online
            if ($_SESSION['oBasket']->getPaymentMethod()->isOnlinePaymentMethod) {
                $_SESSION['iLatestOrderPaymentId'] = $oOrderPayment->orderPaymentId;
            }

            $sRedirectPage = $_SESSION['oBasket']->getPaymentMethod()->redirectPage;
            unset($_SESSION['oBasket']); //empty the basket
            # redirect to payment page
            http_redirect($sRedirectPage);
        } else {
            $aErrorsBasket = array();
            if (!$_SESSION['oBasket']->isPropValid("email"))
                $aErrorsBasket['email'] = 'Vul een geldig emailadres in (bij de factuurgegevens).';
            if (!$_SESSION['oBasket']->isPropValid("invoice_gender"))
                $aErrorsBasket['invoice_gender'] = 'Kies een geslacht (bij de factuurgegevens).';
            if (!$_SESSION['oBasket']->isPropValid("invoice_firstName"))
                $aErrorsBasket['invoice_firstName'] = 'Vul uw voornaam in (bij de factuurgegevens).';
            if (!$_SESSION['oBasket']->isPropValid("invoice_lastName"))
                $aErrorsBasket['invoice_lastName'] = 'Vul uw achternaam in (bij de factuurgegevens).';
            if (!$_SESSION['oBasket']->isPropValid("invoice_address"))
                $aErrorsBasket['invoice_address'] = 'Vul uw adres in (bij de factuurgegevens).';
            if (!$_SESSION['oBasket']->isPropValid("invoice_houseNumber"))
                $aErrorsBasket['invoice_houseNumber'] = 'Vul een geldig huisnummer in met alleen nummers (bij de factuurgegevens).';
            if (!$_SESSION['oBasket']->isPropValid("invoice_postalCode"))
                $aErrorsBasket['invoice_postalCode'] = 'Vul uw postcode in (bij de factuurgegevens).';
            if (!$_SESSION['oBasket']->isPropValid("invoice_city"))
                $aErrorsBasket['invoice_city'] = 'Vul uw plaats in (bij de factuurgegevens).';
            if (!$_SESSION['oBasket']->isPropValid("delivery_gender"))
                $aErrorsBasket['delivery_gender'] = 'Kies een geslacht (bij de aflevergegevens).';
            if (!$_SESSION['oBasket']->isPropValid("delivery_firstName"))
                $aErrorsBasket['delivery_firstName'] = 'Vul een voornaam in (bij de aflevergegevens).';
            if (!$_SESSION['oBasket']->isPropValid("delivery_lastName"))
                $aErrorsBasket['delivery_lastName'] = 'Vul een achternaam in (bij de aflevergegevens).';
            if (!$_SESSION['oBasket']->isPropValid("delivery_address"))
                $aErrorsBasket['delivery_address'] = 'Vul een adres in (bij de aflevergegevens).';
            if (!$_SESSION['oBasket']->isPropValid("delivery_houseNumber"))
                $aErrorsBasket['delivery_houseNumber'] = 'Vul een geldig huisnummer in met alleen nummers (bij de aflevergegevens).';
            if (!$_SESSION['oBasket']->isPropValid("delivery_postalCode"))
                $aErrorsBasket['delivery_postalCode'] = 'Vul een postcode in (bij de aflevergegevens).';
            if (!$_SESSION['oBasket']->isPropValid("delivery_city"))
                $aErrorsBasket['delivery_city'] = 'Vul een plaats in (bij de factuurgegevens).';
            if (!$_SESSION['oBasket']->isPropValid("deliveryMethodId"))
                $aErrorsBasket['deliveryMethodId'] = 'Verzendwijze wordt niet geaccepteerd';
            if (!$_SESSION['oBasket']->isPropValid("paymentMethodId"))
                $aErrorsBasket['paymentMethodId'] = 'Betaalwijze wordt niet geaccepteerd';
            if (!$_SESSION['oBasket']->isPropValid("deliveryPaymentMethodCombination"))
                $aErrorsBasket['deliveryPaymentMethodCombination'] = 'Combinatie van aflevermethode en betaalmethode is niet toegestaan.';
            Debug::logError("", "Frontend Order module php validate error", __FILE__, __LINE__, "Tried to save an Order with wrong values despite javascript check.<br />" . _d($_POST, 1, 1) . _d($_SESSION['oBasket'], 1, 1), Debug::LOG_IN_EMAIL);
        }
    }

    # get page by urlPath (/winkelwagen/bestellen)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $oPageLayout->sPagePath = PAGES_FOLDER . '/orders/order.inc.php';

    $bWithLogin = false;
    if (BB_WITH_CUSTOMERS && empty($oCurrentCustomer)) {
        $bWithLogin = true;
    }
}

# for default text pages
elseif (http_get('param1') == 'bestelling-geplaatst' || http_get('param1') == 'betaling-mislukt' || http_get('param1') == 'betaling-geannuleerd' || http_get('param1') == 'betaling-ontvangen' || http_get('param1') == 'betaling-al-verwerkt') {
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online) {
        showHttpError('404');
    }

    if ($oPage->level > 1)
        $oPageForMenu = PageManager::getPageByUrlPath('/' . http_get('controller'));
    else
        $oPageForMenu = $oPage;

    // get header image for page or get header image from parent if has one
    $oHeaderImage = $oPage->getHeaderImages('first-online');
    if (!$oHeaderImage && $oPage->parentPageId) {
        $oHeaderImage = $oPage->getParent()->getHeaderImages('first-online');
    }

    $oPageLayout->sPagePath = PAGES_FOLDER . '/pages/page_details.inc.php';
    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $aImages = $oPage->getImages();
    $aYoutubeVideos = $oPage->getYoutubeLinks();
    $aFiles = $oPage->getFiles();
    $aLinks = $oPage->getLinks();
}

# basket
else {
    # add a OrderProduct to the basket
    if (http_post("action") == 'addProductToBasket') {
        
        $oResObj = new stdClass(); //standard class for json feedback
        $oResObj->success = false;

        # check for required values
        if (!is_numeric(http_post('catalogProductId'))) {
            $oResObj->orderStatusUpdate = 'Product kon niet toegevoegd worden aan het winkelmandje. Probeer het later nog eens.';
            if ($bAjax) {
                die(json_encode($oResObj));
            }
            $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
            http_redirect('/' . http_get('controller'));
        }

        # get the CatalogProduct
        $oCatalogProduct = CatalogProductManager::getProductById(http_post('catalogProductId'));

        if ($oCatalogProduct->getProductType()->withSizes) {
            $oCatalogProductSize = CatalogProductSizeManager::getProductSizeById(http_post('catalogProductSizeId'));
        } else {
            $oCatalogProductSize = CatalogProductSizeManager::getProductSizeById(CatalogProductSize::sizeId_nosize);
        }

        if ($oCatalogProduct->getProductType()->withColors) {
            $oCatalogProductColor = CatalogProductColorManager::getProductColorById(http_post('catalogProductColorId'));
        } else {
            $oCatalogProductColor = CatalogProductColorManager::getProductColorById(CatalogProductColor::colorId_nocolor);
        }

        // check if product, color and size and stock is available
        if (!empty($oCatalogProduct) && !empty($oCatalogProductSize) && !empty($oCatalogProductColor)) {
            $oSizeColorRelation = CatalogProductSizeColorRelationManager::getCatalogProductSizeColorRelation($oCatalogProduct->catalogProductId, $oCatalogProductSize->catalogProductSizeId, $oCatalogProductColor->catalogProductColorId);
        }

        // size color relation
        if (empty($oSizeColorRelation)) {
            if ($oCatalogProduct->getProductType()->withSizes && $oCatalogProduct->getProductType()->withColors) {
                $oResObj->orderStatusUpdate = 'Kies eerst een maat, kleur en aantal';
            } elseif ($oCatalogProduct->getProductType()->withSizes) {
                $oResObj->orderStatusUpdate = 'Kies eerst een maat en aantal';
            } elseif ($oCatalogProduct->getProductType()->withColors) {
                $oResObj->orderStatusUpdate = 'Kies eerst een kleur en aantal';
            }
            if ($bAjax) {
                die(json_encode($oResObj));
            }
            $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
            http_redirect('/' . http_get('controller'));
        }

        # define the OrderProduct

        $oOrderProduct = new OrderProduct();
        $oOrderProduct->setOrderProduct($oCatalogProduct, $oSizeColorRelation);

        // get possible existing order product
        $oExistingOrderProduct = $_SESSION['oBasket']->getProduct($oCatalogProduct->catalogProductId, $oCatalogProductSize->catalogProductSizeId, $oCatalogProductColor->catalogProductColorId);

        $iAmount = abs(http_post('amount', 1));
        if (is_numeric($iAmount)) {
            $oOrderProduct->amount = $iAmount;
        }

        // check new amount also with old amount
        $iNewAmount = ($oExistingOrderProduct ? $oExistingOrderProduct->amount : 0) + $oOrderProduct->amount;

        if ($oSizeColorRelation->stock !== null && $iNewAmount > $oSizeColorRelation->stock) {
            $oOrderProduct->amount = $oSizeColorRelation->stock;
            $oResObj->orderStatusUpdate = 'Maximum voorraad van ' . $oOrderProduct->amount . ' product(en) is toegevoegd';
            if (!$bAjax) {
                $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
            }
            // not so many in stock, reset product in order with max amount
            $_SESSION['oBasket']->unsetProduct($oSizeColorRelation->catalogProductId, $oSizeColorRelation->catalogProductSizeId, $oSizeColorRelation->catalogProductColorId);
        }


        # add OrderProduct to basket if valid
        if ($oOrderProduct->isValid()) {

            $_SESSION['oBasket']->setProduct($oOrderProduct);
            # calculates  the discount again
            $_SESSION['oBasket']->getDiscount(Settings::get('taxIncluded'));

            # die with object if successful
            if ($bAjax) {
                $oResObj->success = true;
                if (empty($oResObj->orderStatusUpdate))
                    $oResObj->orderStatusUpdate = 'Product is aan het winkelmandje toegevoegd';
                if (!$bAjax && empty($_SESSION['orderStatusUpdate'])) {
                    $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
                }
                $oResObj->countProducts = $_SESSION['oBasket']->countProducts();
                ob_start();
                include(TEMPLATES_FOLDER . '/elements/miniBasketContent.inc.php');
                $oResObj->miniBasketContent = ob_get_clean();
                die(json_encode($oResObj));
            }
        } else {

            $oResObj->orderStatusUpdate = 'Product kon niet toegevoegd worden aan het winkelmandje. Probeer het later nog eens.';

            if ($bAjax) {
                die(json_encode($oResObj));
            }
            $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
        }
        http_redirect('/' . http_get('controller'));
    }

    # remove product from basket
    if (isset($_POST["removeProductFromBasket"])) {
        $oResObj = new stdClass(); //standard class for json feedback
        $oResObj->success = false;
        
        # check for required values
        if (!is_numeric(http_post('catalogProductId')) || !is_numeric(http_post('catalogProductSizeId')) || !is_numeric(http_post('catalogProductColorId'))) {
            $oResObj->orderStatusUpdate = 'Product kon niet verwijderd worden van het winkelmandje. Probeer het later nog eens.';
            if ($bAjax) {  
                die(json_encode($oResObj));
            }
            $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
            http_redirect('/' . http_get('controller'));
        }

        # remove the product from the basket
        $_SESSION['oBasket']->unsetProduct(http_post('catalogProductId'), http_post('catalogProductSizeId'), http_post('catalogProductColorId'));
        $oResObj->countProducts = $_SESSION['oBasket']->countProducts();

        # calculate  the discount again
        $_SESSION['oBasket']->getDiscount(Settings::get('taxIncluded'));

        # die with object if successful
        if ($bAjax) {
            $oResObj->success = true;
            ob_start();
            include(TEMPLATES_FOLDER . '/elements/miniBasketContent.inc.php');
            $oResObj->miniBasketContent = ob_get_clean();

            die(json_encode($oResObj));
        }
        http_redirect('/' . http_get('controller'));
    }

    # edit amount of a product in the basket
    if (isset($_POST["updateProductAmount"])) {
        $oResObj = new stdClass(); //standard class for json feedback
        $oResObj->success = false;

        if (!is_numeric(http_post('catalogProductId')) || !is_numeric(http_post('catalogProductSizeId')) || !is_numeric(http_post('catalogProductColorId')) || !is_numeric(http_post('amount')) || http_post('amount') < 0) {
            $_SESSION['orderStatusUpdate'] = 'Productaantal kon niet gewijzigd worden. Probeer het later nog eens.'; //save status update into session

            if ($bAjax) {
                $oResObj->orderStatusUpdate = 'Productaantal kon niet gewijzigd worden. Probeer het later nog eens.';
                die(json_encode($oResObj));
            }

            http_redirect('/' . http_get('controller'));
        }

        if (http_post('amount') == 0) {
            # remove the product from the basket if the amount is 0
            $_SESSION['oBasket']->unsetProduct(http_post('catalogProductId'), http_post('catalogProductSizeId'), http_post('catalogProductColorId'));
        } else {
            $oCatalogProduct = CatalogProductManager::getProductById(http_post('catalogProductId'));
            if ($oCatalogProduct->getProductType()->withSizes) {
                $oCatalogProductSize = CatalogProductSizeManager::getProductSizeById(http_post('catalogProductSizeId'));
            } else {
                $oCatalogProductSize = CatalogProductSizeManager::getProductSizeById(CatalogProductSize::sizeId_nosize);
            }

            if ($oCatalogProduct->getProductType()->withColors) {
                $oCatalogProductColor = CatalogProductColorManager::getProductColorById(http_post('catalogProductColorId'));
            } else {
                $oCatalogProductColor = CatalogProductColorManager::getProductColorById(CatalogProductColor::colorId_nocolor);
            }

            // check if product, color and size and stock is available
            if (!empty($oCatalogProduct) && !empty($oCatalogProductSize) && !empty($oCatalogProductColor)) {
                $oSizeColorRelation = CatalogProductSizeColorRelationManager::getCatalogProductSizeColorRelation($oCatalogProduct->catalogProductId, $oCatalogProductSize->catalogProductSizeId, $oCatalogProductColor->catalogProductColorId);
            }

            if ($oSizeColorRelation) {
                $iAmount = abs(http_post('amount', 0));
                if (is_numeric($iAmount)) {
                    if ($oSizeColorRelation->stock !== null && $iAmount > $oSizeColorRelation->stock) {
                        $iAmount = $oSizeColorRelation->stock;
                        $_SESSION['orderStatusUpdate'] = 'Maximum voorraad van ' . $iAmount . ' product(en) is toegevoegd'; //save status update into session
                        $oResObj->orderStatusUpdate = 'Maximum voorraad van ' . $iAmount . ' product(en) is toegevoegd';
                    }
                } else {
                    $iAmount = 0;
                }
            } else {
                $_SESSION['orderStatusUpdate'] = 'Kon het aantal niet wijzigen'; //save status update into session
                $oResObj->orderStatusUpdate = 'Kon het aantal niet wijzigen';
            }

            # update the OrderProduct's amount
            $_SESSION['oBasket']->setProductAmount(http_post('catalogProductId'), http_post('catalogProductSizeId'), http_post('catalogProductColorId'), $iAmount);
        }

        # calculates  the discount again
        $_SESSION['oBasket']->getDiscount(Settings::get('taxIncluded'));

        # die with object if Ajax
        if ($bAjax) {
            $oResObj->success = true;
            $oResObj->productId = 'product_' . http_post('catalogProductId') . '_' . http_post('catalogProductSizeId') . '_' . http_post('catalogProductColorId');
            $oResObj->productOriginalId = 'product_original_' . http_post('catalogProductId') . '_' . http_post('catalogProductSizeId') . '_' . http_post('catalogProductColorId');
            $oResObj->formattedProductOriginalSubtotal = decimal2valuta($_SESSION['oBasket']->getProduct(http_post('catalogProductId'), http_post('catalogProductSizeId'), http_post('catalogProductColorId'))->getSubTotalOriginalPrice(true));
            $oResObj->formattedProductSubtotal = decimal2valuta($_SESSION['oBasket']->getProduct(http_post('catalogProductId'), http_post('catalogProductSizeId'), http_post('catalogProductColorId'))->getSubTotalPrice(true));
            $oResObj->formattedSubtotal = decimal2valuta($_SESSION['oBasket']->getSubtotal(true));
            $oResObj->formattedTotalPrice = decimal2valuta($_SESSION['oBasket']->getTotal(false));
            $oResObj->formattedTotalPriceWithTax = decimal2valuta($_SESSION['oBasket']->getTotal(true));
            $oResObj->formattedTotalTaxPrice = decimal2valuta($_SESSION['oBasket']->getBTW());
            $oResObj->formattedTotalDiscount = decimal2valuta($_SESSION['oBasket']->getDiscount(true));

            $oResObj->formattedDeliveryPriceWithTax = decimal2valuta($_SESSION['oBasket']->getDeliveryCosts(true));
            die(json_encode($oResObj));
        }

        http_redirect('/' . http_get('controller'));
    }

    # update the DeliveryMethod in the basket (via ajax)
    if (http_post("action") == 'updateDeliveryMethod') {
        $oResObj = new stdClass(); //standard class for json feedback
        $oResObj->success = false;

        # check for required fields
        if (!is_numeric(http_post('deliveryMethodId'))) {
            $oResObj->orderStatusUpdate = 'Verzendmethode kon niet gewijzigd worden. Probeer het later nog eens.';
            if ($bAjax) {
                die(json_encode($oResObj));
            }
            $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
            http_redirect('/' . http_get('controller') . '/bestellen');
        }

        # get the DeliveryMethod
        $oDeliveryMethod = DeliveryMethodManager::getDeliveryMethodById(http_post('deliveryMethodId'));

        # see if the DeliveryMethod exists
        if (empty($oDeliveryMethod)) {
            $oResObj->orderStatusUpdate = 'Verzendmethode kon niet gewijzigd worden. Probeer het later nog eens.';
            if ($bAjax) {
                die(json_encode($oResObj));
            }
            $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
            http_redirect('/' . http_get('controller') . '/' . http_get('param1'));
        }

        # set the DeliveryMethod
        $_SESSION['oBasket']->setDeliveryMethod($oDeliveryMethod);

        # die with object if Ajax
        if ($bAjax) {
            $oResObj->success = true;
            $oResObj->formattedDeliveryPriceWithTax = decimal2valuta($_SESSION['oBasket']->getDeliveryCosts(true));
            $oResObj->formattedTotalPriceWithoutTax = decimal2valuta($_SESSION['oBasket']->getTotal(false));
            $oResObj->formattedTotalPriceWithTax = decimal2valuta($_SESSION['oBasket']->getTotal(true));
            $oResObj->formattedTotalTaxPrice = decimal2valuta($_SESSION['oBasket']->getBTW());
            $oResObj->deliveryMethodId = $_SESSION['oBasket']->deliveryMethodId;
            $oResObj->paymentMethodId = $_SESSION['oBasket']->paymentMethodId;
            $oResObj->paymentMethodOptions = '';
            $oResObj->formattedTotalDiscount = decimal2valuta($_SESSION['oBasket']->getDiscount(true));
            foreach (PaymentMethodManager::getPaymentMethodsByFilter(array('deliveryMethodId' => $_SESSION['oBasket']->deliveryMethodId)) as $oPaymentMethod) {
                $oResObj->paymentMethodOptions .= '<option value="' . $oPaymentMethod->paymentMethodId . '" ' . ($oPaymentMethod->paymentMethodId == $_SESSION['oBasket']->paymentMethodId ? 'selected' : '') . '>' . _e($oPaymentMethod->name) . '</option>';
            }

            die(json_encode($oResObj));
        }

        http_redirect('/' . http_get('controller') . '/' . http_get('param1'));
    }

    # update the PaymentMethod in the basket (via ajax)
    if (http_post("action") == 'updatePaymentMethod') {
        $oResObj = new stdClass(); //standard class for json feedback
        $oResObj->success = false;

        # check for required fields
        if (!is_numeric(http_post('paymentMethodId'))) {
            $oResObj->orderStatusUpdate = 'Betaalmethode kon niet gewijzigd worden. Probeer het later nog eens.';
            if ($bAjax) {
                die(json_encode($oResObj));
            }
            $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
            http_redirect('/' . http_get('controller') . '/' . http_get('param1'));
        }

        # get the PaymentMethod
        $oPaymentMethod = PaymentMethodManager::getPaymentMethodById(http_post('paymentMethodId'));

        # see if the PaymentMethod exists
        if (empty($oPaymentMethod) || !DeliveryMethodManager::isValidDeliveryMethodPaymentMethodRelation($_SESSION['oBasket']->deliveryMethodId, $oPaymentMethod->paymentMethodId)) {
            $oResObj->orderStatusUpdate = 'Betaalmethode kon niet gewijzigd worden. Probeer het later nog eens.';
            if ($bAjax) {
                die(json_encode($oResObj));
            }
            $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
            http_redirect('/' . http_get('controller') . '/' . http_get('param1'));
        }

        # set the PaymentMethod
        $_SESSION['oBasket']->setPaymentMethod($oPaymentMethod);

        # die with object if Ajax
        if ($bAjax) {
            $oResObj->success = true;
            $oResObj->formattedPaymentPriceWithTax = decimal2valuta($_SESSION['oBasket']->getPaymentCosts(true));
            $oResObj->formattedTotalPrice = decimal2valuta($_SESSION['oBasket']->getTotal(false));
            $oResObj->formattedTotalPriceWithTax = decimal2valuta($_SESSION['oBasket']->getTotal(true));
            $oResObj->formattedTotalTaxPrice = decimal2valuta($_SESSION['oBasket']->getBTW());
            die(json_encode($oResObj));
        }

        http_redirect('/' . http_get('controller') . '/' . http_get('param1'));
    }

    if (http_post("action") == 'applyDiscount') {
        $oResObj = new stdClass(); //standard class for json feedback
        $oResObj->success = false;

        // If no coupons Allow, it doesn't do anything
        if (!BB_WITH_COUPONS) {
            if ($bAjax) {
                die(json_encode($oResObj));
            }
            http_redirect('/' . http_get('controller') . '/' . http_get('param1'));
        }

        # check for required fields
        if (!http_post("couponCode")) {
            $oResObj->orderStatusUpdate = 'U moet een coupon code in voeren.';
            if ($bAjax) {
                die(json_encode($oResObj));
            }
            $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
            http_redirect('/' . http_get('controller') . '/' . http_get('param1'));
        }

        $sCouponCode = http_post('couponCode');

        #gets the coupon from database and verify if its valid
        $oCoupon = CouponManager::getCouponByCode($sCouponCode);

        if (empty($oCoupon) || !$oCoupon->isRedeemable()) {
            $oResObj->orderStatusUpdate = 'Coupon niet geldig';

            if ($bAjax) {
                die(json_encode($oResObj));
            }
            $_SESSION['orderStatusUpdate'] = $oResObj->orderStatusUpdate; //save status update into session
            http_redirect('/' . http_get('controller') . '/' . http_get('param1'));
        } else {
            $_SESSION['oBasket']->setDiscountCoupon($oCoupon);
            $oResObj->orderStatusUpdate = 'Kortingscode geaccepteerd';
        }


        # die with object if Ajax
        if ($bAjax) {
            $oResObj->success = true;
            $oResObj->formattedTotalPrice = decimal2valuta($_SESSION['oBasket']->getTotal(false));
            $oResObj->formattedTotalPriceWithTax = decimal2valuta($_SESSION['oBasket']->getTotal(true));
            $oResObj->formattedTotalTaxPrice = decimal2valuta($_SESSION['oBasket']->getBTW());
            $oResObj->formattedTotalDiscount = decimal2valuta($_SESSION['oBasket']->getDiscount(true));
            die(json_encode($oResObj));
        }

        http_redirect('/' . http_get('controller') . '/' . http_get('param1'));
    }

    if (http_post("action") == 'removeDiscount') {

        $_SESSION['oBasket']->setDiscountCoupon(null);

        # die with object if Ajax
        if ($bAjax) {
            $oResObj = new stdClass(); //standard class for json feedback
            $oResObj->success = true;
            $oResObj->formattedTotalPrice = decimal2valuta($_SESSION['oBasket']->getTotal(false));
            $oResObj->formattedTotalPriceWithTax = decimal2valuta($_SESSION['oBasket']->getTotal(true));
            $oResObj->formattedTotalTaxPrice = decimal2valuta($_SESSION['oBasket']->getBTW());
            $oResObj->formattedTotalDiscount = decimal2valuta($_SESSION['oBasket']->getDiscount(true));
            $oResObj->orderStatusUpdate = 'Korting verwijderd';
            die(json_encode($oResObj));
        }

        http_redirect('/' . http_get('controller') . '/' . http_get('param1'));
    }

    # get page by urlPath (/winkelwagen)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online) {
        showHttpError('404');
    }

    // get header image for page or get header image from parent if has one
    $oHeaderImage = $oPage->getHeaderImages('first-online');
    if (!$oHeaderImage && $oPage->parentPageId) {
        $oHeaderImage = $oPage->getParent()->getHeaderImages('first-online');
    }

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $oPageLayout->sPagePath = PAGES_FOLDER . '/orders/basket.inc.php';
}

# include the error template
include_once TEMPLATES_FOLDER . '/default.tmpl.php';
?>