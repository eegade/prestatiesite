<?php

/*
 * Controller to handle normal content pages
 */
$oPageLayout = new PageLayout();

if (is_numeric(http_get('param1'))) {

    $oBike = MotorenManager::getBikeById(http_get('param1'));

    // bike not found but connection OK
    if ($oBike === false) {
        showHttpError(404);
    } elseif ($oBike === null) {
        $oPage = PageManager::getPageByUrlPath('/motoren/tijdelijk-buiten-gebruik');
        if (empty($oPage) || !$oPage->online) {
            showHttpError('404');
        }

        $oPageForMenu = PageManager::getPageByUrlPath('/' . http_get('controller'));

        // get header image for page or get header image from parent if has one
        $oHeaderImage = $oPage->getHeaderImages('first-online');
        if (!$oHeaderImage && $oPage->parentPageId) {
            $oHeaderImage = $oPage->getParent()->getHeaderImages('first-online');
        }

        $oPageLayout->sPagePath = PAGES_FOLDER . '/pages/page_details.inc.php';
        $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
        $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
        $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
        $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());
        $oPageLayout->sOGType = 'website';
        $oPageLayout->sOGTitle = $oPage->getWindowTitle();
        $oPageLayout->sOGDescription = $oPage->getMetaDescription();
        $oPageLayout->sOGUrl = getCurrentUrl();
        if (($oImage = $oPage->getImages('first-online')) && ($oImageFile = $oImage->getImageFileByReference('crop_small'))) {
            $oPageLayout->sOGImage = CLIENT_HTTP_URL . $oImageFile->link;
        }

        $aImages = $oPage->getImages();
        $aYoutubeVideos = $oPage->getYoutubeLinks();
        $aFiles = $oPage->getFiles();
        $aLinks = $oPage->getLinks();
    } else {
        $oPageLayout->sPagePath = PAGES_FOLDER . '/motoren/motor_details.inc.php';
        $oPageLayout->sWindowTitle = $oBike->title;
        $oPageLayout->sMetaDescription = generateMetaDescription($oBike->description);
        $oPageLayout->sMetaKeywords = '';
        $oPageLayout->sOGType = 'product';
        $oPageLayout->sOGTitle = $oBike->title;
        $oPageLayout->sOGDescription = generateMetaDescription($oBike->description);
        $oPageLayout->sOGUrl = getCurrentUrl();
        if (!empty($oBike->aImages[0]->imageFancyboxImage)) {
            $oPageLayout->sOGImage = $oBike->aImages[0]->imageFancyboxImage;
        }

        $oPageLayout->generateCustomCrumblePath(array('Motoren' => '/motoren', $oBike->title => '/motoren/' . $oBike->bikeId . '/' . prettyUrlPart($oBike->title)));
    }
} else {
    $iPerPage = 8;
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if (!is_numeric($iCurrPage) || $iCurrPage <= 0) {
        http_redirect($oPage->getUrlPath());
    }

    $aBikes = MotorenManager::getBikesByFilter(array(), $iPerPage, $iStart, $iFoundRows);

    if ($aBikes === false) {
        showHttpError(404);
    } elseif ($aBikes === null) {
        $oPage = PageManager::getPageByUrlPath('/motoren/tijdelijk-buiten-gebruik');
        if (empty($oPage) || !$oPage->online) {
            showHttpError('404');
        }

        $oPageForMenu = PageManager::getPageByUrlPath('/' . http_get('controller'));

        // get header image for page or get header image from parent if has one
        $oHeaderImage = $oPage->getHeaderImages('first-online');
        if (!$oHeaderImage && $oPage->parentPageId) {
            $oHeaderImage = $oPage->getParent()->getHeaderImages('first-online');
        }

        $oPageLayout->sPagePath = PAGES_FOLDER . '/pages/page_details.inc.php';
        $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
        $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
        $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
        $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());
        $oPageLayout->sOGType = 'website';
        $oPageLayout->sOGTitle = $oPage->getWindowTitle();
        $oPageLayout->sOGDescription = $oPage->getMetaDescription();
        $oPageLayout->sOGUrl = getCurrentUrl();
        if (($oImage = $oPage->getImages('first-online')) && ($oImageFile = $oImage->getImageFileByReference('crop_small'))) {
            $oPageLayout->sOGImage = CLIENT_HTTP_URL . $oImageFile->link;
        }

        $aImages = $oPage->getImages();
        $aYoutubeVideos = $oPage->getYoutubeLinks();
        $aFiles = $oPage->getFiles();
        $aLinks = $oPage->getLinks();
    } else {

        $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

        if (empty($oPage) || !$oPage->online) {
            showHttpError('404');
        }

        if ($oPage->level > 1)
            $oPageForMenu = PageManager::getPageByUrlPath('/' . http_get('controller'));
        else
            $oPageForMenu = $oPage;

        $oPageLayout->sPagePath = PAGES_FOLDER . '/motoren/motors.inc.php';
        $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
        $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
        $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
        $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());
        $oPageLayout->sOGType = 'website';
        $oPageLayout->sOGTitle = $oPage->getWindowTitle();
        $oPageLayout->sOGDescription = $oPage->getMetaDescription();
        $oPageLayout->sOGUrl = getCurrentUrl();
        if (($oImage = $oPage->getImages('first-online')) && ($oImageFile = $oImage->getImageFileByReference('crop_small'))) {
            $oPageLayout->sOGImage = CLIENT_HTTP_URL . $oImageFile->link;
        }

        $aImages = $oPage->getImages();
        $aYoutubeVideos = $oPage->getYoutubeLinks();
        $aFiles = $oPage->getFiles();
        $aLinks = $oPage->getLinks();

        $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;

        // page is greater than max page count, redirect to main page
        if ($iPageCount > 0 && $iCurrPage > $iPageCount) {
            http_redirect('/motoren');
        }

        // pagecount is greater than 1 and not last page
        if ($iPageCount > 1 && $iCurrPage < $iPageCount) {
            $oPageLayout->sRelNext = CLIENT_HTTP_URL . '/motoren?page=' . ($iCurrPage + 1);
        }

        // pagecount is greater than 1 and not first page
        if ($iPageCount > 1 && $iCurrPage > 1) {
            $oPageLayout->sRelPrev = CLIENT_HTTP_URL . '/motoren';
            // is second page, previous is url without page
            if (($iCurrPage - 1) > 1) {
                $oPageLayout->sRelPrev .= '?page=' . ($iCurrPage - 1);
            }
        }

        // is first page, set canonical in case ?page=1 is set
        if ($iCurrPage == 1) {
            $oPageLayout->sCanonical = CLIENT_HTTP_URL . '/motoren';
        }
    }
}
include_once TEMPLATES_FOLDER . '/default.tmpl.php';
?>