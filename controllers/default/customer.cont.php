<?php

/*
 * controller to handle the customer
 */

// make pageLayout Object
$oPageLayout = new PageLayout();

$sReferrer = http_session("frontendLoginReferrer", "/" . http_get('controller'));

// set accountStatusUpdate
$sAccountStatusUpdate = http_session('accountStatusUpdate');
unset($_SESSION['accountStatusUpdate']); // only show once
// check if an email address exists
if (http_get('ajax') == 'checkEmail') {
    // return if the required fields are empty
    if (empty($_GET['email']))
        die(json_encode(null));

    // return bool (true if email doesn't exists and vice versa)
    die(json_encode(!CustomerManager::emailExists(http_get('email'), http_get('customerId', null))));
}

// edit account
if (http_get('param1') == 'bewerken') {
    // check if Customer is logged in
    if (empty($oCurrentCustomer) || !is_numeric($oCurrentCustomer->customerId))
        http_redirect('/' . http_get('controller'));

    // get page by urlPath (/account/bewerken)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    $oPageForMenu = PageManager::getPageByUrlPath('/' . http_get('controller'));

    // get the current Customer's info from the database
    $oCustomer = CustomerManager::getCustomerById($oCurrentCustomer->customerId);

    // action = save
    if (http_post('action') == 'save') {

        $oCustomer->email = http_post('email');
        $oCustomer->companyName = http_post('companyName');
        $oCustomer->gender = http_post('gender');
        $oCustomer->firstName = http_post('firstName');
        $oCustomer->insertion = http_post('insertion');
        $oCustomer->lastName = http_post('lastName');
        $oCustomer->address = http_post('address');
        $oCustomer->houseNumber = http_post('houseNumber');
        $oCustomer->houseNumberAddition = http_post('houseNumberAddition');
        $oCustomer->postalCode = http_post('postalCode');
        $oCustomer->city = http_post('city');
        $oCustomer->phone = http_post('phone');

        // if object is valid, save
        if ($oCustomer->isValid()) {
            CustomerManager::saveCustomer($oCustomer); //save object
            CustomerManager::setCustomerInSession($oCustomer); // set Customer in session
            $_SESSION['accountStatusUpdate'] = 'Gegevens zijn opgeslagen'; // set statusUpdate to session
            http_redirect('/' . http_get('controller') . '/' . http_get('param1'));
        } else {
            $aCustomerErrors = array();
            if (!$oCustomer->isPropValid("email"))
                $aCustomerErrors['email'] = 'Vul een geldig e-mailadres in.';
            if (!$oCustomer->isPropValid("emailExists"))
                $aCustomerErrors['emailExists'] = 'Het opgegeven e-mailadres is al in gebruik.';
            if (!$oCustomer->isPropValid("gender"))
                $aCustomerErrors['gender'] = 'Kies een geslacht.';
            if (!$oCustomer->isPropValid("firstName"))
                $aCustomerErrors['firstName'] = 'Vul uw voornaam in.';
            if (!$oCustomer->isPropValid("lastName"))
                $aCustomerErrors['lastName'] = 'Vul uw achternaam in.';
            if (!$oCustomer->isPropValid("address"))
                $aCustomerErrors['address'] = 'Vul uw adres in.';
            if (!$oCustomer->isPropValid("houseNumber"))
                $aCustomerErrors['houseNumber'] = 'Vul een geldig huisnummer in met alleen nummers.';
            if (!$oCustomer->isPropValid("postalCode"))
                $aCustomerErrors['postalCode'] = 'Vul uw postcode in.';
            if (!$oCustomer->isPropValid("city"))
                $aCustomerErrors['city'] = 'Vul uw plaats in.';

            Debug::logError("", "Frontend Customer module php validate error", __FILE__, __LINE__, "Tried to update Customer with wrong values despite javascript check.<br />" . _d($oCustomer, 1, 1), Debug::LOG_IN_EMAIL);
        }
    }

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $oPageLayout->sPagePath = PAGES_FOLDER . '/customer/customer_edit.inc.php';
}

// form to change the password (after receiving the forgot-password mail)
elseif (http_get('param1') == 'wachtwoord-vergeten' && http_get('param2') == 'bewerken') {
    // redirect to edit page if Customer is logged in
    if (!empty($oCurrentCustomer))
        http_redirect('/' . http_get('controller') . '/bewerken');

    // get page by urlPath (/account/wachtwoord-vergeten/bewerken)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    $sConfirmCode = http_post('code', http_get('code'));
    $sEmail = http_post('email', http_get('email'));

    // action updatePassword
    if (http_post("action") == 'updatePassword') {
        if (!empty($sConfirmCode) && !empty($sEmail) && http_post('password')) {
            // get the Customer
            $oCustomer = CustomerManager::getCustomerByConfirmCodeAndEmail($sConfirmCode, $sEmail);

            if ($oCustomer) {
                // define the password
                if (!empty($_POST['password']) && strlen(http_post('password')) >= 8) {
                    $oCustomer->password = hashPasswordForDb(http_post('password'));
                } else {
                    $oCustomer->password = null;
                }
            }

            if ($oCustomer && $oCustomer->isValid()) {
                $oCustomer = CustomerManager::updatePasswordAndLogin($oCustomer); //update password and login
                http_redirect('/' . http_get('controller') . '/' . http_get('param1') . '/opgeslagen');
            } elseif ($oCustomer) {
                $aErrorsChangePass = array();
                if (!$oCustomer->isPropValid('password')) {
                    $aErrorsChangePass['password'] = 'Vul een veilig wachtwoord in  met minimaal 8 tekens';
                }
                Debug::logError("", "Frontend Customer module php validate error", __FILE__, __LINE__, "Tried to update a Customer's password with wrong values<br />" . _d($oCustomer, 1, 1), Debug::LOG_IN_EMAIL);
            } else {
                $aErrorsChangePass = array();
                $aErrorsChangePass['combination'] = 'Code en/of e-mailadres niet juist';
            }
        } else {
            $aErrorsChangePass = array();
            $aErrorsChangePass['combination'] = 'Vul uw e-mailadres, code uit de e-mail en uw nieuwe wachtwoord in';
        }
    }

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $oPageLayout->sPagePath = PAGES_FOLDER . '/customer/customer_editPassword.inc.php';
}

// email sent after forgot-password page
elseif (http_get('param1') == 'wachtwoord-vergeten' && http_get('param2') == 'opgeslagen') {
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online) {
        showHttpError('404');
    }

    // get header image for page or get header image from parent if has one
    $oHeaderImage = $oPage->getHeaderImages('first-online');
    if (!$oHeaderImage && $oPage->parentPageId) {
        $oHeaderImage = $oPage->getParent()->getHeaderImages('first-online');
    }

    $oPageLayout->sPagePath = PAGES_FOLDER . '/customer/customer_pageContentOnly.inc.php';
    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());
}

// forgot password
elseif (http_get('param1') == 'wachtwoord-vergeten') {
    // redirect to edit page if Customer is logged in
    if (!empty($oCurrentCustomer))
        http_redirect('/' . http_get('controller') . '/bewerken');

    // get page by urlPath (/account/wachtwoord-vergeten)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    // action requestPassword
    if (http_post("action") == 'requestPassword') {
        if (http_post('email') && ($oCustomer = CustomerManager::getCustomerByEmail(http_post('email')))) {
            // create a unqiue confirmCode
            $oCustomer->confirmCode = substr(hash('sha512', uniqid(microtime() . $oCustomer->email, 1)), 15, 50); // I think it's safe to assume this code will be unique ^_^

            if ($oCustomer->isValid()) {
                CustomerManager::saveCustomer($oCustomer); //save object
                // mail Customer with confirm email
                $sTo = $oCustomer->email;

                $oTemplate = TemplateManager::getTemplateByName('account_nieuw_wachtwoord');

                // check if template exists
                if (empty($oTemplate)) {
                    Debug::logError('', 'Template bestaat niet: `nieuw wachtwoord` (account_nieuw_wachtwoord)', __FILE__, __LINE__, '', Debug::LOG_IN_EMAIL);
                } else {
                    $oTemplate->replaceVariables($oCustomer);
                    $sSubject = $oTemplate->getSubject();
                    $sMailBody = $oTemplate->getTemplate();
                    MailManager::sendMail($sTo, $sSubject, $sMailBody);
                }

                http_redirect('/' . http_get('controller') . '/' . http_get('param1') . '/bewerken');
            } else {
                Debug::logError("", "Frontend Customer module php validate error", __FILE__, __LINE__, "Tried to reset Customer's password with wrong values<br />" . _d($oCustomer, 1, 1), Debug::LOG_IN_EMAIL);
            }
        } else {
            $aErrorsForgotPass = array();
            $aErrorsForgotPass['email'] = 'Vul uw e-mailadres in';
        }
    }

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $oPageLayout->sPagePath = PAGES_FOLDER . '/customer/customer_forgotPassword.inc.php';
}

// confirm (activate) an account
elseif (http_get('param1') == 'bevestigen') {
    // redirect to edit page if Customer is logged in
    if (!empty($oCurrentCustomer) && is_numeric($oCurrentCustomer->customerId))
        http_redirect('/' . http_get('controller') . '/bewerken');

    // get page by urlPath (/account/bevestigen)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    $sConfirmCode = http_post('code', http_get('code'));
    $sEmail = http_post('email', http_get('email'));

    // confirm the account
    if (!empty($sConfirmCode) && !empty($sEmail)) {
        // attempt to confirm the account
        if (CustomerManager::confirmCustomerByConfirmCodeAndEmail($sConfirmCode, $sEmail)) {
            unset($_SESSION['accountConfirmEmail']); // for no use after confirmation
            // redirect to Thank you page
            http_redirect('/' . http_get('controller') . '/aangemaakt');
        } else {
            $aErrorsActivate = array();
            $aErrorsActivate['combinationUnknown'] = 'Combinatie e-mailadres en code is niet juist';
        }
    }

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $oPageLayout->sPagePath = PAGES_FOLDER . '/customer/customer_confirm.inc.php';
}

// Thank you page after registering and confirming
elseif (http_get('param1') == 'aangemaakt') {
    // get page by urlPath (/account/aangemaakt)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $oPageLayout->sPagePath = PAGES_FOLDER . '/customer/customer_confirmed.inc.php';
}

// logout
elseif (http_get('param1') == 'logout') {
    CustomerManager::logout($sReferrer);
}

// login and register form
else {
    // redirect to edit page if Customer is logged in
    if (!empty($oCurrentCustomer))
        http_redirect('/' . http_get('controller') . '/bewerken');

    // get page by urlPath (/account)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    // create Customer for registering
    $oCustomer = new Customer();

    // login
    if (http_post("action") == 'login') {
        if (http_post('email') && http_post('password') && (CustomerManager::login(http_post('email'), http_post('password')))) {
            // redirect to referrer page
            http_redirect($sReferrer);
        } else {
            $aErrorsLogin = array();
            if (!http_post('email'))
                $aErrorsLogin['signup-login-email'] = 'Vul uw e-mailadres in';
            if (!http_post('password'))
                $aErrorsLogin['signup-login-password'] = 'Vul uw wachtwoord in';
            if (!http_post('combination'))
                $aErrorsLogin['signup-login-combination'] = 'Wachtwoord en/of e-mailadres niet juist';
        }
    }

    // register
    if (http_post("action") == 'register') {
        // check password is valid
        if (http_post('password') && strlen(http_post('password')) >= 8) {
            $oCustomer->password = hashPasswordForDb(http_post('password'));
        } else {
            $oCustomer->password = null; //object validation will trigger error
        }

        $oCustomer->email = http_post('email');
        $oCustomer->companyName = http_post('companyName');
        $oCustomer->gender = http_post('gender');
        $oCustomer->firstName = http_post('firstName');
        $oCustomer->insertion = http_post('insertion');
        $oCustomer->lastName = http_post('lastName');
        $oCustomer->address = http_post('address');
        $oCustomer->houseNumber = http_post('houseNumber');
        $oCustomer->houseNumberAddition = http_post('houseNumberAddition');
        $oCustomer->postalCode = http_post('postalCode');
        $oCustomer->city = http_post('city');
        $oCustomer->phone = http_post('phone');

        // if object is valid, save
        if ($oCustomer->isValid()) {
            // create a unqiue confirmCode
            $oCustomer->confirmCode = substr(hash('sha512', uniqid(microtime() . $oCustomer->email, 1)), 15, 50); // I think it's safe to assume this code will be unique ^_^
            CustomerManager::saveCustomer($oCustomer); //save object
            // set email in session to prefill on site
            $_SESSION['accountConfirmEmail'] = $oCustomer->email;

            // mail Customer with confirm email
            $sTo = $oCustomer->email;

            $oTemplate = TemplateManager::getTemplateByName('account_bevestigen');

            // check if template exists
            if (empty($oTemplate)) {
                Debug::logError('', 'Template bestaat niet: `account bevestigen` (account_bevestigen)', __FILE__, __LINE__, '', Debug::LOG_IN_EMAIL);
            } else {
                $oTemplate->replaceVariables($oCustomer);
                $sSubject = $oTemplate->getSubject();
                $sMailBody = $oTemplate->getTemplate();
                MailManager::sendMail($sTo, $sSubject, $sMailBody);
            }

            http_redirect('/' . http_get('controller') . '/bevestigen');
        } else {
            $aErrorsSignup = array();
            if (!$oCustomer->isPropValid("email"))
                $aErrorsSignup['signup-email'] = 'Vul een geldig e-mailadres in';
            if (!$oCustomer->isPropValid("emailExists"))
                $aErrorsSignup['signup-email'] = 'Het opgegeven e-mailadres is al in gebruik';
            if (!$oCustomer->isPropValid("password"))
                $aErrorsSignup['signup-password'] = 'Vul een veilig wachtwoord in  met minimaal 8 tekens';
            if (!$oCustomer->isPropValid("gender"))
                $aErrorsSignup['signup-gender'] = 'Kies een geslacht';
            if (!$oCustomer->isPropValid("firstName"))
                $aErrorsSignup['signup-firstName'] = 'Vul uw voornaam in';
            if (!$oCustomer->isPropValid("lastName"))
                $aErrorsSignup['signup-lastName'] = 'Vul uw achternaam in';
            if (!$oCustomer->isPropValid("address"))
                $aErrorsSignup['signup-address'] = 'Vul uw adres in';
            if (!$oCustomer->isPropValid("houseNumber"))
                $aErrorsSignup['signup-houseNumber'] = 'Vul een geldig huisnummer in met alleen nummers';
            if (!$oCustomer->isPropValid("postalCode"))
                $aErrorsSignup['signup-postalCode'] = 'Vul uw postcode in';
            if (!$oCustomer->isPropValid("city"))
                $aErrorsSignup['signup-city'] = 'Vul uw plaats in';

            Debug::logError("", "Frontend Customer module php validate error", __FILE__, __LINE__, "Tried to save Customer with wrong values despite javascript check.<br />" . _d($oCustomer, 1, 1), Debug::LOG_IN_EMAIL);
        }
    }

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());

    $oPageLayout->sPagePath = PAGES_FOLDER . '/customer/customer_signUp.inc.php';
}

// include the error template
include_once TEMPLATES_FOLDER . '/default.tmpl.php';
?>