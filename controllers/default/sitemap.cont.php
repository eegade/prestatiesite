<?php

# set xml declaration
$sXML = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";

if (http_get('param1')) {
    $oToday = new Date();
    $aUrls = array();
    if (BB_WITH_NEWS && http_get('param1') == 'news') {
        $aNewsItems = NewsItemManager::getNewsItemsByFilter(array(), null, 0, $iFoundRows, array('IFNULL(`ni`.`modified`, `ni`.`created`)' => 'DESC'));
        foreach ($aNewsItems AS $oNewsItem) {
            $oUrl = new stdClass(); // object for sorting on property later
            $oUrl->url = CLIENT_HTTP_URL . $oNewsItem->getUrlPath();
            //$oUrl->date = Date::strToDate(($oNewsItem->modified === null ? $oNewsItem->created : $oNewsItem->modified))->format('%Y-%m-%dT%H:%M:%S%z');
            $date = new DateTime(($oNewsItem->modified === null ? $oNewsItem->created : $oNewsItem->modified));
            $oUrl->date = $date->format('c');
            $aUrls[] = clone $oUrl;
        }
    } elseif (BB_WITH_NEWS && BB_WITH_NEWS_CATEGORIES && http_get('param1') == 'newscategories') {
        $aCategories = NewsItemCategoryManager::getNewsItemCategoriesByFilter(array(), null, 0, $iFoundRows, array('IFNULL(`nic`.`modified`, `nic`.`created`)' => 'DESC'));
        foreach ($aCategories AS $oCategory) {
            $oUrl = new stdClass(); // object for sorting on property later
            $oUrl->url = CLIENT_HTTP_URL . $oCategory->getUrlPath();
            //$oUrl->date = Date::strToDate(($oCategory->modified === null ? $oCategory->created : $oCategory->modified))->format('%Y-%m-%dT%H:%M:%S%z');
            $date = new DateTime(($oCategory->modified === null ? $oCategory->created : $oCategory->modified));
            $oUrl->date = $date->format('c');
            $aUrls[] = clone $oUrl;
        }
    } elseif (http_get('param1') == 'pages') {
        $aPages = PageManager::getPagesByFilter(array('showAll' => 1, 'online' => 1), null, 0, $iFoundRows, array('IFNULL(`p`.`modified`, `p`.`created`)' => 'DESC'));
        foreach ($aPages AS $oPage) {
            $oUrl = new stdClass(); // object for sorting on property later
            $oUrl->url = CLIENT_HTTP_URL . $oPage->getUrlPath();
            //$oUrl->date = Date::strToDate(($oPage->modified === null ? $oPage->created : $oPage->modified))->format('%Y-%m-%dT%H:%M:%S%z');
            $date = new DateTime(($oPage->modified === null ? $oPage->created : $oPage->modified));
            $oUrl->date = $date->format('c');
            $aUrls[] = clone $oUrl;
        }
    } elseif (BB_WITH_PHOTO_ABLUMS && http_get('param1') == 'photoalbums') {
        $aPhotoAlbums = PhotoAlbumManager::getPhotoAlbumsByFilter(array(), null, 0, $iFoundRows, array('IFNULL(`pa`.`modified`, `pa`.`created`)' => 'DESC'));
        foreach ($aPhotoAlbums AS $oPhotoAlbum) {
            $oUrl = new stdClass(); // object for sorting on property later
            $oUrl->url = CLIENT_HTTP_URL . $oPhotoAlbum->getUrlPath();
            //$oUrl->date = Date::strToDate(($oPhotoAlbum->modified === null ? $oPhotoAlbum->created : $oPhotoAlbum->modified))->format('%Y-%m-%dT%H:%M:%S%z');
            $date = new DateTime(($oPhotoAlbum->modified === null ? $oPhotoAlbum->created : $oPhotoAlbum->modified));
            $oUrl->date = $date->format('c');
            $aUrls[] = clone $oUrl;
        }
    } elseif (BB_WITH_CATALOG && http_get('param1') == 'catalog') {
        $aProducts = CatalogProductManager::getProductsByFilter(array(), null, 0, $iFoundRows, array('IFNULL(`cp`.`modified`, `cp`.`created`)' => 'DESC'));
        foreach ($aProducts AS $oProduct) {
            $oUrl = new stdClass(); // object for sorting on property later
            $oUrl->url = CLIENT_HTTP_URL . $oProduct->getUrlPath();
            //$oUrl->date = Date::strToDate(($oProduct->modified === null ? $oProduct->created : $oProduct->modified))->format('%Y-%m-%dT%H:%M:%S%z');
            $date = new DateTime(($oProduct->modified === null ? $oProduct->created : $oProduct->modified));
            $oUrl->date = $date->format('c');
            $aUrls[] = clone $oUrl;
        }
    } elseif (BB_WITH_CATALOG && http_get('param1') == 'catalogcategories') {
        $aCategories = CatalogProductCategoryManager::getProductCategoriesByFilter(array(), null, 0, $iFoundRows, array('IFNULL(`cpc`.`modified`, `cpc`.`created`)' => 'DESC'));
        foreach ($aCategories AS $oCategory) {
            $oUrl = new stdClass(); // object for sorting on property later
            $oUrl->url = CLIENT_HTTP_URL . $oCategory->getUrlPath();
            $date = new DateTime(($oCategory->modified === null ? $oCategory->created : $oCategory->modified));
            $oUrl->date = $date->format('c');
            $aUrls[] = clone $oUrl;
        }
    }elseif (BB_WITH_CATALOG && http_get('param1') == 'agendaitems') {
        $aAgendaItems = AgendaItemManager::getAgendaItemsByFilter(array(), null, 0, $iFoundRows, array('IFNULL(`ai`.`modified`, `ai`.`created`)' => 'DESC'));
        foreach ($aAgendaItems AS $oAgendaItem) {
            $oUrl = new stdClass(); // object for sorting on property later
            $oUrl->url = CLIENT_HTTP_URL . $oAgendaItem->getUrlPath();
            $date = new DateTime(($oAgendaItem->modified === null ? $oAgendaItem->created : $oAgendaItem->modified));
            $oUrl->date = $date->format('c');
            $aUrls[] = clone $oUrl;
        }
    }

    $sXML .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    foreach ($aUrls As $oUrl) {

        $iDaysDiff = ceil(Date::strToDate($oUrl->date)->daysDiff($oToday));
        if ($iDaysDiff == 1) {
            $fPrio = 1.0;
        } elseif ($iDaysDiff == 2) {
            $fPrio = 0.7;
        } else {
            $fPrio = 0.5;
        }

        $sXML .= '  <url>' . PHP_EOL;
        $sXML .= '      <loc>' . $oUrl->url . '</loc>' . PHP_EOL;
        $sXML .= '      <lastmod>' . $oUrl->date . '</lastmod>' . PHP_EOL;
        $sXML .= '      <priority>' . $fPrio . '</priority>' . PHP_EOL;
        $sXML .= '  </url>' . PHP_EOL;
    }

# end urlset
    $sXML .= '</urlset>';
} else {
    # set sitemapindex
    $sXML .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL;
    $sXML .= '<sitemap><loc>' . CLIENT_HTTP_URL . '/sitemap-pages.xml.gz</loc></sitemap>' . PHP_EOL;
    if (BB_WITH_NEWS) {
        $sXML .= '<sitemap><loc>' . CLIENT_HTTP_URL . '/sitemap-news.xml.gz</loc></sitemap>' . PHP_EOL;
        if (BB_WITH_NEWS_CATEGORIES) {
            $sXML .= '<sitemap><loc>' . CLIENT_HTTP_URL . '/sitemap-newscategories.xml.gz</loc></sitemap>' . PHP_EOL;
        }
    }
    if (BB_WITH_PHOTO_ABLUMS) {
        $sXML .= '<sitemap><loc>' . CLIENT_HTTP_URL . '/sitemap-photoalbums.xml.gz</loc></sitemap>' . PHP_EOL;
    }
    if (BB_WITH_CATALOG) {
        $sXML .= '<sitemap><loc>' . CLIENT_HTTP_URL . '/sitemap-catalog.xml.gz</loc></sitemap>' . PHP_EOL;
        $sXML .= '<sitemap><loc>' . CLIENT_HTTP_URL . '/sitemap-catalogcategories.xml.gz</loc></sitemap>' . PHP_EOL;
    }
    if (BB_WITH_AGENDA_ITEMS){
        $sXML .= '<sitemap><loc>' . CLIENT_HTTP_URL . '/sitemap-agendaitems.xml.gz</loc></sitemap>' . PHP_EOL;
    }

    # end sitemap index
    $sXML .= '</sitemapindex>';
}

// make gzip or regular XML
if (http_get('param2') == 'gz') {
    header('content-type: application/x-gzip');
} else {
    header("Content-type: text/xml");
}
echo $sXML;
?>