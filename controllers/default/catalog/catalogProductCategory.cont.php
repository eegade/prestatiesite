<?php

/*
 * controller to handle the catalog's product pages
 */

# make pageLayout Object
$oPageLayout = new PageLayout();

# detail page
if (is_numeric(http_get('param1'))) {
    $oProductCategory = CatalogProductCategoryManager::getProductCategoryById(http_get('param1'));

    if (empty($oProductCategory) || !$oProductCategory->online)
        showHttpError('404');

    $oPageLayout->sWindowTitle = $oProductCategory->getWindowTitle();
    $oPageLayout->sMetaDescription = $oProductCategory->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oProductCategory->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oProductCategory->getCrumbles());
    $oPageLayout->sCanonical = CLIENT_HTTP_URL . $oProductCategory->getUrlPath();
    $oPageLayout->sOGType = 'product.group';
    $oPageLayout->sOGTitle = $oProductCategory->getWindowTitle();
    $oPageLayout->sOGDescription = $oProductCategory->getMetaDescription();
    $oPageLayout->sOGUrl = getCurrentUrl();

    // define default filter
    $aDefaultProductFilter = array();
    
    // set default filter in session if not exists
    if (!isset($_SESSION['aProductFilter'])) {
        $_SESSION['aProductFilter'] = $aDefaultProductFilter; // set filter in session
    }
    
    // unset product filter from session
    if (http_get('resetProductFilter')) {
        unset($_SESSION['aProductFilter']);
        http_redirect(getCurrentUrlPath());
    } 
    if (http_get('productFilter')) {
        $aProductsFilter = http_get('productFilter');
        $_SESSION['aProductFilter'] = $aProductsFilter; // set filter in session
    }
    
    // overwrite catalogProductCategoryId
    $_SESSION['aProductFilter']['catalogProductCategoryId'] = $oProductCategory->catalogProductCategoryId;
    
    // get filter from session
    $aProductsFilter = $_SESSION['aProductFilter'];
    
    
    // set default filter in session if not exists
/*    if (!isset($_SESSION['aProductFiltersByCategory'][$oProductCategory->catalogProductCategoryId])) {
        $_SESSION['aProductFiltersByCategory'][$oProductCategory->catalogProductCategoryId] = $aDefaultProductFilter; // set filter in session
    }

    // unset product filter from session
    if (http_get('resetProductFilter')) {
        unset($_SESSION['aProductFiltersByCategory'][$oProductCategory->catalogProductCategoryId]);
        http_redirect(getCurrentUrlPath());
    }

    if (http_get('productFilter')) {
        $aProductsFilter = http_get('productFilter');
        $_SESSION['aProductFiltersByCategory'][$oProductCategory->catalogProductCategoryId] = $aProductsFilter; // set filter in session
    }

    // overwrite catalogProductCategoryId
    $_SESSION['aProductFiltersByCategory'][$oProductCategory->catalogProductCategoryId]['catalogProductCategoryId'] = $oProductCategory->catalogProductCategoryId;

    // get filter from session
    $aProductsFilter = $_SESSION['aProductFiltersByCategory'][$oProductCategory->catalogProductCategoryId];*/

    # handle perPage
    if (http_post('setPerPage')) {
        $_SESSION['productsPerPage'] = http_post('perPage');
    }

    $iPerPage = http_session('productsPerPage', 9);
    $iCurrPage = http_get('page', 1);
    $iStart = (($iCurrPage - 1) * $iPerPage);
    if(!is_numeric($iCurrPage) || $iCurrPage <= 0){
        http_redirect( $oPageLayout->sCanonical);
    }  
    
    $aProducts = CatalogProductManager::getProductsByFilter($aProductsFilter, $iPerPage, $iStart, $iFoundRows);
    
    $iPageCount = !empty($iPerPage) ? (ceil($iFoundRows / $iPerPage)) : 0;
    
    // set chosen categories for above filter
    $aChosenCategories = array();
    $oChosenCategory = $oProductCategory;
    $aChosenCategories[] = $oChosenCategory;
    while ($oChosenCategory = $oChosenCategory->getParent()) {
        $aChosenCategories[] = $oChosenCategory;
    }
    $aChosenCategories = array_reverse($aChosenCategories);

    $oPageLayout->sPagePath = PAGES_FOLDER . '/catalog/products/productCategories/catalogProductCategory_details.inc.php';
}

# overview
else {
    # get the objects
    $aProductCategories = CatalogProductCategoryManager::getProductCategoriesByFilter(array('level' => 1));

    # get page by urlPath (/categorieen)
    $oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

    if (empty($oPage) || !$oPage->online)
        showHttpError('404');

    $oPageLayout->sWindowTitle = $oPage->getWindowTitle();
    $oPageLayout->sMetaDescription = $oPage->getMetaDescription();
    $oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
    $oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());
    $oPageLayout->sOGType = 'website';
    $oPageLayout->sOGTitle = $oPage->getWindowTitle();
    $oPageLayout->sOGDescription = $oPage->getMetaDescription();
    $oPageLayout->sOGUrl = getCurrentUrl();

    $oPageLayout->sPagePath = PAGES_FOLDER . '/catalog/products/productCategories/catalogProductCategories_overview.inc.php';
}

# include the error template
include_once TEMPLATES_FOLDER . '/default.tmpl.php';
?>