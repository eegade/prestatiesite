<?php

/*
 * Controller to handle normal content pages
 */
$oPageLayout = new PageLayout();

$oPage = PageManager::getPageByUrlPath(getCurrentUrlPath());

if (empty($oPage) || !$oPage->online) {
    showHttpError('404');
}

if ($oPage->level > 1)
    $oPageForMenu = PageManager::getPageByUrlPath('/' . http_get('controller'));
else
    $oPageForMenu = $oPage;

// get header image for page or get header image from parent if has one
$oHeaderImage = $oPage->getHeaderImages('first-online');
if (!$oHeaderImage && $oPage->parentPageId) {
    $oHeaderImage = $oPage->getParent()->getHeaderImages('first-online');
}

$oPageLayout->sPagePath = PAGES_FOLDER . '/pages/page_details.inc.php';
$oPageLayout->sWindowTitle = $oPage->getWindowTitle();
$oPageLayout->sMetaDescription = $oPage->getMetaDescription();
$oPageLayout->sMetaKeywords = $oPage->getMetaKeywords();
$oPageLayout->generateCustomCrumblePath($oPage->getCrumbles());
$oPageLayout->sOGType = 'website';
$oPageLayout->sOGTitle = $oPage->getWindowTitle();
$oPageLayout->sOGDescription = $oPage->getMetaDescription();
$oPageLayout->sOGUrl = getCurrentUrl();
if (($oImage = $oPage->getImages('first-online')) && ($oImageFile = $oImage->getImageFileByReference('crop_small'))) {
    $oPageLayout->sOGImage = CLIENT_HTTP_URL . $oImageFile->link;
}

$aImages = $oPage->getImages();
$aYoutubeVideos = $oPage->getYoutubeLinks();
$aFiles = $oPage->getFiles();
$aLinks = $oPage->getLinks();

include_once TEMPLATES_FOLDER . '/default.tmpl.php';
?>